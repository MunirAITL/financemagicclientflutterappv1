package uk.co.financemagic.client
import io.flutter.app.FlutterApplication
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import io.flutter.plugins.GeneratedPluginRegistrant
class Application : FlutterApplication(), PluginRegistrantCallback {
    // ...
    override fun onCreate() {
        super.onCreate()

        //FlutterFirebaseMessagingBackgroundService.setPluginRegistrant(this)
    }

    override fun registerWith(registry: PluginRegistry) {
        TODO("Not yet implemented")
        GeneratedPluginRegistrant.registerWith(FlutterEngine(applicationContext))
        //PathProviderPlugin.registerWith(registry.registrarFor("io.flutter.plugins.pathprovider.PathProviderPlugin"));
    }

}