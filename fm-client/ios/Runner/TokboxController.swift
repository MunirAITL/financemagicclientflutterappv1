//
//  TokboxController.swift
//  Hello-World
//
//  Created by Roberto Perez Cubero on 11/08/16.
//  Copyright © 2016 tokbox. All rights reserved.
//

import UIKit
import OpenTok

// *** Fill the following variables using your own Project info  ***
// ***            https://tokbox.com/account/#/                  ***
// Replace with your OpenTok API key
/*let kApiKey = "47437851"
// Replace with your generated session ID
let kSessionId = "2_MX40NzQzNzg1MX5-MTY0MzM3MTQyNzAyNH5ZYzY4RTd0QUQ0dFpEWC9SeWIveXVHSk9-fg"
// Replace with your generated token
let kToken = "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9MWMyYTJiZGNhNzczZjlmNGI3MTc1N2U3NmY0MGM5MzA3NDljNzFjNDpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzMzcxODc4Jm5vbmNlPTAuNjY3MzkwMjcyMzQwOTMxNyZyb2xlPW1vZGVyYXRvciZleHBpcmVfdGltZT0xNjQ1OTYzODgwJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9";*/


class TokboxController: UIViewController {
    
    //  external params
    var args:Dictionary<String, Any>!
    var kApiKey:String!
    var kSessionId:String!
    var kToken:String!
    
    //
    let width:CGFloat = UIScreen.main.bounds.width
    let height:CGFloat = UIScreen.main.bounds.height
    var button:UIButton!
    
    lazy var session: OTSession = {
        return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
    }()
    
    lazy var publisher: OTPublisher = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    
    var subscriber: OTSubscriber?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initData()
        self.view.backgroundColor = .white
        self.title = "Video Call - Support"
        //UINavigationBar.appearance().backgroundColor = hexStringToUIColor(hex:"#2D4665")
        //navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        doConnect()
        //addNavBar()
    }
    
    func initData() {
        kApiKey = (args["apiKey"] as! String)
        kSessionId = (args["sessionId"] as! String)
        kToken = (args["token"] as! String)
    }
    
    
    /*func addNavBar() {
        if(button==nil) {
            let statusBarHeight = UIApplication.shared.statusBarFrame.height
            button = UIButton(frame: CGRect(x: 10, y: statusBarHeight+100, width: 60, height: 30))
            button.backgroundColor = .lightGray
            button.setTitleColor(.white, for:.normal)
            button.setTitle("Back", for: .normal)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            button.addTarget(self, action: #selector(backButton), for:.touchUpInside)
            button.isUserInteractionEnabled = true
            self.view.addSubview(button)
        }
        self.view.bringSubviewToFront(button)
    }
    
    @objc func backButton(sender: UIButton!) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true)                    
    }*/
    
    /**
     * Asynchronously begins the session connect process. Some time later, we will
     * expect a delegate method to call us back with the results of this action.
     */
    fileprivate func doConnect() {
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.connect(withToken: kToken, error: &error)
    }
    
    /**
     * Sets up an instance of OTPublisher to use with this session. OTPubilsher
     * binds to the device camera and microphone, and will provide A/V streams
     * to the OpenTok session.
     */
    fileprivate func doPublish() {
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.publish(publisher, error: &error)
        
        if let pubView = publisher.view {
            pubView.frame = CGRect(x: self.width * 40/100, y: self.height*65/100, width: self.width * 50/100, height: self.height * 30/100)
            //pubView.layer.borderWidth = 10
            //pubView.layer.borderColor = UIColor.red.cgColor
            pubView.sizeToFit()
            pubView.layoutSubviews()
            view.addSubview(pubView)
        }
    }
    
    /**
     * Instantiates a subscriber for the given stream and asynchronously begins the
     * process to begin receiving A/V content for this stream. Unlike doPublish,
     * this method does not add the subscriber to the view hierarchy. Instead, we
     * add the subscriber only after it has connected and begins receiving data.
     */
    fileprivate func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        subscriber = OTSubscriber(stream: stream, delegate: self)
        session.subscribe(subscriber!, error: &error)
    }
    
    fileprivate func cleanupSubscriber() {
        subscriber?.view?.removeFromSuperview()
        subscriber = nil
    }
    
    fileprivate func cleanupPublisher() {
        publisher.view?.removeFromSuperview()
    }
    
    fileprivate func processError(_ error: OTError?) {
        if let err = error {
            DispatchQueue.main.async {
                let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - OTSession delegate callbacks
extension TokboxController: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        doPublish()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if subscriber == nil {
            doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
    
}

// MARK: - OTPublisher delegate callbacks
extension TokboxController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Publishing")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        cleanupPublisher()
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - OTSubscriber delegate callbacks
extension TokboxController: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        if let subsView = subscriber?.view {
            subsView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
            view.addSubview(subsView)
            view.bringSubviewToFront(publisher.view!)
            //addNavBar()
        }
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
