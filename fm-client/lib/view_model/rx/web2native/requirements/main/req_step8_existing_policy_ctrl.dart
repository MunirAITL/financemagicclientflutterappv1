import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/MortgageUserExistingPolicyItem.dart';
import 'package:get/get.dart';

class ReqStep8ExistingPolicyCtrl extends GetxController {
  var existingPolicyCtrl = List<MortgageUserExistingPolicyItem>().obs;
}
