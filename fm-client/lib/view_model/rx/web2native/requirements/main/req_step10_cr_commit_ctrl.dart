import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/MortgageUserAffordAbility.dart';
import 'package:get/get.dart';

class ReqStep10CrCommitCtrl extends GetxController {
  var balOutstanding = 0.0.obs;
  var monthlyPayment = 0.0.obs;
  var creditLimit = 0.0.obs;
  var crCommitCtrl = List<MortgageUserAffordAbility>().obs;
}
