import 'package:aitl/model/json/web2native/case/requirements/cur_res_mort/MortgageUserCurrentResidentialMortgage.dart';
import 'package:get/get.dart';

class ReqStep6CurResMortCtrl extends GetxController {
  var curResMortCtrl = MortgageUserCurrentResidentialMortgage().obs;
}
