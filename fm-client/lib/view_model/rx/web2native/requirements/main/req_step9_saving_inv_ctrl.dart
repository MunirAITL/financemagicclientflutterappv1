import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/MortgageUserSavingOrInvestmentItem.dart';
import 'package:get/get.dart';

class ReqStep9SavingInvCtrl extends GetxController {
  var savingInvCtrl = List<MortgageUserSavingOrInvestmentItem>().obs;
}
