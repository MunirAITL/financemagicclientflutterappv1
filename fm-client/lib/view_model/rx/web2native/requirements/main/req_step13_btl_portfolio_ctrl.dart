import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';
import 'package:get/get.dart';

class ReqStep13BtlPortfolioCtrl extends GetxController {
  var btlPortModel = MortgageUserPortfolios().obs;
  var listBtlPortModel = List<MortgageUserPortfolios>().obs;
}
