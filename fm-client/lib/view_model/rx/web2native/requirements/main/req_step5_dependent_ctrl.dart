import 'package:aitl/model/json/web2native/case/requirements/5dependents/MortgageFinancialDependantsModel.dart';
import 'package:get/get.dart';

import '../../../../../view/widgets/dropdown/DropListModel.dart';

class ReqStep5DependentCtrl extends GetxController {
  var dob = "".obs;
  var ddRelationShip = DropListModel([
    OptionItem(id: "1", title: "Son"),
    OptionItem(id: "2", title: "Daughter"),
    OptionItem(id: "3", title: "Parent"),
    OptionItem(id: "4", title: "Brother"),
    OptionItem(id: "5", title: "Sister"),
    OptionItem(id: "6", title: "Family member"),
  ]).obs;
  var optRelationShip = OptionItem(id: null, title: "Select Relationship").obs;

  var listDependentsModelAPIModel =
      List<MortgageFinancialDependantsModel>().obs;
}
