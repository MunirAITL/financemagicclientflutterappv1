import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserBankDetailList.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserKeyInformation.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherContactInfos.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherInfo.dart';
import 'package:get/get.dart';

class ReqStep2ESCtrl extends GetxController {
  //  Requirements Step 2:
  var getES1Model = MortgageUserOtherInfo().obs;
  var getES2Model = MortgageUserKeyInformation().obs;
  var listContactModel = List<MortgageUserOtherContactInfos>().obs;
  var listBnkModel = List<MortgageUserBankDetailList>().obs;
}
