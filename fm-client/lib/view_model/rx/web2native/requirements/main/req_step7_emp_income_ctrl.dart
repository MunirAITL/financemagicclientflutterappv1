import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserInCome.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserOccupation.dart';
import 'package:get/get.dart';

class ReqStep7EmpIncomeCtrl extends GetxController {
  var empCtrl = List<MortgageUserOccupation>().obs;
  var incomeCtrl = List<MortgageUserInCome>().obs;
}
