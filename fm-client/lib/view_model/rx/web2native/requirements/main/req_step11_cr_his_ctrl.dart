import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistory.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';
import 'package:get/get.dart';

class ReqStep11CrHisCtrl extends GetxController {
  var crHisCtrl = MortgageUserCreditHistory().obs;
  var listCrHisItemsCtrl = List<MortgageUserCreditHistoryItems>().obs;
}
