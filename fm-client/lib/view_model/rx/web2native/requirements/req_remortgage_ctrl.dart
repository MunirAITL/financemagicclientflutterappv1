import 'package:get/get.dart';
import '../../../../view/widgets/dropdown/DropListModel.dart';

class ReqReMortgageCtrl extends GetxController {
  var ddReq = DropListModel([
    OptionItem(id: "remortgageOfAnotherProperty", title: "Home Improvement"),
    OptionItem(id: "savings", title: "Debt Reconsolidation"),
    OptionItem(id: "giftFromFamilyMember", title: "New BTL purchase"),
    OptionItem(id: "equityAmount", title: "New residential purchase"),
    OptionItem(id: "otherStateWhat", title: "Loan for business purposes"),
    OptionItem(id: "otherStateWhat", title: "Holiday"),
    OptionItem(id: "otherStateWhat", title: "Other"),
  ]).obs;
  var optReq = OptionItem(id: null, title: "Select").obs;
}
