import 'dart:io';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/view_model/rx/web2native/requirements/case_model_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step13_btl_portfolio_ctrl.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'dart:io' as Io;
import 'package:path_provider/path_provider.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class FTPMgr {
  Future<File> uploadXLS(String xlsName) async {
    try {
      final serviceStatus = await Permission.storage.status;
      if (!serviceStatus.isGranted) {
        await [
          Permission.storage,
        ].request();
      }

      final btlPorfolioCtrl = Get.put(ReqStep13BtlPortfolioCtrl());
      final caseModelCtrl = Get.put(CaseModelCtrl());

      Directory directory;
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await getExternalStorageDirectory();
      }
      final folder = await new Io.Directory('${directory.path}/mm4')
          .create(recursive: true);
      final file = new Io.File(
          '${folder.path}/${xlsName}_${DateTime.now().toUtc().toIso8601String()}.csv');

      //  create csv file
      var data =
          'Id,Task id,Task Title,Asset Type,Owner,Security Property Type,Property Address,Date Purchased,Purchase Price (approximate),Current Value,Monthly Rental Income,Monthly Mortgage Payment,Lender Name,Current Mortgage Balance,LTV(%),Current Mortgage Interest Rate,Mortgage Term Remaining with Current Lender,Current Fixed Period End Date,Interest Type\n';
      double purchasePrice = 0;
      double curValue = 0;
      double monthlyRentIncome = 0;
      double monthlyMortPayment = 0;
      double curMortBalance = 0;
      double ltv = 0;
      double curMortIntRate = 0;

      for (final model in btlPorfolioCtrl.listBtlPortModel) {
        try {
          purchasePrice += model.purchasePrice;
          curValue += model.currentValue;
          monthlyRentIncome += model.rentalIncome;
          monthlyMortPayment += model.monthlyMortgage;
          curMortBalance += model.mortgageBalance;
          ltv += model.lTV;
          curMortIntRate += model.currentMortgageInterestRate;
        } catch (e) {}
        data += (model.id.toString() + ',');
        data += (model.taskId.toString() + ',');
        data += (caseModelCtrl.locModel.value.title.toString() + ',');
        data += (model.propertyType.toString() + ',');
        data += (model.owner.toString() + ',');
        data += (model.propertyStyle.toString() + ',');
        data += ((model.address1 + " " + model.address2 + " " + model.address3)
                .trim() +
            ',');
        data += (DateFun.getDate(model.datePurchased, "dd-MMM-yyyy") + ',');
        data +=
            (AppDefine.CUR_SIGN + model.currentValue.toStringAsFixed(0) + ',');
        data +=
            (AppDefine.CUR_SIGN + model.rentalIncome.toStringAsFixed(0) + ',');
        data += (AppDefine.CUR_SIGN +
            model.monthlyMortgage.toStringAsFixed(0) +
            ',');
        data += (model.lender + ',');
        data += (AppDefine.CUR_SIGN +
            model.mortgageBalance.toStringAsFixed(0) +
            ',');
        data += (AppDefine.CUR_SIGN + model.lTV.toStringAsFixed(0) + "%" + ',');
        data += (AppDefine.CUR_SIGN +
            model.currentMortgageInterestRate.toStringAsFixed(0) +
            ',');
        data += (model.mortgageTermRemainingWithCurrentLender + ',');
        data +=
            (DateFun.getDate(model.currentFixedPeriodEndDate, "dd-MMM-yyyy") +
                ',');
        data += (model.interestType + ',');
        data += (model.interestType.toString() + ',\n');
      }

      data += (',,,,,,,' +
          AppDefine.CUR_SIGN +
          purchasePrice.toStringAsFixed(2) +
          ',' +
          AppDefine.CUR_SIGN +
          curValue.toStringAsFixed(2) +
          ',' +
          AppDefine.CUR_SIGN +
          monthlyRentIncome.toStringAsFixed(2) +
          ',' +
          AppDefine.CUR_SIGN +
          monthlyMortPayment.toStringAsFixed(2) +
          ',,' +
          AppDefine.CUR_SIGN +
          curMortBalance.toStringAsFixed(2) +
          ',' +
          ltv.toStringAsFixed(2) +
          "%" +
          ',' +
          AppDefine.CUR_SIGN +
          curMortIntRate.toStringAsFixed(2) +
          ',,,\n');

      await file.writeAsString(data, mode: FileMode.write, flush: true);
      return file;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
