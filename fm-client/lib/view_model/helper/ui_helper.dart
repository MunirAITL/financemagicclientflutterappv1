import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:auto_size_text/auto_size_text.dart';

import '../../config/AppDefine.dart';
import '../../view/widgets/radio/draw_radio_group.dart';

class UIHelper {
  drawLine({Color colr = Colors.grey, double h = .5}) {
    return Container(color: colr, height: h);
  }

  scrollUp(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  scrollDown(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        scrollController.position.maxScrollExtent + h + 50,
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  drawAppbarTitle({String title, Color txtColor}) {
    if (txtColor == null) {
      txtColor = Colors.white;
    }
    return FittedBox(
      fit: BoxFit.fitWidth,
      child: AutoSizeText(title,
          style: TextStyle(color: txtColor, fontWeight: FontWeight.bold)),
    );
  }

  drawTopbar(bool isShowArrow, String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 40, top: 20),
      child: Container(
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
            Container(
              child: (isShowArrow)
                  ? IconButton(
                      icon: Image.asset(
                        "assets/images/icons/ico_arrow_back.png",
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    )
                  : SizedBox(),
            ),
            Expanded(
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
          ])),
    );
  }

  drawCircle({BuildContext context, Color color, double size = 3}) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;
    //var padding = MediaQuery.of(context).padding;
    //double newheight = height - padding.top - padding.bottom;
    return Container(
      width: width * size / 100,
      height: width * size / 100,
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
    );
  }

  drawCircle2(double size, Color color) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }

  getStarsRow(
      {int rate,
      Color colr,
      CrossAxisAlignment crossAlign = CrossAxisAlignment.center,
      MainAxisAlignment mainAlign = MainAxisAlignment.center,
      double size = 20}) {
    return Row(
      crossAxisAlignment: crossAlign,
      mainAxisAlignment: mainAlign,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            (i < rate) ? Icons.star : Icons.star_outline,
            color: colr,
            size: size,
          ),
      ],
    );
  }

  expandableTxt(String txt, Color txtColor, Color arrowColor) {
    final txt1 = txt.substring(0, txt.indexOf(".")).trim();
    final txt2 = txt.substring(txt.indexOf(".") + 1).trim();

    return Container(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        iconColor: arrowColor,
        child: Theme(
          data: ThemeData.light()
              .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
          child: ExpansionTile(
            title: Txt(
                txt: txt1 + '...',
                txtColor: txtColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                maxLines: 3,
                isBold: false),
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Txt(
                      txt: txt2,
                      txtColor: txtColor,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawReviewBox({BuildContext context, String title}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: new BoxDecoration(
          color: MyTheme.statusBarColor,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(10),
            topRight: const Radius.circular(10),
          )),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(width: 10),
            Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(1.5),
                  child: Icon(
                    Icons.star,
                    color: MyTheme.statusBarColor,
                    size: 15,
                  ),
                )),
            SizedBox(width: 10),
            Expanded(
              child: Txt(
                  txt: title,
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
          ],
        ),
      ),
    );
  }

  draw2Row(String title, String txt) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w500,
              isBold: false),
        ),
        SizedBox(width: 10),
        Flexible(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Txt(
                txt: txt ?? '',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .6,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ],
    );
  }

  addCurSign(val) =>
      AppDefine.CUR_SIGN + (val != null ? val.toString().split('.')[0] : '0');

  drawDivider() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Divider(
        color: MyTheme.statusBarColor,
        height: 1.5,
      ),
    );
  }

  drawRadioTitle(
      {@required BuildContext context,
      String title,
      String subTitle,
      @required Map<int, String> list,
      @required int index,
      radioType = eRadioType.GRID2,
      bool isMainBG = true,
      bool isTitleBold = true,
      @required Function(int index) callback}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title != null
            ? Padding(
                padding: EdgeInsets.only(bottom: subTitle == null ? 5 : 3),
                child: Txt(
                    txt: title,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: isTitleBold),
              )
            : SizedBox(),
        subTitle != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: subTitle,
                    txtColor: Colors.black54,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        drawRadioGroup(
            context: context,
            type: radioType,
            isMainBG: isMainBG,
            map: list,
            selected: index,
            callback: (v) {
              callback(v);
            }),
      ],
    );
  }
}
