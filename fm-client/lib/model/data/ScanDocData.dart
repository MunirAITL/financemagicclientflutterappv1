import 'dart:io';

class ScanDocData {
  static final ScanDocData _scanData = new ScanDocData._internal();

  factory ScanDocData() {
    return _scanData;
  }

  String file_selfie;

  String file_drvlic_front;

  String file_pp_front;

  int requestId;

  ScanDocData._internal();
}

final scanDocData = ScanDocData();
