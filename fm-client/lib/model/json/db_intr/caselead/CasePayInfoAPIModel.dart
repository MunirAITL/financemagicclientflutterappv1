import 'UserPaymentSummaryDataModel.dart';

class CasePayInfoAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  CasePayInfoAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  CasePayInfoAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  UserPaymentSummaryDataModel userPaymentSummaryData;
  ResponseData({this.userPaymentSummaryData});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userPaymentSummaryData = json['UserPaymentSummaryData'] != null
        ? new UserPaymentSummaryDataModel.fromJson(
            json['UserPaymentSummaryData'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userPaymentSummaryData != null) {
      data['UserPaymentSummaryData'] = this.userPaymentSummaryData.toJson();
    }
    return data;
  }
}
