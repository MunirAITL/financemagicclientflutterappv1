class MultipleApplicantAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  MultipleApplicantAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  MultipleApplicantAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<UserCompanyInfos> userCompanyInfos;
  ResponseData({this.userCompanyInfos});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserCompanyInfos'] != null) {
      userCompanyInfos = <UserCompanyInfos>[];
      json['UserCompanyInfos'].forEach((v) {
        userCompanyInfos.add(new UserCompanyInfos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userCompanyInfos != null) {
      data['UserCompanyInfos'] =
          this.userCompanyInfos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserCompanyInfos {
  String companyName;
  String companyType;
  String companyWebsite;
  String officePhoneNumber;
  int companyTypeId;
  String userType;
  int id;

  UserCompanyInfos(
      {this.companyName,
      this.companyType,
      this.companyWebsite,
      this.officePhoneNumber,
      this.companyTypeId,
      this.userType,
      this.id});

  UserCompanyInfos.fromJson(Map<String, dynamic> json) {
    companyName = json['CompanyName'] ?? '';
    companyType = json['CompanyType'] ?? '';
    companyWebsite = json['CompanyWebsite'] ?? '';
    officePhoneNumber = json['OfficePhoneNumber'] ?? '';
    companyTypeId = json['CompanyTypeId'] ?? 0;
    userType = json['UserType'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyName'] = this.companyName;
    data['CompanyType'] = this.companyType;
    data['CompanyWebsite'] = this.companyWebsite;
    data['OfficePhoneNumber'] = this.officePhoneNumber;
    data['CompanyTypeId'] = this.companyTypeId;
    data['UserType'] = this.userType;
    data['Id'] = this.id;
    return data;
  }
}
