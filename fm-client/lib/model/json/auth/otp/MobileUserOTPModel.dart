class MobileUserOTPModel {
  String mobileNumber;
  int status;
  int id;

  MobileUserOTPModel({this.mobileNumber, this.status, this.id});
  MobileUserOTPModel.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'] ?? '';
    status = json['Status'] ?? 0;
    id = json['Id'] ?? 0;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Status'] = this.status;
    data['Id'] = this.id;
    return data;
  }
}
