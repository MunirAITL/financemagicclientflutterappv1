class PostDocVerifyAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostDocVerifyAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostDocVerifyAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<Response> response;
  ResponseData({this.response});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['response'] != null) {
      response = [];
      json['response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['response'] = this.response.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Response {
  int id;
  String name;
  String lastName;
  String dateofBirth;
  int id1;
  int userId;
  int status;
  String creationDate;
  int versionNumber;
  int userCompanyId;
  bool isIdentity;
  double confidence;
  String documentType;
  String guessedAge;
  int realAge;
  String imageUrl1;
  String imageUrl2;
  String passportNumber;
  String passportExpiryDate;

  Response({
    this.id,
    this.name,
    this.lastName,
    this.dateofBirth,
    this.id1,
    this.userId,
    this.status,
    this.creationDate,
    this.versionNumber,
    this.userCompanyId,
    this.isIdentity,
    this.confidence,
    this.documentType,
    this.guessedAge,
    this.realAge,
    this.imageUrl1,
    this.imageUrl2,
    this.passportNumber,
    this.passportExpiryDate,
  });

  Response.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    name = json['Name'] ?? '';
    lastName = json['LastName'] ?? '';
    dateofBirth = json['DateofBirth'] ?? '';
    id1 = json['Id1'] ?? 0;
    userId = json['UserId'] ?? 0;
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? '';
    userCompanyId = json['UserCompanyId'] ?? 0;
    isIdentity = json['IsIdentity'] ?? false;
    confidence = json['Confidence'] ?? 0;
    documentType = json['DocumentType'] ?? '';
    guessedAge = json['GuessedAge'] ?? '';
    realAge = json['RealAge'] ?? 0;
    imageUrl1 = json['ImageUrl1'] ?? '';
    imageUrl2 = json['ImageUrl2'] ?? '';
    passportNumber = json['PassportNumber'] ?? '';
    passportExpiryDate = json['PassportExpiryDate'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Name'] = this.name;
    data['LastName'] = this.lastName;
    data['DateofBirth'] = this.dateofBirth;
    data['Id1'] = this.id1;
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['VersionNumber'] = this.versionNumber;
    data['UserCompanyId'] = this.userCompanyId;
    data['IsIdentity'] = this.isIdentity;
    data['Confidence'] = this.confidence;
    data['DocumentType'] = this.documentType;
    data['GuessedAge'] = this.guessedAge;
    data['RealAge'] = this.realAge;
    data['ImageUrl1'] = this.imageUrl1;
    data['ImageUrl2'] = this.imageUrl2;
    data['PassportNumber'] = this.passportNumber;
    data['PassportExpiryDate'] = this.passportExpiryDate;
    return data;
  }
}
