class GetInsight4PersonnelAccountsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetInsight4PersonnelAccountsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetInsight4PersonnelAccountsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  GetInsight4AccResponseData responseData;
  ResponseData({this.responseData});
  ResponseData.fromJson(Map<String, dynamic> json) {
    responseData = json['ResponseData'] != null
        ? new GetInsight4AccResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (responseData != null) {
      data['ResponseData'] = responseData.toJson();
    }
    return data;
  }
}

class GetInsight4AccResponseData {
  Income income;
  Income expenditure;
  FinancialPatterns financialPatterns;
  FinancialMarkers financialMarkers;
  Employment employment;

  GetInsight4AccResponseData(
      {this.income,
      this.expenditure,
      this.financialPatterns,
      this.financialMarkers,
      this.employment});

  GetInsight4AccResponseData.fromJson(Map<String, dynamic> json) {
    income =
        json['income'] != null ? new Income.fromJson(json['income']) : null;
    expenditure = json['expenditure'] != null
        ? new Income.fromJson(json['expenditure'])
        : null;
    financialPatterns = json['financial_patterns'] != null
        ? new FinancialPatterns.fromJson(json['financial_patterns'])
        : null;
    financialMarkers = json['financial_markers'] != null
        ? new FinancialMarkers.fromJson(json['financial_markers'])
        : null;
    employment = json['employment'] != null
        ? new Employment.fromJson(json['employment'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.income != null) {
      data['income'] = this.income.toJson();
    }
    if (this.expenditure != null) {
      data['expenditure'] = this.expenditure.toJson();
    }
    if (this.financialPatterns != null) {
      data['financial_patterns'] = this.financialPatterns.toJson();
    }
    if (this.financialMarkers != null) {
      data['financial_markers'] = this.financialMarkers.toJson();
    }
    if (this.employment != null) {
      data['employment'] = this.employment.toJson();
    }
    return data;
  }
}

class Income {
  String currency;
  List<MonthlyAmounts> monthlyAmounts;
  List<Categories> categories;

  Income({this.currency, this.monthlyAmounts, this.categories});

  Income.fromJson(Map<String, dynamic> json) {
    currency = json['currency'];
    if (json['monthly_amounts'] != null) {
      monthlyAmounts = [];
      json['monthly_amounts'].forEach((v) {
        monthlyAmounts.add(new MonthlyAmounts.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currency'] = this.currency;
    if (this.monthlyAmounts != null) {
      data['monthly_amounts'] =
          this.monthlyAmounts.map((v) => v.toJson()).toList();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MonthlyAmounts {
  double amount;
  int year;
  int month;

  MonthlyAmounts({this.amount, this.year, this.month});

  MonthlyAmounts.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    year = json['year'];
    month = json['month'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['year'] = this.year;
    data['month'] = this.month;
    return data;
  }
}

/*class Categories {
  String name;
  List<MonthlyAmounts> monthlyAmounts;

  Categories({this.name, this.monthlyAmounts});

  Categories.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['monthly_amounts'] != null) {
      monthlyAmounts = [];
      json['monthly_amounts'].forEach((v) {
        monthlyAmounts.add(new MonthlyAmounts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.monthlyAmounts != null) {
      data['monthly_amounts'] =
          this.monthlyAmounts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}*/

class Categories {
  String name;
  String fixedFlexible;
  List<MonthlyAmounts> monthlyAmounts;

  Categories({this.name, this.fixedFlexible, this.monthlyAmounts});

  Categories.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    fixedFlexible = json['fixed_flexible'];
    if (json['monthly_amounts'] != null) {
      monthlyAmounts = [];
      json['monthly_amounts'].forEach((v) {
        monthlyAmounts.add(new MonthlyAmounts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['fixed_flexible'] = this.fixedFlexible;
    if (this.monthlyAmounts != null) {
      data['monthly_amounts'] =
          this.monthlyAmounts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FinancialPatterns {
  String currency;
  List<Recurrence> recurrence;
  List<ByCategory> byCategory;

  FinancialPatterns({this.currency, this.recurrence, this.byCategory});

  FinancialPatterns.fromJson(Map<String, dynamic> json) {
    currency = json['currency'];
    if (json['recurrence'] != null) {
      recurrence = [];
      json['recurrence'].forEach((v) {
        recurrence.add(new Recurrence.fromJson(v));
      });
    }
    if (json['by_category'] != null) {
      byCategory = [];
      json['by_category'].forEach((v) {
        byCategory.add(new ByCategory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currency'] = this.currency;
    if (this.recurrence != null) {
      data['recurrence'] = this.recurrence.map((v) => v.toJson()).toList();
    }
    if (this.byCategory != null) {
      data['by_category'] = this.byCategory.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Recurrence {
  String description;
  double averageAmount;
  double amountVariance;
  List<String> transactionIds;

  Recurrence(
      {this.description,
      this.averageAmount,
      this.amountVariance,
      this.transactionIds});

  Recurrence.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    averageAmount = json['average_amount'];
    amountVariance = json['amount_variance'];
    transactionIds = json['transaction_ids'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['average_amount'] = this.averageAmount;
    data['amount_variance'] = this.amountVariance;
    data['transaction_ids'] = this.transactionIds;
    return data;
  }
}

class ByCategory {
  String category;
  double min;
  double mean;
  double max;
  double variance;
  double frequency;
  int continuity;
  int recency;

  ByCategory(
      {this.category,
      this.min,
      this.mean,
      this.max,
      this.variance,
      this.frequency,
      this.continuity,
      this.recency});

  ByCategory.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    min = json['min'];
    mean = json['mean'];
    max = json['max'];
    variance = json['variance'];
    frequency = json['frequency'];
    continuity = json['continuity'];
    recency = json['recency'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['min'] = this.min;
    data['mean'] = this.mean;
    data['max'] = this.max;
    data['variance'] = this.variance;
    data['frequency'] = this.frequency;
    data['continuity'] = this.continuity;
    data['recency'] = this.recency;
    return data;
  }
}

class FinancialMarkers {
  BankCharges bankCharges;
  bool newDebtIndicator;
  CreditRepayments creditRepayments;
  String currency;
  BankCharges returnedDirectDebits;
  double negativeBalanceRatio;
  bool debtManagementIndicator;
  DebitBankTransfer debitBankTransfer;
  SummaryAverages summaryAverages;
  BankCharges utilityPayments;
  Gambling gambling;
  Gambling cashWithdrawals;

  FinancialMarkers(
      {this.bankCharges,
      this.newDebtIndicator,
      this.creditRepayments,
      this.currency,
      this.returnedDirectDebits,
      this.negativeBalanceRatio,
      this.debtManagementIndicator,
      this.debitBankTransfer,
      this.summaryAverages,
      this.utilityPayments,
      this.gambling,
      this.cashWithdrawals});

  FinancialMarkers.fromJson(Map<String, dynamic> json) {
    bankCharges = json['bank_charges'] != null
        ? new BankCharges.fromJson(json['bank_charges'])
        : null;
    newDebtIndicator = json['new_debt_indicator'];
    creditRepayments = json['credit_repayments'] != null
        ? new CreditRepayments.fromJson(json['credit_repayments'])
        : null;
    currency = json['currency'];
    returnedDirectDebits = json['returned_direct_debits'] != null
        ? new BankCharges.fromJson(json['returned_direct_debits'])
        : null;
    negativeBalanceRatio = json['negative_balance_ratio'];
    debtManagementIndicator = json['debt_management_indicator'];
    debitBankTransfer = json['debit_bank_transfer'] != null
        ? new DebitBankTransfer.fromJson(json['debit_bank_transfer'])
        : null;
    summaryAverages = json['summary_averages'] != null
        ? new SummaryAverages.fromJson(json['summary_averages'])
        : null;
    utilityPayments = json['utility_payments'] != null
        ? new BankCharges.fromJson(json['utility_payments'])
        : null;
    gambling = json['gambling'] != null
        ? new Gambling.fromJson(json['gambling'])
        : null;
    cashWithdrawals = json['cash_withdrawals'] != null
        ? new Gambling.fromJson(json['cash_withdrawals'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bankCharges != null) {
      data['bank_charges'] = this.bankCharges.toJson();
    }
    data['new_debt_indicator'] = this.newDebtIndicator;
    if (this.creditRepayments != null) {
      data['credit_repayments'] = this.creditRepayments.toJson();
    }
    data['currency'] = this.currency;
    if (this.returnedDirectDebits != null) {
      data['returned_direct_debits'] = this.returnedDirectDebits.toJson();
    }
    data['negative_balance_ratio'] = this.negativeBalanceRatio;
    data['debt_management_indicator'] = this.debtManagementIndicator;
    if (this.debitBankTransfer != null) {
      data['debit_bank_transfer'] = this.debitBankTransfer.toJson();
    }
    if (this.summaryAverages != null) {
      data['summary_averages'] = this.summaryAverages.toJson();
    }
    if (this.utilityPayments != null) {
      data['utility_payments'] = this.utilityPayments.toJson();
    }
    if (this.gambling != null) {
      data['gambling'] = this.gambling.toJson();
    }
    if (this.cashWithdrawals != null) {
      data['cash_withdrawals'] = this.cashWithdrawals.toJson();
    }
    return data;
  }
}

class BankCharges {
  NumLast3m numLast3m;
  NumLast3m numLast6m;
  NumLast3m numLast12m;

  BankCharges({this.numLast3m, this.numLast6m, this.numLast12m});

  BankCharges.fromJson(Map<String, dynamic> json) {
    numLast3m = json['num_last_3m'] != null
        ? new NumLast3m.fromJson(json['num_last_3m'])
        : null;
    numLast6m = json['num_last_6m'] != null
        ? new NumLast3m.fromJson(json['num_last_6m'])
        : null;
    numLast12m = json['num_last_12m'] != null
        ? new NumLast3m.fromJson(json['num_last_12m'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.numLast3m != null) {
      data['num_last_3m'] = this.numLast3m.toJson();
    }
    if (this.numLast6m != null) {
      data['num_last_6m'] = this.numLast6m.toJson();
    }
    if (this.numLast12m != null) {
      data['num_last_12m'] = this.numLast12m.toJson();
    }
    return data;
  }
}

class NumLast3m {
  int timestampFrom;
  int timestampTo;
  int count;

  NumLast3m({this.timestampFrom, this.timestampTo, this.count});

  NumLast3m.fromJson(Map<String, dynamic> json) {
    timestampFrom = json['timestamp_from'];
    timestampTo = json['timestamp_to'];
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['timestamp_from'] = this.timestampFrom;
    data['timestamp_to'] = this.timestampTo;
    data['count'] = this.count;
    return data;
  }
}

class CreditRepayments {
  double rateOfChange;
  double predNextMonth;
  double ratioToIncome;

  CreditRepayments({this.rateOfChange, this.predNextMonth, this.ratioToIncome});

  CreditRepayments.fromJson(Map<String, dynamic> json) {
    rateOfChange = json['rate_of_change'] ?? 0.0;
    predNextMonth = json['pred_next_month'] ?? 0.0;
    ratioToIncome = json['ratio_to_income'] ?? 0.0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rate_of_change'] = this.rateOfChange;
    data['pred_next_month'] = this.predNextMonth;
    data['ratio_to_income'] = this.ratioToIncome;
    return data;
  }
}

class DebitBankTransfer {
  double rateOfChange;
  double predNextMonth;
  double ratioToIncome;

  DebitBankTransfer(
      {this.rateOfChange, this.predNextMonth, this.ratioToIncome});

  DebitBankTransfer.fromJson(Map<String, dynamic> json) {
    rateOfChange = json['rate_of_change'] ?? 0.0;
    predNextMonth = json['pred_next_month'] ?? 0.0;
    ratioToIncome = json['ratio_to_income'] ?? 0.0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rate_of_change'] = this.rateOfChange;
    data['pred_next_month'] = this.predNextMonth;
    data['ratio_to_income'] = this.ratioToIncome;
    return data;
  }
}

class SummaryAverages {
  double averageFixedExp;
  double averageFlexibleExp;
  double averageIncome;

  SummaryAverages(
      {this.averageFixedExp, this.averageFlexibleExp, this.averageIncome});

  SummaryAverages.fromJson(Map<String, dynamic> json) {
    averageFixedExp = json['average_fixed_exp'];
    averageFlexibleExp = json['average_flexible_exp'];
    averageIncome = json['average_income'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['average_fixed_exp'] = this.averageFixedExp;
    data['average_flexible_exp'] = this.averageFlexibleExp;
    data['average_income'] = this.averageIncome;
    return data;
  }
}

class Gambling {
  double rateOfChange;
  double predNextMonth;
  double ratioToIncome;

  Gambling({this.rateOfChange, this.predNextMonth, this.ratioToIncome});

  Gambling.fromJson(Map<String, dynamic> json) {
    rateOfChange = json['rate_of_change'] ?? 0.0;
    predNextMonth = json['pred_next_month'] ?? 0.0;
    ratioToIncome = json['ratio_to_income'] ?? 0.0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rate_of_change'] = this.rateOfChange;
    data['pred_next_month'] = this.predNextMonth;
    data['ratio_to_income'] = this.ratioToIncome;
    return data;
  }
}

class Employment {
  int numberOfSalaries;
  List<dynamic> expectedSalary;
  String currency;
  bool possibleLossOfIncomeIndicator;
  bool outOfWorkBenefitIndicator;
  bool newEmployerIndicator;

  Employment(
      {this.numberOfSalaries,
      this.expectedSalary,
      this.currency,
      this.possibleLossOfIncomeIndicator,
      this.outOfWorkBenefitIndicator,
      this.newEmployerIndicator});

  Employment.fromJson(Map<String, dynamic> json) {
    numberOfSalaries = json['number_of_salaries'];
    if (json['expected_salary'] != null) {
      expectedSalary = [];
      json['expected_salary'].forEach((v) {
        expectedSalary.add(v);
      });
    }
    currency = json['currency'];
    possibleLossOfIncomeIndicator = json['possible_loss_of_income_indicator'];
    outOfWorkBenefitIndicator = json['out_of_work_benefit_indicator'];
    newEmployerIndicator = json['new_employer_indicator'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number_of_salaries'] = this.numberOfSalaries;
    if (this.expectedSalary != null) {
      data['expected_salary'] = this.expectedSalary.map((v) => v).toList();
    }
    data['currency'] = this.currency;
    data['possible_loss_of_income_indicator'] =
        this.possibleLossOfIncomeIndicator;
    data['out_of_work_benefit_indicator'] = this.outOfWorkBenefitIndicator;
    data['new_employer_indicator'] = this.newEmployerIndicator;
    return data;
  }
}
