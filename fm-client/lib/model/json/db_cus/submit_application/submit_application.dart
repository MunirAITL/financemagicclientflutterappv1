class SubmitApplication {
  int id;
  int userId;
  int status;
  int mortgageCaseInfoId;
  int taskId;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String defineSolution;
  String lenderProvider;
  String term;
  int loanAmount;
  int rate;
  int value;
  String fixedPeriodEnds;
  int procFees;
  int adviceFee;
  int introducerFeeShare;
  String introducerPaymentMade;
  String reasonForRecommendation;
  String anyOtherComments;
  String caseStatus;
  String remarks;
  int monthlyPaymentAmount;
  int otherChargeAmount;
  int rentalIncomeAmount;
  String valuationDate;
  String fileUrl;

  SubmitApplication(
      {this.id,
        this.userId,
        this.status,
        this.mortgageCaseInfoId,
        this.taskId,
        this.creationDate,
        this.updatedDate,
        this.versionNumber,
        this.defineSolution,
        this.lenderProvider,
        this.term,
        this.loanAmount,
        this.rate,
        this.value,
        this.fixedPeriodEnds,
        this.procFees,
        this.adviceFee,
        this.introducerFeeShare,
        this.introducerPaymentMade,
        this.reasonForRecommendation,
        this.anyOtherComments,
        this.caseStatus,
        this.remarks,
        this.monthlyPaymentAmount,
        this.otherChargeAmount,
        this.rentalIncomeAmount,
        this.valuationDate,
        this.fileUrl});

  SubmitApplication.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userId = json['UserId'];
    status = json['Status'];
    mortgageCaseInfoId = json['MortgageCaseInfoId'];
    taskId = json['TaskId'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    defineSolution = json['DefineSolution'];
    lenderProvider = json['LenderProvider'];
    term = json['Term'];
    loanAmount = json['LoanAmount'];
    rate = json['Rate'];
    value = json['Value'];
    fixedPeriodEnds = json['FixedPeriodEnds'];
    procFees = json['ProcFees'];
    adviceFee = json['AdviceFee'];
    introducerFeeShare = json['IntroducerFeeShare'];
    introducerPaymentMade = json['IntroducerPaymentMade'];
    reasonForRecommendation = json['ReasonForRecommendation'];
    anyOtherComments = json['AnyOtherComments'];
    caseStatus = json['CaseStatus'];
    remarks = json['Remarks'];
    monthlyPaymentAmount = json['MonthlyPaymentAmount'];
    otherChargeAmount = json['OtherChargeAmount'];
    rentalIncomeAmount = json['RentalIncomeAmount'];
    valuationDate = json['ValuationDate'];
    fileUrl = json['FileUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['TaskId'] = this.taskId;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['DefineSolution'] = this.defineSolution;
    data['LenderProvider'] = this.lenderProvider;
    data['Term'] = this.term;
    data['LoanAmount'] = this.loanAmount;
    data['Rate'] = this.rate;
    data['Value'] = this.value;
    data['FixedPeriodEnds'] = this.fixedPeriodEnds;
    data['ProcFees'] = this.procFees;
    data['AdviceFee'] = this.adviceFee;
    data['IntroducerFeeShare'] = this.introducerFeeShare;
    data['IntroducerPaymentMade'] = this.introducerPaymentMade;
    data['ReasonForRecommendation'] = this.reasonForRecommendation;
    data['AnyOtherComments'] = this.anyOtherComments;
    data['CaseStatus'] = this.caseStatus;
    data['Remarks'] = this.remarks;
    data['MonthlyPaymentAmount'] = this.monthlyPaymentAmount;
    data['OtherChargeAmount'] = this.otherChargeAmount;
    data['RentalIncomeAmount'] = this.rentalIncomeAmount;
    data['ValuationDate'] = this.valuationDate;
    data['FileUrl'] = this.fileUrl;
    return data;
  }
}
