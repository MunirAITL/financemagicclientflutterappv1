import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';

class PostCaseAPIModel {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  PostCaseAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory PostCaseAPIModel.fromJson(Map<String, dynamic> j) {
    return PostCaseAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {}

class _Messages {
  List<dynamic> task_post;
  _Messages({this.task_post});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      task_post: j['task_post'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'task_post': task_post,
      };
}

class _ResponseData {
  Task task;
  _ResponseData({this.task});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var taskMap = (j['Task'] != null) ? Task.fromJson(j['Task']) : {};
    return _ResponseData(task: taskMap);
  }
  Map<String, dynamic> toMap() => {
        'Task': task,
      };
}
