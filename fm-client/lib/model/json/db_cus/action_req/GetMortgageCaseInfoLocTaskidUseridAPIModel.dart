import 'package:aitl/model/json/web2native/case/doc/MortgageCaseDocumentInfos.dart';
import 'package:aitl/view/db_cus/timeline/chat/TimeLinePostScreen.dart';

class GetMortgageCaseInfoLocTaskidUseridAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetMortgageCaseInfoLocTaskidUseridAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetMortgageCaseInfoLocTaskidUseridAPIModel.fromJson(
      Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageCaseInfo mortgageCaseInfo;
  MortgageCaseDocumentInfos mortgageCaseDocumentInfo;

  ResponseData({this.mortgageCaseInfo, this.mortgageCaseDocumentInfo});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageCaseInfo = json['MortgageCaseInfo'] != null
        ? new MortgageCaseInfo.fromJson(json['MortgageCaseInfo'])
        : null;
    mortgageCaseDocumentInfo = json['MortgageCaseDocumentInfo'] != null
        ? new MortgageCaseDocumentInfos.fromJson(
            json['MortgageCaseDocumentInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageCaseInfo != null) {
      data['MortgageCaseInfo'] = this.mortgageCaseInfo.toJson();
    }
    if (this.mortgageCaseDocumentInfo != null) {
      data['MortgageCaseDocumentInfo'] = this.mortgageCaseDocumentInfo.toJson();
    }
    return data;
  }
}

class MortgageCaseInfo {
  int userId;
  User user;
  int companyId;
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String caseType;
  String customerType;
  String isSmoker;
  String isDoYouHaveAnyFinancialDependants;
  String remarks;
  String isAnyOthers;
  int taskId;
  int coapplicantUserId;
  String customerName;
  String customerEmail;
  String customerMobileNumber;
  String customerAddress;
  String customerDateofBirth;
  String customerGender;
  String customerAreYouASmoker;
  String profileImageUrl;
  String namePrefix;
  String areYouBuyingThePropertyInNameOfASPV;
  String companyName;
  String registeredAddress;
  String dateRegistered;
  String companyRegistrationNumber;
  int applicationNumber;
  String customerEmail1;
  String customerEmail2;
  String customerEmail3;
  String isDoYouHaveAnyBTLPortfulio;
  String isClientAgreement;
  double adminFee;
  String adminFeeWhenPayable;
  double adviceFee;
  String adviceFeeWhenPayable;
  String isFeesRefundable;
  String feesRefundable;
  String iPAdddress;
  String iPLocation;
  String deviceType;
  String reportLogo;
  String officePhoneNumber;
  String reportFooter;
  String adviceFeeType;
  String clientAgreementStatus;
  String recommendationAgreementStatus;
  String recommendationAgreementSignature;
  String isThereAnySavingsOrInvestments;
  String isThereAnyExistingPolicy;
  String taskTitleUrl;
  String customerAddress1;
  String customerAddress2;
  String customerAddress3;
  String customerPostcode;
  String customerTown;
  String customerLastName;
  int id;

  MortgageCaseInfo(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.caseType,
      this.customerType,
      this.isSmoker,
      this.isDoYouHaveAnyFinancialDependants,
      this.remarks,
      this.isAnyOthers,
      this.taskId,
      this.coapplicantUserId,
      this.customerName,
      this.customerEmail,
      this.customerMobileNumber,
      this.customerAddress,
      this.customerDateofBirth,
      this.customerGender,
      this.customerAreYouASmoker,
      this.profileImageUrl,
      this.namePrefix,
      this.areYouBuyingThePropertyInNameOfASPV,
      this.companyName,
      this.registeredAddress,
      this.dateRegistered,
      this.companyRegistrationNumber,
      this.applicationNumber,
      this.customerEmail1,
      this.customerEmail2,
      this.customerEmail3,
      this.isDoYouHaveAnyBTLPortfulio,
      this.isClientAgreement,
      this.adminFee,
      this.adminFeeWhenPayable,
      this.adviceFee,
      this.adviceFeeWhenPayable,
      this.isFeesRefundable,
      this.feesRefundable,
      this.iPAdddress,
      this.iPLocation,
      this.deviceType,
      this.reportLogo,
      this.officePhoneNumber,
      this.reportFooter,
      this.adviceFeeType,
      this.clientAgreementStatus,
      this.recommendationAgreementStatus,
      this.recommendationAgreementSignature,
      this.isThereAnySavingsOrInvestments,
      this.isThereAnyExistingPolicy,
      this.taskTitleUrl,
      this.customerAddress1,
      this.customerAddress2,
      this.customerAddress3,
      this.customerPostcode,
      this.customerTown,
      this.customerLastName,
      this.id});

  MortgageCaseInfo.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    companyId = json['CompanyId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    caseType = json['CaseType'];
    customerType = json['CustomerType'];
    isSmoker = json['IsSmoker'];
    isDoYouHaveAnyFinancialDependants =
        json['IsDoYouHaveAnyFinancialDependants'];
    remarks = json['Remarks'];
    isAnyOthers = json['IsAnyOthers'];
    taskId = json['TaskId'];
    coapplicantUserId = json['CoapplicantUserId'];
    customerName = json['CustomerName'];
    customerEmail = json['CustomerEmail'];
    customerMobileNumber = json['CustomerMobileNumber'];
    customerAddress = json['CustomerAddress'];
    customerDateofBirth = json['CustomerDateofBirth'];
    customerGender = json['CustomerGender'];
    customerAreYouASmoker = json['CustomerAreYouASmoker'];
    profileImageUrl = json['ProfileImageUrl'];
    namePrefix = json['NamePrefix'];
    areYouBuyingThePropertyInNameOfASPV =
        json['AreYouBuyingThePropertyInNameOfASPV'];
    companyName = json['CompanyName'];
    registeredAddress = json['RegisteredAddress'];
    dateRegistered = json['DateRegistered'];
    companyRegistrationNumber = json['CompanyRegistrationNumber'];
    applicationNumber = json['ApplicationNumber'];
    customerEmail1 = json['CustomerEmail1'];
    customerEmail2 = json['CustomerEmail2'];
    customerEmail3 = json['CustomerEmail3'];
    isDoYouHaveAnyBTLPortfulio = json['IsDoYouHaveAnyBTLPortfulio'];
    isClientAgreement = json['IsClientAgreement'];
    adminFee = json['AdminFee'];
    adminFeeWhenPayable = json['AdminFeeWhenPayable'];
    adviceFee = json['AdviceFee'];
    adviceFeeWhenPayable = json['AdviceFeeWhenPayable'];
    isFeesRefundable = json['IsFeesRefundable'];
    feesRefundable = json['FeesRefundable'];
    iPAdddress = json['IPAdddress'];
    iPLocation = json['IPLocation'];
    deviceType = json['DeviceType'];
    reportLogo = json['ReportLogo'];
    officePhoneNumber = json['OfficePhoneNumber'];
    reportFooter = json['ReportFooter'];
    adviceFeeType = json['AdviceFeeType'];
    clientAgreementStatus = json['ClientAgreementStatus'];
    recommendationAgreementStatus = json['RecommendationAgreementStatus'];
    recommendationAgreementSignature = json['RecommendationAgreementSignature'];
    isThereAnySavingsOrInvestments = json['IsThereAnySavingsOrInvestments'];
    isThereAnyExistingPolicy = json['IsThereAnyExistingPolicy'];
    taskTitleUrl = json['TaskTitleUrl'];
    customerAddress1 = json['CustomerAddress1'];
    customerAddress2 = json['CustomerAddress2'];
    customerAddress3 = json['CustomerAddress3'];
    customerPostcode = json['CustomerPostcode'];
    customerTown = json['CustomerTown'];
    customerLastName = json['CustomerLastName'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['data'] = this.user.toJson();
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['CaseType'] = this.caseType;
    data['CustomerType'] = this.customerType;
    data['IsSmoker'] = this.isSmoker;
    data['IsDoYouHaveAnyFinancialDependants'] =
        this.isDoYouHaveAnyFinancialDependants;
    data['Remarks'] = this.remarks;
    data['IsAnyOthers'] = this.isAnyOthers;
    data['TaskId'] = this.taskId;
    data['CoapplicantUserId'] = this.coapplicantUserId;
    data['CustomerName'] = this.customerName;
    data['CustomerEmail'] = this.customerEmail;
    data['CustomerMobileNumber'] = this.customerMobileNumber;
    data['CustomerAddress'] = this.customerAddress;
    data['CustomerDateofBirth'] = this.customerDateofBirth;
    data['CustomerGender'] = this.customerGender;
    data['CustomerAreYouASmoker'] = this.customerAreYouASmoker;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['NamePrefix'] = this.namePrefix;
    data['AreYouBuyingThePropertyInNameOfASPV'] =
        this.areYouBuyingThePropertyInNameOfASPV;
    data['CompanyName'] = this.companyName;
    data['RegisteredAddress'] = this.registeredAddress;
    data['DateRegistered'] = this.dateRegistered;
    data['CompanyRegistrationNumber'] = this.companyRegistrationNumber;
    data['ApplicationNumber'] = this.applicationNumber;
    data['CustomerEmail1'] = this.customerEmail1;
    data['CustomerEmail2'] = this.customerEmail2;
    data['CustomerEmail3'] = this.customerEmail3;
    data['IsDoYouHaveAnyBTLPortfulio'] = this.isDoYouHaveAnyBTLPortfulio;
    data['IsClientAgreement'] = this.isClientAgreement;
    data['AdminFee'] = this.adminFee;
    data['AdminFeeWhenPayable'] = this.adminFeeWhenPayable;
    data['AdviceFee'] = this.adviceFee;
    data['AdviceFeeWhenPayable'] = this.adviceFeeWhenPayable;
    data['IsFeesRefundable'] = this.isFeesRefundable;
    data['FeesRefundable'] = this.feesRefundable;
    data['IPAdddress'] = this.iPAdddress;
    data['IPLocation'] = this.iPLocation;
    data['DeviceType'] = this.deviceType;
    data['ReportLogo'] = this.reportLogo;
    data['OfficePhoneNumber'] = this.officePhoneNumber;
    data['ReportFooter'] = this.reportFooter;
    data['AdviceFeeType'] = this.adviceFeeType;
    data['ClientAgreementStatus'] = this.clientAgreementStatus;
    data['RecommendationAgreementStatus'] = this.recommendationAgreementStatus;
    data['RecommendationAgreementSignature'] =
        this.recommendationAgreementSignature;
    data['IsThereAnySavingsOrInvestments'] =
        this.isThereAnySavingsOrInvestments;
    data['IsThereAnyExistingPolicy'] = this.isThereAnyExistingPolicy;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['CustomerAddress1'] = this.customerAddress1;
    data['CustomerAddress2'] = this.customerAddress2;
    data['CustomerAddress3'] = this.customerAddress3;
    data['CustomerPostcode'] = this.customerPostcode;
    data['CustomerTown'] = this.customerTown;
    data['CustomerLastName'] = this.customerLastName;
    data['Id'] = this.id;
    return data;
  }
}
