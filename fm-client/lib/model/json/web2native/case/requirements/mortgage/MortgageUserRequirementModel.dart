class MortgageUserRequirementModel {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  String mortgageType;
  String addressOfPropertyToBeMortgaged;
  double priceOfPropertyBeingPurchasedCurrentValuationOfProperty;
  double howMuchDoYouWishToBorrow;
  double amountOfDepositEquity;
  String sourceOfDeposit;
  double remortgageOfAnotherProperty;
  double savings;
  double giftFromFamilyMember;
  double otherStateWhat;
  String areFundsAvailableToPayFeesInConnectionWithMortgage;
  String doesExistingLenderFacilitateFurtherAdvances;
  String currentLenderAccountNumber;
  String propertyType;
  String propertyTenure;
  String ifLeaseholdHowLongIsLeftOnTheLease;
  int numberOfBedrooms;
  String floorsInTheBuilding;
  String whichFloorIsTheProperty;
  String yearPropertyWasBuilt;
  String anyExtensionOrLoftConversionDone;
  String isThePropertyOfANonStandardConstruction;
  String isItExCouncil;
  String isThisANewBuiltProperty;
  String hasTheVendorOwnedThePropertyOverSixMonths;
  String
      areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods;
  String isThisAnAuctionOrRepossessionPurchase;
  String hasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub;
  String notes;
  String remarks;
  String financeType;
  String auctionHouseLotDetails;
  String repaymentType;
  String exitStrategy;
  String details;
  String gVD;
  String areYouLooking;
  double balanceOutstanding;
  double lTVAmount;
  double amountofLoanRequired;
  String typeofLoan;
  String doYouHaveAccountsForTheBusiness;
  String doYouHaveManagementAccounts;
  String nonstandardConstructionDetails;
  String areYouRaisingAnyMoneyfromAboveProperty;
  double
      areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue;
  double areFundsAvailableToPayFeesInConnectionWithMortgageValue;
  int preferredMortgageTermYear;
  int preferredMortgageTermMonth;
  int numberOfKoichens;
  int numberOfBaahroom;
  int numberOfWC;
  String isTheFlatAboveShopRestaurant;
  String doesTheFlatHaveDeckORBalconyAccess;
  String areYouLookingToRemortgageYourCurrentResidentialProperty;
  String didYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation;
  String areThereAnyRightToBuyOrSimilarRestrictionImposed;
  String restrictionExpireDate;
  String areYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation;
  String doYouHaveExistingMortgageOnTheProperty;
  String currentLender;
  String datePropertyPurchased;
  double currentMortgageToBeRedeemed;
  double purchasePrice;
  double amountRaisingOverCurrentMortgage;
  String doYouWantToApplyForInterestOnlyLoan;
  double monthlyMortgagePayment;
  double rentalIncomeAchievableAmount;
  String isThePropertyAlreadyTenanted;
  String tenancyType;
  String areYouCurrentlyResidingInThisProperty;
  String
      areYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty;
  String areYouAFirstTimeLandLord;
  String haveYouBeenAnExperiencedLandlordForMoreThan12Months;
  String haveYouEverLivedInThisProperty;
  String areYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty;
  String propertyDescription;
  String incomeFromTheProperty;
  String whatIsYourLoanRepaymentStrategy;
  double equityAmount;
  String preferredRepaymentType;
  double serviceChargeAmount;
  double groundRentAmount;
  String clientExperience;
  String plan;
  String costofRefurb;
  String postCode;
  String propertyEPCRating;
  double sharedHolderPercentage;
  String doesThePropertyHaveALift;
  String doesThisPropertyHaveWarranty;
  String warrantyType;
  int id;

  MortgageUserRequirementModel(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.mortgageType,
      this.addressOfPropertyToBeMortgaged,
      this.priceOfPropertyBeingPurchasedCurrentValuationOfProperty,
      this.howMuchDoYouWishToBorrow,
      this.amountOfDepositEquity,
      this.sourceOfDeposit,
      this.remortgageOfAnotherProperty,
      this.savings,
      this.giftFromFamilyMember,
      this.otherStateWhat,
      this.areFundsAvailableToPayFeesInConnectionWithMortgage,
      this.doesExistingLenderFacilitateFurtherAdvances,
      this.currentLenderAccountNumber,
      this.propertyType,
      this.propertyTenure,
      this.ifLeaseholdHowLongIsLeftOnTheLease,
      this.numberOfBedrooms,
      this.floorsInTheBuilding,
      this.whichFloorIsTheProperty,
      this.yearPropertyWasBuilt,
      this.anyExtensionOrLoftConversionDone,
      this.isThePropertyOfANonStandardConstruction,
      this.isItExCouncil,
      this.isThisANewBuiltProperty,
      this.hasTheVendorOwnedThePropertyOverSixMonths,
      this.areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods,
      this.isThisAnAuctionOrRepossessionPurchase,
      this.hasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub,
      this.notes,
      this.remarks,
      this.financeType,
      this.auctionHouseLotDetails,
      this.repaymentType,
      this.exitStrategy,
      this.details,
      this.gVD,
      this.areYouLooking,
      this.balanceOutstanding,
      this.lTVAmount,
      this.amountofLoanRequired,
      this.typeofLoan,
      this.doYouHaveAccountsForTheBusiness,
      this.doYouHaveManagementAccounts,
      this.nonstandardConstructionDetails,
      this.areYouRaisingAnyMoneyfromAboveProperty,
      this.areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue,
      this.areFundsAvailableToPayFeesInConnectionWithMortgageValue,
      this.preferredMortgageTermYear,
      this.preferredMortgageTermMonth,
      this.numberOfKoichens,
      this.numberOfBaahroom,
      this.numberOfWC,
      this.isTheFlatAboveShopRestaurant,
      this.doesTheFlatHaveDeckORBalconyAccess,
      this.areYouLookingToRemortgageYourCurrentResidentialProperty,
      this.didYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation,
      this.areThereAnyRightToBuyOrSimilarRestrictionImposed,
      this.restrictionExpireDate,
      this.areYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation,
      this.doYouHaveExistingMortgageOnTheProperty,
      this.currentLender,
      this.datePropertyPurchased,
      this.currentMortgageToBeRedeemed,
      this.purchasePrice,
      this.amountRaisingOverCurrentMortgage,
      this.doYouWantToApplyForInterestOnlyLoan,
      this.monthlyMortgagePayment,
      this.rentalIncomeAchievableAmount,
      this.isThePropertyAlreadyTenanted,
      this.tenancyType,
      this.areYouCurrentlyResidingInThisProperty,
      this.areYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty,
      this.areYouAFirstTimeLandLord,
      this.haveYouBeenAnExperiencedLandlordForMoreThan12Months,
      this.haveYouEverLivedInThisProperty,
      this.areYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty,
      this.propertyDescription,
      this.incomeFromTheProperty,
      this.whatIsYourLoanRepaymentStrategy,
      this.equityAmount,
      this.preferredRepaymentType,
      this.serviceChargeAmount,
      this.groundRentAmount,
      this.clientExperience,
      this.plan,
      this.costofRefurb,
      this.postCode,
      this.propertyEPCRating,
      this.sharedHolderPercentage,
      this.doesThePropertyHaveALift,
      this.doesThisPropertyHaveWarranty,
      this.warrantyType,
      this.id});

  MortgageUserRequirementModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    mortgageType = json['MortgageType'] ?? '';
    addressOfPropertyToBeMortgaged =
        json['AddressOfPropertyToBeMortgaged'] ?? '';
    priceOfPropertyBeingPurchasedCurrentValuationOfProperty =
        json['PriceOfPropertyBeingPurchasedCurrentValuationOfProperty'] ?? 0;
    howMuchDoYouWishToBorrow = json['HowMuchDoYouWishToBorrow'] ?? 0;
    amountOfDepositEquity = json['AmountOfDepositEquity'] ?? 0;
    sourceOfDeposit = json['SourceOfDeposit'] ?? '';
    remortgageOfAnotherProperty = json['RemortgageOfAnotherProperty'] ?? 0;
    savings = json['Savings'] ?? 0;
    giftFromFamilyMember = json['GiftFromFamilyMember'] ?? 0;
    otherStateWhat = json['OtherStateWhat'] ?? 0;
    areFundsAvailableToPayFeesInConnectionWithMortgage =
        json['AreFundsAvailableToPayFeesInConnectionWithMortgage'] ?? '';
    doesExistingLenderFacilitateFurtherAdvances =
        json['DoesExistingLenderFacilitateFurtherAdvances'] ?? '';
    currentLenderAccountNumber = json['CurrentLenderAccountNumber'] ?? '';
    propertyType = json['PropertyType'] ?? '';
    propertyTenure = json['PropertyTenure'] ?? '';
    ifLeaseholdHowLongIsLeftOnTheLease =
        json['IfLeaseholdHowLongIsLeftOnTheLease'] ?? '';
    numberOfBedrooms = json['NumberOfBedrooms'] ?? 0;
    floorsInTheBuilding = json['FloorsInTheBuilding'] ?? '';
    whichFloorIsTheProperty = json['WhichFloorIsTheProperty'] ?? '';
    yearPropertyWasBuilt = json['YearPropertyWasBuilt'] ?? '';
    anyExtensionOrLoftConversionDone =
        json['AnyExtensionOrLoftConversionDone'] ?? '';
    isThePropertyOfANonStandardConstruction =
        json['IsThePropertyOfANonStandardConstruction'] ?? '';
    isItExCouncil = json['IsItExCouncil'] ?? '';
    isThisANewBuiltProperty = json['IsThisANewBuiltProperty'] ?? '';
    hasTheVendorOwnedThePropertyOverSixMonths =
        json['HasTheVendorOwnedThePropertyOverSixMonths'] ?? '';
    areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods =
        json['AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods'] ??
            '';
    isThisAnAuctionOrRepossessionPurchase =
        json['IsThisAnAuctionOrRepossessionPurchase'] ?? '';
    hasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub =
        json['HasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub'] ?? '';
    notes = json['Notes'] ?? '';
    remarks = json['Remarks'] ?? '';
    financeType = json['FinanceType'] ?? '';
    auctionHouseLotDetails = json['AuctionHouseLotDetails'] ?? '';
    repaymentType = json['RepaymentType'] ?? '';
    exitStrategy = json['ExitStrategy'] ?? '';
    details = json['Details'] ?? '';
    gVD = json['GVD'] ?? '';
    areYouLooking = json['AreYouLooking'] ?? '';
    balanceOutstanding = json['BalanceOutstanding'] ?? 0;
    lTVAmount = json['LTVAmount'] ?? 0;
    amountofLoanRequired = json['AmountofLoanRequired'] ?? 0;
    typeofLoan = json['TypeofLoan'] ?? '';
    doYouHaveAccountsForTheBusiness =
        json['DoYouHaveAccountsForTheBusiness'] ?? '';
    doYouHaveManagementAccounts = json['DoYouHaveManagementAccounts'] ?? '';
    nonstandardConstructionDetails =
        json['NonstandardConstructionDetails'] ?? '';
    areYouRaisingAnyMoneyfromAboveProperty =
        json['AreYouRaisingAnyMoneyfromAboveProperty'] ?? '';
    areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue =
        json['AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue'] ??
            0;
    areFundsAvailableToPayFeesInConnectionWithMortgageValue =
        json['AreFundsAvailableToPayFeesInConnectionWithMortgageValue'] ?? 0;
    preferredMortgageTermYear = json['PreferredMortgageTermYear'] ?? 0;
    preferredMortgageTermMonth = json['PreferredMortgageTermMonth'] ?? 0;
    numberOfKoichens = json['NumberOfKoichens'] ?? 0;
    numberOfBaahroom = json['NumberOfBaahroom'] ?? 0;
    numberOfWC = json['NumberOfWC'] ?? 0;
    isTheFlatAboveShopRestaurant = json['IsTheFlatAboveShopRestaurant'] ?? '';
    doesTheFlatHaveDeckORBalconyAccess =
        json['DoesTheFlatHaveDeckORBalconyAccess'] ?? '';
    areYouLookingToRemortgageYourCurrentResidentialProperty =
        json['AreYouLookingToRemortgageYourCurrentResidentialProperty'] ?? '';
    didYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation = json[
            'DidYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation'] ??
        '';
    areThereAnyRightToBuyOrSimilarRestrictionImposed =
        json['AreThereAnyRightToBuyOrSimilarRestrictionImposed'] ?? '';
    restrictionExpireDate = json['RestrictionExpireDate'] ?? '';
    areYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation = json[
            'AreYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation'] ??
        '';
    doYouHaveExistingMortgageOnTheProperty =
        json['DoYouHaveExistingMortgageOnTheProperty'] ?? '';
    currentLender = json['CurrentLender'] ?? '';
    datePropertyPurchased = json['DatePropertyPurchased'] ?? '';
    currentMortgageToBeRedeemed = json['CurrentMortgageToBeRedeemed'] ?? 0;
    purchasePrice = json['PurchasePrice'] ?? 0;
    amountRaisingOverCurrentMortgage =
        json['AmountRaisingOverCurrentMortgage'] ?? 0;
    doYouWantToApplyForInterestOnlyLoan =
        json['DoYouWantToApplyForInterestOnlyLoan'] ?? '';
    monthlyMortgagePayment = json['MonthlyMortgagePayment'] ?? 0;
    rentalIncomeAchievableAmount = json['RentalIncomeAchievableAmount'] ?? 0;
    isThePropertyAlreadyTenanted = json['IsThePropertyAlreadyTenanted'] ?? '';
    tenancyType = json['TenancyType'] ?? '';
    areYouCurrentlyResidingInThisProperty =
        json['AreYouCurrentlyResidingInThisProperty'] ?? '';
    areYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty =
        json['AreYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty'] ??
            '';
    areYouAFirstTimeLandLord = json['AreYouAFirstTimeLandLord'] ?? '';
    haveYouBeenAnExperiencedLandlordForMoreThan12Months =
        json['HaveYouBeenAnExperiencedLandlordForMoreThan12Months'] ?? '';
    haveYouEverLivedInThisProperty =
        json['HaveYouEverLivedInThisProperty'] ?? '';
    areYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty =
        json['AreYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty'] ??
            '';
    propertyDescription = json['PropertyDescription'] ?? '';
    incomeFromTheProperty = json['IncomeFromTheProperty'] ?? '';
    whatIsYourLoanRepaymentStrategy =
        json['WhatIsYourLoanRepaymentStrategy'] ?? '';
    equityAmount = json['EquityAmount'] ?? 0;
    preferredRepaymentType = json['PreferredRepaymentType'] ?? '';
    serviceChargeAmount = json['ServiceChargeAmount'] ?? 0;
    groundRentAmount = json['GroundRentAmount'] ?? 0;
    clientExperience = json['ClientExperience'] ?? '';
    plan = json['Plan'] ?? '';
    costofRefurb = json['CostofRefurb'] ?? '';
    postCode = json['PostCode'] ?? '';
    propertyEPCRating = json['PropertyEPCRating'] ?? '';
    sharedHolderPercentage = json['SharedHolderPercentage'] ?? 0;
    doesThePropertyHaveALift = json['DoesThePropertyHaveALift'] ?? '';
    doesThisPropertyHaveWarranty = json['DoesThisPropertyHaveWarranty'] ?? '';
    warrantyType = json['WarrantyType'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['MortgageType'] = this.mortgageType;
    data['AddressOfPropertyToBeMortgaged'] =
        this.addressOfPropertyToBeMortgaged;
    data['PriceOfPropertyBeingPurchasedCurrentValuationOfProperty'] =
        this.priceOfPropertyBeingPurchasedCurrentValuationOfProperty;
    data['HowMuchDoYouWishToBorrow'] = this.howMuchDoYouWishToBorrow;
    data['AmountOfDepositEquity'] = this.amountOfDepositEquity;
    data['SourceOfDeposit'] = this.sourceOfDeposit;
    data['RemortgageOfAnotherProperty'] = this.remortgageOfAnotherProperty;
    data['Savings'] = this.savings;
    data['GiftFromFamilyMember'] = this.giftFromFamilyMember;
    data['OtherStateWhat'] = this.otherStateWhat;
    data['AreFundsAvailableToPayFeesInConnectionWithMortgage'] =
        this.areFundsAvailableToPayFeesInConnectionWithMortgage;
    data['DoesExistingLenderFacilitateFurtherAdvances'] =
        this.doesExistingLenderFacilitateFurtherAdvances;
    data['CurrentLenderAccountNumber'] = this.currentLenderAccountNumber;
    data['PropertyType'] = this.propertyType;
    data['PropertyTenure'] = this.propertyTenure;
    data['IfLeaseholdHowLongIsLeftOnTheLease'] =
        this.ifLeaseholdHowLongIsLeftOnTheLease;
    data['NumberOfBedrooms'] = this.numberOfBedrooms;
    data['FloorsInTheBuilding'] = this.floorsInTheBuilding;
    data['WhichFloorIsTheProperty'] = this.whichFloorIsTheProperty;
    data['YearPropertyWasBuilt'] = this.yearPropertyWasBuilt;
    data['AnyExtensionOrLoftConversionDone'] =
        this.anyExtensionOrLoftConversionDone;
    data['IsThePropertyOfANonStandardConstruction'] =
        this.isThePropertyOfANonStandardConstruction;
    data['IsItExCouncil'] = this.isItExCouncil;
    data['IsThisANewBuiltProperty'] = this.isThisANewBuiltProperty;
    data['HasTheVendorOwnedThePropertyOverSixMonths'] =
        this.hasTheVendorOwnedThePropertyOverSixMonths;
    data['AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods'] =
        this.areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods;
    data['IsThisAnAuctionOrRepossessionPurchase'] =
        this.isThisAnAuctionOrRepossessionPurchase;
    data['HasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub'] =
        this.hasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub;
    data['Notes'] = this.notes;
    data['Remarks'] = this.remarks;
    data['FinanceType'] = this.financeType;
    data['AuctionHouseLotDetails'] = this.auctionHouseLotDetails;
    data['RepaymentType'] = this.repaymentType;
    data['ExitStrategy'] = this.exitStrategy;
    data['Details'] = this.details;
    data['GVD'] = this.gVD;
    data['AreYouLooking'] = this.areYouLooking;
    data['BalanceOutstanding'] = this.balanceOutstanding;
    data['LTVAmount'] = this.lTVAmount;
    data['AmountofLoanRequired'] = this.amountofLoanRequired;
    data['TypeofLoan'] = this.typeofLoan;
    data['DoYouHaveAccountsForTheBusiness'] =
        this.doYouHaveAccountsForTheBusiness;
    data['DoYouHaveManagementAccounts'] = this.doYouHaveManagementAccounts;
    data['NonstandardConstructionDetails'] =
        this.nonstandardConstructionDetails;
    data['AreYouRaisingAnyMoneyfromAboveProperty'] =
        this.areYouRaisingAnyMoneyfromAboveProperty;
    data['AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue'] =
        this.areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue;
    data['AreFundsAvailableToPayFeesInConnectionWithMortgageValue'] =
        this.areFundsAvailableToPayFeesInConnectionWithMortgageValue;
    data['PreferredMortgageTermYear'] = this.preferredMortgageTermYear;
    data['PreferredMortgageTermMonth'] = this.preferredMortgageTermMonth;
    data['NumberOfKoichens'] = this.numberOfKoichens;
    data['NumberOfBaahroom'] = this.numberOfBaahroom;
    data['NumberOfWC'] = this.numberOfWC;
    data['IsTheFlatAboveShopRestaurant'] = this.isTheFlatAboveShopRestaurant;
    data['DoesTheFlatHaveDeckORBalconyAccess'] =
        this.doesTheFlatHaveDeckORBalconyAccess;
    data['AreYouLookingToRemortgageYourCurrentResidentialProperty'] =
        this.areYouLookingToRemortgageYourCurrentResidentialProperty;
    data['DidYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation'] =
        this.didYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation;
    data['AreThereAnyRightToBuyOrSimilarRestrictionImposed'] =
        this.areThereAnyRightToBuyOrSimilarRestrictionImposed;
    data['RestrictionExpireDate'] = this.restrictionExpireDate;
    data['AreYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation'] =
        this.areYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation;
    data['DoYouHaveExistingMortgageOnTheProperty'] =
        this.doYouHaveExistingMortgageOnTheProperty;
    data['CurrentLender'] = this.currentLender;
    data['DatePropertyPurchased'] = this.datePropertyPurchased;
    data['CurrentMortgageToBeRedeemed'] = this.currentMortgageToBeRedeemed;
    data['PurchasePrice'] = this.purchasePrice;
    data['AmountRaisingOverCurrentMortgage'] =
        this.amountRaisingOverCurrentMortgage;
    data['DoYouWantToApplyForInterestOnlyLoan'] =
        this.doYouWantToApplyForInterestOnlyLoan;
    data['MonthlyMortgagePayment'] = this.monthlyMortgagePayment;
    data['RentalIncomeAchievableAmount'] = this.rentalIncomeAchievableAmount;
    data['IsThePropertyAlreadyTenanted'] = this.isThePropertyAlreadyTenanted;
    data['TenancyType'] = this.tenancyType;
    data['AreYouCurrentlyResidingInThisProperty'] =
        this.areYouCurrentlyResidingInThisProperty;
    data['AreYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty'] =
        this.areYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty;
    data['AreYouAFirstTimeLandLord'] = this.areYouAFirstTimeLandLord;
    data['HaveYouBeenAnExperiencedLandlordForMoreThan12Months'] =
        this.haveYouBeenAnExperiencedLandlordForMoreThan12Months;
    data['HaveYouEverLivedInThisProperty'] =
        this.haveYouEverLivedInThisProperty;
    data['AreYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty'] =
        this.areYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty;
    data['PropertyDescription'] = this.propertyDescription;
    data['IncomeFromTheProperty'] = this.incomeFromTheProperty;
    data['WhatIsYourLoanRepaymentStrategy'] =
        this.whatIsYourLoanRepaymentStrategy;
    data['EquityAmount'] = this.equityAmount;
    data['PreferredRepaymentType'] = this.preferredRepaymentType;
    data['ServiceChargeAmount'] = this.serviceChargeAmount;
    data['GroundRentAmount'] = this.groundRentAmount;
    data['ClientExperience'] = this.clientExperience;
    data['Plan'] = this.plan;
    data['CostofRefurb'] = this.costofRefurb;
    data['PostCode'] = this.postCode;
    data['PropertyEPCRating'] = this.propertyEPCRating;
    data['SharedHolderPercentage'] = this.sharedHolderPercentage;
    data['DoesThePropertyHaveALift'] = this.doesThePropertyHaveALift;
    data['DoesThisPropertyHaveWarranty'] = this.doesThisPropertyHaveWarranty;
    data['WarrantyType'] = this.warrantyType;
    data['Id'] = this.id;
    return data;
  }
}
