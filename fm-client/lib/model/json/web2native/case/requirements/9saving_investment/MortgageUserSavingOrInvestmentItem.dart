class MortgageUserSavingOrInvestmentItem {
  int userId;
  int userCompanyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int taskId;
  String typeofAsset;
  String savingsType;
  String owners;
  String provider;
  String objective;
  double balance;
  String balanceDate;
  double interestRate;
  String maturityDate;
  String incomeTakenOrReinvested;
  String accountNumber;
  String areRegularDepositOrOrInvestmentBeingMade;
  double depositOrInvestmentAmount;
  String depositOrInvestmentFrequency;
  String willThisFormPartOfTheEstateOnDeath;
  String furtherDetails;
  String platform;
  double initialInvestmentAmount;
  String initialInvestmentStartDate;
  String isRegularIncomeCurrentlyBeingDrawn;
  double incomeCurrentlyBeingDrawnAmount;
  String incomeCurrentlyBeingDrawnFrequency;
  String willThisFormPartOfTheSpousesEstateOnDeath;
  String assetDescription;
  double valuationAmount;
  String valuationDate;
  String notes;
  String purchasePrice;
  String purchaseDate;
  String remarks;
  String doesTheClientHaveAnEmergencyFund;
  String doesTheClientHaveAnEmergencyFundDetails;
  String wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months;
  String wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails;
  String providerOtherName;
  int id;

  MortgageUserSavingOrInvestmentItem(
      {this.userId,
      this.userCompanyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.taskId,
      this.typeofAsset,
      this.savingsType,
      this.owners,
      this.provider,
      this.objective,
      this.balance,
      this.balanceDate,
      this.interestRate,
      this.maturityDate,
      this.incomeTakenOrReinvested,
      this.accountNumber,
      this.areRegularDepositOrOrInvestmentBeingMade,
      this.depositOrInvestmentAmount,
      this.depositOrInvestmentFrequency,
      this.willThisFormPartOfTheEstateOnDeath,
      this.furtherDetails,
      this.platform,
      this.initialInvestmentAmount,
      this.initialInvestmentStartDate,
      this.isRegularIncomeCurrentlyBeingDrawn,
      this.incomeCurrentlyBeingDrawnAmount,
      this.incomeCurrentlyBeingDrawnFrequency,
      this.willThisFormPartOfTheSpousesEstateOnDeath,
      this.assetDescription,
      this.valuationAmount,
      this.valuationDate,
      this.notes,
      this.purchasePrice,
      this.purchaseDate,
      this.remarks,
      this.doesTheClientHaveAnEmergencyFund,
      this.doesTheClientHaveAnEmergencyFundDetails,
      this.wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months,
      this.wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails,
      this.providerOtherName,
      this.id});

  MortgageUserSavingOrInvestmentItem.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    userCompanyId = json['UserCompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    typeofAsset = json['TypeofAsset'] ?? '';
    savingsType = json['SavingsType'] ?? '';
    owners = json['Owners'] ?? '';
    provider = json['Provider'] ?? '';
    objective = json['Objective'] ?? '';
    balance = json['Balance'] ?? 0;
    balanceDate = json['BalanceDate'] ?? '';
    interestRate = json['InterestRate'] ?? 0;
    maturityDate = json['MaturityDate'] ?? '';
    incomeTakenOrReinvested = json['IncomeTakenOrReinvested'] ?? '';
    accountNumber = json['AccountNumber'] ?? '';
    areRegularDepositOrOrInvestmentBeingMade =
        json['AreRegularDepositOrOrInvestmentBeingMade'] ?? '';
    depositOrInvestmentAmount = json['DepositOrInvestmentAmount'] ?? 0;
    depositOrInvestmentFrequency = json['DepositOrInvestmentFrequency'] ?? '';
    willThisFormPartOfTheEstateOnDeath =
        json['WillThisFormPartOfTheEstateOnDeath'] ?? '';
    furtherDetails = json['FurtherDetails'] ?? '';
    platform = json['Platform'] ?? '';
    initialInvestmentAmount = json['InitialInvestmentAmount'] ?? 0;
    initialInvestmentStartDate = json['InitialInvestmentStartDate'] ?? '';
    isRegularIncomeCurrentlyBeingDrawn =
        json['IsRegularIncomeCurrentlyBeingDrawn'] ?? '';
    incomeCurrentlyBeingDrawnAmount =
        json['IncomeCurrentlyBeingDrawnAmount'] ?? 0;
    incomeCurrentlyBeingDrawnFrequency =
        json['IncomeCurrentlyBeingDrawnFrequency'] ?? '';
    willThisFormPartOfTheSpousesEstateOnDeath =
        json['WillThisFormPartOfTheSpousesEstateOnDeath'] ?? '';
    assetDescription = json['AssetDescription'] ?? '';
    valuationAmount = json['ValuationAmount'] ?? 0;
    valuationDate = json['ValuationDate'] ?? '';
    notes = json['Notes'] ?? '';
    purchasePrice = json['PurchasePrice'] ?? '';
    purchaseDate = json['PurchaseDate'] ?? '';
    remarks = json['Remarks'] ?? '';
    doesTheClientHaveAnEmergencyFund =
        json['DoesTheClientHaveAnEmergencyFund'] ?? '';
    doesTheClientHaveAnEmergencyFundDetails =
        json['DoesTheClientHaveAnEmergencyFundDetails'] ?? '';
    wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months =
        json['WereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months'] ??
            '';
    wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails = json[
            'WereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails'] ??
        '';
    providerOtherName = json['ProviderOtherName'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['TaskId'] = this.taskId;
    data['TypeofAsset'] = this.typeofAsset;
    data['SavingsType'] = this.savingsType;
    data['Owners'] = this.owners;
    data['Provider'] = this.provider;
    data['Objective'] = this.objective;
    data['Balance'] = this.balance;
    data['BalanceDate'] = this.balanceDate;
    data['InterestRate'] = this.interestRate;
    data['MaturityDate'] = this.maturityDate;
    data['IncomeTakenOrReinvested'] = this.incomeTakenOrReinvested;
    data['AccountNumber'] = this.accountNumber;
    data['AreRegularDepositOrOrInvestmentBeingMade'] =
        this.areRegularDepositOrOrInvestmentBeingMade;
    data['DepositOrInvestmentAmount'] = this.depositOrInvestmentAmount;
    data['DepositOrInvestmentFrequency'] = this.depositOrInvestmentFrequency;
    data['WillThisFormPartOfTheEstateOnDeath'] =
        this.willThisFormPartOfTheEstateOnDeath;
    data['FurtherDetails'] = this.furtherDetails;
    data['Platform'] = this.platform;
    data['InitialInvestmentAmount'] = this.initialInvestmentAmount;
    data['InitialInvestmentStartDate'] = this.initialInvestmentStartDate;
    data['IsRegularIncomeCurrentlyBeingDrawn'] =
        this.isRegularIncomeCurrentlyBeingDrawn;
    data['IncomeCurrentlyBeingDrawnAmount'] =
        this.incomeCurrentlyBeingDrawnAmount;
    data['IncomeCurrentlyBeingDrawnFrequency'] =
        this.incomeCurrentlyBeingDrawnFrequency;
    data['WillThisFormPartOfTheSpousesEstateOnDeath'] =
        this.willThisFormPartOfTheSpousesEstateOnDeath;
    data['AssetDescription'] = this.assetDescription;
    data['ValuationAmount'] = this.valuationAmount;
    data['ValuationDate'] = this.valuationDate;
    data['Notes'] = this.notes;
    data['PurchasePrice'] = this.purchasePrice;
    data['PurchaseDate'] = this.purchaseDate;
    data['Remarks'] = this.remarks;
    data['DoesTheClientHaveAnEmergencyFund'] =
        this.doesTheClientHaveAnEmergencyFund;
    data['DoesTheClientHaveAnEmergencyFundDetails'] =
        this.doesTheClientHaveAnEmergencyFundDetails;
    data['WereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months'] =
        this.wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months;
    data['WereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails'] =
        this.wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails;
    data['ProviderOtherName'] = this.providerOtherName;
    data['Id'] = this.id;
    return data;
  }
}
