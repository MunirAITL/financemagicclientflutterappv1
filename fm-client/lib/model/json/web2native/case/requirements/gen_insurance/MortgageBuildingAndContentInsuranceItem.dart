class MortgageBuildingAndContentInsuranceItem {
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int userId;
  int companyId;
  int taskId;
  String type;
  double costToReplaceAmount;
  String itemDescription;
  String coverTyped;
  String remarks;
  int id;

  MortgageBuildingAndContentInsuranceItem(
      {this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.userId,
      this.companyId,
      this.taskId,
      this.type,
      this.costToReplaceAmount,
      this.itemDescription,
      this.coverTyped,
      this.remarks,
      this.id});

  MortgageBuildingAndContentInsuranceItem.fromJson(Map<String, dynamic> json) {
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    userId = json['UserId'] ?? 0;
    companyId = json['CompanyId'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    type = json['Type'] ?? '';
    costToReplaceAmount = json['CostToReplaceAmount'] ?? 0;
    itemDescription = json['ItemDescription'] ?? '';
    coverTyped = json['CoverTyped'] ?? '';
    remarks = json['Remarks'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['UserId'] = this.userId;
    data['CompanyId'] = this.companyId;
    data['TaskId'] = this.taskId;
    data['Type'] = this.type;
    data['CostToReplaceAmount'] = this.costToReplaceAmount;
    data['ItemDescription'] = this.itemDescription;
    data['CoverTyped'] = this.coverTyped;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
