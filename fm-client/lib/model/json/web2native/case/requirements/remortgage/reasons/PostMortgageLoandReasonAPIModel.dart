import 'package:aitl/model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';

class PostMortgageLoandReasonAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostMortgageLoandReasonAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostMortgageLoandReasonAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageLoanReasonsModel mortgageLoanReason;
  ResponseData({this.mortgageLoanReason});
  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageLoanReason = json['MortgageLoanReason'] != null
        ? new MortgageLoanReasonsModel.fromJson(json['MortgageLoanReason'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageLoanReason != null) {
      data['MortgageLoanReason'] = this.mortgageLoanReason.toJson();
    }
    return data;
  }
}
