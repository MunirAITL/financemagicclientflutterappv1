class MortgageLoanReasonsModel {
  int userId;
  dynamic user;
  int status;
  String creationDate;
  int taskId;
  String loanReasonName;
  double amount;
  String remarks;
  int id;

  MortgageLoanReasonsModel(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.taskId,
      this.loanReasonName,
      this.amount,
      this.remarks,
      this.id});

  MortgageLoanReasonsModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    taskId = json['TaskId'];
    loanReasonName = json['LoanReasonName'];
    amount = json['Amount'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['LoanReasonName'] = this.loanReasonName;
    data['Amount'] = this.amount;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
