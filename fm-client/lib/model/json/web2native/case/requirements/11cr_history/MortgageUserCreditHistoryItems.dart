class MortgageUserCreditHistoryItems {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  int mortgageUserCreditHistoryId;
  String type;
  String dateofRegistered;
  double amount;
  String isThisSettled;
  String dateofSettled;
  String reasonWhyThisHasHappended;
  String remarks;
  String missedMortgagePaymentInLast12Months;
  String missedMortgagePaymentInLast24Months;
  String missedMortgagePaymentInLast36Months;
  String missedUnsecuredCreditPaymentInLast12Months;
  String paydayLoansInLast12Month;
  int entityId;
  String entityName;
  int id;

  MortgageUserCreditHistoryItems(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.mortgageUserCreditHistoryId,
      this.type,
      this.dateofRegistered,
      this.amount,
      this.isThisSettled,
      this.dateofSettled,
      this.reasonWhyThisHasHappended,
      this.remarks,
      this.missedMortgagePaymentInLast12Months,
      this.missedMortgagePaymentInLast24Months,
      this.missedMortgagePaymentInLast36Months,
      this.missedUnsecuredCreditPaymentInLast12Months,
      this.paydayLoansInLast12Month,
      this.entityId,
      this.entityName,
      this.id});

  MortgageUserCreditHistoryItems.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'] ?? 0;
    mortgageUserCreditHistoryId = json['MortgageUserCreditHistoryId'] ?? 0;
    type = json['Type'] ?? '';
    dateofRegistered = json['DateofRegistered'] ?? '';
    amount = json['Amount'] ?? 0;
    isThisSettled = json['IsThisSettled'] ?? '';
    dateofSettled = json['DateofSettled'] ?? '';
    reasonWhyThisHasHappended = json['ReasonWhyThisHasHappended'] ?? '';
    remarks = json['Remarks'] ?? '';
    missedMortgagePaymentInLast12Months =
        json['MissedMortgagePaymentInLast12Months'] ?? '';
    missedMortgagePaymentInLast24Months =
        json['MissedMortgagePaymentInLast24Months'] ?? '';
    missedMortgagePaymentInLast36Months =
        json['MissedMortgagePaymentInLast36Months'] ?? '';
    missedUnsecuredCreditPaymentInLast12Months =
        json['MissedUnsecuredCreditPaymentInLast12Months'] ?? '';
    paydayLoansInLast12Month = json['PaydayLoansInLast12Month'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['MortgageUserCreditHistoryId'] = this.mortgageUserCreditHistoryId;
    data['Type'] = this.type;
    data['DateofRegistered'] = this.dateofRegistered;
    data['Amount'] = this.amount;
    data['IsThisSettled'] = this.isThisSettled;
    data['DateofSettled'] = this.dateofSettled;
    data['ReasonWhyThisHasHappended'] = this.reasonWhyThisHasHappended;
    data['Remarks'] = this.remarks;
    data['MissedMortgagePaymentInLast12Months'] =
        this.missedMortgagePaymentInLast12Months;
    data['MissedMortgagePaymentInLast24Months'] =
        this.missedMortgagePaymentInLast24Months;
    data['MissedMortgagePaymentInLast36Months'] =
        this.missedMortgagePaymentInLast36Months;
    data['MissedUnsecuredCreditPaymentInLast12Months'] =
        this.missedUnsecuredCreditPaymentInLast12Months;
    data['PaydayLoansInLast12Month'] = this.paydayLoansInLast12Month;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['Id'] = this.id;
    return data;
  }
}
