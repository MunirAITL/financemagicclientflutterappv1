import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistory.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';

class GetCreditHisItemsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetCreditHisItemsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetCreditHisItemsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserCreditHistoryItems> mortgageUserCreditHistoryItems;
  ResponseData({this.mortgageUserCreditHistoryItems});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserCreditHistoryItems'] != null) {
      mortgageUserCreditHistoryItems = <MortgageUserCreditHistoryItems>[];
      json['MortgageUserCreditHistoryItems'].forEach((v) {
        mortgageUserCreditHistoryItems
            .add(new MortgageUserCreditHistoryItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserCreditHistoryItems != null) {
      data['MortgageUserCreditHistoryItems'] =
          this.mortgageUserCreditHistoryItems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
