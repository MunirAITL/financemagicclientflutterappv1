class MortgageUserExistingPolicyItem {
  int userId;
  int userCompanyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int taskId;
  String policyType;
  String lifePolicyType;
  String provider;
  String purpose;
  String policyNumber;
  double premium;
  String frequency;
  double coverAmount;
  String cICOrSICSumInsured;
  String cICOrSICBenefitType;
  String policyStartDate;
  String termBasis;
  String guaranteed;
  String renewable;
  String isThePolicyIndexed;
  String doesThePolicyIncludeWaiverOfPremium;
  String isThePolicyOnTrust;
  String isThePolicyUsedWithAMortgage;
  String policyToBeCancelledOrReplaced;
  String needForCover;
  String otherApplicableBenefits;
  String notes;
  String initialbenefit;
  String initialDeferredPeriod;
  String additionalBenefit;
  String additionalDeferredPeriod;
  String propertyAddress;
  String noClaimsBonus;
  String accidentalDamage;
  String arePersonalPossessionsIncluded;
  String isCycleCoverIncluded;
  String areSpecificItemsIncluded;
  String isThePolicyToBeCancelledOrReplaced;
  String policyExpiryDate;
  String anyPoliciesLapsedOrCancelledInTheLast12Months;
  String details;
  String providerOtherName;
  int id;

  MortgageUserExistingPolicyItem({
    this.userId,
    this.userCompanyId,
    this.status,
    this.mortgageCaseInfoId,
    this.creationDate,
    this.updatedDate,
    this.versionNumber,
    this.taskId,
    this.policyType,
    this.lifePolicyType,
    this.provider,
    this.purpose,
    this.policyNumber,
    this.premium,
    this.frequency,
    this.coverAmount,
    this.cICOrSICSumInsured,
    this.cICOrSICBenefitType,
    this.policyStartDate,
    this.termBasis,
    this.guaranteed,
    this.renewable,
    this.isThePolicyIndexed,
    this.doesThePolicyIncludeWaiverOfPremium,
    this.isThePolicyOnTrust,
    this.isThePolicyUsedWithAMortgage,
    this.policyToBeCancelledOrReplaced,
    this.needForCover,
    this.otherApplicableBenefits,
    this.notes,
    this.initialbenefit,
    this.initialDeferredPeriod,
    this.additionalBenefit,
    this.additionalDeferredPeriod,
    this.propertyAddress,
    this.noClaimsBonus,
    this.accidentalDamage,
    this.arePersonalPossessionsIncluded,
    this.isCycleCoverIncluded,
    this.areSpecificItemsIncluded,
    this.isThePolicyToBeCancelledOrReplaced,
    this.policyExpiryDate,
    this.anyPoliciesLapsedOrCancelledInTheLast12Months,
    this.details,
    this.providerOtherName,
    this.id,
  });

  MortgageUserExistingPolicyItem.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    userCompanyId = json['UserCompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    policyType = json['PolicyType'] ?? '';
    lifePolicyType = json['LifePolicyType'] ?? '';
    provider = json['Provider'] ?? '';
    purpose = json['Purpose'] ?? '';
    policyNumber = json['PolicyNumber'] ?? '';
    premium = json['Premium'] ?? 0;
    frequency = json['Frequency'] ?? '';
    coverAmount = json['CoverAmount'] ?? 0;
    cICOrSICSumInsured = json['CICOrSICSumInsured'] ?? '';
    cICOrSICBenefitType = json['CICOrSICBenefitType'] ?? '';
    policyStartDate = json['PolicyStartDate'] ?? '';
    termBasis = json['TermBasis'] ?? '';
    guaranteed = json['Guaranteed'] ?? '';
    renewable = json['Renewable'] ?? '';
    isThePolicyIndexed = json['IsThePolicyIndexed'] ?? '';
    doesThePolicyIncludeWaiverOfPremium =
        json['DoesThePolicyIncludeWaiverOfPremium'] ?? '';
    isThePolicyOnTrust = json['IsThePolicyOnTrust'] ?? '';
    isThePolicyUsedWithAMortgage = json['IsThePolicyUsedWithAMortgage'] ?? '';
    policyToBeCancelledOrReplaced = json['PolicyToBeCancelledOrReplaced'] ?? '';
    needForCover = json['NeedForCover'] ?? '';
    otherApplicableBenefits = json['OtherApplicableBenefits'] ?? '';
    notes = json['Notes'] ?? '';
    initialbenefit = json['Initialbenefit'] ?? '';
    initialDeferredPeriod = json['InitialDeferredPeriod'] ?? '';
    additionalBenefit = json['AdditionalBenefit'] ?? '';
    additionalDeferredPeriod = json['AdditionalDeferredPeriod'] ?? '';
    propertyAddress = json['PropertyAddress'] ?? '';
    noClaimsBonus = json['NoClaimsBonus'] ?? '';
    accidentalDamage = json['AccidentalDamage'] ?? '';
    arePersonalPossessionsIncluded =
        json['ArePersonalPossessionsIncluded'] ?? '';
    isCycleCoverIncluded = json['IsCycleCoverIncluded'] ?? '';
    areSpecificItemsIncluded = json['AreSpecificItemsIncluded'] ?? '';
    isThePolicyToBeCancelledOrReplaced =
        json['IsThePolicyToBeCancelledOrReplaced'] ?? '';
    policyExpiryDate = json['PolicyExpiryDate'] ?? '';
    anyPoliciesLapsedOrCancelledInTheLast12Months =
        json['AnyPoliciesLapsedOrCancelledInTheLast12Months'] ?? '';
    details = json['Details'] ?? '';
    providerOtherName = json['ProviderOtherName'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['TaskId'] = this.taskId;
    data['PolicyType'] = this.policyType;
    data['LifePolicyType'] = this.lifePolicyType;
    data['Provider'] = this.provider;
    data['Purpose'] = this.purpose;
    data['PolicyNumber'] = this.policyNumber;
    data['Premium'] = this.premium;
    data['Frequency'] = this.frequency;
    data['CoverAmount'] = this.coverAmount;
    data['CICOrSICSumInsured'] = this.cICOrSICSumInsured;
    data['CICOrSICBenefitType'] = this.cICOrSICBenefitType;
    data['PolicyStartDate'] = this.policyStartDate;
    data['TermBasis'] = this.termBasis;
    data['Guaranteed'] = this.guaranteed;
    data['Renewable'] = this.renewable;
    data['IsThePolicyIndexed'] = this.isThePolicyIndexed;
    data['DoesThePolicyIncludeWaiverOfPremium'] =
        this.doesThePolicyIncludeWaiverOfPremium;
    data['IsThePolicyOnTrust'] = this.isThePolicyOnTrust;
    data['IsThePolicyUsedWithAMortgage'] = this.isThePolicyUsedWithAMortgage;
    data['PolicyToBeCancelledOrReplaced'] = this.policyToBeCancelledOrReplaced;
    data['NeedForCover'] = this.needForCover;
    data['OtherApplicableBenefits'] = this.otherApplicableBenefits;
    data['Notes'] = this.notes;
    data['Initialbenefit'] = this.initialbenefit;
    data['InitialDeferredPeriod'] = this.initialDeferredPeriod;
    data['AdditionalBenefit'] = this.additionalBenefit;
    data['AdditionalDeferredPeriod'] = this.additionalDeferredPeriod;
    data['PropertyAddress'] = this.propertyAddress;
    data['NoClaimsBonus'] = this.noClaimsBonus;
    data['AccidentalDamage'] = this.accidentalDamage;
    data['ArePersonalPossessionsIncluded'] =
        this.arePersonalPossessionsIncluded;
    data['IsCycleCoverIncluded'] = this.isCycleCoverIncluded;
    data['AreSpecificItemsIncluded'] = this.areSpecificItemsIncluded;
    data['IsThePolicyToBeCancelledOrReplaced'] =
        this.isThePolicyToBeCancelledOrReplaced;
    data['PolicyExpiryDate'] = this.policyExpiryDate;
    data['AnyPoliciesLapsedOrCancelledInTheLast12Months'] =
        this.anyPoliciesLapsedOrCancelledInTheLast12Months;
    data['Details'] = this.details;
    data['ProviderOtherName'] = this.providerOtherName;
    data['Id'] = this.id;
    return data;
  }
}
