class MortgageUserOtherContactInfos {
  int userId;
  dynamic user;
  int contactId;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  String contactName;
  String companyName;
  String address;
  String postcode;
  String phone;
  String email;
  String agentType;
  String remarks;
  int id;

  MortgageUserOtherContactInfos(
      {this.userId,
      this.user,
      this.contactId,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.contactName,
      this.companyName,
      this.address,
      this.postcode,
      this.phone,
      this.email,
      this.agentType,
      this.remarks,
      this.id});

  MortgageUserOtherContactInfos.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    contactId = json['ContactId'] ?? 0;
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    contactName = json['ContactName'] ?? '';
    companyName = json['CompanyName'] ?? '';
    address = json['Address'] ?? '';
    postcode = json['Postcode'] ?? '';
    phone = json['Phone'] ?? '';
    email = json['Email'] ?? '';
    agentType = json['AgentType'] ?? '';
    remarks = json['Remarks'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['ContactId'] = this.contactId;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['ContactName'] = this.contactName;
    data['CompanyName'] = this.companyName;
    data['Address'] = this.address;
    data['Postcode'] = this.postcode;
    data['Phone'] = this.phone;
    data['Email'] = this.email;
    data['AgentType'] = this.agentType;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
