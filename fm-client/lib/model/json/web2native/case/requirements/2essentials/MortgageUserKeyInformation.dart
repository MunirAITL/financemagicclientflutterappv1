class MortgageUserKeyInformation {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  String isIncome;
  String approximateTimescaleAmountReasonIncome;
  String isExpenditure;
  String approximateTimescaleAmountReasonExpenditure;
  String doYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture;
  double approximateAmount;
  String approximatetimescaleAmountReasonForeseeableFuture;
  String
      areYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction;
  String isLargerThanApproximateAmount;
  String isSmallerThanApproximateAmount;
  String
      approximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction;
  String mortgageRequirements;
  String anUpperLimitOnYourMortgageCostsForASpecificPeriod;
  String reasonAndForHowLongAnUpperLimit;
  String toFixYourMortgageCostsForASpecificPeriod;
  String reasonAndForHowLongToFixYourMortgageCosts;
  String aRateLinkedToTheBankOfEnglandBaseRate;
  String reasonAndForHowLongARateLinkedToTheBank;
  String aDiscountOnYourMortgageRepaymentsInTheEarlyYears;
  String reasonAndForHowLongADiscountOnYourMortgageRepayments;
  String accessToAnInitialCashSum;
  String accessToAnInitialCashSumReasonAndForHowLong;
  String noEarlyRepaymentChargeOnYourMortgageAtAnyPoint;
  String noEarlyRepaymentChargeOverhangAfterSelectedRateEnds;
  String noHighLendingCharge;
  String speedOfMortgageCompletion;
  String abilityToAddFeesToTheLoan;
  String abilityToTakeRepaymentHolidays;
  String abilityToMakeUnderpaymentsOrOverpayments;
  String freeLegalFees;
  String novaluationFee;
  String haveValuationsFeesRefunded;
  String notes;
  bool isAcceptTermsAndCondition;
  String wouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability;
  String wHATMORTGAGEFEATURESDOYOUREQUIRE;
  String remarks;
  String mortgageFeatureDetails;
  int id;

  MortgageUserKeyInformation(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.isIncome,
      this.approximateTimescaleAmountReasonIncome,
      this.isExpenditure,
      this.approximateTimescaleAmountReasonExpenditure,
      this.doYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture,
      this.approximateAmount,
      this.approximatetimescaleAmountReasonForeseeableFuture,
      this.areYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction,
      this.isLargerThanApproximateAmount,
      this.isSmallerThanApproximateAmount,
      this.approximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction,
      this.mortgageRequirements,
      this.anUpperLimitOnYourMortgageCostsForASpecificPeriod,
      this.reasonAndForHowLongAnUpperLimit,
      this.toFixYourMortgageCostsForASpecificPeriod,
      this.reasonAndForHowLongToFixYourMortgageCosts,
      this.aRateLinkedToTheBankOfEnglandBaseRate,
      this.reasonAndForHowLongARateLinkedToTheBank,
      this.aDiscountOnYourMortgageRepaymentsInTheEarlyYears,
      this.reasonAndForHowLongADiscountOnYourMortgageRepayments,
      this.accessToAnInitialCashSum,
      this.accessToAnInitialCashSumReasonAndForHowLong,
      this.noEarlyRepaymentChargeOnYourMortgageAtAnyPoint,
      this.noEarlyRepaymentChargeOverhangAfterSelectedRateEnds,
      this.noHighLendingCharge,
      this.speedOfMortgageCompletion,
      this.abilityToAddFeesToTheLoan,
      this.abilityToTakeRepaymentHolidays,
      this.abilityToMakeUnderpaymentsOrOverpayments,
      this.freeLegalFees,
      this.novaluationFee,
      this.haveValuationsFeesRefunded,
      this.notes,
      this.isAcceptTermsAndCondition,
      this.wouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability,
      this.wHATMORTGAGEFEATURESDOYOUREQUIRE,
      this.remarks,
      this.mortgageFeatureDetails,
      this.id});

  MortgageUserKeyInformation.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'];
    status = json['Status'];
    mortgageCaseInfoId = json['MortgageCaseInfoId'];
    creationDate = json['CreationDate'];
    taskId = json['TaskId'];
    isIncome = json['IsIncome'];
    approximateTimescaleAmountReasonIncome =
        json['ApproximateTimescaleAmountReasonIncome'];
    isExpenditure = json['IsExpenditure'];
    approximateTimescaleAmountReasonExpenditure =
        json['ApproximateTimescaleAmountReasonExpenditure'];
    doYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture = json[
        'DoYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture'];
    approximateAmount = json['ApproximateAmount'];
    approximatetimescaleAmountReasonForeseeableFuture =
        json['ApproximatetimescaleAmountReasonForeseeableFuture'];
    areYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction = json[
        'AreYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction'];
    isLargerThanApproximateAmount = json['IsLargerThanApproximateAmount'];
    isSmallerThanApproximateAmount = json['IsSmallerThanApproximateAmount'];
    approximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction =
        json[
            'ApproximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction'];
    mortgageRequirements = json['MortgageRequirements'];
    anUpperLimitOnYourMortgageCostsForASpecificPeriod =
        json['AnUpperLimitOnYourMortgageCostsForASpecificPeriod'];
    reasonAndForHowLongAnUpperLimit = json['ReasonAndForHowLongAnUpperLimit'];
    toFixYourMortgageCostsForASpecificPeriod =
        json['ToFixYourMortgageCostsForASpecificPeriod'];
    reasonAndForHowLongToFixYourMortgageCosts =
        json['ReasonAndForHowLongToFixYourMortgageCosts'];
    aRateLinkedToTheBankOfEnglandBaseRate =
        json['ARateLinkedToTheBankOfEnglandBaseRate'];
    reasonAndForHowLongARateLinkedToTheBank =
        json['ReasonAndForHowLongARateLinkedToTheBank'];
    aDiscountOnYourMortgageRepaymentsInTheEarlyYears =
        json['ADiscountOnYourMortgageRepaymentsInTheEarlyYears'];
    reasonAndForHowLongADiscountOnYourMortgageRepayments =
        json['ReasonAndForHowLongADiscountOnYourMortgageRepayments'];
    accessToAnInitialCashSum = json['AccessToAnInitialCashSum'];
    accessToAnInitialCashSumReasonAndForHowLong =
        json['AccessToAnInitialCashSumReasonAndForHowLong'];
    noEarlyRepaymentChargeOnYourMortgageAtAnyPoint =
        json['NoEarlyRepaymentChargeOnYourMortgageAtAnyPoint'];
    noEarlyRepaymentChargeOverhangAfterSelectedRateEnds =
        json['NoEarlyRepaymentChargeOverhangAfterSelectedRateEnds'];
    noHighLendingCharge = json['NoHighLendingCharge'];
    speedOfMortgageCompletion = json['SpeedOfMortgageCompletion'];
    abilityToAddFeesToTheLoan = json['AbilityToAddFeesToTheLoan'];
    abilityToTakeRepaymentHolidays = json['AbilityToTakeRepaymentHolidays'];
    abilityToMakeUnderpaymentsOrOverpayments =
        json['AbilityToMakeUnderpaymentsOrOverpayments'];
    freeLegalFees = json['FreeLegalFees'];
    novaluationFee = json['NovaluationFee'];
    haveValuationsFeesRefunded = json['HaveValuationsFeesRefunded'];
    notes = json['Notes'];
    isAcceptTermsAndCondition = json['IsAcceptTermsAndCondition'];
    wouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability = json[
        'WouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability'];
    wHATMORTGAGEFEATURESDOYOUREQUIRE = json['WHATMORTGAGEFEATURESDOYOUREQUIRE'];
    remarks = json['Remarks'];
    mortgageFeatureDetails = json['MortgageFeatureDetails'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['IsIncome'] = this.isIncome;
    data['ApproximateTimescaleAmountReasonIncome'] =
        this.approximateTimescaleAmountReasonIncome;
    data['IsExpenditure'] = this.isExpenditure;
    data['ApproximateTimescaleAmountReasonExpenditure'] =
        this.approximateTimescaleAmountReasonExpenditure;
    data['DoYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture'] =
        this.doYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture;
    data['ApproximateAmount'] = this.approximateAmount;
    data['ApproximatetimescaleAmountReasonForeseeableFuture'] =
        this.approximatetimescaleAmountReasonForeseeableFuture;
    data['AreYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction'] =
        this.areYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction;
    data['IsLargerThanApproximateAmount'] = this.isLargerThanApproximateAmount;
    data['IsSmallerThanApproximateAmount'] =
        this.isSmallerThanApproximateAmount;
    data['ApproximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction'] =
        this.approximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction;
    data['MortgageRequirements'] = this.mortgageRequirements;
    data['AnUpperLimitOnYourMortgageCostsForASpecificPeriod'] =
        this.anUpperLimitOnYourMortgageCostsForASpecificPeriod;
    data['ReasonAndForHowLongAnUpperLimit'] =
        this.reasonAndForHowLongAnUpperLimit;
    data['ToFixYourMortgageCostsForASpecificPeriod'] =
        this.toFixYourMortgageCostsForASpecificPeriod;
    data['ReasonAndForHowLongToFixYourMortgageCosts'] =
        this.reasonAndForHowLongToFixYourMortgageCosts;
    data['ARateLinkedToTheBankOfEnglandBaseRate'] =
        this.aRateLinkedToTheBankOfEnglandBaseRate;
    data['ReasonAndForHowLongARateLinkedToTheBank'] =
        this.reasonAndForHowLongARateLinkedToTheBank;
    data['ADiscountOnYourMortgageRepaymentsInTheEarlyYears'] =
        this.aDiscountOnYourMortgageRepaymentsInTheEarlyYears;
    data['ReasonAndForHowLongADiscountOnYourMortgageRepayments'] =
        this.reasonAndForHowLongADiscountOnYourMortgageRepayments;
    data['AccessToAnInitialCashSum'] = this.accessToAnInitialCashSum;
    data['AccessToAnInitialCashSumReasonAndForHowLong'] =
        this.accessToAnInitialCashSumReasonAndForHowLong;
    data['NoEarlyRepaymentChargeOnYourMortgageAtAnyPoint'] =
        this.noEarlyRepaymentChargeOnYourMortgageAtAnyPoint;
    data['NoEarlyRepaymentChargeOverhangAfterSelectedRateEnds'] =
        this.noEarlyRepaymentChargeOverhangAfterSelectedRateEnds;
    data['NoHighLendingCharge'] = this.noHighLendingCharge;
    data['SpeedOfMortgageCompletion'] = this.speedOfMortgageCompletion;
    data['AbilityToAddFeesToTheLoan'] = this.abilityToAddFeesToTheLoan;
    data['AbilityToTakeRepaymentHolidays'] =
        this.abilityToTakeRepaymentHolidays;
    data['AbilityToMakeUnderpaymentsOrOverpayments'] =
        this.abilityToMakeUnderpaymentsOrOverpayments;
    data['FreeLegalFees'] = this.freeLegalFees;
    data['NovaluationFee'] = this.novaluationFee;
    data['HaveValuationsFeesRefunded'] = this.haveValuationsFeesRefunded;
    data['Notes'] = this.notes;
    data['IsAcceptTermsAndCondition'] = this.isAcceptTermsAndCondition;
    data['WouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability'] =
        this.wouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability;
    data['WHATMORTGAGEFEATURESDOYOUREQUIRE'] =
        this.wHATMORTGAGEFEATURESDOYOUREQUIRE;
    data['Remarks'] = this.remarks;
    data['MortgageFeatureDetails'] = this.mortgageFeatureDetails;
    data['Id'] = this.id;
    return data;
  }
}
