import 'MortgageUserBankDetailList.dart';

class GetESBnkAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetESBnkAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetESBnkAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserBankDetailList> mortgageUserBankDetailList;
  ResponseData({this.mortgageUserBankDetailList});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserBankDetailList'] != null) {
      mortgageUserBankDetailList = <MortgageUserBankDetailList>[];
      json['MortgageUserBankDetailList'].forEach((v) {
        mortgageUserBankDetailList
            .add(new MortgageUserBankDetailList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserBankDetailList != null) {
      data['MortgageUserBankDetailList'] =
          this.mortgageUserBankDetailList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
