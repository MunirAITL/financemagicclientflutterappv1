import 'MortgageUserOtherContactInfos.dart';

class GetContactAutoSugAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetContactAutoSugAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetContactAutoSugAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserOtherContactInfos> mortgageUserOtherContactInfos;

  ResponseData({this.mortgageUserOtherContactInfos});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserOtherContactInfos'] != null) {
      mortgageUserOtherContactInfos = <MortgageUserOtherContactInfos>[];
      json['MortgageUserOtherContactInfos'].forEach((v) {
        mortgageUserOtherContactInfos
            .add(new MortgageUserOtherContactInfos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserOtherContactInfos != null) {
      data['MortgageUserOtherContactInfos'] =
          this.mortgageUserOtherContactInfos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
