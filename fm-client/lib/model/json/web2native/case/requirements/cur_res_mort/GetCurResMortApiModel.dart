import 'MortgageUserCurrentResidentialMortgage.dart';

class GetCurResMortApiModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetCurResMortApiModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetCurResMortApiModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageUserCurrentResidentialMortgage mortgageUserCurrentResidentialMortgage;
  ResponseData({this.mortgageUserCurrentResidentialMortgage});
  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserCurrentResidentialMortgage =
        json['MortgageUserCurrentResidentialMortgage'] != null
            ? new MortgageUserCurrentResidentialMortgage.fromJson(
                json['MortgageUserCurrentResidentialMortgage'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserCurrentResidentialMortgage != null) {
      data['MortgageUserCurrentResidentialMortgage'] =
          this.mortgageUserCurrentResidentialMortgage.toJson();
    }
    return data;
  }
}
