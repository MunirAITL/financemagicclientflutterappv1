class MortgageUserCurrentResidentialMortgage {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  double amountOutstanding;
  String termOutstanding;
  String repaymentType;
  String lender;
  String accountNumber;
  double currentMonthlyPaymentAndInterestRate;
  String interestRateType;
  String endDateForInterestRateType;
  String areThereAnyEarlyRepaymentChargesPayable;
  double ifEarlyRepaymentChargesArePayableStateFigure;
  String areYouPreparedToPayTheEarlyRepaymentCharges;
  String isTheCurrentMortgagePortableToANewProperty;
  double ifTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice;
  String isTheCurrentMortgageRepaidInTheEventOfDeath;
  String isTheCurrentMortgageRepaidInTheEventOfCriticalIllness;
  String
      isTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy;
  String isBuildingsAndContentsInsuranceInPlace;
  String remarks;
  double purchasePrice;
  double currentValueOfTheResidentialProperty;
  String landlordName;
  String contactNumber;
  String address;
  String emailAddress;
  String purchaseDate;
  String occupantType;
  int id;

  MortgageUserCurrentResidentialMortgage(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.amountOutstanding,
      this.termOutstanding,
      this.repaymentType,
      this.lender,
      this.accountNumber,
      this.currentMonthlyPaymentAndInterestRate,
      this.interestRateType,
      this.endDateForInterestRateType,
      this.areThereAnyEarlyRepaymentChargesPayable,
      this.ifEarlyRepaymentChargesArePayableStateFigure,
      this.areYouPreparedToPayTheEarlyRepaymentCharges,
      this.isTheCurrentMortgagePortableToANewProperty,
      this.ifTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice,
      this.isTheCurrentMortgageRepaidInTheEventOfDeath,
      this.isTheCurrentMortgageRepaidInTheEventOfCriticalIllness,
      this.isTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy,
      this.isBuildingsAndContentsInsuranceInPlace,
      this.remarks,
      this.purchasePrice,
      this.currentValueOfTheResidentialProperty,
      this.landlordName,
      this.contactNumber,
      this.address,
      this.emailAddress,
      this.purchaseDate,
      this.occupantType,
      this.id});

  MortgageUserCurrentResidentialMortgage.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'];
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    amountOutstanding = json['AmountOutstanding'] ?? 0;
    termOutstanding = json['TermOutstanding'] ?? '';
    repaymentType = json['RepaymentType'] ?? '';
    lender = json['Lender'] ?? '';
    accountNumber = json['AccountNumber'] ?? '';
    currentMonthlyPaymentAndInterestRate =
        json['CurrentMonthlyPaymentAndInterestRate'] ?? 0;
    interestRateType = json['InterestRateType'] ?? '';
    endDateForInterestRateType = json['EndDateForInterestRateType'] ?? '';
    areThereAnyEarlyRepaymentChargesPayable =
        json['AreThereAnyEarlyRepaymentChargesPayable'] ?? '';
    ifEarlyRepaymentChargesArePayableStateFigure =
        json['IfEarlyRepaymentChargesArePayableStateFigure'] ?? 0;
    areYouPreparedToPayTheEarlyRepaymentCharges =
        json['AreYouPreparedToPayTheEarlyRepaymentCharges'] ?? '';
    isTheCurrentMortgagePortableToANewProperty =
        json['IsTheCurrentMortgagePortableToANewProperty'] ?? '';
    ifTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice =
        json['IfTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice'] ?? 0;
    isTheCurrentMortgageRepaidInTheEventOfDeath =
        json['IsTheCurrentMortgageRepaidInTheEventOfDeath'] ?? '';
    isTheCurrentMortgageRepaidInTheEventOfCriticalIllness =
        json['IsTheCurrentMortgageRepaidInTheEventOfCriticalIllness'] ?? '';
    isTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy =
        json['IsTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy'] ??
            '';
    isBuildingsAndContentsInsuranceInPlace =
        json['IsBuildingsAndContentsInsuranceInPlace'] ?? '';
    remarks = json['Remarks'] ?? '';
    purchasePrice = json['PurchasePrice'] ?? 0;
    currentValueOfTheResidentialProperty =
        json['CurrentValueOfTheResidentialProperty'] ?? 0;
    landlordName = json['LandlordName'] ?? '';
    contactNumber = json['ContactNumber'] ?? '';
    address = json['Address'] ?? '';
    emailAddress = json['EmailAddress'] ?? '';
    purchaseDate = json['PurchaseDate'] ?? '';
    occupantType = json['OccupantType'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['AmountOutstanding'] = this.amountOutstanding;
    data['TermOutstanding'] = this.termOutstanding;
    data['RepaymentType'] = this.repaymentType;
    data['Lender'] = this.lender;
    data['AccountNumber'] = this.accountNumber;
    data['CurrentMonthlyPaymentAndInterestRate'] =
        this.currentMonthlyPaymentAndInterestRate;
    data['InterestRateType'] = this.interestRateType;
    data['EndDateForInterestRateType'] = this.endDateForInterestRateType;
    data['AreThereAnyEarlyRepaymentChargesPayable'] =
        this.areThereAnyEarlyRepaymentChargesPayable;
    data['IfEarlyRepaymentChargesArePayableStateFigure'] =
        this.ifEarlyRepaymentChargesArePayableStateFigure;
    data['AreYouPreparedToPayTheEarlyRepaymentCharges'] =
        this.areYouPreparedToPayTheEarlyRepaymentCharges;
    data['IsTheCurrentMortgagePortableToANewProperty'] =
        this.isTheCurrentMortgagePortableToANewProperty;
    data['IfTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice'] =
        this.ifTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice;
    data['IsTheCurrentMortgageRepaidInTheEventOfDeath'] =
        this.isTheCurrentMortgageRepaidInTheEventOfDeath;
    data['IsTheCurrentMortgageRepaidInTheEventOfCriticalIllness'] =
        this.isTheCurrentMortgageRepaidInTheEventOfCriticalIllness;
    data['IsTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy'] =
        this.isTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy;
    data['IsBuildingsAndContentsInsuranceInPlace'] =
        this.isBuildingsAndContentsInsuranceInPlace;
    data['Remarks'] = this.remarks;
    data['PurchasePrice'] = this.purchasePrice;
    data['CurrentValueOfTheResidentialProperty'] =
        this.currentValueOfTheResidentialProperty;
    data['LandlordName'] = this.landlordName;
    data['ContactNumber'] = this.contactNumber;
    data['Address'] = this.address;
    data['EmailAddress'] = this.emailAddress;
    data['PurchaseDate'] = this.purchaseDate;
    data['OccupantType'] = this.occupantType;
    data['Id'] = this.id;
    return data;
  }
}
