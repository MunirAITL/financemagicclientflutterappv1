class UserAddresss {
  int userId;
  dynamic user;
  int status;
  String creationDate;
  int taskId;
  String addressLine1;
  String addressLine2;
  String addressLine3;
  String town;
  String county;
  String postcode;
  String telNumber;
  String nationalInsuranceNumber;
  String nationality;
  String countryofBirth;
  String countryofResidency;
  String passportNumber;
  String maritalStatus;
  String occupantType;
  String livingDate;
  String visaExpiryDate;
  String livingEndDate;
  String areYouLivingAtThisAddress;
  String remarks;
  String visaName;
  String otherVisaName;
  int companyId;
  int id;

  UserAddresss(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.taskId,
      this.addressLine1,
      this.addressLine2,
      this.addressLine3,
      this.town,
      this.county,
      this.postcode,
      this.telNumber,
      this.nationalInsuranceNumber,
      this.nationality,
      this.countryofBirth,
      this.countryofResidency,
      this.passportNumber,
      this.maritalStatus,
      this.occupantType,
      this.livingDate,
      this.visaExpiryDate,
      this.livingEndDate,
      this.areYouLivingAtThisAddress,
      this.remarks,
      this.visaName,
      this.otherVisaName,
      this.companyId,
      this.id});

  UserAddresss.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'] ?? 0;
    addressLine1 = json['AddressLine1'] ?? '';
    addressLine2 = json['AddressLine2'] ?? '';
    addressLine3 = json['AddressLine3'] ?? '';
    town = json['Town'] ?? '';
    county = json['County'] ?? '';
    postcode = json['Postcode'] ?? '';
    telNumber = json['TelNumber'] ?? '';
    nationalInsuranceNumber = json['NationalInsuranceNumber'] ?? '';
    nationality = json['Nationality'] ?? '';
    countryofBirth = json['CountryofBirth'] ?? '';
    countryofResidency = json['CountryofResidency'] ?? '';
    passportNumber = json['PassportNumber'] ?? '';
    maritalStatus = json['MaritalStatus'] ?? '';
    occupantType = json['OccupantType'] ?? '';
    livingDate = json['LivingDate'] ?? '';
    visaExpiryDate = json['VisaExpiryDate'] ?? '';
    livingEndDate = json['LivingEndDate'] ?? '';
    areYouLivingAtThisAddress = json['AreYouLivingAtThisAddress'] ?? '';
    remarks = json['Remarks'] ?? '';
    visaName = json['VisaName'] ?? '';
    otherVisaName = json['OtherVisaName'] ?? '';
    companyId = json['CompanyId'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['AddressLine1'] = this.addressLine1;
    data['AddressLine2'] = this.addressLine2;
    data['AddressLine3'] = this.addressLine3;
    data['Town'] = this.town;
    data['County'] = this.county;
    data['Postcode'] = this.postcode;
    data['TelNumber'] = this.telNumber;
    data['NationalInsuranceNumber'] = this.nationalInsuranceNumber;
    data['Nationality'] = this.nationality;
    data['CountryofBirth'] = this.countryofBirth;
    data['CountryofResidency'] = this.countryofResidency;
    data['PassportNumber'] = this.passportNumber;
    data['MaritalStatus'] = this.maritalStatus;
    data['OccupantType'] = this.occupantType;
    data['LivingDate'] = this.livingDate;
    data['VisaExpiryDate'] = this.visaExpiryDate;
    data['LivingEndDate'] = this.livingEndDate;
    data['AreYouLivingAtThisAddress'] = this.areYouLivingAtThisAddress;
    data['Remarks'] = this.remarks;
    data['VisaName'] = this.visaName;
    data['OtherVisaName'] = this.otherVisaName;
    data['CompanyId'] = this.companyId;
    data['Id'] = this.id;
    return data;
  }
}
