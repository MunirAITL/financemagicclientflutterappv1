class MortgageUserAffordAbility {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  String typeOfFinance;
  String financeProvider;
  double outstandingBalance;
  double creditLimit;
  double monthlyPayment;
  String secured;
  String repayBeforeMortgage;
  String remarks;
  String endDate;
  String isAddItemForAffordabilityCalculation;
  int entityId;
  int id;

  MortgageUserAffordAbility(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.typeOfFinance,
      this.financeProvider,
      this.outstandingBalance,
      this.creditLimit,
      this.monthlyPayment,
      this.secured,
      this.repayBeforeMortgage,
      this.remarks,
      this.endDate,
      this.isAddItemForAffordabilityCalculation,
      this.entityId,
      this.id});

  MortgageUserAffordAbility.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'] ?? 0;
    typeOfFinance = json['TypeOfFinance'] ?? '';
    financeProvider = json['FinanceProvider'] ?? '';
    outstandingBalance = json['OutstandingBalance'] ?? 0;
    creditLimit = json['CreditLimit'] ?? 0;
    monthlyPayment = json['MonthlyPayment'] ?? 0;
    secured = json['Secured'] ?? '';
    repayBeforeMortgage = json['RepayBeforeMortgage'] ?? '';
    remarks = json['Remarks'] ?? '';
    endDate = json['EndDate'] ?? '';
    isAddItemForAffordabilityCalculation =
        json['IsAddItemForAffordabilityCalculation'] ?? '';
    entityId = json['EntityId'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['TypeOfFinance'] = this.typeOfFinance;
    data['FinanceProvider'] = this.financeProvider;
    data['OutstandingBalance'] = this.outstandingBalance;
    data['CreditLimit'] = this.creditLimit;
    data['MonthlyPayment'] = this.monthlyPayment;
    data['Secured'] = this.secured;
    data['RepayBeforeMortgage'] = this.repayBeforeMortgage;
    data['Remarks'] = this.remarks;
    data['EndDate'] = this.endDate;
    data['IsAddItemForAffordabilityCalculation'] =
        this.isAddItemForAffordabilityCalculation;
    data['EntityId'] = this.entityId;
    data['Id'] = this.id;
    return data;
  }
}
