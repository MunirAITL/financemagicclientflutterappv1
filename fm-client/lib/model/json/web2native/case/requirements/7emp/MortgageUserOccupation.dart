class MortgageUserOccupation {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  String currentEmployer;
  String basisOfEmployment;
  String email;
  String occupation;
  String contactNumber;
  int aticipatedRetirementAge;
  String employmentStatus;
  String customerName;
  String remarks;
  String isOwnSharesMoreThanTwenty;
  String natureOfYourBusiness;
  String tradeName;
  String apartmentNumber;
  String buildingName;
  String buildingNumber;
  String street;
  String city;
  String postcode;
  String howAreYouEmployed;
  String whatWasYourStartDate;
  double currentSalary;
  String isOverTime;
  double overTimeSalary;
  String isEarnBonuses;
  double bonusesAmount;
  String isEarnCommission;
  double earnCommission;
  String isOthersourcesOfIncome;
  String incorporatedDate;
  double percentageShare;
  String isOwnShares;
  String howLongHasTheContractLeftToRun;
  String isTheContractRenewable;
  double whatIsYourDailyPayRate;
  double yourAnnualIncome;
  String isHaveYouSubmittedYourSelfAssessmentTaxReturns;
  String isDoYouReceiveAnyOtherPersonalOccupationalPension;
  double otherPensionAmount;
  String isDoYouReceiveAnyOtherBenefits;
  String descriptionOfBenefit;
  double benefitAmount;
  String haveYouSubmittedYourSelfAssessmentTaxReturns;
  double incomeRecentYear;
  String recentYearEndingDate;
  double incomePreviousYear;
  String previousYearEndingDate;
  double netProfitYearBeforeThat;
  String netProfitYearEndingDate;
  String isAreAccountsAvailable;
  String isAreSA302sAvailable;
  double monthlyIncomePayslipAmount;
  String paySlipReferenceNumber;
  String notes;
  String businessAddress;
  String employmentOthersStatus;
  String address;
  String areYouCurrentlyWorkingThere;
  String whatWasYourEndDate;
  int id;

  MortgageUserOccupation(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.currentEmployer,
      this.basisOfEmployment,
      this.email,
      this.occupation,
      this.contactNumber,
      this.aticipatedRetirementAge,
      this.employmentStatus,
      this.customerName,
      this.remarks,
      this.isOwnSharesMoreThanTwenty,
      this.natureOfYourBusiness,
      this.tradeName,
      this.apartmentNumber,
      this.buildingName,
      this.buildingNumber,
      this.street,
      this.city,
      this.postcode,
      this.howAreYouEmployed,
      this.whatWasYourStartDate,
      this.currentSalary,
      this.isOverTime,
      this.overTimeSalary,
      this.isEarnBonuses,
      this.bonusesAmount,
      this.isEarnCommission,
      this.earnCommission,
      this.isOthersourcesOfIncome,
      this.incorporatedDate,
      this.percentageShare,
      this.isOwnShares,
      this.howLongHasTheContractLeftToRun,
      this.isTheContractRenewable,
      this.whatIsYourDailyPayRate,
      this.yourAnnualIncome,
      this.isHaveYouSubmittedYourSelfAssessmentTaxReturns,
      this.isDoYouReceiveAnyOtherPersonalOccupationalPension,
      this.otherPensionAmount,
      this.isDoYouReceiveAnyOtherBenefits,
      this.descriptionOfBenefit,
      this.benefitAmount,
      this.haveYouSubmittedYourSelfAssessmentTaxReturns,
      this.incomeRecentYear,
      this.recentYearEndingDate,
      this.incomePreviousYear,
      this.previousYearEndingDate,
      this.netProfitYearBeforeThat,
      this.netProfitYearEndingDate,
      this.isAreAccountsAvailable,
      this.isAreSA302sAvailable,
      this.monthlyIncomePayslipAmount,
      this.paySlipReferenceNumber,
      this.notes,
      this.businessAddress,
      this.employmentOthersStatus,
      this.address,
      this.areYouCurrentlyWorkingThere,
      this.whatWasYourEndDate,
      this.id});

  MortgageUserOccupation.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    currentEmployer = json['CurrentEmployer'] ?? '';
    basisOfEmployment = json['BasisOfEmployment'] ?? '';
    email = json['Email'] ?? '';
    occupation = json['Occupation'] ?? '';
    contactNumber = json['ContactNumber'] ?? '';
    aticipatedRetirementAge = json['AticipatedRetirementAge'] ?? 0;
    employmentStatus = json['EmploymentStatus'] ?? '';
    customerName = json['CustomerName'] ?? '';
    remarks = json['Remarks'] ?? '';
    isOwnSharesMoreThanTwenty = json['IsOwnSharesMoreThanTwenty'] ?? '';
    natureOfYourBusiness = json['NatureOfYourBusiness'] ?? '';
    tradeName = json['TradeName'] ?? '';
    apartmentNumber = json['ApartmentNumber'] ?? '';
    buildingName = json['BuildingName'] ?? '';
    buildingNumber = json['BuildingNumber'] ?? '';
    street = json['Street'] ?? '';
    city = json['City'] ?? '';
    postcode = json['Postcode'] ?? '';
    howAreYouEmployed = json['HowAreYouEmployed'] ?? '';
    whatWasYourStartDate = json['WhatWasYourStartDate'] ?? '';
    currentSalary = json['CurrentSalary'] ?? 0;
    isOverTime = json['IsOverTime'] ?? '';
    overTimeSalary = json['OverTimeSalary'] ?? 0;
    isEarnBonuses = json['IsEarnBonuses'] ?? '';
    bonusesAmount = json['BonusesAmount'] ?? 0;
    isEarnCommission = json['IsEarnCommission'] ?? '';
    earnCommission = json['EarnCommission'] ?? 0;
    isOthersourcesOfIncome = json['IsOthersourcesOfIncome'] ?? '';
    incorporatedDate = json['IncorporatedDate'] ?? '';
    percentageShare = json['PercentageShare'] ?? 0;
    isOwnShares = json['IsOwnShares'] ?? '';
    howLongHasTheContractLeftToRun =
        json['HowLongHasTheContractLeftToRun'] ?? '';
    isTheContractRenewable = json['IsTheContractRenewable'] ?? '';
    whatIsYourDailyPayRate = json['WhatIsYourDailyPayRate'] ?? 0;
    yourAnnualIncome = json['YourAnnualIncome'] ?? 0;
    isHaveYouSubmittedYourSelfAssessmentTaxReturns =
        json['IsHaveYouSubmittedYourSelfAssessmentTaxReturns'] ?? '';
    isDoYouReceiveAnyOtherPersonalOccupationalPension =
        json['IsDoYouReceiveAnyOtherPersonalOccupationalPension'] ?? '';
    otherPensionAmount = json['OtherPensionAmount'] ?? 0;
    isDoYouReceiveAnyOtherBenefits =
        json['IsDoYouReceiveAnyOtherBenefits'] ?? '';
    descriptionOfBenefit = json['DescriptionOfBenefit'] ?? '';
    benefitAmount = json['BenefitAmount'] ?? 0;
    haveYouSubmittedYourSelfAssessmentTaxReturns =
        json['HaveYouSubmittedYourSelfAssessmentTaxReturns'] ?? '';
    incomeRecentYear = json['IncomeRecentYear'] ?? 0;
    recentYearEndingDate = json['RecentYearEndingDate'] ?? '';
    incomePreviousYear = json['IncomePreviousYear'] ?? 0;
    previousYearEndingDate = json['PreviousYearEndingDate'] ?? '';
    netProfitYearBeforeThat = json['NetProfitYearBeforeThat'] ?? 0;
    netProfitYearEndingDate = json['NetProfitYearEndingDate'] ?? '';
    isAreAccountsAvailable = json['IsAreAccountsAvailable'] ?? '';
    isAreSA302sAvailable = json['IsAreSA302sAvailable'] ?? '';
    monthlyIncomePayslipAmount = json['MonthlyIncomePayslipAmount'] ?? 0;
    paySlipReferenceNumber = json['PaySlipReferenceNumber'] ?? '';
    notes = json['Notes'] ?? '';
    businessAddress = json['BusinessAddress'] ?? '';
    employmentOthersStatus = json['EmploymentOthersStatus'] ?? '';
    address = json['Address'] ?? '';
    areYouCurrentlyWorkingThere = json['AreYouCurrentlyWorkingThere'] ?? '';
    whatWasYourEndDate = json['WhatWasYourEndDate'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['CurrentEmployer'] = this.currentEmployer;
    data['BasisOfEmployment'] = this.basisOfEmployment;
    data['Email'] = this.email;
    data['Occupation'] = this.occupation;
    data['ContactNumber'] = this.contactNumber;
    data['AticipatedRetirementAge'] = this.aticipatedRetirementAge;
    data['EmploymentStatus'] = this.employmentStatus;
    data['CustomerName'] = this.customerName;
    data['Remarks'] = this.remarks;
    data['IsOwnSharesMoreThanTwenty'] = this.isOwnSharesMoreThanTwenty;
    data['NatureOfYourBusiness'] = this.natureOfYourBusiness;
    data['TradeName'] = this.tradeName;
    data['ApartmentNumber'] = this.apartmentNumber;
    data['BuildingName'] = this.buildingName;
    data['BuildingNumber'] = this.buildingNumber;
    data['Street'] = this.street;
    data['City'] = this.city;
    data['Postcode'] = this.postcode;
    data['HowAreYouEmployed'] = this.howAreYouEmployed;
    data['WhatWasYourStartDate'] = this.whatWasYourStartDate;
    data['CurrentSalary'] = this.currentSalary;
    data['IsOverTime'] = this.isOverTime;
    data['OverTimeSalary'] = this.overTimeSalary;
    data['IsEarnBonuses'] = this.isEarnBonuses;
    data['BonusesAmount'] = this.bonusesAmount;
    data['IsEarnCommission'] = this.isEarnCommission;
    data['EarnCommission'] = this.earnCommission;
    data['IsOthersourcesOfIncome'] = this.isOthersourcesOfIncome;
    data['IncorporatedDate'] = this.incorporatedDate;
    data['PercentageShare'] = this.percentageShare;
    data['IsOwnShares'] = this.isOwnShares;
    data['HowLongHasTheContractLeftToRun'] =
        this.howLongHasTheContractLeftToRun;
    data['IsTheContractRenewable'] = this.isTheContractRenewable;
    data['WhatIsYourDailyPayRate'] = this.whatIsYourDailyPayRate;
    data['YourAnnualIncome'] = this.yourAnnualIncome;
    data['IsHaveYouSubmittedYourSelfAssessmentTaxReturns'] =
        this.isHaveYouSubmittedYourSelfAssessmentTaxReturns;
    data['IsDoYouReceiveAnyOtherPersonalOccupationalPension'] =
        this.isDoYouReceiveAnyOtherPersonalOccupationalPension;
    data['OtherPensionAmount'] = this.otherPensionAmount;
    data['IsDoYouReceiveAnyOtherBenefits'] =
        this.isDoYouReceiveAnyOtherBenefits;
    data['DescriptionOfBenefit'] = this.descriptionOfBenefit;
    data['BenefitAmount'] = this.benefitAmount;
    data['HaveYouSubmittedYourSelfAssessmentTaxReturns'] =
        this.haveYouSubmittedYourSelfAssessmentTaxReturns;
    data['IncomeRecentYear'] = this.incomeRecentYear;
    data['RecentYearEndingDate'] = this.recentYearEndingDate;
    data['IncomePreviousYear'] = this.incomePreviousYear;
    data['PreviousYearEndingDate'] = this.previousYearEndingDate;
    data['NetProfitYearBeforeThat'] = this.netProfitYearBeforeThat;
    data['NetProfitYearEndingDate'] = this.netProfitYearEndingDate;
    data['IsAreAccountsAvailable'] = this.isAreAccountsAvailable;
    data['IsAreSA302sAvailable'] = this.isAreSA302sAvailable;
    data['MonthlyIncomePayslipAmount'] = this.monthlyIncomePayslipAmount;
    data['PaySlipReferenceNumber'] = this.paySlipReferenceNumber;
    data['Notes'] = this.notes;
    data['BusinessAddress'] = this.businessAddress;
    data['EmploymentOthersStatus'] = this.employmentOthersStatus;
    data['Address'] = this.address;
    data['AreYouCurrentlyWorkingThere'] = this.areYouCurrentlyWorkingThere;
    data['WhatWasYourEndDate'] = this.whatWasYourEndDate;
    data['Id'] = this.id;
    return data;
  }
}
