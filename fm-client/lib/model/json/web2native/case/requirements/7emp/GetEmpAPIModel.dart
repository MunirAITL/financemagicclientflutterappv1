import 'MortgageUserOccupation.dart';

class GetEmpAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetEmpAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetEmpAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserOccupation> mortgageUserOccupations;

  ResponseData({this.mortgageUserOccupations});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserOccupations'] != null) {
      mortgageUserOccupations = <MortgageUserOccupation>[];
      json['MortgageUserOccupations'].forEach((v) {
        mortgageUserOccupations.add(new MortgageUserOccupation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserOccupations != null) {
      data['MortgageUserOccupations'] =
          this.mortgageUserOccupations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
