import 'MortgageUserInCome.dart';

class GetIncomeAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetIncomeAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetIncomeAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserInCome> mortgageUserInComes;
  ResponseData({this.mortgageUserInComes});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserInComes'] != null) {
      mortgageUserInComes = <MortgageUserInCome>[];
      json['MortgageUserInComes'].forEach((v) {
        mortgageUserInComes.add(new MortgageUserInCome.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserInComes != null) {
      data['MortgageUserInComes'] =
          this.mortgageUserInComes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
