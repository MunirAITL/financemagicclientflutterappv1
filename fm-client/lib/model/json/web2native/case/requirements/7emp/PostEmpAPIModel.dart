import 'MortgageUserOccupation.dart';

class PostEmpAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  PostEmpAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostEmpAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> occupationPost;
  Messages({this.occupationPost});
  Messages.fromJson(Map<String, dynamic> json) {
    occupationPost = json['occupation_post'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['occupation_post'] = this.occupationPost;
    return data;
  }
}

class ResponseData {
  MortgageUserOccupation mortgageUserOccupation;
  ResponseData({this.mortgageUserOccupation});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserOccupation = json['MortgageUserOccupation'] != null
        ? new MortgageUserOccupation.fromJson(json['MortgageUserOccupation'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserOccupation != null) {
      data['MortgageUserOccupation'] = this.mortgageUserOccupation.toJson();
    }
    return data;
  }
}
