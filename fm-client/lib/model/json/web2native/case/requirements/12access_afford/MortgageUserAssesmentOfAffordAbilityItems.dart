class MortgageUserAssesmentOfAffordAbilityItems {
  int userId;
  int status;
  String creationDate;
  int taskId;
  int userCompanyId;
  int mortgageUserAssesmentOfAffordAbilityId;
  String itemType;
  double itemAmount;
  String remarks;
  String category;
  int id;

  MortgageUserAssesmentOfAffordAbilityItems(
      {this.userId,
      this.status,
      this.creationDate,
      this.taskId,
      this.userCompanyId,
      this.mortgageUserAssesmentOfAffordAbilityId,
      this.itemType,
      this.itemAmount,
      this.remarks,
      this.category,
      this.id});

  MortgageUserAssesmentOfAffordAbilityItems.fromJson(
      Map<String, dynamic> json) {
    userId = json['UserId'];
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'] ?? 0;
    userCompanyId = json['UserCompanyId'] ?? 0;
    mortgageUserAssesmentOfAffordAbilityId =
        json['MortgageUserAssesmentOfAffordAbilityId'] ?? 0;
    itemType = json['ItemType'] ?? '';
    itemAmount = json['ItemAmount'] ?? 0;
    remarks = json['Remarks'] ?? '';
    category = json['Category'] ?? '';
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['UserCompanyId'] = this.userCompanyId;
    data['MortgageUserAssesmentOfAffordAbilityId'] =
        this.mortgageUserAssesmentOfAffordAbilityId;
    data['ItemType'] = this.itemType;
    data['ItemAmount'] = this.itemAmount;
    data['Remarks'] = this.remarks;
    data['Category'] = this.category;
    data['Id'] = this.id;
    return data;
  }
}
