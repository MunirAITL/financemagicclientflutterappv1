class MortgageUserAssesmentOfAffordAbility {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  double totalOfContinuingLoansCreditAsAbove;
  double currentOrExpectedFutureExpenditure;
  double councilTax;
  double gas;
  double electric;
  double water;
  double propertyMaintenanceGroundRent;
  double buildingsContentsInsurance;
  double familyFoodHouseholdCosts;
  double childCareCosts;
  double clothing;
  double tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences;
  double mobilePhoneCosts;
  double regularSubscriptions;
  double carCosts;
  double mOTRoadTaxGeneralMaintenanceRepairs;
  double carFuelCosts;
  double transportTrainTramBus;
  double petsFoodInsuranceGrooming;
  double maintenancePayments;
  double regularSchoolFeesContributions;
  double otherSchoolingCostsMealsUniformOutings;
  double insurancesLifeHealthMedicalDentalPhone;
  double pensionContributions;
  double regularSavings;
  double tobaccoOrRelatedProducts;
  double socialCostsMealsOutDrinksTheatre;
  double holiday;
  double otherDdetails;
  double totalMonthlyExpenditure;
  double netMonthlyIncome;
  double netMonthlyDisposableIncome;
  double television;
  double groceries;
  double homeHelp;
  double livingCostClothing;
  double laundry;
  double homePhone;
  double internet;
  double investments;
  double entertainment;
  double sports;
  double cigarettes;
  double alcohol;
  double pension;
  double leisure;
  double travel;
  double petrol;
  double commutingCosts;
  double motorInsurance;
  double otherTransportCost;
  double dayCare;
  double clothes;
  String remarks;
  int id;

  MortgageUserAssesmentOfAffordAbility(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.totalOfContinuingLoansCreditAsAbove,
      this.currentOrExpectedFutureExpenditure,
      this.councilTax,
      this.gas,
      this.electric,
      this.water,
      this.propertyMaintenanceGroundRent,
      this.buildingsContentsInsurance,
      this.familyFoodHouseholdCosts,
      this.childCareCosts,
      this.clothing,
      this.tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences,
      this.mobilePhoneCosts,
      this.regularSubscriptions,
      this.carCosts,
      this.mOTRoadTaxGeneralMaintenanceRepairs,
      this.carFuelCosts,
      this.transportTrainTramBus,
      this.petsFoodInsuranceGrooming,
      this.maintenancePayments,
      this.regularSchoolFeesContributions,
      this.otherSchoolingCostsMealsUniformOutings,
      this.insurancesLifeHealthMedicalDentalPhone,
      this.pensionContributions,
      this.regularSavings,
      this.tobaccoOrRelatedProducts,
      this.socialCostsMealsOutDrinksTheatre,
      this.holiday,
      this.otherDdetails,
      this.totalMonthlyExpenditure,
      this.netMonthlyIncome,
      this.netMonthlyDisposableIncome,
      this.television,
      this.groceries,
      this.homeHelp,
      this.livingCostClothing,
      this.laundry,
      this.homePhone,
      this.internet,
      this.investments,
      this.entertainment,
      this.sports,
      this.cigarettes,
      this.alcohol,
      this.pension,
      this.leisure,
      this.travel,
      this.petrol,
      this.commutingCosts,
      this.motorInsurance,
      this.otherTransportCost,
      this.dayCare,
      this.clothes,
      this.remarks,
      this.id});

  MortgageUserAssesmentOfAffordAbility.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'];
    status = json['Status'];
    mortgageCaseInfoId = json['MortgageCaseInfoId'];
    creationDate = json['CreationDate'];
    totalOfContinuingLoansCreditAsAbove =
        json['TotalOfContinuingLoansCreditAsAbove'] ?? 0;
    currentOrExpectedFutureExpenditure =
        json['CurrentOrExpectedFutureExpenditure'] ?? 0;
    councilTax = json['CouncilTax'] ?? 0;
    gas = json['Gas'] ?? 0;
    electric = json['Electric'] ?? 0;
    water = json['Water'] ?? 0;
    propertyMaintenanceGroundRent = json['PropertyMaintenanceGroundRent'] ?? 0;
    buildingsContentsInsurance = json['BuildingsContentsInsurance'] ?? 0;
    familyFoodHouseholdCosts = json['FamilyFoodHouseholdCosts'] ?? 0;
    childCareCosts = json['ChildCareCosts'] ?? 0;
    clothing = json['Clothing'] ?? 0;
    tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences = json[
            'TVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences'] ??
        0;
    mobilePhoneCosts = json['MobilePhoneCosts'] ?? 0;
    regularSubscriptions = json['RegularSubscriptions'] ?? 0;
    carCosts = json['CarCosts'] ?? 0;
    mOTRoadTaxGeneralMaintenanceRepairs =
        json['MOTRoadTaxGeneralMaintenanceRepairs'] ?? 0;
    carFuelCosts = json['CarFuelCosts'] ?? 0;
    transportTrainTramBus = json['TransportTrainTramBus'] ?? 0;
    petsFoodInsuranceGrooming = json['PetsFoodInsuranceGrooming'] ?? 0;
    maintenancePayments = json['MaintenancePayments'] ?? 0;
    regularSchoolFeesContributions =
        json['RegularSchoolFeesContributions'] ?? 0;
    otherSchoolingCostsMealsUniformOutings =
        json['OtherSchoolingCostsMealsUniformOutings'] ?? 0;
    insurancesLifeHealthMedicalDentalPhone =
        json['InsurancesLifeHealthMedicalDentalPhone'] ?? 0;
    pensionContributions = json['PensionContributions'] ?? 0;
    regularSavings = json['RegularSavings'] ?? 0;
    tobaccoOrRelatedProducts = json['TobaccoOrRelatedProducts'] ?? 0;
    socialCostsMealsOutDrinksTheatre =
        json['SocialCostsMealsOutDrinksTheatre'] ?? 0;
    holiday = json['Holiday'] ?? 0;
    otherDdetails = json['OtherDdetails'] ?? 0;
    totalMonthlyExpenditure = json['TotalMonthlyExpenditure'] ?? 0;
    netMonthlyIncome = json['NetMonthlyIncome'] ?? 0;
    netMonthlyDisposableIncome = json['NetMonthlyDisposableIncome'] ?? 0;
    television = json['Television'] ?? 0;
    groceries = json['Groceries'] ?? 0;
    homeHelp = json['HomeHelp'] ?? 0;
    livingCostClothing = json['LivingCostClothing'] ?? 0;
    laundry = json['Laundry'] ?? 0;
    homePhone = json['HomePhone'] ?? 0;
    internet = json['Internet'] ?? 0;
    investments = json['Investments'] ?? 0;
    entertainment = json['Entertainment'] ?? 0;
    sports = json['Sports'] ?? 0;
    cigarettes = json['Cigarettes'] ?? 0;
    alcohol = json['Alcohol'] ?? 0;
    pension = json['Pension'] ?? 0;
    leisure = json['Leisure'] ?? 0;
    travel = json['Travel'] ?? 0;
    petrol = json['Petrol'] ?? 0;
    commutingCosts = json['CommutingCosts'] ?? 0;
    motorInsurance = json['MotorInsurance'] ?? 0;
    otherTransportCost = json['OtherTransportCost'] ?? 0;
    dayCare = json['DayCare'] ?? 0;
    clothes = json['Clothes'] ?? 0;
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TotalOfContinuingLoansCreditAsAbove'] =
        this.totalOfContinuingLoansCreditAsAbove;
    data['CurrentOrExpectedFutureExpenditure'] =
        this.currentOrExpectedFutureExpenditure;
    data['CouncilTax'] = this.councilTax;
    data['Gas'] = this.gas;
    data['Electric'] = this.electric;
    data['Water'] = this.water;
    data['PropertyMaintenanceGroundRent'] = this.propertyMaintenanceGroundRent;
    data['BuildingsContentsInsurance'] = this.buildingsContentsInsurance;
    data['FamilyFoodHouseholdCosts'] = this.familyFoodHouseholdCosts;
    data['ChildCareCosts'] = this.childCareCosts;
    data['Clothing'] = this.clothing;
    data['TVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences'] =
        this.tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences;
    data['MobilePhoneCosts'] = this.mobilePhoneCosts;
    data['RegularSubscriptions'] = this.regularSubscriptions;
    data['CarCosts'] = this.carCosts;
    data['MOTRoadTaxGeneralMaintenanceRepairs'] =
        this.mOTRoadTaxGeneralMaintenanceRepairs;
    data['CarFuelCosts'] = this.carFuelCosts;
    data['TransportTrainTramBus'] = this.transportTrainTramBus;
    data['PetsFoodInsuranceGrooming'] = this.petsFoodInsuranceGrooming;
    data['MaintenancePayments'] = this.maintenancePayments;
    data['RegularSchoolFeesContributions'] =
        this.regularSchoolFeesContributions;
    data['OtherSchoolingCostsMealsUniformOutings'] =
        this.otherSchoolingCostsMealsUniformOutings;
    data['InsurancesLifeHealthMedicalDentalPhone'] =
        this.insurancesLifeHealthMedicalDentalPhone;
    data['PensionContributions'] = this.pensionContributions;
    data['RegularSavings'] = this.regularSavings;
    data['TobaccoOrRelatedProducts'] = this.tobaccoOrRelatedProducts;
    data['SocialCostsMealsOutDrinksTheatre'] =
        this.socialCostsMealsOutDrinksTheatre;
    data['Holiday'] = this.holiday;
    data['OtherDdetails'] = this.otherDdetails;
    data['TotalMonthlyExpenditure'] = this.totalMonthlyExpenditure;
    data['NetMonthlyIncome'] = this.netMonthlyIncome;
    data['NetMonthlyDisposableIncome'] = this.netMonthlyDisposableIncome;
    data['Television'] = this.television;
    data['Groceries'] = this.groceries;
    data['HomeHelp'] = this.homeHelp;
    data['LivingCostClothing'] = this.livingCostClothing;
    data['Laundry'] = this.laundry;
    data['HomePhone'] = this.homePhone;
    data['Internet'] = this.internet;
    data['Investments'] = this.investments;
    data['Entertainment'] = this.entertainment;
    data['Sports'] = this.sports;
    data['Cigarettes'] = this.cigarettes;
    data['Alcohol'] = this.alcohol;
    data['Pension'] = this.pension;
    data['Leisure'] = this.leisure;
    data['Travel'] = this.travel;
    data['Petrol'] = this.petrol;
    data['CommutingCosts'] = this.commutingCosts;
    data['MotorInsurance'] = this.motorInsurance;
    data['OtherTransportCost'] = this.otherTransportCost;
    data['DayCare'] = this.dayCare;
    data['Clothes'] = this.clothes;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
