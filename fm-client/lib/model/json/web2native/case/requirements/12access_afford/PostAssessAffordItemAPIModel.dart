import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbilityItems.dart';

class PostAssessAffordItemAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostAssessAffordItemAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostAssessAffordItemAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageUserAssesmentOfAffordAbilityItems
      mortgageUserAssesmentOfAffordAbilityItem;
  ResponseData({this.mortgageUserAssesmentOfAffordAbilityItem});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserAssesmentOfAffordAbilityItem =
        json['MortgageUserAssesmentOfAffordAbilityItem'] != null
            ? new MortgageUserAssesmentOfAffordAbilityItems.fromJson(
                json['MortgageUserAssesmentOfAffordAbilityItem'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserAssesmentOfAffordAbilityItem != null) {
      data['MortgageUserAssesmentOfAffordAbilityItem'] =
          this.mortgageUserAssesmentOfAffordAbilityItem.toJson();
    }
    return data;
  }
}
