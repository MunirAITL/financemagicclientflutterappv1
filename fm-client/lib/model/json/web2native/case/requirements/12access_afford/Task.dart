class Task {
  int userId;
  dynamic user;
  dynamic status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String title;
  String description;
  bool isInPersonOrOnline;
  int dutDateType;
  String deliveryDate;
  String deliveryTime;
  int workerNumber;
  String skill;
  bool isFixedPrice;
  double hourlyRate;
  double fixedBudgetAmount;
  double netTotalAmount;
  double paidAmount;
  double dueAmount;
  String jobCategory;
  int employeeId;
  int totalBidsNumber;
  bool isArchive;
  String preferedLocation;
  double latitude;
  double longitude;
  String ownerName;
  String thumbnailPath;
  String remarks;
  String taskReferenceNumber;
  String imageServerUrl;
  String ownerImageUrl;
  String requirements;
  double totalHours;
  String taskTitleUrl;
  String ownerProfileUrl;
  int totalAcceptedNumber;
  int totalCompletedNumber;
  int companyId;
  String companyName;
  int entityId;
  String entityName;
  double introducerFeeShareAmount;
  double introducerPaymentedAmount;
  double adviserAmount;
  double adviserPaymentedAmount;
  int notificationUnreadTaskCount;
  int notificationTaskCount;
  String addressOfPropertyToBeMortgaged;
  String overAllCaseGrade;
  int caseObservationAssessmentId;
  String namePrefix;
  String lastName;
  int caseStatus;
  String areYouChargingAFee;
  double chargeFeeAmount;
  String chargingFeeWhenPayable;
  String chargingFeeRefundable;
  String areYouChargingAnotherFee;
  double chargeAnotherFeeAmount;
  String chargingAnotherFeeWhenPayable;
  String chargingAnotherFeeRefundable;
  int packagerCompanyId;
  int caseComplianceAdviserDeclarationId;
  String isTheCompanyATradingCompany;
  String adviceType;
  String disclosureMethod;
  String caseClassification;
  List<MortgageCaseInfoEntityModelList> mortgageCaseInfoEntityModelList;
  int id;
  String referenceSource;
  String adviserName;
  String introducerName;
  String adviserNameProfileImageUrl;
  String introducerProfileImageUrl;

  Task({
    this.userId,
    this.user,
    this.status,
    this.creationDate,
    this.updatedDate,
    this.versionNumber,
    this.title,
    this.description,
    this.isInPersonOrOnline,
    this.dutDateType,
    this.deliveryDate,
    this.deliveryTime,
    this.workerNumber,
    this.skill,
    this.isFixedPrice,
    this.hourlyRate,
    this.fixedBudgetAmount,
    this.netTotalAmount,
    this.paidAmount,
    this.dueAmount,
    this.jobCategory,
    this.employeeId,
    this.totalBidsNumber,
    this.isArchive,
    this.preferedLocation,
    this.latitude,
    this.longitude,
    this.ownerName,
    this.thumbnailPath,
    this.remarks,
    this.taskReferenceNumber,
    this.imageServerUrl,
    this.ownerImageUrl,
    this.requirements,
    this.totalHours,
    this.taskTitleUrl,
    this.ownerProfileUrl,
    this.totalAcceptedNumber,
    this.totalCompletedNumber,
    this.companyId,
    this.companyName,
    this.entityId,
    this.entityName,
    this.introducerFeeShareAmount,
    this.introducerPaymentedAmount,
    this.adviserAmount,
    this.adviserPaymentedAmount,
    this.notificationUnreadTaskCount,
    this.notificationTaskCount,
    this.addressOfPropertyToBeMortgaged,
    this.overAllCaseGrade,
    this.caseObservationAssessmentId,
    this.namePrefix,
    this.lastName,
    this.caseStatus,
    this.areYouChargingAFee,
    this.chargeFeeAmount,
    this.chargingFeeWhenPayable,
    this.chargingFeeRefundable,
    this.areYouChargingAnotherFee,
    this.chargeAnotherFeeAmount,
    this.chargingAnotherFeeWhenPayable,
    this.chargingAnotherFeeRefundable,
    this.packagerCompanyId,
    this.caseComplianceAdviserDeclarationId,
    this.isTheCompanyATradingCompany,
    this.adviceType,
    this.disclosureMethod,
    this.caseClassification,
    this.mortgageCaseInfoEntityModelList,
    this.id,
    this.referenceSource,
    this.adviserName,
    this.introducerName,
    this.adviserNameProfileImageUrl,
    this.introducerProfileImageUrl,
  });

  Task.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    status = json['Status'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? '';
    title = json['Title'] ?? '';
    description = json['Description'] ?? '';
    isInPersonOrOnline = json['IsInPersonOrOnline'] ?? false;
    dutDateType = json['DutDateType'] ?? 0;
    deliveryDate = json['DeliveryDate'] ?? '';
    deliveryTime = json['DeliveryTime'] ?? '';
    workerNumber = json['WorkerNumber'] ?? 0;
    skill = json['Skill'] ?? '';
    isFixedPrice = json['IsFixedPrice'] ?? false;
    hourlyRate = json['HourlyRate'] ?? 0;
    fixedBudgetAmount = json['FixedBudgetAmount'] ?? 0;
    netTotalAmount = json['NetTotalAmount'] ?? 0;
    paidAmount = json['PaidAmount'] ?? 0;
    dueAmount = json['DueAmount'] ?? 0;
    jobCategory = json['JobCategory'] ?? '';
    employeeId = json['EmployeeId'] ?? 0;
    totalBidsNumber = json['TotalBidsNumber'] ?? 0;
    isArchive = json['IsArchive'] ?? false;
    preferedLocation = json['PreferedLocation'] ?? '';
    latitude = json['Latitude'] ?? 0;
    longitude = json['Longitude'] ?? 0;
    ownerName = json['OwnerName'] ?? '';
    thumbnailPath = json['ThumbnailPath'] ?? '';
    remarks = json['Remarks'] ?? '';
    taskReferenceNumber = json['TaskReferenceNumber'] ?? '';
    imageServerUrl = json['ImageServerUrl'] ?? '';
    ownerImageUrl = json['OwnerImageUrl'] ?? '';
    requirements = json['Requirements'] ?? '';
    totalHours = json['TotalHours'] ?? 0;
    taskTitleUrl = json['TaskTitleUrl'] ?? '';
    ownerProfileUrl = json['OwnerProfileUrl'] ?? '';
    totalAcceptedNumber = json['TotalAcceptedNumber'] ?? 0;
    totalCompletedNumber = json['TotalCompletedNumber'] ?? 0;
    companyId = json['CompanyId'] ?? 0;
    companyName = json['CompanyName'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    introducerFeeShareAmount = json['IntroducerFeeShareAmount'] ?? 0;
    introducerPaymentedAmount = json['IntroducerPaymentedAmount'] ?? 0;
    adviserAmount = json['AdviserAmount'] ?? 0;
    adviserPaymentedAmount = json['AdviserPaymentedAmount'] ?? 0;
    notificationUnreadTaskCount = json['NotificationUnreadTaskCount'] ?? 0;
    notificationTaskCount = json['NotificationTaskCount'] ?? 0;
    addressOfPropertyToBeMortgaged =
        json['AddressOfPropertyToBeMortgaged'] ?? '';
    overAllCaseGrade = json['OverAllCaseGrade'] ?? '';
    caseObservationAssessmentId = json['CaseObservationAssessmentId'] ?? 0;
    namePrefix = json['NamePrefix'] ?? '';
    lastName = json['LastName'] ?? '';
    caseStatus = json['CaseStatus'] ?? 0;
    areYouChargingAFee = json['AreYouChargingAFee'] ?? '';
    chargeFeeAmount = json['ChargeFeeAmount'] ?? 0;
    chargingFeeWhenPayable = json['ChargingFeeWhenPayable'] ?? '';
    chargingFeeRefundable = json['ChargingFeeRefundable'] ?? '';
    areYouChargingAnotherFee = json['AreYouChargingAnotherFee'] ?? '';
    chargeAnotherFeeAmount = json['ChargeAnotherFeeAmount'] ?? 0;
    chargingAnotherFeeWhenPayable = json['ChargingAnotherFeeWhenPayable'] ?? '';
    chargingAnotherFeeRefundable = json['ChargingAnotherFeeRefundable'] ?? '';
    packagerCompanyId = json['PackagerCompanyId'] ?? 0;
    caseComplianceAdviserDeclarationId =
        json['CaseComplianceAdviserDeclarationId'] ?? 0;
    isTheCompanyATradingCompany = json['IsTheCompanyATradingCompany'] ?? '';
    adviceType = json['AdviceType'] ?? '';
    disclosureMethod = json['DisclosureMethod'] ?? '';
    caseClassification = json['CaseClassification'] ?? '';
    if (json['MortgageCaseInfoEntityModelList'] != null) {
      mortgageCaseInfoEntityModelList = <MortgageCaseInfoEntityModelList>[];
      json['MortgageCaseInfoEntityModelList'].forEach((v) {
        mortgageCaseInfoEntityModelList
            .add(new MortgageCaseInfoEntityModelList.fromJson(v));
      });
    }
    id = json['Id'];
    referenceSource = json['ReferenceSource'] ?? '';
    adviserName = json['AdviserName'] ?? '';
    introducerName = json['IntroducerName'] ?? '';
    adviserNameProfileImageUrl = json['AdviserNameProfileImageUrl'] ?? '';
    introducerProfileImageUrl = json['IntroducerProfileImageUrl'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['DutDateType'] = this.dutDateType;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['WorkerNumber'] = this.workerNumber;
    data['Skill'] = this.skill;
    data['IsFixedPrice'] = this.isFixedPrice;
    data['HourlyRate'] = this.hourlyRate;
    data['FixedBudgetAmount'] = this.fixedBudgetAmount;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['PaidAmount'] = this.paidAmount;
    data['DueAmount'] = this.dueAmount;
    data['JobCategory'] = this.jobCategory;
    data['EmployeeId'] = this.employeeId;
    data['TotalBidsNumber'] = this.totalBidsNumber;
    data['IsArchive'] = this.isArchive;
    data['PreferedLocation'] = this.preferedLocation;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['OwnerName'] = this.ownerName;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['Remarks'] = this.remarks;
    data['TaskReferenceNumber'] = this.taskReferenceNumber;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['Requirements'] = this.requirements;
    data['TotalHours'] = this.totalHours;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['TotalAcceptedNumber'] = this.totalAcceptedNumber;
    data['TotalCompletedNumber'] = this.totalCompletedNumber;
    data['CompanyId'] = this.companyId;
    data['CompanyName'] = this.companyName;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['IntroducerFeeShareAmount'] = this.introducerFeeShareAmount;
    data['IntroducerPaymentedAmount'] = this.introducerPaymentedAmount;
    data['AdviserAmount'] = this.adviserAmount;
    data['AdviserPaymentedAmount'] = this.adviserPaymentedAmount;
    data['NotificationUnreadTaskCount'] = this.notificationUnreadTaskCount;
    data['NotificationTaskCount'] = this.notificationTaskCount;
    data['AddressOfPropertyToBeMortgaged'] =
        this.addressOfPropertyToBeMortgaged;
    data['OverAllCaseGrade'] = this.overAllCaseGrade;
    data['CaseObservationAssessmentId'] = this.caseObservationAssessmentId;
    data['NamePrefix'] = this.namePrefix;
    data['LastName'] = this.lastName;
    data['CaseStatus'] = this.caseStatus;
    data['AreYouChargingAFee'] = this.areYouChargingAFee;
    data['ChargeFeeAmount'] = this.chargeFeeAmount;
    data['ChargingFeeWhenPayable'] = this.chargingFeeWhenPayable;
    data['ChargingFeeRefundable'] = this.chargingFeeRefundable;
    data['AreYouChargingAnotherFee'] = this.areYouChargingAnotherFee;
    data['ChargeAnotherFeeAmount'] = this.chargeAnotherFeeAmount;
    data['ChargingAnotherFeeWhenPayable'] = this.chargingAnotherFeeWhenPayable;
    data['ChargingAnotherFeeRefundable'] = this.chargingAnotherFeeRefundable;
    data['PackagerCompanyId'] = this.packagerCompanyId;
    data['CaseComplianceAdviserDeclarationId'] =
        this.caseComplianceAdviserDeclarationId;
    data['IsTheCompanyATradingCompany'] = this.isTheCompanyATradingCompany;
    data['AdviceType'] = this.adviceType;
    data['DisclosureMethod'] = this.disclosureMethod;
    data['CaseClassification'] = this.caseClassification;
    if (this.mortgageCaseInfoEntityModelList != null) {
      data['MortgageCaseInfoEntityModelList'] =
          this.mortgageCaseInfoEntityModelList.map((v) => v.toJson()).toList();
    }
    data['Id'] = this.id;
    data['ReferenceSource'] = this.referenceSource;
    data['AdviserName'] = this.adviserName;
    data['IntroducerName'] = this.introducerName;
    data['AdviserNameProfileImageUrl'] = this.adviserNameProfileImageUrl;
    data['IntroducerProfileImageUrl'] = this.introducerProfileImageUrl;
    return data;
  }
}

class MortgageCaseInfoEntityModelList {
  int id;
  int userId;
  int companyId;
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String caseType;
  String customerType;
  String isSmoker;
  String isDoYouHaveAnyFinancialDependants;
  String remarks;
  String isAnyOthers;
  int taskId;
  int coapplicantUserId;
  String customerName;
  String customerEmail;
  String customerMobileNumber;
  String customerAddress;
  String profileImageUrl;
  String namePrefix;
  String areYouBuyingThePropertyInNameOfASPV;
  String companyName;
  String registeredAddress;
  String dateRegistered;
  String companyRegistrationNumber;
  int applicationNumber;
  String customerEmail1;
  String customerEmail2;
  String customerEmail3;
  String isDoYouHaveAnyBTLPortfulio;
  String isClientAgreement;
  double adminFee;
  String adminFeeWhenPayable;
  double adviceFee;
  String adviceFeeWhenPayable;
  String isFeesRefundable;
  String feesRefundable;
  String iPAdddress;
  String iPLocation;
  String deviceType;
  String reportLogo;
  String officePhoneNumber;
  String reportFooter;
  String adviceFeeType;
  String clientAgreementStatus;
  String recommendationAgreementStatus;
  String recommendationAgreementSignature;
  String isThereAnySavingsOrInvestments;
  String isThereAnyExistingPolicy;
  String taskTitleUrl;
  String customerAddress1;
  String customerAddress2;
  String customerAddress3;
  String customerPostcode;
  String customerTown;
  String customerLastName;
  String customerDateofBirth;
  String customerGender;
  String customerAreYouASmoker;
  String occupationCode;
  String isTheCompanyATradingCompany;

  MortgageCaseInfoEntityModelList(
      {this.id,
      this.userId,
      this.companyId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.caseType,
      this.customerType,
      this.isSmoker,
      this.isDoYouHaveAnyFinancialDependants,
      this.remarks,
      this.isAnyOthers,
      this.taskId,
      this.coapplicantUserId,
      this.customerName,
      this.customerEmail,
      this.customerMobileNumber,
      this.customerAddress,
      this.profileImageUrl,
      this.namePrefix,
      this.areYouBuyingThePropertyInNameOfASPV,
      this.companyName,
      this.registeredAddress,
      this.dateRegistered,
      this.companyRegistrationNumber,
      this.applicationNumber,
      this.customerEmail1,
      this.customerEmail2,
      this.customerEmail3,
      this.isDoYouHaveAnyBTLPortfulio,
      this.isClientAgreement,
      this.adminFee,
      this.adminFeeWhenPayable,
      this.adviceFee,
      this.adviceFeeWhenPayable,
      this.isFeesRefundable,
      this.feesRefundable,
      this.iPAdddress,
      this.iPLocation,
      this.deviceType,
      this.reportLogo,
      this.officePhoneNumber,
      this.reportFooter,
      this.adviceFeeType,
      this.clientAgreementStatus,
      this.recommendationAgreementStatus,
      this.recommendationAgreementSignature,
      this.isThereAnySavingsOrInvestments,
      this.isThereAnyExistingPolicy,
      this.taskTitleUrl,
      this.customerAddress1,
      this.customerAddress2,
      this.customerAddress3,
      this.customerPostcode,
      this.customerTown,
      this.customerLastName,
      this.customerDateofBirth,
      this.customerGender,
      this.customerAreYouASmoker,
      this.occupationCode,
      this.isTheCompanyATradingCompany});

  MortgageCaseInfoEntityModelList.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userId = json['UserId'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    caseType = json['CaseType'] ?? '';
    customerType = json['CustomerType'] ?? '';
    isSmoker = json['IsSmoker'] ?? '';
    isDoYouHaveAnyFinancialDependants =
        json['IsDoYouHaveAnyFinancialDependants'] ?? '';
    remarks = json['Remarks'] ?? '';
    isAnyOthers = json['IsAnyOthers'] ?? '';
    taskId = json['TaskId'] ?? 0;
    coapplicantUserId = json['CoapplicantUserId'] ?? 0;
    customerName = json['CustomerName'] ?? '';
    customerEmail = json['CustomerEmail'] ?? '';
    customerMobileNumber = json['CustomerMobileNumber'] ?? '';
    customerAddress = json['CustomerAddress'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    namePrefix = json['NamePrefix'] ?? '';
    areYouBuyingThePropertyInNameOfASPV =
        json['AreYouBuyingThePropertyInNameOfASPV'] ?? '';
    companyName = json['CompanyName'] ?? '';
    registeredAddress = json['RegisteredAddress'] ?? '';
    dateRegistered = json['DateRegistered'] ?? '';
    companyRegistrationNumber = json['CompanyRegistrationNumber'] ?? '';
    applicationNumber = json['ApplicationNumber'] ?? 0;
    customerEmail1 = json['CustomerEmail1'] ?? '';
    customerEmail2 = json['CustomerEmail2'] ?? '';
    customerEmail3 = json['CustomerEmail3'] ?? '';
    isDoYouHaveAnyBTLPortfulio = json['IsDoYouHaveAnyBTLPortfulio'] ?? '';
    isClientAgreement = json['IsClientAgreement'] ?? '';
    adminFee = json['AdminFee'] ?? 0;
    adminFeeWhenPayable = json['AdminFeeWhenPayable'] ?? '';
    adviceFee = json['AdviceFee'] ?? 0;
    adviceFeeWhenPayable = json['AdviceFeeWhenPayable'] ?? '';
    isFeesRefundable = json['IsFeesRefundable'] ?? '';
    feesRefundable = json['FeesRefundable'] ?? '';
    iPAdddress = json['IPAdddress'] ?? '';
    iPLocation = json['IPLocation'] ?? '';
    deviceType = json['DeviceType'] ?? '';
    reportLogo = json['ReportLogo'] ?? '';
    officePhoneNumber = json['OfficePhoneNumber'] ?? '';
    reportFooter = json['ReportFooter'] ?? '';
    adviceFeeType = json['AdviceFeeType'] ?? '';
    clientAgreementStatus = json['ClientAgreementStatus'] ?? '';
    recommendationAgreementStatus = json['RecommendationAgreementStatus'] ?? '';
    recommendationAgreementSignature =
        json['RecommendationAgreementSignature'] ?? '';
    isThereAnySavingsOrInvestments =
        json['IsThereAnySavingsOrInvestments'] ?? '';
    isThereAnyExistingPolicy = json['IsThereAnyExistingPolicy'] ?? '';
    taskTitleUrl = json['TaskTitleUrl'] ?? '';
    customerAddress1 = json['CustomerAddress1'] ?? '';
    customerAddress2 = json['CustomerAddress2'] ?? '';
    customerAddress3 = json['CustomerAddress3'] ?? '';
    customerPostcode = json['CustomerPostcode'] ?? '';
    customerTown = json['CustomerTown'] ?? '';
    customerLastName = json['CustomerLastName'] ?? '';
    customerDateofBirth = json['CustomerDateofBirth'] ?? '';
    customerGender = json['CustomerGender'] ?? '';
    customerAreYouASmoker = json['CustomerAreYouASmoker'] ?? '';
    occupationCode = json['OccupationCode'] ?? '';
    isTheCompanyATradingCompany = json['IsTheCompanyATradingCompany'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserId'] = this.userId;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['CaseType'] = this.caseType;
    data['CustomerType'] = this.customerType;
    data['IsSmoker'] = this.isSmoker;
    data['IsDoYouHaveAnyFinancialDependants'] =
        this.isDoYouHaveAnyFinancialDependants;
    data['Remarks'] = this.remarks;
    data['IsAnyOthers'] = this.isAnyOthers;
    data['TaskId'] = this.taskId;
    data['CoapplicantUserId'] = this.coapplicantUserId;
    data['CustomerName'] = this.customerName;
    data['CustomerEmail'] = this.customerEmail;
    data['CustomerMobileNumber'] = this.customerMobileNumber;
    data['CustomerAddress'] = this.customerAddress;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['NamePrefix'] = this.namePrefix;
    data['AreYouBuyingThePropertyInNameOfASPV'] =
        this.areYouBuyingThePropertyInNameOfASPV;
    data['CompanyName'] = this.companyName;
    data['RegisteredAddress'] = this.registeredAddress;
    data['DateRegistered'] = this.dateRegistered;
    data['CompanyRegistrationNumber'] = this.companyRegistrationNumber;
    data['ApplicationNumber'] = this.applicationNumber;
    data['CustomerEmail1'] = this.customerEmail1;
    data['CustomerEmail2'] = this.customerEmail2;
    data['CustomerEmail3'] = this.customerEmail3;
    data['IsDoYouHaveAnyBTLPortfulio'] = this.isDoYouHaveAnyBTLPortfulio;
    data['IsClientAgreement'] = this.isClientAgreement;
    data['AdminFee'] = this.adminFee;
    data['AdminFeeWhenPayable'] = this.adminFeeWhenPayable;
    data['AdviceFee'] = this.adviceFee;
    data['AdviceFeeWhenPayable'] = this.adviceFeeWhenPayable;
    data['IsFeesRefundable'] = this.isFeesRefundable;
    data['FeesRefundable'] = this.feesRefundable;
    data['IPAdddress'] = this.iPAdddress;
    data['IPLocation'] = this.iPLocation;
    data['DeviceType'] = this.deviceType;
    data['ReportLogo'] = this.reportLogo;
    data['OfficePhoneNumber'] = this.officePhoneNumber;
    data['ReportFooter'] = this.reportFooter;
    data['AdviceFeeType'] = this.adviceFeeType;
    data['ClientAgreementStatus'] = this.clientAgreementStatus;
    data['RecommendationAgreementStatus'] = this.recommendationAgreementStatus;
    data['RecommendationAgreementSignature'] =
        this.recommendationAgreementSignature;
    data['IsThereAnySavingsOrInvestments'] =
        this.isThereAnySavingsOrInvestments;
    data['IsThereAnyExistingPolicy'] = this.isThereAnyExistingPolicy;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['CustomerAddress1'] = this.customerAddress1;
    data['CustomerAddress2'] = this.customerAddress2;
    data['CustomerAddress3'] = this.customerAddress3;
    data['CustomerPostcode'] = this.customerPostcode;
    data['CustomerTown'] = this.customerTown;
    data['CustomerLastName'] = this.customerLastName;
    data['CustomerDateofBirth'] = this.customerDateofBirth;
    data['CustomerGender'] = this.customerGender;
    data['CustomerAreYouASmoker'] = this.customerAreYouASmoker;
    data['OccupationCode'] = this.occupationCode;
    data['IsTheCompanyATradingCompany'] = this.isTheCompanyATradingCompany;
    return data;
  }
}
