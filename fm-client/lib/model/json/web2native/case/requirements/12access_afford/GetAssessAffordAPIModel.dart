import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbility.dart';

class GetAssessAffordAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetAssessAffordAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetAssessAffordAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageUserAssesmentOfAffordAbility mortgageUserAssesmentOfAffordAbility;
  ResponseData({this.mortgageUserAssesmentOfAffordAbility});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserAssesmentOfAffordAbility =
        json['MortgageUserAssesmentOfAffordAbility'] != null
            ? new MortgageUserAssesmentOfAffordAbility.fromJson(
                json['MortgageUserAssesmentOfAffordAbility'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserAssesmentOfAffordAbility != null) {
      data['MortgageUserAssesmentOfAffordAbility'] =
          this.mortgageUserAssesmentOfAffordAbility.toJson();
    }
    return data;
  }
}
