import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistory.dart';

import 'MortgageUserAssesmentOfAffordAbility.dart';

class PostAssessAffordAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  PostAssessAffordAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostAssessAffordAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> assesmentPost;
  Messages({this.assesmentPost});

  Messages.fromJson(Map<String, dynamic> json) {
    assesmentPost = json['assesment_post'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['assesment_post'] = this.assesmentPost;
    return data;
  }
}

class ResponseData {
  MortgageUserAssesmentOfAffordAbility mortgageUserAssesmentOfAffordAbility;
  List<dynamic> mortgageUserAffordAbilitys;
  MortgageUserCreditHistory mortgageUserCreditHistory;

  ResponseData(
      {this.mortgageUserAssesmentOfAffordAbility,
      this.mortgageUserAffordAbilitys,
      this.mortgageUserCreditHistory});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserAssesmentOfAffordAbility =
        json['MortgageUserAssesmentOfAffordAbility'] != null
            ? new MortgageUserAssesmentOfAffordAbility.fromJson(
                json['MortgageUserAssesmentOfAffordAbility'])
            : null;
    if (json['MortgageUserAffordAbilitys'] != null) {
      mortgageUserAffordAbilitys = <Null>[];
      json['MortgageUserAffordAbilitys'].forEach((v) {
        mortgageUserAffordAbilitys.add(v);
      });
    }
    mortgageUserCreditHistory = json['MortgageUserCreditHistory'] != null
        ? new MortgageUserCreditHistory.fromJson(
            json['MortgageUserCreditHistory'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserAssesmentOfAffordAbility != null) {
      data['MortgageUserAssesmentOfAffordAbility'] =
          this.mortgageUserAssesmentOfAffordAbility.toJson();
    }
    if (this.mortgageUserAffordAbilitys != null) {
      data['MortgageUserAffordAbilitys'] =
          this.mortgageUserAffordAbilitys.map((v) => v.toJson()).toList();
    }
    if (this.mortgageUserCreditHistory != null) {
      data['MortgageUserCreditHistory'] =
          this.mortgageUserCreditHistory.toJson();
    }
    return data;
  }
}
