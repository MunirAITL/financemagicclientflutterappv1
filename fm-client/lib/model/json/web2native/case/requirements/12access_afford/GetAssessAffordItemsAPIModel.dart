import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbilityItems.dart';

class GetAssessAffordItemsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetAssessAffordItemsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetAssessAffordItemsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserAssesmentOfAffordAbilityItems>
      mortgageUserAssesmentOfAffordAbilityItems;
  ResponseData({this.mortgageUserAssesmentOfAffordAbilityItems});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserAssesmentOfAffordAbilityItems'] != null) {
      mortgageUserAssesmentOfAffordAbilityItems =
          <MortgageUserAssesmentOfAffordAbilityItems>[];
      json['MortgageUserAssesmentOfAffordAbilityItems'].forEach((v) {
        mortgageUserAssesmentOfAffordAbilityItems
            .add(new MortgageUserAssesmentOfAffordAbilityItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserAssesmentOfAffordAbilityItems != null) {
      data['MortgageUserAssesmentOfAffordAbilityItems'] = this
          .mortgageUserAssesmentOfAffordAbilityItems
          .map((v) => v.toJson())
          .toList();
    }
    return data;
  }
}
