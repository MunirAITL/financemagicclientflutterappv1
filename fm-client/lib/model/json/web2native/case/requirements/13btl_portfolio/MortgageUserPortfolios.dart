class MortgageUserPortfolios {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  String doYouOwnOneOrMoreInvestmentProperties;
  String propertyType;
  String address1;
  String address2;
  String address3;
  String city;
  String country;
  String postCode;
  String datePurchased;
  double purchasePrice;
  double currentValue;
  String lender;
  double mortgageBalance;
  double monthlyMortgage;
  double rentalIncome;
  String notes;
  String remarks;
  String customerName;
  String hMO;
  String owner;
  double lTV;
  String numberofBed;
  String mortgageType;
  String propertyStyle;
  String interestType;
  String currentRateandExpiryDate;
  String mortgageTermRemainingWithCurrentLender;
  double currentMortgageInterestRate;
  String currentFixedPeriodEndDate;
  String securityPropertyTypeOthers;
  String sPVCompanyName;
  int id;

  MortgageUserPortfolios(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.doYouOwnOneOrMoreInvestmentProperties,
      this.propertyType,
      this.address1,
      this.address2,
      this.address3,
      this.city,
      this.country,
      this.postCode,
      this.datePurchased,
      this.purchasePrice,
      this.currentValue,
      this.lender,
      this.mortgageBalance,
      this.monthlyMortgage,
      this.rentalIncome,
      this.notes,
      this.remarks,
      this.customerName,
      this.hMO,
      this.owner,
      this.lTV,
      this.numberofBed,
      this.mortgageType,
      this.propertyStyle,
      this.interestType,
      this.currentRateandExpiryDate,
      this.mortgageTermRemainingWithCurrentLender,
      this.currentMortgageInterestRate,
      this.currentFixedPeriodEndDate,
      this.securityPropertyTypeOthers,
      this.sPVCompanyName,
      this.id});

  MortgageUserPortfolios.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'];
    status = json['Status'];
    mortgageCaseInfoId = json['MortgageCaseInfoId'];
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'];
    doYouOwnOneOrMoreInvestmentProperties =
        json['DoYouOwnOneOrMoreInvestmentProperties'] ?? '';
    propertyType = json['PropertyType'] ?? '';
    address1 = json['Address1'] ?? '';
    address2 = json['Address2'] ?? '';
    address3 = json['Address3'] ?? '';
    city = json['City'] ?? '';
    country = json['Country'] ?? '';
    postCode = json['PostCode'] ?? '';
    datePurchased = json['DatePurchased'] ?? '';
    purchasePrice = json['PurchasePrice'] ?? 0;
    currentValue = json['CurrentValue'] ?? 0;
    lender = json['Lender'] ?? '';
    mortgageBalance = json['MortgageBalance'] ?? 0;
    monthlyMortgage = json['MonthlyMortgage'] ?? 0;
    rentalIncome = json['RentalIncome'] ?? 0;
    notes = json['Notes'] ?? '';
    remarks = json['Remarks'] ?? '';
    customerName = json['CustomerName'] ?? '';
    hMO = json['HMO'] ?? '';
    owner = json['Owner'] ?? '';
    lTV = json['LTV'] ?? 0;
    numberofBed = json['NumberofBed'] ?? '';
    mortgageType = json['MortgageType'] ?? '';
    propertyStyle = json['PropertyStyle'] ?? '';
    interestType = json['InterestType'] ?? '';
    currentRateandExpiryDate = json['CurrentRateandExpiryDate'] ?? '';
    mortgageTermRemainingWithCurrentLender =
        json['MortgageTermRemainingWithCurrentLender'] ?? '';
    currentMortgageInterestRate = json['CurrentMortgageInterestRate'] ?? 0;
    currentFixedPeriodEndDate = json['CurrentFixedPeriodEndDate'] ?? '';
    securityPropertyTypeOthers = json['SecurityPropertyTypeOthers'] ?? '';
    sPVCompanyName = json['SPVCompanyName'] ?? '';
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['DoYouOwnOneOrMoreInvestmentProperties'] =
        this.doYouOwnOneOrMoreInvestmentProperties;
    data['PropertyType'] = this.propertyType;
    data['Address1'] = this.address1;
    data['Address2'] = this.address2;
    data['Address3'] = this.address3;
    data['City'] = this.city;
    data['Country'] = this.country;
    data['PostCode'] = this.postCode;
    data['DatePurchased'] = this.datePurchased;
    data['PurchasePrice'] = this.purchasePrice;
    data['CurrentValue'] = this.currentValue;
    data['Lender'] = this.lender;
    data['MortgageBalance'] = this.mortgageBalance;
    data['MonthlyMortgage'] = this.monthlyMortgage;
    data['RentalIncome'] = this.rentalIncome;
    data['Notes'] = this.notes;
    data['Remarks'] = this.remarks;
    data['CustomerName'] = this.customerName;
    data['HMO'] = this.hMO;
    data['Owner'] = this.owner;
    data['LTV'] = this.lTV;
    data['NumberofBed'] = this.numberofBed;
    data['MortgageType'] = this.mortgageType;
    data['PropertyStyle'] = this.propertyStyle;
    data['InterestType'] = this.interestType;
    data['CurrentRateandExpiryDate'] = this.currentRateandExpiryDate;
    data['MortgageTermRemainingWithCurrentLender'] =
        this.mortgageTermRemainingWithCurrentLender;
    data['CurrentMortgageInterestRate'] = this.currentMortgageInterestRate;
    data['CurrentFixedPeriodEndDate'] = this.currentFixedPeriodEndDate;
    data['SecurityPropertyTypeOthers'] = this.securityPropertyTypeOthers;
    data['SPVCompanyName'] = this.sPVCompanyName;
    data['Id'] = this.id;
    return data;
  }
}
