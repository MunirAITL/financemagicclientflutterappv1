import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';

class GetBtlPortfolioAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetBtlPortfolioAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetBtlPortfolioAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageUserPortfolios> mortgageUserPortfolios;
  ResponseData({this.mortgageUserPortfolios});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageUserPortfolios'] != null) {
      mortgageUserPortfolios = <MortgageUserPortfolios>[];
      json['MortgageUserPortfolios'].forEach((v) {
        mortgageUserPortfolios.add(new MortgageUserPortfolios.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserPortfolios != null) {
      data['MortgageUserPortfolios'] =
          this.mortgageUserPortfolios.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
