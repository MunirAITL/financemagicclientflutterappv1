class MortgageFinancialDependantsModel {
  int userId;
  dynamic user;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  int companyId;
  String name;
  String dateOfBirth;
  String relationship;
  String livingWith;
  String remarks;
  String isTheDependantSameAsFirstApplicant;
  String custmerRelationShipWithAnotherCustomer;
  int customerId;
  String customerName;
  int age;
  int id;

  MortgageFinancialDependantsModel(
      {this.userId,
      this.user,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.companyId,
      this.name,
      this.dateOfBirth,
      this.relationship,
      this.livingWith,
      this.remarks,
      this.isTheDependantSameAsFirstApplicant,
      this.custmerRelationShipWithAnotherCustomer,
      this.customerId,
      this.customerName,
      this.age,
      this.id});

  MortgageFinancialDependantsModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'] ?? 0;
    companyId = json['CompanyId'] ?? 0;
    name = json['Name'] ?? '';
    dateOfBirth = json['DateOfBirth'] ?? '';
    relationship = json['Relationship'] ?? '';
    livingWith = json['LivingWith'] ?? '';
    remarks = json['Remarks'] ?? '';
    isTheDependantSameAsFirstApplicant =
        json['IsTheDependantSameAsFirstApplicant'] ?? '';
    custmerRelationShipWithAnotherCustomer =
        json['CustmerRelationShipWithAnotherCustomer'] ?? '';
    customerId = json['CustomerId'] ?? 0;
    customerName = json['CustomerName'] ?? '';
    age = json['Age'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['CompanyId'] = this.companyId;
    data['Name'] = this.name;
    data['DateOfBirth'] = this.dateOfBirth;
    data['Relationship'] = this.relationship;
    data['LivingWith'] = this.livingWith;
    data['Remarks'] = this.remarks;
    data['IsTheDependantSameAsFirstApplicant'] =
        this.isTheDependantSameAsFirstApplicant;
    data['CustmerRelationShipWithAnotherCustomer'] =
        this.custmerRelationShipWithAnotherCustomer;
    data['CustomerId'] = this.customerId;
    data['CustomerName'] = this.customerName;
    data['Age'] = this.age;
    data['Id'] = this.id;
    return data;
  }
}
