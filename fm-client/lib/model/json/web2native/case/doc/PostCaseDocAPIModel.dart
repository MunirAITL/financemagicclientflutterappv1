import 'package:aitl/model/json/db_cus/action_req/GetMortgageCaseInfoLocTaskidUseridAPIModel.dart';
import 'package:aitl/model/json/web2native/case/doc/MortgageCaseDocumentInfos.dart';

class PostCaseDocAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostCaseDocAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostCaseDocAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageCaseDocumentInfos mortgageCaseDocumentInfo;
  ResponseData({this.mortgageCaseDocumentInfo});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageCaseDocumentInfo = json['MortgageCaseDocumentInfo'] != null
        ? new MortgageCaseDocumentInfos.fromJson(
            json['MortgageCaseDocumentInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageCaseDocumentInfo != null) {
      data['MortgageCaseDocumentInfo'] = this.mortgageCaseDocumentInfo.toJson();
    }
    return data;
  }
}
