class MortgageCaseDocumentInfos {
  int userId;
  int taskId;
  int status;
  String creationDate;
  String updatedDate;
  String name;
  String purpose;
  String type;
  String expiryDate;
  String remarks;
  String documentUrl;
  int entityId;
  String entityName;
  String mimeType;
  String referenceName;
  String referenceId;
  int initiatorId;
  String initiatorName;
  bool isSelected;
  String documentCategory;
  String isRequiredDocuments;
  bool isVerified;
  int id;

  MortgageCaseDocumentInfos(
      {this.userId,
      this.taskId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.name,
      this.purpose,
      this.type,
      this.expiryDate,
      this.remarks,
      this.documentUrl,
      this.entityId,
      this.entityName,
      this.mimeType,
      this.referenceName,
      this.referenceId,
      this.initiatorId,
      this.initiatorName,
      this.isSelected,
      this.documentCategory,
      this.isRequiredDocuments,
      this.isVerified,
      this.id});

  MortgageCaseDocumentInfos.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    taskId = json['TaskId'] ?? 0;
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    name = json['Name'] ?? '';
    purpose = json['Purpose'] ?? '';
    type = json['Type'] ?? '';
    expiryDate = json['ExpiryDate'] ?? '';
    remarks = json['Remarks'] ?? '';
    documentUrl = json['DocumentUrl'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    mimeType = json['MimeType'] ?? '';
    referenceName = json['ReferenceName'] ?? '';
    referenceId = json['ReferenceId'] ?? '';
    initiatorId = json['InitiatorId'] ?? 0;
    initiatorName = json['InitiatorName'] ?? '';
    isSelected = json['IsSelected'] ?? false;
    documentCategory = json['DocumentCategory'] ?? '';
    isRequiredDocuments = json['IsRequiredDocuments'] ?? '';
    isVerified = json['IsVerified'] ?? false;
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['TaskId'] = this.taskId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['Name'] = this.name;
    data['Purpose'] = this.purpose;
    data['Type'] = this.type;
    data['ExpiryDate'] = this.expiryDate;
    data['Remarks'] = this.remarks;
    data['DocumentUrl'] = this.documentUrl;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['MimeType'] = this.mimeType;
    data['ReferenceName'] = this.referenceName;
    data['ReferenceId'] = this.referenceId;
    data['InitiatorId'] = this.initiatorId;
    data['InitiatorName'] = this.initiatorName;
    data['IsSelected'] = this.isSelected;
    data['DocumentCategory'] = this.documentCategory;
    data['IsRequiredDocuments'] = this.isRequiredDocuments;
    data['IsVerified'] = this.isVerified;
    data['Id'] = this.id;
    return data;
  }
}
