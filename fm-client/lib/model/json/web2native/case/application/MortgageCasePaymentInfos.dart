class MortgageCasePaymentInfos {
  int userId;
  dynamic user;
  int companyId;
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String defineSolution;
  String lenderProvider;
  String term;
  double loanAmount;
  double rate;
  double value;
  String fixedPeriodEnds;
  double procFees;
  double adviceFee;
  double introducerFeeShare;
  double adviserFeeShare;
  double netIncomeAmount;
  String introducerPaymentMade;
  String reasonForRecommendation;
  String anyOtherComments;
  String remarks;
  int taskId;
  double monthlyPaymentAmount;
  double otherChargeAmount;
  double rentalIncomeAmount;
  String valuationDate;
  String fileUrl;
  String termMonth;
  String interestOnlyLoan;
  String addingFeesToTheLoan;
  String lendingIntoRetirement;
  int lenderProductId;
  int mortgageClubId;
  String lenderProductName;
  String premiumType;
  String premiumBasis;
  String benefitType;
  String basis;
  String deferredPeriod;
  double durationofBenefitPayable;
  double benefitPayable;
  double totalPremiumsOverTerm;
  double monthlyPremiumPayable;
  String coverType;
  String provider;
  String affordabilityDetails;
  String waiverOfPremiumDetails;
  String otherNeedsIdentifiedDetails;
  String reCapCurrentSituationDetails;
  String yourExistingLifeCoverArrangementsDetails;
  String trustsDetails;
  String reasonforChoosingProviderDetails;
  String objectiveDetails;
  String howWeAreRemuneratedDetails;
  String alternativeIncomeProtectionOptionsConsideredDetails;
  String deferredPeriodDetails;
  String benefitTermsDetails;
  String policyTermDetails;
  String amountOfBenefitPaybleDetails;
  String benefitCoverBasis;
  String criticalIllnessCoverSelectedFirstEvent;
  String waiverOfPremiumBenefit;
  String isObjectiveDetails;
  String isHowWeAreRemuneratedDetails;
  String isAlternativeIncomeProtectionOptionsConsideredDetails;
  String isDeferredPeriodDetails;
  String isBenefitTermsDetails;
  String isPolicyTermDetails;
  String isAmountOfBenefitPaybleDetails;
  String isBenefitCoverBasis;
  String isCriticalIllnessCoverSelectedFirstEvent;
  String isWaiverOfPremiumBenefit;
  String whatAreTheRisks;
  String underwriting;
  String coolingOffPeriod;
  String dutyOfDisclosure;
  String makingAClaim;
  String noticeOfCost;
  String wills;
  String reviews;
  String isWhatAreTheRisks;
  String isUnderwriting;
  String isCoolingOffPeriod;
  String isDutyOfDisclosure;
  String isMakingAClaim;
  String isNoticeOfCost;
  String isWills;
  String isReviews;
  String isAffordabilityDetails;
  String isReCapCurrentSituationDetails;
  String isOtherNeedsIdentified;
  String isYourExistingLifeCoverArrangementsDetails;
  String isTrustsDetails;
  String isReasonforChoosingProviderDetails;
  String letterMainBody;
  String letterClosing;
  String appendix;
  String interestOnlyLoanDetails;
  String addingFeesToTheLoanDetails;
  String lendingIntoRetirementDetails;
  double commission;
  double commissionRate;
  String commissionBasis;
  String declareACommissionToClient;
  String specialProduct;
  String vulnerableClient;
  String isVulnerableClient;
  int entityId;
  String entityName;
  String scopeOfAdvice;
  String yourCircumstances;
  String repaymentMethod;
  String mortgageTerm;
  String mortgageType;
  String changestoPersonalCircumstances;
  String fees;
  String creditHistory;
  String propertyValue;
  String exitPenaltyfromExistingMortgage;
  String earlyRepaymentsOrOverpayments;
  String jointAndSeveralLiability;
  String nextSteps;
  String solicitorsReferral;
  String isScopeOfAdvice;
  String isYourCircumstances;
  String isRepaymentMethod;
  String isMortgageTerm;
  String isMortgageType;
  String isChangestoPersonalCircumstances;
  String isFees;
  String isCreditHistory;
  String isPropertyValue;
  String isExitPenaltyfromExistingMortgage;
  String isEarlyRepaymentsOrOverpayments;
  String isJointAndSeveralLiability;
  String isNextSteps;
  String isSolicitorsReferral;
  double introducerSecondFeeShare;
  String riskofAddingDebttoMortgage;
  String bindingOfferOrReflectionPeriod;
  String portingMortgage;
  String productTransfer;
  String isRiskofAddingDebttoMortgage;
  String isBindingOfferOrReflectionPeriod;
  String isPortingMortgage;
  String isProductTransfer;
  String isGenerateLetter;
  double coverAmount;
  String templateType;
  String fullFormatReport;
  int caseActionStatus;
  String isDoYouWantToSingleLetter;
  String policyNumber;
  double adminFee;
  String fileUrlList;
  String rCreationDate;
  String mortgageFinancialDependantsEntityModelList;
  int id;

  MortgageCasePaymentInfos(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.defineSolution,
      this.lenderProvider,
      this.term,
      this.loanAmount,
      this.rate,
      this.value,
      this.fixedPeriodEnds,
      this.procFees,
      this.adviceFee,
      this.introducerFeeShare,
      this.adviserFeeShare,
      this.netIncomeAmount,
      this.introducerPaymentMade,
      this.reasonForRecommendation,
      this.anyOtherComments,
      this.remarks,
      this.taskId,
      this.monthlyPaymentAmount,
      this.otherChargeAmount,
      this.rentalIncomeAmount,
      this.valuationDate,
      this.fileUrl,
      this.termMonth,
      this.interestOnlyLoan,
      this.addingFeesToTheLoan,
      this.lendingIntoRetirement,
      this.lenderProductId,
      this.mortgageClubId,
      this.lenderProductName,
      this.premiumType,
      this.premiumBasis,
      this.benefitType,
      this.basis,
      this.deferredPeriod,
      this.durationofBenefitPayable,
      this.benefitPayable,
      this.totalPremiumsOverTerm,
      this.monthlyPremiumPayable,
      this.coverType,
      this.provider,
      this.affordabilityDetails,
      this.waiverOfPremiumDetails,
      this.otherNeedsIdentifiedDetails,
      this.reCapCurrentSituationDetails,
      this.yourExistingLifeCoverArrangementsDetails,
      this.trustsDetails,
      this.reasonforChoosingProviderDetails,
      this.objectiveDetails,
      this.howWeAreRemuneratedDetails,
      this.alternativeIncomeProtectionOptionsConsideredDetails,
      this.deferredPeriodDetails,
      this.benefitTermsDetails,
      this.policyTermDetails,
      this.amountOfBenefitPaybleDetails,
      this.benefitCoverBasis,
      this.criticalIllnessCoverSelectedFirstEvent,
      this.waiverOfPremiumBenefit,
      this.isObjectiveDetails,
      this.isHowWeAreRemuneratedDetails,
      this.isAlternativeIncomeProtectionOptionsConsideredDetails,
      this.isDeferredPeriodDetails,
      this.isBenefitTermsDetails,
      this.isPolicyTermDetails,
      this.isAmountOfBenefitPaybleDetails,
      this.isBenefitCoverBasis,
      this.isCriticalIllnessCoverSelectedFirstEvent,
      this.isWaiverOfPremiumBenefit,
      this.whatAreTheRisks,
      this.underwriting,
      this.coolingOffPeriod,
      this.dutyOfDisclosure,
      this.makingAClaim,
      this.noticeOfCost,
      this.wills,
      this.reviews,
      this.isWhatAreTheRisks,
      this.isUnderwriting,
      this.isCoolingOffPeriod,
      this.isDutyOfDisclosure,
      this.isMakingAClaim,
      this.isNoticeOfCost,
      this.isWills,
      this.isReviews,
      this.isAffordabilityDetails,
      this.isReCapCurrentSituationDetails,
      this.isOtherNeedsIdentified,
      this.isYourExistingLifeCoverArrangementsDetails,
      this.isTrustsDetails,
      this.isReasonforChoosingProviderDetails,
      this.letterMainBody,
      this.letterClosing,
      this.appendix,
      this.interestOnlyLoanDetails,
      this.addingFeesToTheLoanDetails,
      this.lendingIntoRetirementDetails,
      this.commission,
      this.commissionRate,
      this.commissionBasis,
      this.declareACommissionToClient,
      this.specialProduct,
      this.vulnerableClient,
      this.isVulnerableClient,
      this.entityId,
      this.entityName,
      this.scopeOfAdvice,
      this.yourCircumstances,
      this.repaymentMethod,
      this.mortgageTerm,
      this.mortgageType,
      this.changestoPersonalCircumstances,
      this.fees,
      this.creditHistory,
      this.propertyValue,
      this.exitPenaltyfromExistingMortgage,
      this.earlyRepaymentsOrOverpayments,
      this.jointAndSeveralLiability,
      this.nextSteps,
      this.solicitorsReferral,
      this.isScopeOfAdvice,
      this.isYourCircumstances,
      this.isRepaymentMethod,
      this.isMortgageTerm,
      this.isMortgageType,
      this.isChangestoPersonalCircumstances,
      this.isFees,
      this.isCreditHistory,
      this.isPropertyValue,
      this.isExitPenaltyfromExistingMortgage,
      this.isEarlyRepaymentsOrOverpayments,
      this.isJointAndSeveralLiability,
      this.isNextSteps,
      this.isSolicitorsReferral,
      this.introducerSecondFeeShare,
      this.riskofAddingDebttoMortgage,
      this.bindingOfferOrReflectionPeriod,
      this.portingMortgage,
      this.productTransfer,
      this.isRiskofAddingDebttoMortgage,
      this.isBindingOfferOrReflectionPeriod,
      this.isPortingMortgage,
      this.isProductTransfer,
      this.isGenerateLetter,
      this.coverAmount,
      this.templateType,
      this.fullFormatReport,
      this.caseActionStatus,
      this.isDoYouWantToSingleLetter,
      this.policyNumber,
      this.adminFee,
      this.fileUrlList,
      this.rCreationDate,
      this.mortgageFinancialDependantsEntityModelList,
      this.id});

  MortgageCasePaymentInfos.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    defineSolution = json['DefineSolution'];
    lenderProvider = json['LenderProvider'];
    term = json['Term'];
    loanAmount = json['LoanAmount'];
    rate = json['Rate'];
    value = json['Value'];
    fixedPeriodEnds = json['FixedPeriodEnds'];
    procFees = json['ProcFees'];
    adviceFee = json['AdviceFee'];
    introducerFeeShare = json['IntroducerFeeShare'];
    adviserFeeShare = json['AdviserFeeShare'];
    netIncomeAmount = json['NetIncomeAmount'];
    introducerPaymentMade = json['IntroducerPaymentMade'];
    reasonForRecommendation = json['ReasonForRecommendation'];
    anyOtherComments = json['AnyOtherComments'];
    remarks = json['Remarks'];
    taskId = json['TaskId'];
    monthlyPaymentAmount = json['MonthlyPaymentAmount'];
    otherChargeAmount = json['OtherChargeAmount'];
    rentalIncomeAmount = json['RentalIncomeAmount'];
    valuationDate = json['ValuationDate'];
    fileUrl = json['FileUrl'];
    termMonth = json['TermMonth'];
    interestOnlyLoan = json['InterestOnlyLoan'];
    addingFeesToTheLoan = json['AddingFeesToTheLoan'];
    lendingIntoRetirement = json['LendingIntoRetirement'];
    lenderProductId = json['LenderProductId'];
    mortgageClubId = json['MortgageClubId'];
    lenderProductName = json['LenderProductName'];
    premiumType = json['PremiumType'];
    premiumBasis = json['PremiumBasis'];
    benefitType = json['BenefitType'];
    basis = json['Basis'];
    deferredPeriod = json['DeferredPeriod'];
    durationofBenefitPayable = json['DurationofBenefitPayable'];
    benefitPayable = json['BenefitPayable'];
    totalPremiumsOverTerm = json['TotalPremiumsOverTerm'];
    monthlyPremiumPayable = json['MonthlyPremiumPayable'];
    coverType = json['CoverType'];
    provider = json['Provider'];
    affordabilityDetails = json['AffordabilityDetails'];
    waiverOfPremiumDetails = json['WaiverOfPremiumDetails'];
    otherNeedsIdentifiedDetails = json['OtherNeedsIdentifiedDetails'];
    reCapCurrentSituationDetails = json['ReCapCurrentSituationDetails'];
    yourExistingLifeCoverArrangementsDetails =
        json['YourExistingLifeCoverArrangementsDetails'];
    trustsDetails = json['TrustsDetails'];
    reasonforChoosingProviderDetails = json['ReasonforChoosingProviderDetails'];
    objectiveDetails = json['ObjectiveDetails'];
    howWeAreRemuneratedDetails = json['HowWeAreRemuneratedDetails'];
    alternativeIncomeProtectionOptionsConsideredDetails =
        json['AlternativeIncomeProtectionOptionsConsideredDetails'];
    deferredPeriodDetails = json['DeferredPeriodDetails'];
    benefitTermsDetails = json['BenefitTermsDetails'];
    policyTermDetails = json['PolicyTermDetails'];
    amountOfBenefitPaybleDetails = json['AmountOfBenefitPaybleDetails'];
    benefitCoverBasis = json['BenefitCoverBasis'];
    criticalIllnessCoverSelectedFirstEvent =
        json['CriticalIllnessCoverSelectedFirstEvent'];
    waiverOfPremiumBenefit = json['WaiverOfPremiumBenefit'];
    isObjectiveDetails = json['IsObjectiveDetails'];
    isHowWeAreRemuneratedDetails = json['IsHowWeAreRemuneratedDetails'];
    isAlternativeIncomeProtectionOptionsConsideredDetails =
        json['IsAlternativeIncomeProtectionOptionsConsideredDetails'];
    isDeferredPeriodDetails = json['IsDeferredPeriodDetails'];
    isBenefitTermsDetails = json['IsBenefitTermsDetails'];
    isPolicyTermDetails = json['IsPolicyTermDetails'];
    isAmountOfBenefitPaybleDetails = json['IsAmountOfBenefitPaybleDetails'];
    isBenefitCoverBasis = json['IsBenefitCoverBasis'];
    isCriticalIllnessCoverSelectedFirstEvent =
        json['IsCriticalIllnessCoverSelectedFirstEvent'];
    isWaiverOfPremiumBenefit = json['IsWaiverOfPremiumBenefit'];
    whatAreTheRisks = json['WhatAreTheRisks'];
    underwriting = json['Underwriting'];
    coolingOffPeriod = json['CoolingOffPeriod'];
    dutyOfDisclosure = json['DutyOfDisclosure'];
    makingAClaim = json['MakingAClaim'];
    noticeOfCost = json['NoticeOfCost'];
    wills = json['Wills'];
    reviews = json['Reviews'];
    isWhatAreTheRisks = json['IsWhatAreTheRisks'];
    isUnderwriting = json['IsUnderwriting'];
    isCoolingOffPeriod = json['IsCoolingOffPeriod'];
    isDutyOfDisclosure = json['IsDutyOfDisclosure'];
    isMakingAClaim = json['IsMakingAClaim'];
    isNoticeOfCost = json['IsNoticeOfCost'];
    isWills = json['IsWills'];
    isReviews = json['IsReviews'];
    isAffordabilityDetails = json['IsAffordabilityDetails'];
    isReCapCurrentSituationDetails = json['IsReCapCurrentSituationDetails'];
    isOtherNeedsIdentified = json['IsOtherNeedsIdentified'];
    isYourExistingLifeCoverArrangementsDetails =
        json['IsYourExistingLifeCoverArrangementsDetails'];
    isTrustsDetails = json['IsTrustsDetails'];
    isReasonforChoosingProviderDetails =
        json['IsReasonforChoosingProviderDetails'];
    letterMainBody = json['LetterMainBody'];
    letterClosing = json['LetterClosing'];
    appendix = json['Appendix'];
    interestOnlyLoanDetails = json['InterestOnlyLoanDetails'];
    addingFeesToTheLoanDetails = json['AddingFeesToTheLoanDetails'];
    lendingIntoRetirementDetails = json['LendingIntoRetirementDetails'];
    commission = json['Commission'];
    commissionRate = json['CommissionRate'];
    commissionBasis = json['CommissionBasis'];
    declareACommissionToClient = json['DeclareACommissionToClient'];
    specialProduct = json['SpecialProduct'];
    vulnerableClient = json['VulnerableClient'];
    isVulnerableClient = json['IsVulnerableClient'];
    entityId = json['EntityId'];
    entityName = json['EntityName'];
    scopeOfAdvice = json['ScopeOfAdvice'];
    yourCircumstances = json['YourCircumstances'];
    repaymentMethod = json['RepaymentMethod'];
    mortgageTerm = json['MortgageTerm'];
    mortgageType = json['MortgageType'];
    changestoPersonalCircumstances = json['ChangestoPersonalCircumstances'];
    fees = json['Fees'];
    creditHistory = json['CreditHistory'];
    propertyValue = json['PropertyValue'];
    exitPenaltyfromExistingMortgage = json['ExitPenaltyfromExistingMortgage'];
    earlyRepaymentsOrOverpayments = json['EarlyRepaymentsOrOverpayments'];
    jointAndSeveralLiability = json['JointAndSeveralLiability'];
    nextSteps = json['NextSteps'];
    solicitorsReferral = json['SolicitorsReferral'];
    isScopeOfAdvice = json['IsScopeOfAdvice'];
    isYourCircumstances = json['IsYourCircumstances'];
    isRepaymentMethod = json['IsRepaymentMethod'];
    isMortgageTerm = json['IsMortgageTerm'];
    isMortgageType = json['IsMortgageType'];
    isChangestoPersonalCircumstances = json['IsChangestoPersonalCircumstances'];
    isFees = json['IsFees'];
    isCreditHistory = json['IsCreditHistory'];
    isPropertyValue = json['IsPropertyValue'];
    isExitPenaltyfromExistingMortgage =
        json['IsExitPenaltyfromExistingMortgage'];
    isEarlyRepaymentsOrOverpayments = json['IsEarlyRepaymentsOrOverpayments'];
    isJointAndSeveralLiability = json['IsJointAndSeveralLiability'];
    isNextSteps = json['IsNextSteps'];
    isSolicitorsReferral = json['IsSolicitorsReferral'];
    introducerSecondFeeShare = json['IntroducerSecondFeeShare'];
    riskofAddingDebttoMortgage = json['RiskofAddingDebttoMortgage'];
    bindingOfferOrReflectionPeriod = json['BindingOfferOrReflectionPeriod'];
    portingMortgage = json['PortingMortgage'];
    productTransfer = json['ProductTransfer'];
    isRiskofAddingDebttoMortgage = json['IsRiskofAddingDebttoMortgage'];
    isBindingOfferOrReflectionPeriod = json['IsBindingOfferOrReflectionPeriod'];
    isPortingMortgage = json['IsPortingMortgage'];
    isProductTransfer = json['IsProductTransfer'];
    isGenerateLetter = json['IsGenerateLetter'];
    coverAmount = json['CoverAmount'];
    templateType = json['TemplateType'];
    fullFormatReport = json['FullFormatReport'];
    caseActionStatus = json['CaseActionStatus'];
    isDoYouWantToSingleLetter = json['IsDoYouWantToSingleLetter'];
    policyNumber = json['PolicyNumber'];
    adminFee = json['AdminFee'];
    fileUrlList = json['FileUrlList'];
    rCreationDate = json['RCreationDate'];
    mortgageFinancialDependantsEntityModelList =
        json['MortgageFinancialDependantsEntityModelList'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['DefineSolution'] = this.defineSolution;
    data['LenderProvider'] = this.lenderProvider;
    data['Term'] = this.term;
    data['LoanAmount'] = this.loanAmount;
    data['Rate'] = this.rate;
    data['Value'] = this.value;
    data['FixedPeriodEnds'] = this.fixedPeriodEnds;
    data['ProcFees'] = this.procFees;
    data['AdviceFee'] = this.adviceFee;
    data['IntroducerFeeShare'] = this.introducerFeeShare;
    data['AdviserFeeShare'] = this.adviserFeeShare;
    data['NetIncomeAmount'] = this.netIncomeAmount;
    data['IntroducerPaymentMade'] = this.introducerPaymentMade;
    data['ReasonForRecommendation'] = this.reasonForRecommendation;
    data['AnyOtherComments'] = this.anyOtherComments;
    data['Remarks'] = this.remarks;
    data['TaskId'] = this.taskId;
    data['MonthlyPaymentAmount'] = this.monthlyPaymentAmount;
    data['OtherChargeAmount'] = this.otherChargeAmount;
    data['RentalIncomeAmount'] = this.rentalIncomeAmount;
    data['ValuationDate'] = this.valuationDate;
    data['FileUrl'] = this.fileUrl;
    data['TermMonth'] = this.termMonth;
    data['InterestOnlyLoan'] = this.interestOnlyLoan;
    data['AddingFeesToTheLoan'] = this.addingFeesToTheLoan;
    data['LendingIntoRetirement'] = this.lendingIntoRetirement;
    data['LenderProductId'] = this.lenderProductId;
    data['MortgageClubId'] = this.mortgageClubId;
    data['LenderProductName'] = this.lenderProductName;
    data['PremiumType'] = this.premiumType;
    data['PremiumBasis'] = this.premiumBasis;
    data['BenefitType'] = this.benefitType;
    data['Basis'] = this.basis;
    data['DeferredPeriod'] = this.deferredPeriod;
    data['DurationofBenefitPayable'] = this.durationofBenefitPayable;
    data['BenefitPayable'] = this.benefitPayable;
    data['TotalPremiumsOverTerm'] = this.totalPremiumsOverTerm;
    data['MonthlyPremiumPayable'] = this.monthlyPremiumPayable;
    data['CoverType'] = this.coverType;
    data['Provider'] = this.provider;
    data['AffordabilityDetails'] = this.affordabilityDetails;
    data['WaiverOfPremiumDetails'] = this.waiverOfPremiumDetails;
    data['OtherNeedsIdentifiedDetails'] = this.otherNeedsIdentifiedDetails;
    data['ReCapCurrentSituationDetails'] = this.reCapCurrentSituationDetails;
    data['YourExistingLifeCoverArrangementsDetails'] =
        this.yourExistingLifeCoverArrangementsDetails;
    data['TrustsDetails'] = this.trustsDetails;
    data['ReasonforChoosingProviderDetails'] =
        this.reasonforChoosingProviderDetails;
    data['ObjectiveDetails'] = this.objectiveDetails;
    data['HowWeAreRemuneratedDetails'] = this.howWeAreRemuneratedDetails;
    data['AlternativeIncomeProtectionOptionsConsideredDetails'] =
        this.alternativeIncomeProtectionOptionsConsideredDetails;
    data['DeferredPeriodDetails'] = this.deferredPeriodDetails;
    data['BenefitTermsDetails'] = this.benefitTermsDetails;
    data['PolicyTermDetails'] = this.policyTermDetails;
    data['AmountOfBenefitPaybleDetails'] = this.amountOfBenefitPaybleDetails;
    data['BenefitCoverBasis'] = this.benefitCoverBasis;
    data['CriticalIllnessCoverSelectedFirstEvent'] =
        this.criticalIllnessCoverSelectedFirstEvent;
    data['WaiverOfPremiumBenefit'] = this.waiverOfPremiumBenefit;
    data['IsObjectiveDetails'] = this.isObjectiveDetails;
    data['IsHowWeAreRemuneratedDetails'] = this.isHowWeAreRemuneratedDetails;
    data['IsAlternativeIncomeProtectionOptionsConsideredDetails'] =
        this.isAlternativeIncomeProtectionOptionsConsideredDetails;
    data['IsDeferredPeriodDetails'] = this.isDeferredPeriodDetails;
    data['IsBenefitTermsDetails'] = this.isBenefitTermsDetails;
    data['IsPolicyTermDetails'] = this.isPolicyTermDetails;
    data['IsAmountOfBenefitPaybleDetails'] =
        this.isAmountOfBenefitPaybleDetails;
    data['IsBenefitCoverBasis'] = this.isBenefitCoverBasis;
    data['IsCriticalIllnessCoverSelectedFirstEvent'] =
        this.isCriticalIllnessCoverSelectedFirstEvent;
    data['IsWaiverOfPremiumBenefit'] = this.isWaiverOfPremiumBenefit;
    data['WhatAreTheRisks'] = this.whatAreTheRisks;
    data['Underwriting'] = this.underwriting;
    data['CoolingOffPeriod'] = this.coolingOffPeriod;
    data['DutyOfDisclosure'] = this.dutyOfDisclosure;
    data['MakingAClaim'] = this.makingAClaim;
    data['NoticeOfCost'] = this.noticeOfCost;
    data['Wills'] = this.wills;
    data['Reviews'] = this.reviews;
    data['IsWhatAreTheRisks'] = this.isWhatAreTheRisks;
    data['IsUnderwriting'] = this.isUnderwriting;
    data['IsCoolingOffPeriod'] = this.isCoolingOffPeriod;
    data['IsDutyOfDisclosure'] = this.isDutyOfDisclosure;
    data['IsMakingAClaim'] = this.isMakingAClaim;
    data['IsNoticeOfCost'] = this.isNoticeOfCost;
    data['IsWills'] = this.isWills;
    data['IsReviews'] = this.isReviews;
    data['IsAffordabilityDetails'] = this.isAffordabilityDetails;
    data['IsReCapCurrentSituationDetails'] =
        this.isReCapCurrentSituationDetails;
    data['IsOtherNeedsIdentified'] = this.isOtherNeedsIdentified;
    data['IsYourExistingLifeCoverArrangementsDetails'] =
        this.isYourExistingLifeCoverArrangementsDetails;
    data['IsTrustsDetails'] = this.isTrustsDetails;
    data['IsReasonforChoosingProviderDetails'] =
        this.isReasonforChoosingProviderDetails;
    data['LetterMainBody'] = this.letterMainBody;
    data['LetterClosing'] = this.letterClosing;
    data['Appendix'] = this.appendix;
    data['InterestOnlyLoanDetails'] = this.interestOnlyLoanDetails;
    data['AddingFeesToTheLoanDetails'] = this.addingFeesToTheLoanDetails;
    data['LendingIntoRetirementDetails'] = this.lendingIntoRetirementDetails;
    data['Commission'] = this.commission;
    data['CommissionRate'] = this.commissionRate;
    data['CommissionBasis'] = this.commissionBasis;
    data['DeclareACommissionToClient'] = this.declareACommissionToClient;
    data['SpecialProduct'] = this.specialProduct;
    data['VulnerableClient'] = this.vulnerableClient;
    data['IsVulnerableClient'] = this.isVulnerableClient;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['ScopeOfAdvice'] = this.scopeOfAdvice;
    data['YourCircumstances'] = this.yourCircumstances;
    data['RepaymentMethod'] = this.repaymentMethod;
    data['MortgageTerm'] = this.mortgageTerm;
    data['MortgageType'] = this.mortgageType;
    data['ChangestoPersonalCircumstances'] =
        this.changestoPersonalCircumstances;
    data['Fees'] = this.fees;
    data['CreditHistory'] = this.creditHistory;
    data['PropertyValue'] = this.propertyValue;
    data['ExitPenaltyfromExistingMortgage'] =
        this.exitPenaltyfromExistingMortgage;
    data['EarlyRepaymentsOrOverpayments'] = this.earlyRepaymentsOrOverpayments;
    data['JointAndSeveralLiability'] = this.jointAndSeveralLiability;
    data['NextSteps'] = this.nextSteps;
    data['SolicitorsReferral'] = this.solicitorsReferral;
    data['IsScopeOfAdvice'] = this.isScopeOfAdvice;
    data['IsYourCircumstances'] = this.isYourCircumstances;
    data['IsRepaymentMethod'] = this.isRepaymentMethod;
    data['IsMortgageTerm'] = this.isMortgageTerm;
    data['IsMortgageType'] = this.isMortgageType;
    data['IsChangestoPersonalCircumstances'] =
        this.isChangestoPersonalCircumstances;
    data['IsFees'] = this.isFees;
    data['IsCreditHistory'] = this.isCreditHistory;
    data['IsPropertyValue'] = this.isPropertyValue;
    data['IsExitPenaltyfromExistingMortgage'] =
        this.isExitPenaltyfromExistingMortgage;
    data['IsEarlyRepaymentsOrOverpayments'] =
        this.isEarlyRepaymentsOrOverpayments;
    data['IsJointAndSeveralLiability'] = this.isJointAndSeveralLiability;
    data['IsNextSteps'] = this.isNextSteps;
    data['IsSolicitorsReferral'] = this.isSolicitorsReferral;
    data['IntroducerSecondFeeShare'] = this.introducerSecondFeeShare;
    data['RiskofAddingDebttoMortgage'] = this.riskofAddingDebttoMortgage;
    data['BindingOfferOrReflectionPeriod'] =
        this.bindingOfferOrReflectionPeriod;
    data['PortingMortgage'] = this.portingMortgage;
    data['ProductTransfer'] = this.productTransfer;
    data['IsRiskofAddingDebttoMortgage'] = this.isRiskofAddingDebttoMortgage;
    data['IsBindingOfferOrReflectionPeriod'] =
        this.isBindingOfferOrReflectionPeriod;
    data['IsPortingMortgage'] = this.isPortingMortgage;
    data['IsProductTransfer'] = this.isProductTransfer;
    data['IsGenerateLetter'] = this.isGenerateLetter;
    data['CoverAmount'] = this.coverAmount;
    data['TemplateType'] = this.templateType;
    data['FullFormatReport'] = this.fullFormatReport;
    data['CaseActionStatus'] = this.caseActionStatus;
    data['IsDoYouWantToSingleLetter'] = this.isDoYouWantToSingleLetter;
    data['PolicyNumber'] = this.policyNumber;
    data['AdminFee'] = this.adminFee;
    data['FileUrlList'] = this.fileUrlList;
    data['RCreationDate'] = this.rCreationDate;
    data['MortgageFinancialDependantsEntityModelList'] =
        this.mortgageFinancialDependantsEntityModelList;
    data['Id'] = this.id;
    return data;
  }
}
