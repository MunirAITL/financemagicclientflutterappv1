import 'package:aitl/model/json/auth/User.dart';

class GetClientAgreementAcceptedEmailAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  GetClientAgreementAcceptedEmailAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetClientAgreementAcceptedEmailAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> registrationWelcomeemail;
  Messages({this.registrationWelcomeemail});

  Messages.fromJson(Map<String, dynamic> json) {
    registrationWelcomeemail = json['registration_welcomeemail'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['registration_welcomeemail'] = this.registrationWelcomeemail;
    return data;
  }
}

class ResponseData {
  User user;
  ResponseData({this.user});

  ResponseData.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new User.fromJson(json['User']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
