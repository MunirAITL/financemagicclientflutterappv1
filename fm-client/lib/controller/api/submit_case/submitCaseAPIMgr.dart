import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/submit_case/SubmitCaseAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class SubmitCaseAPIMgr with Mixin {
  static final SubmitCaseAPIMgr _shared = SubmitCaseAPIMgr._internal();

  factory SubmitCaseAPIMgr() {
    return _shared;
  }

  SubmitCaseAPIMgr._internal();

  wsOnPostCase({
    BuildContext context,
    dynamic param,
    Function(SubmitCaseAPIModel) callback,
  }) async {
    try {
      myLog(param);
      await NetworkMgr()
          .req<SubmitCaseAPIModel, Null>(
        context: context,
        url: Server.SUBMITCASE_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
