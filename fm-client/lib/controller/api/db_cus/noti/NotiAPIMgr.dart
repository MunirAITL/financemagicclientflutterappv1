import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_noti/NotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/api/db_cus/noti/DeleteNotification.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class NotiAPIMgr with Mixin {
  static final NotiAPIMgr _shared = NotiAPIMgr._internal();

  factory NotiAPIMgr() {
    return _shared;
  }

  NotiAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    Function(NotiAPIModel) callback,
  }) async {
    try {
      final url =
          NotiHelper().getUrl(pageStart: pageStart, pageCount: pageCount);
      myLog(url);
      await NetworkMgr()
          .req<NotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsDeleteNotification(
      {BuildContext context,
      String id,
      Function(DeleteNotificationModel model) callback}) async {
    try {
      await NetworkMgr().req<DeleteNotificationModel, Null>(
        context: context,
        url: Server.NOTI_DELETE_URL.replaceAll("#notiId#", id.toString()),
        reqType: ReqType.Delete,
        param: {},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
