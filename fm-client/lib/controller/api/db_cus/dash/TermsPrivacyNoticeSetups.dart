class TermsPrivacyNoticeSetups {
  Null user;
  int userId;
  int userCompanyId;
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String type;
  String title;
  String description;
  String webUrl;
  String remarks;
  int id;

  TermsPrivacyNoticeSetups(
      {this.user,
        this.userId,
        this.userCompanyId,
        this.status,
        this.creationDate,
        this.updatedDate,
        this.versionNumber,
        this.type,
        this.title,
        this.description,
        this.webUrl,
        this.remarks,
        this.id});

  TermsPrivacyNoticeSetups.fromJson(Map<String, dynamic> json) {
    user = json['User'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    type = json['Type'];
    title = json['Title'];
    description = json['Description'];
    webUrl = json['WebUrl'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['User'] = this.user;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['Type'] = this.type;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['WebUrl'] = this.webUrl;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
