import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as path;

class MediaUploadAPIMgr with Mixin {
  static final MediaUploadAPIMgr _shared = MediaUploadAPIMgr._internal();

  factory MediaUploadAPIMgr() {
    return _shared;
  }

  MediaUploadAPIMgr._internal();

  wsMediaUploadFileAPI({
    BuildContext context,
    File file,
    Function(MediaUploadFilesAPIModel) callback,
  }) async {
    try {
      myLog("File name " + file.absolute.path.toString());
      myLog("File name extension =  ${path.extension(file.path)}");
      await NetworkMgr().uploadFiles<MediaUploadFilesAPIModel, Null>(
        context: context,
        url: Server.MEDIA_UPLOADFILES_URL,
        files: [file],
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsUploadXLSFileAPI({
    BuildContext context,
    File file,
    Function(CommonAPIModel) callback,
  }) async {
    try {
      myLog("File name " + file.absolute.path.toString());
      myLog("File name extension =  ${path.extension(file.path)}");
      await NetworkMgr().uploadFiles<CommonAPIModel, Null>(
        context: context,
        url: SrvW2NCase.UPLOAD_XLS_FILE_URL,
        files: [file],
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
