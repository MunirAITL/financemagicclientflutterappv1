// ignore_for_file: file_names

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:intl/intl.dart';

class Common {
  static String getEnum2Str<T>(e) {
    return (e.toString().split('.').last).toString().replaceAll("_", " ");
  }

  static Future<String> getUDID(BuildContext context) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  static String unEscapeString(String str) {
    //return json.decode(json.decode(str));
    return str;
  }

  static String removeTag(htmlString) {
    return htmlString;
  }

  static String parseHtmlString(String htmlString) {
    //return Bidi.stripHtmlIfNeeded(htmlString);
    return htmlString;
  }

  static num tryParse(input) {
    String source = input.trim();
    return int.tryParse(source) ?? double.tryParse(source);
  }

  static bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    // ignore: deprecated_member_use
    return double.parse(s, (e) => null) != null;
  }

  static findIndexFromList(List<dynamic> list, val) {
    for (int i = 0; i < list.length; i++) {
      if (list[i] == val) return i;
    }
    return 0;
  }

  static String stripCountryCodePhone(String phone) {
    return phone.trim();
  }

  static String getPhoneNumber(String mobileNumber) {
    var phoneNumber = mobileNumber;
    try {
      debugPrint("Phone number substring(0,2)= " + mobileNumber[0]);
      if (mobileNumber[0] == "+") {
        if (mobileNumber.substring(0, 3) == "+88") {
          phoneNumber = mobileNumber.substring(3);
        } else if (mobileNumber.substring(0, 3) == "+44") {
          phoneNumber = mobileNumber.substring(3);
        }
      } else {
        if (mobileNumber.substring(0, 2) == "88") {
          phoneNumber = mobileNumber.substring(2);
        } else if (mobileNumber.substring(0, 2) == "44") {
          phoneNumber = mobileNumber.substring(2);
        }
      }
      //remove double 00
      debugPrint("Phonenumber substring(0,2) 2= " + phoneNumber);
      if (phoneNumber.substring(0, 2) == "00") {
        phoneNumber = phoneNumber.substring(1);
      }
      // add a single  0
      //     if (phoneNumber.substring(0, 1) != "0") {
      //       phoneNumber = "0" + phoneNumber;
      //     }
    } catch (e) {}
    return phoneNumber;
  }

  static splitName(String name) {
    try {
      final fName = name.substring(0, name.indexOf(" "));
      final lName = name.substring(name.indexOf(" ") + 1);
      return {'fname': fName, 'lname': lName};
    } catch (e) {}
    return {'fname': name, 'lname': ''};
  }

  static getMapKeyByVal(Map<dynamic, dynamic> map, String v) {
    for (var entry in map.entries) {
      if (v == entry.value) return entry.key;
    }
    return "";
  }

  static isNotAllowdEmailAddress(String str) {
    final repStr = '*';
    final listSkipWord = [
      "at the rate",
      "(at the rate)",
      "attherate",
      "@",
      "dotcom",
      "dotnet",
      "mail",
      "gmail",
      "yahoo",
      "yopmail",
      "msn",
      "outlook",
      "proton",
      "aol",
      "zoho",
      "icloud",
      "gmx",
      "tutanota",
      "inbox",
      "startmail",
      "thexyz",
      "dot ",
      " com",
      ".com",
      ".net",
      " one ",
      " two ",
      " three ",
      " four ",
      " five ",
      " six ",
      " seven ",
      " eight ",
      " nine ",
      " ten ",
      " eleven ",
      " twelve ",
      " thirteen ",
      " fourteen ",
      " fifteen ",
      " sixteen ",
      " seventeen ",
      " eighteen ",
      " nineteen ",
      " twenty ",
      " thirty ",
      " fourty ",
      " fifty ",
      " sixty ",
      " seventy ",
      " eighty ",
      " ninty ",
      " hundred ",
      " i ",
      "ii",
      " iii ",
      " iv ",
      " v ",
      " vi ",
      " vii ",
      " viii ",
      " ix ",
      " x "
    ];
    //  EMAIL PARSER  **********************************************
    try {
      //  checking by email address
      final mailPattern = r"\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b";
      final regEx = RegExp(mailPattern, multiLine: true);
      final obtainedMail =
          regEx.allMatches(str).map((m) => m.group(0)).join(' ');
      print(obtainedMail);
      final star = List.filled(obtainedMail.length, repStr).join();
      if (obtainedMail.isNotEmpty)
        return str.replaceAll(obtainedMail, star).trim();
    } catch (e) {}
    try {
      //if (listSkipWord.contains(str)) {
      //str = str.replaceAll("*", str);
      //}

      var ss = str.toLowerCase().trim();
      for (String s in listSkipWord) {
        //bool isFound = false;
        String regex = r'[~!@#$%^&*()_+`{}|<>?;:./,=\-\[\]\(\)]';
        final s2 = ss.replaceAll(RegExp(regex, unicode: true), '').trim();
        if (s2.toLowerCase().contains(s.toLowerCase())) {
          ss = s2.replaceAll(s.toLowerCase().trim(), "*");
        }
        print(ss);
        /*for (final m in listSkipWord) {
          if (m.contains(s2)) {
            //ss += List.filled(s2.length, repStr).join();
            ss += ss.replaceAll("*", m);
            isFound = true;
            break;
          }
        }
        if (!isFound) ss += (s + " ");*/
      }
      if (ss.isNotEmpty) return ss.trim();
    } catch (e) {}
    return str.trim();
  }

  static isNotAllowdPhoneNumber(String str) {
    final num = str.replaceAll(RegExp(r'(\D+)'), '');
    if (num.length > 7) {
      //final star = List.filled(num.length, "*").join();
      return str.replaceAll(RegExp(r'[0-9]'), "*").trim();
    } else {
      return str;
    }
  }
}
