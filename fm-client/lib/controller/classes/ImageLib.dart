// ignore_for_file: file_names

import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class ImageLib {
  getPngByGlobalKey(globalKey) async {
    RenderRepaintBoundary boundary =
        globalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    //Uint8List pngBytes = byteData.buffer.asUint8List();
    //print(pngBytes);
    return byteData.buffer.asUint8List();
  }

  getGrayScaleImage({File file, globalKey}) {
    return RepaintBoundary(
      key: globalKey,
      child: ColorFiltered(
        colorFilter: const ColorFilter.matrix(<double>[
          0.2126,
          0.7152,
          0.0722,
          0,
          0,
          0.2126,
          0.7152,
          0.0722,
          0,
          0,
          0.2126,
          0.7152,
          0.0722,
          0,
          0,
          0,
          0,
          0,
          1,
          0,
        ]),
        /*_item.enabled
            ? ColorFilter.mode(
                Colors.transparent,
                BlendMode.multiply,
              )
            : ColorFilter.mode(
                Colors.grey,
                BlendMode.saturation,
              ),*/
        child: Image.file(file, fit: BoxFit.fill),
      ),
    );
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future<File> convertWidget2Image(BuildContext context, GlobalKey key) async {
    try {
      RenderRepaintBoundary boundary = key.currentContext.findRenderObject();
      var image = await boundary.toImage();
      ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      final tempDir = await getTemporaryDirectory();
      final file = await new File('${tempDir.path}/image.png').create();
      await file.writeAsBytes(pngBytes);
      return file;
      //final channel = const MethodChannel('channel:uk.co.aitl/share');
      //channel.invokeMethod('shareFile', 'image.png');
    } catch (e) {}
    return null;
  }

  static Future<File> getImageFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');
    final file = File('${(await getTemporaryDirectory()).path}/test.jpg');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    return file;
  }

  Future<File> getImageFileFromAssets({String path, String img}) async {
    final byteData = await rootBundle.load('assets/$path/$img');
    final file = File('${(await getTemporaryDirectory()).path}/$img');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    return file;
  }
}
