import 'package:aitl/config/Server.dart';

class APIEidCfg {
  static const identificationStatus = {
    101: "waiting for an agent's review",
    102: "",
    103: "",
    104: "",
    105: ""
  };

  static String getIdenficationStatus(int statusId) {
    try {
      return identificationStatus[statusId];
    } catch (e) {
      return statusId.toString();
    }
  }

  static const String EID_URL =
      Server.BASE_URL + "/mrg/mark-eid-mobile-verification";

  static const SELFIE_POST_URL =
      Server.BASE_URL + "/api/camcapture/postselfieBydase64data";
  static const DOC_POST_URL =
      Server.BASE_URL + "/api/camcapture/postdocumentbybase64bitdata";
  static const VERIFY_POST_URL =
      Server.BASE_URL + "/api/camcapture/postdocumentverify";
}
