import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';

abstract class W2NBase<T extends StatefulWidget> extends State<T> with Mixin {
  drawLayout();

  String getDDIndex(List<OptionItem> listOptionItems, String title) {
    for (final opt in listOptionItems) {
      if (opt.title.toLowerCase() == title.toLowerCase()) return opt.id;
    }
    return null;
  }

  addCurSign(val) => AppDefine.CUR_SIGN + val;
  drawDivider() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Divider(
        color: MyTheme.statusBarColor,
        height: 1.5,
      ),
    );
  }

  drawHeading(String txt) {
    return Txt(
        txt: txt,
        txtColor: MyTheme.statusBarColor,
        txtSize: MyTheme.txtSize,
        txtAlign: TextAlign.center,
        isBold: true);
  }

  drawTitleHeading(String txt1, String txt2) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(
            txt: txt1,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .4,
            txtAlign: TextAlign.start,
            fontWeight: FontWeight.w500,
            isBold: false),
        SizedBox(height: 5),
        Txt(
            txt: txt2,
            txtColor: Colors.black87,
            txtSize: MyTheme.txtSize - .4,
            txtAlign: TextAlign.start,
            isBold: false),
      ],
    );
  }

  drawDottedBox(String title, Function callback) {
    return DottedBorder(
      borderType: BorderType.RRect,
      radius: Radius.circular(5),
      padding: EdgeInsets.only(top: 10, bottom: 10),
      color: MyTheme.statusBarColor,
      strokeWidth: 1,
      child: GestureDetector(
        onTap: () async {
          callback();
        },
        child: Container(
          //height: getHP(context, 4),
          width: getW(context),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.add_circle_outline,
                color: Colors.black87,
                size: 17,
              ),
              SizedBox(width: 3),
              Flexible(
                child: Txt(
                    txt: title,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawTextArea(
      {String title,
      String subTitle,
      TextEditingController tf,
      String ph = '',
      int maxLen = 500}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: title,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: true),
              )
            : SizedBox(),
        subTitle != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: subTitle,
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 1),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: TextField(
            controller: tf,
            minLines: 2,
            maxLines: 3,
            //expands: true,
            autocorrect: false,
            maxLength: maxLen,
            keyboardType: TextInputType.multiline,
            style: TextStyle(color: Colors.black, fontSize: 15),
            decoration: InputDecoration(
              hintText: ph,
              hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
              //labelText: 'Your message',
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              contentPadding: EdgeInsets.all(10),
            ),
          ),
        ),
      ],
    );
  }

  drawCurrencyBox(String title, String subTitle, TextEditingController tf,
      FocusNode focus, FocusNode focusNxt, Function(String v) onChange) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(
            txt: title,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 5),
        subTitle != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: subTitle,
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        drawInputCurrencyBox(
          context: context,
          tf: tf,
          hintTxt: 0,
          len: 10,
          focusNode: focus,
          focusNodeNext: focusNxt,
          onChange: (v) => onChange(v),
        ),
      ],
    );
  }

  drawBullet(String txt) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
                child: Padding(
                    padding: const EdgeInsets.only(top: 7),
                    child: UIHelper().drawCircle(
                        context: context,
                        color: MyTheme.statusBarColor,
                        size: 1.5))),
            SizedBox(width: 10),
            Expanded(
              flex: 7,
              child: Txt(
                  txt: txt,
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ],
        ),
      ),
    );
  }
}
