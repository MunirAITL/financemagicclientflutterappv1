import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/SrvW2NMisc.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/model/json/web2native/case/misc/caseusertype/CaseTaskBiddings.dart';
import 'package:aitl/model/json/web2native/case/misc/caseusertype/PutIntroPermissionAPIModel.dart';
import 'package:aitl/view/db_cus/timeline/chat/TimeLinePostScreen.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/doc/doc_info_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/10credit_commitment/req_cr_commit_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/11credit_history/req_cr_his_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/12assessment_affordibility/req_assess_afford_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/13btl_portfolio/req_btl_portfolio_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/2es_others/req_other_es_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/4address_history/req_addr_his_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/6cur_residential_mort/cur_res_mort_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/7emp_income/req_emp_income_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/8existing_policy/req_existing_policy_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/9saving_investment/req_saving_inv_mixin.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/tile/ExpansionTileCard.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/doc/doc_case_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/misc/case_user_type_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/case_model_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step10_cr_commit_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step11_cr_his_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step12_assess_afford_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step13_btl_portfolio_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step2_es_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step4_addr_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step5_dependent_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step6_cur_res_mort_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step7_emp_income_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step8_existing_policy_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step9_saving_inv_ctrl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';

import '../../../view_model/rx/web2native/requirements/requirements_ctrl.dart';
import '../../widgets/btn/BtnCase.dart';
import 'case/mixin/requirements/5dependent/req_dependent_mixin.dart';
import 'case/mixin/requirements/req_b2lm_mixin.dart';
import 'case/mixin/requirements/req_b2lrm_mixin.dart';
import 'case/mixin/requirements/req_bl_mixin.dart';
import 'case/mixin/requirements/req_cml_mixin.dart';
import 'case/mixin/requirements/req_df_mixin.dart';
import 'case/mixin/requirements/req_er_mixin.dart';
import 'case/mixin/requirements/req_ginc_mixin.dart';
import 'case/mixin/requirements/req_l2b_mixin.dart';
import 'case/mixin/requirements/req_mortgage_mixin.dart';
import 'case/mixin/requirements/req_remortgage_mixin.dart';
import 'case/mixin/requirements/req_scb2letc_mixin.dart';
import 'case/mixin/requirements/req_scr_mixin.dart';
import 'w2n_base.dart';

abstract class CaseBase<T extends StatefulWidget> extends W2NBase<T>
    with
        ReqGIncMixin,
        ReqERMixin,
        ReqBLMixin,
        ReqL2BMixin,
        ReqDFMixin,
        ReqCMLMixin,
        ReqSCB2LetCMixin,
        ReqSCRMixin,
        ReqMortgageMixin,
        ReqReMortgageMixin,
        ReqB2LMMixin,
        ReqB2LRMMixin,
        ReqStep2OtherESMixin,
        ReqStep4AddrHisMixin,
        ReqStep5DependentMixin,
        ReqStep6CurResMortMixin,
        ReqStep7EmpIncomeMixin,
        ReqStep8ExistingPolicyMixin,
        ReqStep9SavingInvestmentMixin,
        ReqStep10CrComMixin,
        ReqStep11CrHisMixin,
        ReqStep12AssessAffordMixin,
        ReqStep13BtlPortfolioMixin,
        DocInfoMixin {
  final caseModelCtrl = Get.put(CaseModelCtrl());
  final caseUserTypeCtrl = Get.put(CaseUserTypeCtrl());
  final requirementsCtrl = Get.put(RequirementsCtrl());
  //  steps rx
  final otherESCtrl = Get.put(ReqStep2ESCtrl());
  final addrHistoryCtrl = Get.put(ReqStep4AddrHistoryCtrl());
  final dependentCtrl = Get.put(ReqStep5DependentCtrl());
  final curResMortCtrl = Get.put(ReqStep6CurResMortCtrl());
  final empIncomeCtrl = Get.put(ReqStep7EmpIncomeCtrl());
  final existingPolicyCtrl = Get.put(ReqStep8ExistingPolicyCtrl());
  final savingInvCtrl = Get.put(ReqStep9SavingInvCtrl());
  final crComCtrl = Get.put(ReqStep10CrCommitCtrl());
  final crHisCtrl = Get.put(ReqStep11CrHisCtrl());
  final assessAffordCtrl = Get.put(ReqStep12AssessAffordCtrl());
  final btlPortfCtrl = Get.put(ReqStep13BtlPortfolioCtrl());
  final docCaseModel = Get.put(DocCaseCtrl());

  var isCaseApplicationShow = true.obs;
  var isShareInfoWithIntroducer = false.obs;
  var isShareInfoWithIntroducerDone = false.obs;

  //  REQUIREMENTS
  showReqCaseExpandDetailsView() {
    if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.General_Insurance)) {
      return drawGenIncDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Equity_Release)) {
      return drawERDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Bridging_Loan)) {
      return drawBLDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Let_to_Buy)) {
      return drawL2BDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Development_Finance)) {
      return drawDFDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Business_Lending)) {
      return drawBLDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(
            eCaseTitle.Commercial_Mortgages0SPLASH0_Loans)) {
      return drawCMLDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(
            eCaseTitle.Second_Charge_0HYPN0_Buy_to_Let_0AND0_Commercial)) {
      return drawSCB2LCDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Buy_to_Let_Mortgage)) {
      return drawB2LMDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Buy_to_Let_Remortgage)) {
      return drawB2LRMDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(
            eCaseTitle.Second_Charge_0HYPN0_Residential)) {
      return drawSCRDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Residential_Remortgage)) {
      return drawReMortgageDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    } else if (caseModelCtrl.locModel.value.title ==
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Residential_Mortgage)) {
      return drawMortgageDetailsView(
        context: context,
        caseModel: caseModelCtrl.locModel.value,
        requirementsCtrl: requirementsCtrl,
      );
    }
  }

  //  **************************************************************************************  Custom Resuable Methods start here...

  drawCaseRowTile({
    @required GlobalKey<ExpansionTileCardState> key,
    @required IconData icon,
    @required String title,
    Widget expandableTitle,
    bool isLoading = false,
    @required Function(bool) callback,
  }) {
    final status = (expandableTitle != null || title == 'Personal Details')
        ? 'Completed'
        : null;
    final btnTxt = (expandableTitle == null) ? 'Continue' : null;

    return expandableTitle != null
        ? ExpansionTileCard(
            //trailing: SizedBox.shrink(),
            initiallyExpanded: false,
            initialPadding: EdgeInsets.zero,
            contentPadding: EdgeInsets.zero,
            elevation: 0,
            baseColor: MyTheme.bgColor2,
            expandedColor: MyTheme.bgColor2,
            key: key,
            title: _drawCaseRow(
              icon: icon,
              title: title,
              status: status,
              btnTxt: btnTxt,
              expandableTitle: expandableTitle,
              isLoading: isLoading,
              callback: callback,
            ),
            children: <Widget>[expandableTitle],
          )
        : _drawCaseRow(
            icon: icon,
            title: title,
            status: status,
            btnTxt: btnTxt,
            expandableTitle: expandableTitle,
            isLoading: isLoading,
            callback: callback,
          );
  }

  _drawCaseRow({
    @required IconData icon,
    @required String title,
    String status,
    String btnTxt,
    Widget expandableTitle,
    @required bool isLoading,
    Function(bool) callback,
  }) {
    return Padding(
      padding: EdgeInsets.only(left: 3, right: expandableTitle == null ? 5 : 0),
      child: Container(
        //color: Colors.black,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(child: Icon(icon, color: Colors.grey, size: 18)),
            Expanded(
              flex: expandableTitle != null ? 4 : 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: title,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  title != 'Document Info'
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 5),
                            status != null
                                ? Container(
                                    //color: Colors.black,
                                    child: Row(
                                      children: [
                                        Flexible(
                                          child: Container(
                                            color: Colors.green.withOpacity(.2),
                                            child: Padding(
                                              padding: const EdgeInsets.all(5),
                                              child: Txt(
                                                  txt: status,
                                                  txtColor: Colors.green,
                                                  txtSize: MyTheme.txtSize - .6,
                                                  txtAlign: TextAlign.start,
                                                  isBold: true),
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        isLoading
                                            ? drawLoading()
                                            : caseModelCtrl.locModel.value
                                                        .status ==
                                                    NewCaseCfg.SUBMITTED
                                                ? Flexible(
                                                    child: GestureDetector(
                                                        onTap: () {
                                                          callback(true);
                                                        },
                                                        child: Txt(
                                                            txt: " Edit ",
                                                            txtColor:
                                                                Colors.blue,
                                                            txtSize: MyTheme
                                                                    .txtSize -
                                                                .4,
                                                            txtAlign:
                                                                TextAlign.start,
                                                            isBold: false)))
                                                : SizedBox(),
                                      ],
                                    ),
                                  )
                                : SizedBox()
                          ],
                        )
                      : SizedBox(),
                ],
              ),
            ),
            //expandableTitle == null ? Spacer() : SizedBox(),
            isLoading && expandableTitle == null
                ? drawLoading()
                : expandableTitle == null &&
                        caseModelCtrl.locModel.value.status ==
                            NewCaseCfg.SUBMITTED
                    ? Flexible(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: BtnCase(
                              txt: btnTxt,
                              txtColor: expandableTitle == null
                                  ? Colors.white
                                  : Colors.black,
                              bgColor: expandableTitle == null
                                  ? MyTheme.radioBGColor
                                  : Colors.transparent,
                              radius: 5,
                              callback: () {
                                callback(false);
                              }),
                        ),
                      )
                    : SizedBox()
          ],
        ),
      ),
    );
  }

  drawLoading() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: SizedBox(
        width: 20,
        height: 20,
        child: CircularProgressIndicator(
          color: MyTheme.statusBarColor,
          strokeWidth: 1,
        ),
      ),
    );
  }

  //  expanstion tiles  ***********************************************************
  personalDetailView() {
    final user = userData.userModel;
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          UIHelper().draw2Row("What's your first name?", user.firstName),
          SizedBox(height: 5),
          UIHelper().draw2Row("What are your middle names?", user.middleName),
          SizedBox(height: 5),
          UIHelper().draw2Row("What's your surname?", user.lastName),
          SizedBox(height: 5),
          UIHelper().draw2Row("What is your date of birth?",
              DateFun.getDate(user.dateofBirth, "dd-MMM-yyyy")),
          SizedBox(height: 5),
          UIHelper().draw2Row("Gender", user.cohort),
          SizedBox(height: 5),
          UIHelper().draw2Row("What's your phone number?", user.mobileNumber),
          SizedBox(height: 5),
          UIHelper().draw2Row("What's your email address?", user.email),
          SizedBox(height: 5),
          UIHelper().draw2Row("What is your nationality?", user.nationality),
          SizedBox(height: 5),
          UIHelper().draw2Row("Other visa name?", user.otherVisaName),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "What is your relationship status?", user.maritalStatus),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "National Insurance Number", user.nationalInsuranceNumber),
          SizedBox(height: 5),
          UIHelper().draw2Row("Country of birth", user.countryofBirth),
          SizedBox(height: 5),
          UIHelper().draw2Row("Passport Number", user.passportNumber),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Passport Expiry Date",
              !user.passportExpiryDate.contains("0001-01-01")
                  ? DateFun.getDate(user.passportExpiryDate, "dd-MMM-yyyy")
                  : ''),
          SizedBox(height: 5),
        ],
      ),
    );
  }

  //  form on continue click  ***********************************************************

  //  helper functions  ***********************************************************

  drawProgressView(int pa) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Txt(
              txt: pa.toString() + "% complete",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.center,
              isBold: true),
          SizedBox(height: 5),
          LinearProgressIndicator(
            backgroundColor: Colors.green.withOpacity(.3),
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.greenAccent,
            ),
            value: pa / 100,
          ),
          pa > 5
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Align(
                    alignment: Alignment.center,
                    child: drawCaseInfo(false),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }

  drawCaseInfo(bool isMainPage) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Txt(
            txt: "Case title -  " + caseModelCtrl.locModel.value.title,
            txtColor: MyTheme.statusBarColor,
            txtSize: isMainPage ? MyTheme.txtSize - .1 : MyTheme.txtSize - 0.2,
            txtAlign: TextAlign.center,
            isBold: true),
        caseModelCtrl.locModel.value.description != ''
            ? Padding(
                padding: const EdgeInsets.only(top: 2),
                child: Txt(
                    txt: "Sub case : " +
                        caseModelCtrl.locModel.value.description,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - (isMainPage ? 0.3 : 0.5),
                    txtAlign: TextAlign.start,
                    isBold: false))
            : SizedBox(),
        SizedBox(height: 2),
        Txt(
            txt: "Case No : " + caseModelCtrl.locModel.value.id.toString(),
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - (isMainPage ? 0.3 : 0.5),
            txtAlign: TextAlign.start,
            isBold: false),
      ],
    );
  }

  calDepositAmount({@required String loanAmt, @required String purchasePrice}) {
    //  calculation for deposit amount
    try {
      final v1 = loanAmt.trim().isEmpty ? '0' : loanAmt.trim();
      final v2 = purchasePrice.trim().isEmpty ? '0' : purchasePrice.trim();
      final lonAmt = int.parse(v1);
      final purPrice = int.parse(v2);
      final v = lonAmt > purPrice ? (lonAmt - purPrice) : (purPrice - lonAmt);
      return v.toStringAsFixed(2).replaceAll(".00", "");
    } catch (e) {}
  }

  calLTVAmount(
      {@required String balOutstanding, @required String curValProperty}) {
    try {
      final v1 = balOutstanding.trim().isEmpty ? '0' : balOutstanding.trim();
      final v2 = curValProperty.trim().isEmpty ? '0' : curValProperty.trim();
      final balOut = int.parse(v1);
      final curValProp = int.parse(v2);
      if (balOut > 0 && curValProp > 0) {
        final v = ((balOut / curValProp) * 100);
        return v.toStringAsFixed(2).replaceAll(".00", "");
      } else
        return 0;
    } catch (e) {}
  }

  drawEntityModelList() {
    final entityModel =
        assessAffordCtrl.taskCtrl.value.mortgageCaseInfoEntityModelList;
    return entityModel != null && entityModel.length > 0
        ? Padding(
            padding: const EdgeInsets.all(10),
            child: AlignedGridView.count(
              shrinkWrap: true,
              crossAxisCount: 2,
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              itemCount: entityModel.length,
              itemBuilder: (context, index) {
                final model = entityModel[index];
                return GestureDetector(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    padding: EdgeInsets.only(bottom: 10),
                    //alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            color: index % 2 == 0 ? Colors.grey : Colors.purple,
                            width: .5)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 0),
                          decoration: BoxDecoration(
                            color: index % 2 == 0 ? Colors.grey : Colors.purple,
                            borderRadius: BorderRadius.circular(2),
                            //border:
                            //Border.all(color: Colors.grey, width: .5)
                          ),
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Txt(
                                txt: 'Applicant ' + (index + 1).toString(),
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .5,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                        ),
                        SizedBox(height: 5),
                        CircleAvatar(
                          radius: 18,
                          backgroundColor: Colors.transparent,
                          backgroundImage: CachedNetworkImageProvider(
                            MyNetworkImage.checkUrl(
                                model.profileImageUrl != null &&
                                        model.profileImageUrl.isNotEmpty
                                    ? model.profileImageUrl
                                    : Server.MISSING_IMG),
                          ),
                        ),
                        SizedBox(height: 5),
                        Txt(
                            txt: model.customerName ?? '',
                            txtColor: Colors.black87,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ],
                    ),
                  ),
                );
              },
            ),
          )
        : SizedBox();
  }

  showLessView(GlobalKey<ExpansionTileCardState> key) => Align(
        alignment: Alignment.centerRight,
        child: GestureDetector(
            onTap: () {
              key.currentState.collapse();
              Scrollable.ensureVisible(key.currentContext);
            },
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Flexible(
                    child: Txt(
                        txt: "Show less",
                        txtColor: Colors.blueAccent,
                        txtSize: MyTheme.txtSize - .7,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Flexible(
                    child: Icon(Icons.arrow_drop_up_outlined,
                        color: Colors.blueAccent, size: 20),
                  ),
                ])),
      );

  showUserTypeUI(CaseTaskBiddings model) {
    var title = '';
    bool isAdmin = false;
    bool isIntroducer = false;
    bool isAdvisor = false;
    bool isGetAPIRes = false;
    for (final m in SrvW2NMisc.listUserType) {
      final list = m['communityId'] as List;
      if (list.contains(model.communityId)) {
        if (model.id != null) {
          isGetAPIRes = true;
        }
        title = m['title'];
        if (title == "Admin")
          isAdmin = true;
        else if (title == "Introducer")
          isIntroducer = true;
        else if (title == "Advisor") isAdvisor = true;
        break;
      }
    }
    //if (title.isEmpty) return SizedBox();

    if (isGetAPIRes && !isShareInfoWithIntroducerDone.value) {
      isShareInfoWithIntroducerDone.value = true;
      isShareInfoWithIntroducer.value =
          model.isViewPermission == 'Yes' ? true : false;
    }

    return Flexible(
      child: Padding(
        padding: const EdgeInsets.only(left: 5, right: 5),
        child: Container(
          alignment: Alignment.center,
          //padding: const EdgeInsets.all(10),
          width: getW(context) / 3.28,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.black54, width: .7),
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: double.infinity,
                color: Colors.grey,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(width: isAdmin ? 16 : 0),
                    Expanded(
                      child: Txt(
                          txt: title.toUpperCase(),
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - .7,
                          txtAlign: TextAlign.center,
                          isBold: true),
                    ),
                    isAdmin
                        ? Flexible(
                            child: GestureDetector(
                            onTap: () {
                              showToolTips(
                                  context: context,
                                  txt:
                                      "This is the case administrator assigned to help the adviser.");
                            },
                            child: Icon(
                              Icons.info_outline_rounded,
                              color: Colors.white,
                              size: 18,
                            ),
                          ))
                        : SizedBox(height: 18),
                  ],
                ),
              ),
              SizedBox(height: 5),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  !isGetAPIRes
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 18,
                              backgroundColor: Colors.transparent,
                              backgroundImage: CachedNetworkImageProvider(
                                MyNetworkImage.checkUrl(
                                    model.ownerImageUrl != null &&
                                            model.ownerImageUrl.isNotEmpty
                                        ? model.ownerImageUrl
                                        : Server.MISSING_IMG),
                              ),
                            ),
                            SizedBox(height: 5),
                            Txt(
                                txt: model.ownerName ?? 'Not assigned',
                                txtColor: model.ownerName == null
                                    ? Colors.red
                                    : Colors.blueAccent,
                                txtSize: MyTheme.txtSize - .5,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            SizedBox(
                                height: isIntroducer && model.ownerName == null
                                    ? 5
                                    : 0),
                          ],
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 20,
                              backgroundColor: Colors.transparent,
                              backgroundImage: CachedNetworkImageProvider(
                                  MyNetworkImage.checkUrl(model.ownerImageUrl)),
                            ),
                            SizedBox(height: 5),
                            Txt(
                                txt: model.ownerName ?? 'Not assigned',
                                txtColor: model.ownerName == null
                                    ? Colors.red
                                    : Colors.blueAccent,
                                txtSize: MyTheme.txtSize - .5,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            SizedBox(height: 5),
                            isIntroducer
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Txt(
                                          txt:
                                              'Share Personal Information with Introducer',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .75,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                      SizedBox(height: 5),
                                      FlutterSwitch(
                                        //width: 125.0,
                                        //height: 55.0,
                                        valueFontSize: 14,
                                        toggleSize: 18.0,
                                        value: isShareInfoWithIntroducer.value,
                                        borderRadius: 20.0,
                                        padding: 5,
                                        showOnOff: true,
                                        activeText: "Yes",
                                        inactiveText: "No",
                                        activeColor: Colors.red,
                                        onToggle: (isViewPermission) async {
                                          try {
                                            isShareInfoWithIntroducer.value =
                                                isViewPermission;
                                            final now =
                                                DateTime.now().toString();
                                            await APIViewModel().req<
                                                    PutIntroPermissionAPIModel>(
                                                context: context,
                                                url: SrvW2NMisc
                                                    .PUT_INTRODUCER_PERMISSION_URL,
                                                reqType: ReqType.Put,
                                                param: {
                                                  "UserId":
                                                      userData.userModel.id,
                                                  "Status": 101,
                                                  "CreationDate": now,
                                                  "UpdatedDate": now,
                                                  "VersionNumber": 1,
                                                  "TaskId": caseModelCtrl
                                                      .locModel.value.id,
                                                  "CoverLetter":
                                                      model.coverLetter ?? '',
                                                  "Description":
                                                      model.description ?? '',
                                                  "DeliveryDate": now,
                                                  "DeliveryTime":
                                                      model.deliveryTime ?? '',
                                                  "FixedbiddigAmount":
                                                      model.fixedbiddigAmount ??
                                                          0,
                                                  "HourlyBiddingAmount": model
                                                          .hourlyBiddingAmount ??
                                                      0,
                                                  "TotalHourPerWeek":
                                                      model.totalHourPerWeek ??
                                                          0,
                                                  "TotalHour":
                                                      model.totalHour ?? 0,
                                                  "OwnerName":
                                                      model.ownerName ?? '',
                                                  "ThumbnailPath":
                                                      model.thumbnailPath ?? '',
                                                  "ReferenceType":
                                                      model.referenceType ?? '',
                                                  "ReferenceId":
                                                      model.referenceId ?? "0",
                                                  "Remarks":
                                                      model.remarks ?? '',
                                                  "ImageServerUrl":
                                                      model.imageServerUrl ??
                                                          '',
                                                  "OwnerImageUrl":
                                                      model.ownerImageUrl ?? '',
                                                  "OwnerProfileUrl":
                                                      model.ownerProfileUrl ??
                                                          '',
                                                  "TaskCompletionRate": model
                                                          .taskCompletionRate ??
                                                      0,
                                                  "AverageRating":
                                                      model.averageRating ?? 0,
                                                  "RatingCount":
                                                      model.ratingCount ?? 0,
                                                  "IsTaskOwner":
                                                      model.isTaskOwner ??
                                                          false,
                                                  "TaskOwnerId":
                                                      model.taskOwnerId ?? 0,
                                                  "IsReview":
                                                      model.isReview ?? false,
                                                  "IsReviewByClient":
                                                      model.isReviewByClient ??
                                                          false,
                                                  "ReferenceBiddingUserId":
                                                      model.referenceBiddingUserId ??
                                                          '0',
                                                  "ReferenceBiddingUserType":
                                                      model.referenceBiddingUserType ??
                                                          '',
                                                  "PaymentStatus": 0,
                                                  "DiscountAmount":
                                                      model.discountAmount ?? 0,
                                                  "NetTotalAmount":
                                                      model.netTotalAmount ?? 0,
                                                  "UserPromotionId":
                                                      model.userPromotionId ??
                                                          0,
                                                  "IsInPersonOrOnline": model
                                                          .isInPersonOrOnline ??
                                                      false,
                                                  "TotalComments":
                                                      model.totalComments ?? 0,
                                                  "TaskPaymentPaymentMethod":
                                                      model.taskPaymentPaymentMethod ??
                                                          '',
                                                  "TaskPaymentAccountStatus":
                                                      model.taskPaymentAccountStatus ??
                                                          '',
                                                  "TaskPaymentId":
                                                      model.taskPaymentId ?? 0,
                                                  "TaskTitle":
                                                      model.taskTitle ?? '',
                                                  "TaskTitleUrl":
                                                      model.taskTitleUrl ?? '',
                                                  "CommunityId":
                                                      model.communityId ?? 0,
                                                  "CaseProcessorCompanyId":
                                                      model.caseProcessorCompanyId ??
                                                          0,
                                                  "CaseProcessorCompanyName":
                                                      model.caseProcessorCompanyName ??
                                                          '',
                                                  "CommunityName":
                                                      model.communityName ?? '',
                                                  "IsOnline":
                                                      model.isOnline ?? false,
                                                  "CompletedTaskerTaskCount":
                                                      model.completedTaskerTaskCount ??
                                                          0,
                                                  "TotalTaskerAsignTask": model
                                                          .totalTaskerAsignTask ??
                                                      0,
                                                  "IsReviewTask":
                                                      model.isReviewTask ??
                                                          false,
                                                  "IsViewPermission":
                                                      isShareInfoWithIntroducer
                                                              .value
                                                          ? 'Yes'
                                                          : 'No',
                                                  "CaseInvitationStatus": model
                                                          .caseInvitationStatus ??
                                                      0,
                                                  "OwnerEmail":
                                                      model.ownerEmail ?? '',
                                                  "OwnerMobileNumber":
                                                      model.ownerMobileNumber ??
                                                          '',
                                                  "Id": model.id
                                                },
                                                callback: (model) async {
                                                  if (mounted &&
                                                      model != null) {
                                                    if (model.success) {
                                                      //setState(() {});
                                                    }
                                                  }
                                                });
                                          } catch (e) {}
                                        },
                                      ),
                                      SizedBox(height: 5)
                                    ],
                                  )
                                : SizedBox(),
                          ],
                        ),
                  !isIntroducer &&
                          model.ownerName != null &&
                          model.ownerName.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(left: 5, right: 5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: GestureDetector(
                                onTap: () {
                                  if (model.ownerMobileNumber != null) {
                                    openUrl(context,
                                        "tel://" + model.ownerMobileNumber);
                                  } else {
                                    showToast(
                                        context: context,
                                        msg: "Mobile number not found");
                                  }
                                },
                                child: Icon(
                                  Icons.phone_outlined,
                                  color: MyTheme.radioBGColor,
                                  size: 16,
                                ),
                              )),
                              Flexible(
                                  child: GestureDetector(
                                onTap: () {
                                  Get.to(() => TimeLinePostScreen(
                                          timelineUserModel:
                                              TimelineUserModel.fromJson({
                                        'Name': model.ownerName,
                                        'Id': model.taskOwnerId,
                                        'IsOnline': model.isOnline,
                                        'ProfileImageUrl': model.ownerImageUrl
                                      })));
                                },
                                child: Icon(
                                  Icons.chat_outlined,
                                  color: MyTheme.radioBGColor,
                                  size: 18,
                                ),
                              )),
                            ],
                          ),
                        )
                      : SizedBox(height: 5),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  getAffordableTotalExpense() {
    try {
      try {
        assessAffordCtrl.totalOtherCustom.value = 0;
        for (final m in assessAffordCtrl.listAssessAffordItemModel) {
          assessAffordCtrl.totalOtherCustom.value += m.itemAmount;
        }
      } catch (e) {}

      var total = 0.0;
      final model = assessAffordCtrl.assessAffordModel.value;
      assessAffordCtrl.totalExpenses.value = 0;
      total += assessAffordCtrl.totalOtherCustom.value;
      total += crComCtrl.monthlyPayment.value;
      //
      total += model.propertyMaintenanceGroundRent;
      total += model.electric;
      total += model.councilTax;
      total +=
          model.tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences;
      total += model.buildingsContentsInsurance;
      total += model.gas;
      total += model.water;
      total += model.otherDdetails;
      total += model.familyFoodHouseholdCosts;
      total += model.clothing;
      total += model.homeHelp;
      total += model.laundry;
      total += model.mobilePhoneCosts;
      total += model.homePhone;
      total += model.internet;
      total += model.investments;
      total += model.maintenancePayments;
      total += model.socialCostsMealsOutDrinksTheatre;
      total += model.holiday;
      total += model.sports;
      total += model.cigarettes;
      total += model.tobaccoOrRelatedProducts;
      total += model.pensionContributions;
      total += model.leisure;
      total += model.travel;
      total += model.regularSubscriptions;
      total += model.petsFoodInsuranceGrooming;
      total += model.insurancesLifeHealthMedicalDentalPhone;
      total += model.regularSavings;
      total += model.petrol;
      total += model.commutingCosts;
      total += model.carCosts;
      total += model.carFuelCosts;
      total += model.motorInsurance;
      total += model.transportTrainTramBus;
      total += model.childCareCosts;
      total += model.regularSchoolFeesContributions;
      total += model.otherSchoolingCostsMealsUniformOutings;
      total += model.clothes;
      assessAffordCtrl.totalExpenses.value = total;
      assessAffordCtrl.update();
      return total;
    } catch (e) {}
    return 0;
  }
}

class MyInheritedData extends InheritedWidget {
  final String myField;
  final ValueChanged<String> onMyFieldChange;

  MyInheritedData({
    Key key,
    this.myField,
    this.onMyFieldChange,
    Widget child,
  }) : super(key: key, child: child);

  static MyInheritedData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MyInheritedData>();
  }

  @override
  bool updateShouldNotify(MyInheritedData oldWidget) {
    return oldWidget.myField != myField ||
        oldWidget.onMyFieldChange != onMyFieldChange;
  }
}
