import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../w2n_base.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:get/get.dart';

abstract class ClientAgreementBase<T extends StatefulWidget>
    extends W2NBase<T> {
  var tick2Agree = false.obs;

  onPostAcceptAgreementAPIs();

  drawPdfView(String url) {
    return Container(
      width: getW(context),
      height: getHP(context, 40),
      child: SfPdfViewer.network(
        url,
      ),
    );
  }

  drawAgreementView(MortgageCaseInfos mortgageCaseInfos) {
    return ListView(shrinkWrap: true, primary: false, children: [
      Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: getW(context),
              child: DottedBorder(
                  borderType: BorderType.RRect,
                  radius: Radius.circular(5),
                  padding: EdgeInsets.zero,
                  color: MyTheme.statusBarColor,
                  strokeWidth: .5,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Fee Arrangement",
                            txtColor: MyTheme.statusBarColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        SizedBox(height: 10),
                        _drawRow("Admin fees:",
                            addCurSign(mortgageCaseInfos.adminFee.toString())),
                        SizedBox(height: 10),
                        _drawRow("Admin Fee When Payable:",
                            mortgageCaseInfos.adminFeeWhenPayable ?? ''),
                        SizedBox(height: 10),
                        _drawRow("Advice fees:",
                            addCurSign(mortgageCaseInfos.adviceFee.toString())),
                        SizedBox(height: 10),
                        _drawRow("Advice Fee When Payable:",
                            mortgageCaseInfos.adviceFeeWhenPayable ?? ''),
                      ],
                    ),
                  )),
            ),
            SizedBox(height: 20),
            Theme(
              data: MyTheme.radioThemeData,
              child: Transform.translate(
                offset: Offset(-10, 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Checkbox(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            splashRadius: 20,
                            fillColor:
                                MaterialStateProperty.all(MyTheme.radioBGColor),
                            focusColor: Colors.white,
                            checkColor: Colors.white,
                            value: tick2Agree.value,
                            onChanged: (value) {
                              tick2Agree.value = value;
                            })),
                    Expanded(
                      flex: 7,
                      child: Txt(
                          txt:
                              "This is our standard agreement upon which we intend to rely. For your own benefit and protection you should read the terms carefully before signing. If you do not understand any of these, please ask for further information. I/We are aware of the costs of the services and agree to the amount. By checking this box I confirm that I have read and agree to be bound by the Agreement above. I also confirm that I am of the legal age of majority in the jurisdiction in which I reside (at least 18 years of age in many countries).",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .6,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Align(
              alignment: Alignment.center,
              child: Btn(
                  txt: "Accept",
                  txtColor: tick2Agree.value ? Colors.white : Colors.black,
                  bgColor: tick2Agree.value ? Colors.green : Colors.grey,
                  width: getWP(context, 50),
                  height: getHP(context, 6),
                  callback: () {
                    if (tick2Agree.value) {
                      onPostAcceptAgreementAPIs();
                    }
                  }),
            )
          ],
        ),
      ),
    ]);
  }

  _drawRow(String title, String val) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
            child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: true)),
        SizedBox(width: 10),
        Flexible(
            child: Align(
          alignment: Alignment.centerLeft,
          child: Txt(
              txt: val,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false),
        )),
      ],
    );
  }
}
