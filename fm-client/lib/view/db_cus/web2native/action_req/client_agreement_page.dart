import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NAR.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/DashBoardListType.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import 'package:aitl/model/json/web2native/action_req/GetClientAgreementAcceptedEmailAPIModel.dart';
import 'package:aitl/model/json/web2native/action_req/GetMortCaseInfoByTaskIdAPIModel.dart';
import 'package:aitl/model/json/web2native/action_req/PostClientAgreementAcceptAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/action_req/client_agreement_base.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ClientAgreementPage extends StatefulWidget {
  final DashBoardListType dashBoardListType;
  final String type;
  const ClientAgreementPage(
      {Key key, @required this.dashBoardListType, @required this.type})
      : super(key: key);
  @override
  State<ClientAgreementPage> createState() => _ClientAgreementPageState();
}

class _ClientAgreementPageState
    extends ClientAgreementBase<ClientAgreementPage> {
  MortgageCaseInfos mortgageCaseInfos;

  bool isLoading = false;

  var pdfUrl = "".obs;

  onGetTCAPI() async {
    try {
      await PrivacyPolicyAPIMgr().wsOnLoad(
        context: context,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                final termsPrivacyNoticeSetupsList =
                    model.responseData.termsPrivacyNoticeSetupsList;
                for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                    in termsPrivacyNoticeSetupsList) {
                  if (termsPrivacyPoliceSetups.type.toString() == widget.type) {
                    pdfUrl.value = termsPrivacyPoliceSetups.webUrl;
                    break;
                  }
                }
              } else {}
            } catch (e) {}
          }
        },
      );
    } catch (e) {}
  }

  onGetMortCaseInfoByTaskIdAPI() async {
    try {
      await APIViewModel().req<GetMortCaseInfoByTaskIdAPIModel>(
          context: context,
          url: SrvW2NAR.GET_MORTCASEINFO_BYTASKID_URL +
              widget.dashBoardListType.taskID,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                mortgageCaseInfos = model.responseData.mortgageCaseInfos[0];
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  onPostAcceptAgreementAPIs() async {
    try {
      final now = DateTime.now().toString();
      await APIViewModel().req<PostClientAgreementAcceptAPIModel>(
          context: context,
          url: SrvW2NAR.POST_ACCEPT_CLIENT_AGREEMENT_URL,
          reqType: ReqType.Put,
          param: {
            "UserId": userData.userModel.id,
            "User": null,
            "CompanyId": userData.userModel.userCompanyInfo.id,
            "Status": 101,
            "CreationDate": now,
            "UpdatedDate": now,
            "VersionNumber": 1,
            "CaseType": null,
            "CustomerType": "",
            "IsSmoker": mortgageCaseInfos.isSmoker,
            "IsDoYouHaveAnyFinancialDependants":
                mortgageCaseInfos.isDoYouHaveAnyFinancialDependants,
            "Remarks": mortgageCaseInfos.remarks,
            "IsAnyOthers": mortgageCaseInfos.isAnyOthers,
            "TaskId": widget.dashBoardListType.taskID,
            "CoapplicantUserId": mortgageCaseInfos.coapplicantUserId,
            "CustomerName": mortgageCaseInfos.customerName,
            "CustomerEmail": mortgageCaseInfos.customerEmail,
            "CustomerMobileNumber": mortgageCaseInfos.customerMobileNumber,
            "CustomerAddress": mortgageCaseInfos.customerAddress,
            "CustomerDateofBirth": mortgageCaseInfos.customerDateofBirth,
            "CustomerGender": mortgageCaseInfos.customerGender,
            "CustomerAreYouASmoker": mortgageCaseInfos.isSmoker,
            "ProfileImageUrl": userData.userModel.profileImageURL,
            "NamePrefix": mortgageCaseInfos.namePrefix,
            "AreYouBuyingThePropertyInNameOfASPV":
                mortgageCaseInfos.areYouBuyingThePropertyInNameOfASPV,
            "CompanyName": mortgageCaseInfos.companyName,
            "RegisteredAddress": mortgageCaseInfos.registeredAddress,
            "DateRegistered": userData.userModel.dateCreatedUTC,
            "CompanyRegistrationNumber":
                mortgageCaseInfos.companyRegistrationNumber,
            "ApplicationNumber": mortgageCaseInfos.applicationNumber ?? 1,
            "CustomerEmail1": "",
            "CustomerEmail2": "",
            "CustomerEmail3": "",
            "IsDoYouHaveAnyBTLPortfulio":
                mortgageCaseInfos.isDoYouHaveAnyBTLPortfulio,
            "IsClientAgreement": mortgageCaseInfos.isClientAgreement,
            "AdminFee": mortgageCaseInfos.adminFee,
            "AdminFeeWhenPayable": mortgageCaseInfos.adminFeeWhenPayable,
            "AdviceFee": mortgageCaseInfos.adviceFee,
            "AdviceFeeWhenPayable": mortgageCaseInfos.adviceFeeWhenPayable,
            "IsFeesRefundable": mortgageCaseInfos.isFeesRefundable ?? 'No',
            "FeesRefundable": mortgageCaseInfos.feesRefundable,
            "IPAdddress": mortgageCaseInfos.iPAdddress,
            "IPLocation": mortgageCaseInfos.iPLocation,
            "DeviceType": mortgageCaseInfos.deviceType,
            "ReportLogo": mortgageCaseInfos.reportLogo,
            "OfficePhoneNumber": mortgageCaseInfos.officePhoneNumber,
            "ReportFooter": mortgageCaseInfos.reportFooter,
            "AdviceFeeType": mortgageCaseInfos.adviceFeeType,
            "ClientAgreementStatus": mortgageCaseInfos.clientAgreementStatus,
            "RecommendationAgreementStatus":
                mortgageCaseInfos.recommendationAgreementStatus,
            "RecommendationAgreementSignature":
                mortgageCaseInfos.recommendationAgreementSignature,
            "IsThereAnySavingsOrInvestments":
                mortgageCaseInfos.isThereAnySavingsOrInvestments,
            "IsThereAnyExistingPolicy":
                mortgageCaseInfos.isThereAnyExistingPolicy,
            "TaskTitleUrl": mortgageCaseInfos.taskTitleUrl,
            "CustomerAddress1": mortgageCaseInfos.customerAddress1,
            "CustomerAddress2": mortgageCaseInfos.customerAddress2,
            "CustomerAddress3": mortgageCaseInfos.customerAddress3,
            "CustomerPostcode": mortgageCaseInfos.customerPostcode,
            "CustomerTown": mortgageCaseInfos.customerTown,
            "CustomerLastName": mortgageCaseInfos.customerLastName,
            "Id": mortgageCaseInfos.id,
            "ContentData": "",
            "CustomerAgreementUrl": pdfUrl.value
          },
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                try {
                  await APIViewModel().req<
                          GetClientAgreementAcceptedEmailAPIModel>(
                      context: context,
                      url: SrvW2NAR.GET_ACCEPTED_CLIENT_AGREEMENT_EMAIL_URL +
                          "UserId=" +
                          userData.userModel.id.toString() +
                          "&Title=" +
                          widget.dashBoardListType.taskID +
                          "&UserCompanyId=" +
                          userData.userModel.userCompanyInfo.id.toString(),
                      reqType: ReqType.Get,
                      callback: (model) async {
                        if (mounted && model != null) {
                          if (model.success) {
                            showToast(
                                context: context,
                                msg: "Client agreement accepted successfully.");
                            Future.delayed(Duration(seconds: 3),
                                () => Get.back(result: true));
                          }
                        }
                      });
                } catch (e) {}
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      /*requirementsCtrl.dispose();
      dependentCtrl.dispose();
      caseModelCtrl.dispose();
      mainCrCommitCtrl.dispose();
      reqStep2ESCtrl.dispose();*/
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      await onGetTCAPI();
      onGetMortCaseInfoByTaskIdAPI();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: Colors.white,
              appBar: AppBar(
                centerTitle: true,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: widget.type),
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
                  child: (isLoading)
                      ? AppbarBotProgBar(
                          backgroundColor: MyTheme.appbarProgColor)
                      : SizedBox(),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            pdfUrl.value != '' ? drawPdfView(pdfUrl.value) : SizedBox(),
            mortgageCaseInfos != null
                ? Expanded(child: drawAgreementView(mortgageCaseInfos))
                : SizedBox(),
          ],
        ));
  }
}
