import 'package:aitl/config/SrvW2NMisc.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/submit_case/submitCaseAPIMgr.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/DashBoardListType.dart';
import 'package:aitl/model/json/web2native/case/application/GetMortCasePaymentAPIModel.dart';
import 'package:aitl/model/json/web2native/case/doc/GetCaseDocAPIModel.dart';
import 'package:aitl/model/json/web2native/case/misc/caseusertype/CaseTaskBiddings.dart';
import 'package:aitl/model/json/web2native/case/misc/caseusertype/GetCaseUserTypeAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/GetCreditCommitAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/GetCreditHisAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/GetCreditHisItemsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetAssessAffordAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetAssessAffordItemsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetTaskAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbility.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/GetBtlPortfolioAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetContactAutoSugAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetES1ApiModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetES2APIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetESBnkAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/GetAddrHistoryAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/5dependents/GetDependentsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/GetEmpAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/GetIncomeAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/GetExistingPolicyAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/GetSavingInvAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/cur_res_mort/GetCurResMortApiModel.dart';
import 'package:aitl/view/db_cus/web2native/action_req/client_agreement_page.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/application/submit_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/doc/doc_case_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/10credit_commitment/cr_commit_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/11credit_history/cr_his_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/12assessment_affordibility/assess_afford_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/13btl_portfolio/btl_portf_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/4address_history/addr_history_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/6cur_residential_mort/cur_res_mort_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/7emp_income/emp_income_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/8existing_policy/existing_policy_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/9saving_investment/saving_inv_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/biz_landing/req_bl_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/bridging_loan/req_bloan_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/buy2letmortgage/req_b2letm_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/buy2letremortgage/req_b2letrm_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/com_mortgage_loans/req_cml_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/dev_finance/req_df_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/gen_insurance/req_ginc_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/let2buy/req_l2b_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/mortgage/req_mortgage_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/remortgage/req_remortgage_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/second_charge_buy2letcom/req_scb2letc_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/second_charge_residential/req_scr_page.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../config/MyTheme.dart';
import '../../../../config/SrvW2NCase.dart';
import '../../../../controller/network/NetworkMgr.dart';
import '../../../../model/data/UserData.dart';
import '../../../../model/json/web2native/case/requirements/equity_release/GetEquityReleasePowerAttorneyAPIModel.dart';
import '../../../../model/json/web2native/case/requirements/gen_insurance/GetGenInsRequirementsAPIModel.dart';
import '../../../../model/json/web2native/case/requirements/gen_insurance/GetPersonalProcessionAPIModel.dart';
import '../../../../model/json/web2native/case/requirements/mortgage/GetMortgageRequirementsAPIModel.dart';
import '../../../../model/json/web2native/case/requirements/remortgage/reasons/GetMortgageLoanReasonsAPIModel.dart';
import '../../../../view_model/api/api_view_model.dart';
import '../../../../view_model/helper/ui_helper.dart';
import '../../../widgets/progress/AppbarBotProgbar.dart';
import '../../../widgets/tile/ExpansionTileCard.dart';
import '../case_base.dart';
import 'views/requirements/2other_essentials/es_other_page.dart';
import 'views/requirements/3profile/profile_details_page.dart';
import 'views/requirements/5dependents/dependents_page.dart';
import 'views/requirements/equity_release/req_er_page.dart';

class CaseDetailsPage extends StatefulWidget {
  final Task caseModel;
  final bool isReqPage;
  const CaseDetailsPage({Key key, this.caseModel, this.isReqPage})
      : super(key: key);
  @override
  State<CaseDetailsPage> createState() => _CaseDetailsPageState();
}

class _CaseDetailsPageState extends CaseBase<CaseDetailsPage>
    with SingleTickerProviderStateMixin, StateListener {
  final GlobalKey<ExpansionTileCardState> keyReq = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyOtherES = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyPersonalDetails = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyDependents = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyAddrHistory = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyCurResMort = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyEmpIncome = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyExistingPolicy = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keySavingInvestment = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyCrCom = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyCrHis = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyAssessAfford = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyBtlPortfolio = GlobalKey();
  final GlobalKey<ExpansionTileCardState> keyDoc = GlobalKey();

  Widget widgetEmpIncome;

  var reqModel;

  bool isLoading = false;
  bool isReqLoading = false;
  bool isOtherLoading = false;
  var totalAffordableExpenses = 0.0;
  var reqCallback = {};

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state) async {
    try {
      if (state == ObserverState.STATE_W2N_REQUIREMENTS_LISTNER) {
        if (caseModelCtrl.locModel.value.title ==
            NewCaseCfg.getEnumCaseTitle(eCaseTitle.General_Insurance)) {
          reqModel = requirementsCtrl.mortgageBuildingAndContentInsurance.value;
        } else {
          reqModel = requirementsCtrl.requirementsModelAPIModel.value;
        }
        if (mounted) {
          setState(() {});
        }
      }
    } catch (e) {}
  }

  onGetRequirementsAPI() async {
    try {
      var shouldLoading = widget.isReqPage != null ? true : false;
      if (mounted) {
        setState(() {
          isLoading = true;
          isReqLoading = true;
          isOtherLoading = true;
        });
      }

      //  reset states
      requirementsCtrl.listMortgageReasons.clear();
      otherESCtrl.getES1Model.value = null;
      otherESCtrl.getES2Model.value = null;

      //  CASE USER TYPE -> ADVISOR, ADMIN, INTRODUCER
      var url = SrvW2NMisc.GET_CASE_USERTYPE_DETAILS_URL +
          caseModelCtrl.locModel.value.id.toString();
      await APIViewModel().req<GetCaseUserTypeAPIModel>(
        context: context,
        url: url,
        reqType: ReqType.Get,
        isLoading: shouldLoading,
        loadingMsg: shouldLoading ? "Redirecting...\nPlease wait" : null,
        callback: (model) async {
          if (model != null) {
            if (model.success) {
              if (model.responseData.taskBiddings.length > 0) {
                final list = model.responseData.taskBiddings;
                caseUserTypeCtrl.listCaseTaskBiddings.clear();
                //  avoid duplicate
                for (final l in list) {
                  var caseTaskBiddings = caseUserTypeCtrl.listCaseTaskBiddings[
                      caseUserTypeCtrl.listCaseTaskBiddings.indexWhere(
                          (element) => element.taskTitle == l.taskTitle)];
                  if (caseTaskBiddings.taskTitleUrl != l.taskTitle)
                    caseUserTypeCtrl.listCaseTaskBiddings.add(l);
                }
                caseUserTypeCtrl.update();
              } else {
                caseUserTypeCtrl.listCaseTaskBiddings.value = [
                  CaseTaskBiddings.fromJson({'CommunityId': 9}),
                  CaseTaskBiddings.fromJson({'CommunityId': 2}),
                  CaseTaskBiddings.fromJson({'CommunityId': 5}),
                ];
              }
              if (caseUserTypeCtrl.listCaseTaskBiddings.length < 3) {
                for (final m in caseUserTypeCtrl.listCaseTaskBiddings) {
                  for (final map in SrvW2NMisc.listUserType) {
                    final List listCommunityId = map['communityId'];
                    if (listCommunityId.contains(m.communityId) ||
                        m.taskTitle != map['Title']) continue;
                    caseUserTypeCtrl.listCaseTaskBiddings.add(
                        CaseTaskBiddings.fromJson(
                            {'CommunityId': listCommunityId[0]}));
                  }
                }
              }
            }
          }
        },
      );

      if (caseModelCtrl.locModel.value.title ==
          NewCaseCfg.getEnumCaseTitle(eCaseTitle.General_Insurance)) {
        //  REQUIREMENTS:

        var url = SrvW2NCase.GET_REQUIREMENTS_GEN_INC_URL +
            caseModelCtrl.locModel.value.id.toString();
        await APIViewModel().req<GetGenInsRequirementsAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            isLoading: shouldLoading,
            loadingMsg: shouldLoading ? "Redirecting...\nPlease wait" : null,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  requirementsCtrl.mortgageBuildingAndContentInsurance.value =
                      model.responseData.mortgageBuildingAndContentInsurance;
                  reqModel = requirementsCtrl
                      .mortgageBuildingAndContentInsurance.value;
                  requirementsCtrl.update();
                  StateProvider()
                      .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                  if (mounted) {
                    setState(() {
                      isReqLoading = false;
                    });
                  }
                }
              }
            });

        //  REQUIREMENTS: GENERAL INSURANCE

        url = SrvW2NCase.GET_PERSONAL_PROCESSION_URL +
            caseModelCtrl.locModel.value.id.toString();
        await APIViewModel().req<GetPersonalProcessionAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            isLoading: shouldLoading,
            loadingMsg: shouldLoading ? "Redirecting...\nPlease wait" : null,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  requirementsCtrl.listInsuranceItems.value = model
                      .responseData.mortgageBuildingAndContentInsuranceItems;
                  requirementsCtrl.update();
                  StateProvider()
                      .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                }
              }
            });
      } else {
        var url = SrvW2NCase.GET_REQUIREMENTS_URL +
            caseModelCtrl.locModel.value.id.toString();
        await APIViewModel().req<GetMortgageRequirementsAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            isLoading: shouldLoading,
            loadingMsg: shouldLoading ? "Redirecting...\nPlease wait" : null,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  requirementsCtrl.requirementsModelAPIModel.value =
                      model.responseData.mortgageUserRequirement;
                  reqModel = requirementsCtrl.requirementsModelAPIModel.value;
                  requirementsCtrl.update();
                  StateProvider()
                      .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                  if (mounted) {
                    setState(() {
                      isReqLoading = false;
                    });
                  }
                }
              }
            });

        //  ******************************************
        if (widget.isReqPage != null) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Get.to(reqCallback[caseModelCtrl.locModel.value.title]);
          });
        }

        //  REMORTGAGE REASONS

        url = SrvW2NCase.GET_MORTGAGELOAN_REASONS_URL +
            caseModelCtrl.locModel.value.id.toString();
        await APIViewModel().req<GetMortgageLoanReasonsAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  requirementsCtrl.listMortgageReasons.value =
                      model.responseData.mortgageLoanReasons;
                  requirementsCtrl.update();
                  StateProvider()
                      .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                }
              }
            });

        //  REQUIREMENTS: EQUITY RELEASE

        url = SrvW2NCase.GET_EQUITYRELEASE_POWERATTORNEY_URL +
            caseModelCtrl.locModel.value.id.toString();
        await APIViewModel().req<GetEquityReleasePowerAttorneyAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  requirementsCtrl.mortgageEquityReleasePowerOfAttorney.value =
                      model.responseData.mortgageEquityReleasePowerOfAttorney;
                  requirementsCtrl.update();
                }
              }
            });
      }

      //  OTHER ESSENTIAL INFORMATION
      if (caseModelCtrl.locModel.value.title !=
          NewCaseCfg.getEnumCaseTitle(eCaseTitle.Equity_Release)) {
        try {
          await APIViewModel().req<GetES1APIModel>(
              context: context,
              url: SrvW2NCase.GET_ES1_URL +
                  caseModelCtrl.locModel.value.id.toString(),
              reqType: ReqType.Get,
              isLoading: false,
              callback: (model) async {
                if (model != null) {
                  if (model.success) {
                    otherESCtrl.getES1Model.value =
                        model.responseData.mortgageUserOtherInfo;
                    otherESCtrl.update();
                  }
                }
              });
        } catch (e) {}
        try {
          await APIViewModel().req<GetES2APIModel>(
              context: context,
              url: SrvW2NCase.GET_ES2_URL +
                  caseModelCtrl.locModel.value.id.toString(),
              reqType: ReqType.Get,
              isLoading: false,
              callback: (model) async {
                if (model != null) {
                  if (model.success) {
                    otherESCtrl.getES2Model.value =
                        model.responseData.mortgageUserKeyInformation;
                    otherESCtrl.update();
                    if (mounted) {
                      setState(() {
                        isOtherLoading = false;
                      });
                    }
                  }
                }
              });
        } catch (e) {}
        try {
          await APIViewModel().req<GetContactAutoSugAPIModel>(
              context: context,
              url: SrvW2NCase.GET_LIST_CONTACT_AUTO_SUG_URL +
                  caseModelCtrl.locModel.value.id.toString(),
              reqType: ReqType.Get,
              isLoading: false,
              callback: (model) async {
                if (model != null) {
                  if (model.success) {
                    otherESCtrl.listContactModel.value =
                        model.responseData.mortgageUserOtherContactInfos;
                    otherESCtrl.update();
                  }
                }
              });
        } catch (e) {}
        try {
          await APIViewModel().req<GetESBnkAPIModel>(
              context: context,
              url: SrvW2NCase.GET_ESBANK_URL +
                  caseModelCtrl.locModel.value.id.toString(),
              reqType: ReqType.Get,
              isLoading: false,
              callback: (model) async {
                if (model != null) {
                  if (model.success) {
                    otherESCtrl.listBnkModel.value =
                        model.responseData.mortgageUserBankDetailList;
                    otherESCtrl.update();
                  }
                }
              });
        } catch (e) {}
      }

      //  DEPENDENTS
      try {
        var url = SrvW2NCase.GET_DEPENDENTS_URL.replaceAll(
            "#userCompanyId#", userData.userModel.userCompanyID.toString());
        url = url.replaceAll("#userId#", userData.userModel.id.toString());
        await APIViewModel().req<GetDependentsAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  dependentCtrl.listDependentsModelAPIModel.value =
                      model.responseData.mortgageFinancialDependants;
                  dependentCtrl.update();
                }
              }
            });
      } catch (e) {}

      //  ADDRESS HISTORY
      try {
        await APIViewModel().req<GetAddrHistoryAPIModel>(
            context: context,
            url: SrvW2NCase.GET_ADDR_HISTORY_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  addrHistoryCtrl.listUserAddress.value =
                      model.responseData.userAddresss;
                }
              }
            });
      } catch (e) {}

      //  CURRENT RESIDENTIAL MORTGAGE
      try {
        await APIViewModel().req<GetCurResMortApiModel>(
            context: context,
            url: SrvW2NCase.GET_CUR_RES_MORT_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  curResMortCtrl.curResMortCtrl.value =
                      model.responseData.mortgageUserCurrentResidentialMortgage;
                  curResMortCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  EMPLOYMENT

        await APIViewModel().req<GetEmpAPIModel>(
            context: context,
            url: SrvW2NCase.GET_EMP_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  empIncomeCtrl.empCtrl.value =
                      model.responseData.mortgageUserOccupations;
                  empIncomeCtrl.update();
                }
              }
            });

        //  INCOME

        await APIViewModel().req<GetIncomeAPIModel>(
            context: context,
            url: SrvW2NCase.GET_INCOME_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  empIncomeCtrl.incomeCtrl.value =
                      model.responseData.mortgageUserInComes;
                  empIncomeCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  EXISTING POLICY

        await APIViewModel().req<GetExistingPolicyAPIModel>(
            context: context,
            url: SrvW2NCase.GET_EXISTING_POLICY_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  existingPolicyCtrl.existingPolicyCtrl.value =
                      model.responseData.mortgageUserExistingPolicyItems;
                  existingPolicyCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  SAVING INVESTMENT

        await APIViewModel().req<GetSavingInvAPIModel>(
            context: context,
            url: SrvW2NCase.GET_SAVING_INV_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  savingInvCtrl.savingInvCtrl.value =
                      model.responseData.mortgageUserSavingOrInvestmentItems;
                  savingInvCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  CREDIT COMMITMENT

        await APIViewModel().req<GetCreditCommitAPIModel>(
            context: context,
            url: SrvW2NCase.GET_CREDIT_COMMITMENT_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  crComCtrl.crCommitCtrl.value =
                      model.responseData.mortgageUserAffordAbilitys;
                  crComCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  CREDIT HISTORY

        await APIViewModel().req<GetCreditHisAPIModel>(
            context: context,
            url: SrvW2NCase.GET_CREDIT_HISTORY_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  crHisCtrl.crHisCtrl.value =
                      model.responseData.mortgageUserCreditHistory;
                  crHisCtrl.update();
                }
              }
            });

        await APIViewModel().req<GetCreditHisItemsAPIModel>(
            context: context,
            url: SrvW2NCase.GET_CREDIT_HISTORYITEMS_URL +
                "UserId=" +
                userData.userModel.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  crHisCtrl.listCrHisItemsCtrl.value =
                      model.responseData.mortgageUserCreditHistoryItems;
                  crHisCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  ASSESSIBILITY AFFORDIBILITY
        await APIViewModel().req<GetTaskAPIModel>(
            context: context,
            url: SrvW2NCase.GET_TASK_URL +
                caseModelCtrl.locModel.value.id.toString(),
            reqType: ReqType.Get,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  assessAffordCtrl.taskCtrl.value = model.responseData.task;
                  assessAffordCtrl.update();
                  try {
                    if (assessAffordCtrl.taskCtrl.value
                            .mortgageCaseInfoEntityModelList.length >
                        0) {
                      setState(() {});
                    }
                  } catch (e) {}
                }
              }
            });
        await APIViewModel().req<GetAssessAffordAPIModel>(
            context: context,
            url: SrvW2NCase.GET_ASSESS_AFFORD_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  assessAffordCtrl.assessAffordModel.value =
                      model.responseData.mortgageUserAssesmentOfAffordAbility;
                  if (assessAffordCtrl.assessAffordModel.value == null) {
                    final mod = MortgageUserAssesmentOfAffordAbility();
                    mod.propertyMaintenanceGroundRent = 0.0;
                    mod.electric = 0.0;
                    mod.councilTax = 0.0;
                    mod.tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences =
                        0.0;
                    mod.buildingsContentsInsurance = 0.0;
                    mod.gas = 0.0;
                    mod.water = 0.0;
                    mod.otherDdetails = 0.0;
                    mod.familyFoodHouseholdCosts = 0.0;
                    mod.clothing = 0.0;
                    mod.homeHelp = 0.0;
                    mod.laundry = 0.0;
                    mod.mobilePhoneCosts = 0.0;
                    mod.homePhone = 0.0;
                    mod.internet = 0.0;
                    mod.investments = 0.0;
                    mod.maintenancePayments = 0.0;
                    mod.socialCostsMealsOutDrinksTheatre = 0.0;
                    mod.holiday = 0.0;
                    mod.sports = 0.0;
                    mod.cigarettes = 0.0;
                    mod.tobaccoOrRelatedProducts = 0.0;
                    mod.pensionContributions = 0.0;
                    mod.leisure = 0.0;
                    mod.travel = 0.0;
                    mod.regularSubscriptions = 0.0;
                    mod.petsFoodInsuranceGrooming = 0.0;
                    mod.insurancesLifeHealthMedicalDentalPhone = 0.0;
                    mod.regularSavings = 0.0;
                    mod.petrol = 0.0;
                    mod.commutingCosts = 0.0;
                    mod.carCosts = 0.0;
                    mod.carFuelCosts = 0.0;
                    mod.motorInsurance = 0.0;
                    mod.transportTrainTramBus = 0.0;
                    mod.childCareCosts = 0.0;
                    mod.regularSchoolFeesContributions = 0.0;
                    mod.otherSchoolingCostsMealsUniformOutings = 0.0;
                    mod.clothes = 0.0;
                    mod.remarks = '';
                    assessAffordCtrl.assessAffordModel.value = mod;
                  }
                  assessAffordCtrl.update();
                }
              }
            });

        await APIViewModel().req<GetAssessAffordItemsAPIModel>(
            context: context,
            url: SrvW2NCase.GET_ASSESS_AFFORD_ITEMS_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  assessAffordCtrl.listAssessAffordItemModel.value = model
                      .responseData.mortgageUserAssesmentOfAffordAbilityItems;
                  assessAffordCtrl.update();
                }
              }
            });

        totalAffordableExpenses = getAffordableTotalExpense();
      } catch (e) {}

      try {
        //  BTL PORTFILIO

        await APIViewModel().req<GetBtlPortfolioAPIModel>(
            context: context,
            url: SrvW2NCase.GET_BTL_PORTFOLIO_URL +
                "UserId=" +
                userData.userModel.id.toString() +
                "&UserCompanyId=" +
                userData.userModel.userCompanyInfo.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  btlPortfCtrl.listBtlPortModel.value =
                      model.responseData.mortgageUserPortfolios;
                  btlPortfCtrl.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  CASE DOCUMENT

        await APIViewModel().req<GetCaseDocAPIModel>(
            context: context,
            url: SrvW2NCase.GET_DOC_CASE_URL +
                caseModelCtrl.locModel.value.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  docCaseModel.listDocModel.value =
                      model.responseData.mortgageCaseDocumentInfos;
                  docCaseModel.update();
                }
              }
            });
      } catch (e) {}

      try {
        //  CASE APPLICATION

        await APIViewModel().req<GetMortCasePaymentAPIModel>(
            context: context,
            url: SrvW2NCase.GET_MORT_CASE_PAYMENT_URL +
                caseModelCtrl.locModel.value.id.toString(),
            reqType: ReqType.Get,
            isLoading: false,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  if (model.responseData.mortgageCasePaymentInfos.length == 0)
                    isCaseApplicationShow.value = true;
                  else
                    isCaseApplicationShow.value = false;
                }
              }
            });
      } catch (e) {}

      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {}
  }

  drawPreSubLayout() {
    //  Draw Employement Income Layout
    try {
      widgetEmpIncome = empIncomeCtrl.empCtrl != null &&
              empIncomeCtrl.empCtrl.length > 0
          ? Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              empIncomeCtrl.empCtrl.length > 0
                  ? Padding(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(width: .5),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: "Employment Information",
                                  txtColor: MyTheme.statusBarColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                              drawEmpExpandableList(
                                  context,
                                  empIncomeCtrl.empCtrl,
                                  false,
                                  (model, isEdit) {}),
                            ],
                          ),
                        ),
                      ),
                    )
                  : SizedBox(),
              empIncomeCtrl.incomeCtrl.length > 0
                  ? Padding(
                      padding: EdgeInsets.only(
                          top: empIncomeCtrl.incomeCtrl.length > 0 ? 5 : 0,
                          left: 5,
                          right: 5),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(width: .5),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: "Income Information",
                                  txtColor: MyTheme.statusBarColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                              drawIncomeExpandableList(
                                  context,
                                  empIncomeCtrl.incomeCtrl,
                                  false,
                                  (model, isEdit) {})
                            ],
                          ),
                        ),
                      ),
                    )
                  : SizedBox(),
              SizedBox(height: 10),
              showLessView(keyEmpIncome)
            ])
          : SizedBox();
      setState(() {});
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      /*requirementsCtrl.dispose();
      dependentCtrl.dispose();
      caseModelCtrl.dispose();
      mainCrCommitCtrl.dispose();
      reqStep2ESCtrl.dispose();*/
    } catch (e) {}
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      caseModelCtrl.locModel.value = widget.caseModel;
      reqCallback = {
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Residential_Mortgage): () =>
            ReqMortgagePage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Residential_Remortgage): () =>
            ReqReMortgagePage(),
        NewCaseCfg.getEnumCaseTitle(
            eCaseTitle.Second_Charge_0HYPN0_Residential): () => ReqSCRPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Buy_to_Let_Mortgage): () =>
            ReqB2LetMPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Buy_to_Let_Remortgage): () =>
            ReqB2LetRMPage(),
        NewCaseCfg.getEnumCaseTitle(
                eCaseTitle.Second_Charge_0HYPN0_Buy_to_Let_0AND0_Commercial):
            () => ReqSCB2LetCPage(),
        NewCaseCfg.getEnumCaseTitle(
            eCaseTitle.Commercial_Mortgages0SPLASH0_Loans): () => ReqCMLPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Business_Lending): () =>
            ReqBLPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Development_Finance): () =>
            ReqDFPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Let_to_Buy): () => ReqL2BPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Bridging_Loan): () =>
            ReqBLoanPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.Equity_Release): () =>
            ReqERPage(),
        NewCaseCfg.getEnumCaseTitle(eCaseTitle.General_Insurance): () =>
            ReqGIncPage(),
      };
    } catch (e) {}

    try {
      await onGetRequirementsAPI();
      drawPreSubLayout();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: true,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Case Details"),
                actions: [
                  //IconButton(onPressed: () {}, icon: Icon(Icons.chat_outlined)),
                  !isLoading
                      ? IconButton(
                          onPressed: () {
                            onGetRequirementsAPI();
                          },
                          icon: Icon(Icons.sync))
                      : SizedBox()
                ],
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
                  child: (isLoading)
                      ? AppbarBotProgBar(
                          backgroundColor: MyTheme.appbarProgColor)
                      : SizedBox(),
                ),
              ),
              body: drawLayout()),
        ),
      ),
    );
  }

  drawLayout() {
    return Obx(() => Container(
          child: SingleChildScrollView(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //_drawImportCreditRepView(),

              SizedBox(height: 5),
              Align(alignment: Alignment.center, child: drawCaseInfo(true)),
              SizedBox(height: 20),
              _drawUserTypeBox(),
              SizedBox(
                  height: caseModelCtrl.locModel.value.status !=
                          NewCaseCfg.SUBMITTED
                      ? 25
                      : 20),
              drawCaseRowTile(
                  key: keyReq,
                  icon: Icons.edit_outlined,
                  title: "Requirements",
                  expandableTitle: reqModel != null
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            showReqCaseExpandDetailsView(),
                            showLessView(keyReq)
                          ],
                        )
                      : null,
                  isLoading: isReqLoading,
                  callback: (isEdit) {
                    Get.to(reqCallback[caseModelCtrl.locModel.value.title]);
                  }),

              caseModelCtrl.locModel.value.title !=
                          NewCaseCfg.getEnumCaseTitle(
                              eCaseTitle.Equity_Release) &&
                      caseModelCtrl.locModel.value.title !=
                          NewCaseCfg.getEnumCaseTitle(
                              eCaseTitle.General_Insurance)
                  ? Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: drawCaseRowTile(
                          key: keyOtherES,
                          icon: Icons.home_outlined,
                          title: "Others",
                          expandableTitle: (otherESCtrl.getES1Model.value !=
                                      null &&
                                  otherESCtrl.getES2Model.value != null)
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    drawESDetailsView(
                                      context: context,
                                      caseModel: caseModelCtrl.locModel.value,
                                      otherESCtrl: otherESCtrl,
                                    ),
                                    !isOtherLoading
                                        ? showLessView(keyOtherES)
                                        : drawLoading()
                                  ],
                                )
                              : null,
                          isLoading: isOtherLoading,
                          callback: (isEdit) {
                            Get.to(() => ESOtherPage());
                          }))
                  : SizedBox(),
              drawEntityModelList(),
              SizedBox(height: 20),
              drawDivider(),
              _drawApplicantDetails(),
              drawCaseRowTile(
                  key: keyPersonalDetails,
                  icon: Icons.home_outlined,
                  title: "Personal Details",
                  expandableTitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      personalDetailView(),
                      showLessView(keyPersonalDetails)
                    ],
                  ),
                  callback: (isEdit) {
                    Get.to(() => ProfileDetailsPage());
                  }),
              drawDivider(),
              Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: drawCaseRowTile(
                      key: keyDependents,
                      icon: Icons.family_restroom_outlined,
                      title: "Dependents",
                      expandableTitle:
                          dependentCtrl.listDependentsModelAPIModel.length > 0
                              ? Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Column(
                                    children: [
                                      drawDependentItems(
                                          context,
                                          false,
                                          dependentCtrl
                                              .listDependentsModelAPIModel,
                                          (model) {},
                                          (model) {}),
                                      showLessView(keyDependents)
                                    ],
                                  ))
                              : null,
                      isLoading: isLoading,
                      callback: (isEdit) {
                        Get.to(() => DependentsPage());
                      })),
              drawDivider(),
              drawCaseRowTile(
                  key: keyAddrHistory,
                  icon: Icons.location_history_outlined,
                  title: "Address History",
                  expandableTitle:
                      dependentCtrl.listDependentsModelAPIModel.length > 0
                          ? Padding(
                              padding: const EdgeInsets.all(5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  drawAddrHistoryView(
                                    context,
                                    addrHistoryCtrl.listUserAddress,
                                    false,
                                    (modelEdit) {},
                                    (modelDel) {},
                                  ),
                                  showLessView(keyAddrHistory)
                                ],
                              ))
                          : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => AddrHistoryPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyCurResMort,
                  icon: Icons.home_work_outlined,
                  title: "Current Residential Mortgage",
                  expandableTitle: curResMortCtrl.curResMortCtrl.value != null
                      ? Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              drawCurResMortDetailsView(
                                context,
                                curResMortCtrl.curResMortCtrl.value,
                                caseModelCtrl.locModel.value,
                              ),
                              showLessView(keyCurResMort)
                            ],
                          ))
                      : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => CurResMortPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyEmpIncome,
                  icon: Icons.money_outlined,
                  title: "Employment and Income",
                  expandableTitle: empIncomeCtrl.empCtrl.length == 0
                      ? null
                      : widgetEmpIncome,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => EmpIncomePage())
                        .then((value) => drawPreSubLayout());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyExistingPolicy,
                  icon: Icons.policy_outlined,
                  title: "Existing Policies",
                  expandableTitle:
                      existingPolicyCtrl.existingPolicyCtrl.length > 0
                          ? Padding(
                              padding: const EdgeInsets.all(5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  drawExistingPolicyList(
                                    context,
                                    false,
                                    existingPolicyCtrl.existingPolicyCtrl,
                                    (model, isEdit) async {},
                                  ),
                                  showLessView(keyExistingPolicy)
                                ],
                              ))
                          : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => ExistingPolicyPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keySavingInvestment,
                  icon: Icons.policy_outlined,
                  title: "Saving & Investment",
                  expandableTitle: savingInvCtrl.savingInvCtrl.length > 0
                      ? Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              drawSavingInvList(
                                context,
                                false,
                                savingInvCtrl.savingInvCtrl,
                                (model, isEdit) async {},
                              ),
                              showLessView(keySavingInvestment)
                            ],
                          ))
                      : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => SavingInvPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyCrCom,
                  icon: Icons.calendar_view_month_outlined,
                  title: "Credit Commitment",
                  expandableTitle: crComCtrl.crCommitCtrl.length > 0
                      ? Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              drawCrCommitList(
                                context,
                                false,
                                crComCtrl,
                                (model, isEdit) async {},
                              ),
                              showLessView(keyCrCom)
                            ],
                          ))
                      : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => CrCommitPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyCrHis,
                  icon: Icons.history_outlined,
                  title: "Credit History",
                  expandableTitle: crHisCtrl.crHisCtrl.value != null
                      ? Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              drawCrHisDetails(crHisCtrl.crHisCtrl.value),
                              crHisCtrl.crHisCtrl.value
                                          .haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou ==
                                      'Yes'
                                  ? drawCrHisItems(
                                      context,
                                      false,
                                      crHisCtrl.listCrHisItemsCtrl,
                                      (model, isEdit) {},
                                    )
                                  : SizedBox(),
                              showLessView(keyCrHis)
                            ],
                          ),
                        )
                      : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => CreditHistoryPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyAssessAfford,
                  icon: Icons.assessment_outlined,
                  title: "Assessment of Affordability",
                  expandableTitle: totalAffordableExpenses > 0 ||
                          assessAffordCtrl.totalExpenses.value > 0
                      ? Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            children: [
                              drawAssessAffordDetails(
                                  context, assessAffordCtrl),
                              showLessView(keyAssessAfford)
                            ],
                          ))
                      : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => AssessAffordPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyBtlPortfolio,
                  icon: Icons.picture_in_picture,
                  title: "BTL Portfolio",
                  expandableTitle: btlPortfCtrl.listBtlPortModel.length > 0
                      ? Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Column(
                            children: [
                              drawBtlPortfolioItems(
                                  context,
                                  false,
                                  btlPortfCtrl.listBtlPortModel,
                                  (model, isEdit) {}),
                              showLessView(keyBtlPortfolio)
                            ],
                          ))
                      : null,
                  isLoading: isLoading,
                  callback: (isEdit) {
                    Get.to(() => BtlPortfolioPage());
                  }),
              drawDivider(),
              drawCaseRowTile(
                  key: keyDoc,
                  icon: Icons.file_present_outlined,
                  title: "Document Info",
                  expandableTitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: BtnOutline(
                              txt: "Upload document",
                              txtColor: Colors.black,
                              borderColor: MyTheme.titleColor,
                              radius: 5,
                              callback: () async {
                                Get.dialog(docCaseDialog(
                                  context: context,
                                  taskId: caseModelCtrl.locModel.value.id,
                                  model2: null,
                                  callbackSuccess: (model) {
                                    docCaseModel.listDocModel.add(model);
                                    docCaseModel.update();
                                    showAlert(
                                        context: context,
                                        msg: "Document submitted successfully");
                                    setState(() {});
                                  },
                                  callbackFailed: (err) {
                                    showToast(context: context, msg: err);
                                  },
                                ));
                              }),
                        ),
                      ),
                      drawDocLayout(context, docCaseModel.listDocModel,
                          (model2) {
                        Get.dialog(docCaseDialog(
                          context: context,
                          taskId: caseModelCtrl.locModel.value.id,
                          model2: model2,
                          callbackSuccess: (model) {
                            docCaseModel.listDocModel.remove(model2);
                            docCaseModel.listDocModel.add(model);
                            docCaseModel.update();
                            showAlert(
                                context: context,
                                msg: "Document submitted successfully");
                          },
                          callbackFailed: (err) {
                            showToast(context: context, msg: err);
                          },
                        ));
                      }, (file) {
                        showToast(
                            context: context,
                            msg: "Document file downloaded successfully.\n\n" +
                                file.path.replaceAll("emulated/0/", ""),
                            which: 1);
                      }),
                      showLessView(keyDoc),
                      SizedBox(height: 10),
                      Divider(color: Colors.black, height: 1),
                      SizedBox(height: 10),
                    ],
                  ),
                  isLoading: isLoading,
                  callback: (isEdit) {}),
              isCaseApplicationShow.value && !isLoading
                  ? Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Center(
                              child: new RichText(
                                textAlign: TextAlign.center,
                                text: new TextSpan(
                                  children: [
                                    new TextSpan(
                                      text:
                                          'By submitting this application, I accept the\n',
                                      style: new TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    new TextSpan(
                                      text: 'Terms and Conditions',
                                      style: new TextStyle(
                                          height: 1.5,
                                          color: Colors.blue,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () {
                                          final caseModel =
                                              caseModelCtrl.locModel.value;
                                          Get.to(() => ClientAgreementPage(
                                              dashBoardListType:
                                                  DashBoardListType(
                                                      type: "Terms of business",
                                                      taskID: caseModel.id
                                                          .toString(),
                                                      title: caseModel.title,
                                                      initiatorName:
                                                          caseModel.entityName,
                                                      comments: ""),
                                              type:
                                                  "Terms of Business")).then(
                                              (value) => Get.back());
                                          /*Get.to(
                                                  () => WebScreen(
                                                        title:
                                                            "Terms of business",
                                                        url: Server.TC_CASE_URL,
                                                      ),
                                                  transition:
                                                      Transition.downToUp,
                                                  duration: Duration(
                                                      milliseconds: AppConfig
                                                          .pageAnimationMilliSecond));*/
                                        },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: MMBtn(
                                    txt: "Back",
                                    height: getHP(context, 6),
                                    width: getW(context),
                                    bgColor: Colors.grey,
                                    radius: 10,
                                    callback: () async {
                                      Get.back();
                                    }),
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: MMBtn(
                                    txt: "Submit Application",
                                    height: getHP(context, 6),
                                    width: getW(context),
                                    radius: 10,
                                    callback: () async {
                                      Get.dialog(submitCaseApplicationDialog(
                                          context: context,
                                          width: getW(context),
                                          callback: () {
                                            final now = DateFun.getDate(
                                                DateTime.now().toString(),
                                                "dd-MMM-yyyy");
                                            SubmitCaseAPIMgr().wsOnPostCase(
                                                context: context,
                                                param: {
                                                  "Id": 0,
                                                  "UserId":
                                                      userData.userModel.id,
                                                  "Status": 101,
                                                  "MortgageCaseInfoId": 0,
                                                  "TaskId": caseModelCtrl
                                                      .locModel.value.id,
                                                  "CreationDate": now,
                                                  "UpdatedDate": now,
                                                  "VersionNumber": 0,
                                                  "DefineSolution": "",
                                                  "LenderProvider": "",
                                                  "Term": "",
                                                  "LoanAmount": 0,
                                                  "Rate": 0,
                                                  "Value": 0,
                                                  "FixedPeriodEnds": "",
                                                  "ProcFees": 0,
                                                  "AdviceFee": 0,
                                                  "IntroducerFeeShare": 0,
                                                  "IntroducerPaymentMade": "",
                                                  "ReasonForRecommendation": "",
                                                  "AnyOtherComments": "",
                                                  "CaseStatus": "",
                                                  "Remarks": "",
                                                  "MonthlyPaymentAmount": 0,
                                                  "OtherChargeAmount": 0,
                                                  "RentalIncomeAmount": 0,
                                                  "ValuationDate": "",
                                                  "FileUrl": ""
                                                },
                                                callback: (model) {
                                                  if (model != null) {
                                                    if (model.success) {
                                                      showToast(
                                                          context: context,
                                                          msg:
                                                              "Application submitted successfully");
                                                      Future.delayed(
                                                          Duration(seconds: 3),
                                                          () => Get.back());
                                                    }
                                                  }
                                                });
                                          }));
                                    }),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : SizedBox(),
            ],
          )),
        ));
  }

  _drawUserTypeBox() {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        primary: false,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            ...caseUserTypeCtrl.listCaseTaskBiddings
                .map((model) => showUserTypeUI(model))
          ],
        ));
  }

  _drawImportCreditRepView() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: MMBtn(
            txt: "Import credit report",
            width: getWP(context, 50),
            height: 35,
            callback: () {}),
      ),
    );
  }

  _drawApplicantDetails() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Txt(
                txt: "Applicant 1 " +
                    caseModelCtrl.locModel.value.ownerName.toString() +
                    " details",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .1,
                txtAlign: TextAlign.start,
                isBold: true),
          ],
        ),
      ),
    );
  }
}
