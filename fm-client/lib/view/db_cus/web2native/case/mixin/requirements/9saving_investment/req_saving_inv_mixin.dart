import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/MortgageUserSavingOrInvestmentItem.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/PriceBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';

mixin ReqStep9SavingInvestmentMixin {
  final listSaving = [
    'Savings',
    'Investments',
    'Other Assets',
    'Emergency Fund'
  ];

  drawSavingInvList(
    BuildContext context,
    bool isShowAction,
    List<MortgageUserSavingOrInvestmentItem> listSavingInvModel,
    Function(MortgageUserSavingOrInvestmentItem model, bool isEdit) callback,
  ) {
    if (listSavingInvModel == null) SizedBox();
    if (listSavingInvModel.length == 0) return SizedBox();
    final List<String> listSavingInvType = [];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...listSaving.map(
          (e) {
            print(e);
            //sleep(Duration(seconds: 1));
            return Column(
              children: [
                ...listSavingInvModel.map((v) {
                  if (v.typeofAsset == e && !listSavingInvType.contains(e)) {
                    listSavingInvType.add(e);
                    return _waitFun(
                        context, isShowAction, listSavingInvModel, e,
                        (model, isEdit) {
                      callback(model, isEdit);
                    });
                  } else {
                    return SizedBox();
                  }
                })
              ],
            );
          },
        ),
      ],
    );
  }

  _waitFun(
    BuildContext context,
    bool isShowAction,
    List<MortgageUserSavingOrInvestmentItem> listSavingInvModel,
    String e,
    Function(MortgageUserSavingOrInvestmentItem model, bool isEdit) callback,
  ) {
    bool isDone = false;
    final ww = Padding(
        padding: EdgeInsets.only(
            left: isShowAction ? 20 : 5, right: isShowAction ? 20 : 5, top: 5),
        child: Card(
          elevation: 5,
          color: MyTheme.bgColor2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    border:
                        Border.all(color: MyTheme.statusBarColor, width: .5)),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ...listSavingInvModel.map(
                        ((model) {
                          if (model.typeofAsset == e) {
                            final tit = e == "Savings"
                                ? "Saving type"
                                : e == "Investments"
                                    ? "Investment type"
                                    : e == "Other Assets"
                                        ? "Asset type"
                                        : "Details";
                            Widget wid = SizedBox();
                            Widget widTitle = SizedBox();
                            widTitle = !isDone
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Txt(
                                          txt: e,
                                          txtColor: MyTheme.statusBarColor,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w500,
                                          isBold: false),
                                      SizedBox(height: 10),
                                      Material(
                                        elevation: 2,
                                        color: MyTheme.bgColor,
                                        child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                flex: 2,
                                                child: Txt(
                                                    txt: tit,
                                                    txtColor: Colors.white,
                                                    txtSize:
                                                        MyTheme.txtSize - .6,
                                                    txtAlign: TextAlign.start,
                                                    fontWeight: FontWeight.w500,
                                                    isBold: true),
                                              ),
                                              tit != 'Details'
                                                  ? Expanded(
                                                      flex: 2,
                                                      child: Txt(
                                                          txt: "Owners",
                                                          txtColor:
                                                              Colors.white,
                                                          txtSize:
                                                              MyTheme.txtSize -
                                                                  .6,
                                                          txtAlign:
                                                              TextAlign.start,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          isBold: true),
                                                    )
                                                  : SizedBox(),
                                              isShowAction
                                                  ? Expanded(
                                                      flex: 2,
                                                      child: SizedBox())
                                                  : SizedBox(),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                : SizedBox();
                            isDone = true;
                            switch (model.typeofAsset) {
                              case 'Savings':
                                wid = _drawSavings(context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              case 'Investments':
                                wid = _drawInvestments(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              case 'Other Assets':
                                wid = _drawOtherAssets(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              case 'Emergency Fund':
                                wid = _drawEmergencyFund(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              default:
                            }
                            return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  widTitle,
                                  wid,
                                ]);
                          } else {
                            return SizedBox();
                          }
                        }),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
    return ww;
  }

  _drawSavings(
      BuildContext context,
      bool isShowAction,
      MortgageUserSavingOrInvestmentItem model,
      Function(MortgageUserSavingOrInvestmentItem model, bool isEdit)
          callback) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isShowAction ? 2 : 3,
                child: Txt(
                    txt: model.savingsType,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: 2,
                child: Txt(
                    txt: model.owners ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isShowAction
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callback(model, true);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          GestureDetector(
                            onTap: () {
                              confirmDialog(
                                  context: context,
                                  title: "Confirmation!!",
                                  msg: "Are you sure to delete this item?",
                                  callbackYes: () {
                                    callback(model, false);
                                  });
                            },
                            child: Icon(Icons.delete_outline,
                                color: Colors.grey, size: 20),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper().draw2Row("Provider", model.provider),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Objective", model.objective),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Balance",
                      AppDefine.CUR_SIGN + model.balance.toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Date",
                      DateFun.getDate(model.balanceDate, "dd-MMM-yyyy")),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Interest Rate (%)",
                      model.interestRate.toStringAsFixed(0) + "%"),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Maturity Date",
                      DateFun.getDate(model.maturityDate, "dd-MMM-yyyy")),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Income taken/reinvested", model.incomeTakenOrReinvested),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Account Number", model.accountNumber),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Repay before mortgage completes?",
                      model.areRegularDepositOrOrInvestmentBeingMade),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Amount",
                      AppDefine.CUR_SIGN +
                          model.depositOrInvestmentAmount.toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Frequency", model.depositOrInvestmentFrequency),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Will this form part of the estate on death?",
                      model.willThisFormPartOfTheEstateOnDeath),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Will this form part of the Spouse’s estate on death?",
                      model.willThisFormPartOfTheSpousesEstateOnDeath),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Further Details", model.furtherDetails),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _drawInvestments(
      BuildContext context,
      bool isShowAction,
      MortgageUserSavingOrInvestmentItem model,
      Function(MortgageUserSavingOrInvestmentItem model, bool isEdit)
          callback) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isShowAction ? 2 : 3,
                child: Txt(
                    txt: model.savingsType,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: 2,
                child: Txt(
                    txt: model.owners ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isShowAction
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callback(model, true);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          GestureDetector(
                              onTap: () {
                                confirmDialog(
                                    context: context,
                                    title: "Confirmation!!",
                                    msg: "Are you sure to delete this item?",
                                    callbackYes: () {
                                      callback(model, false);
                                    });
                              },
                              child: Icon(Icons.delete_outline,
                                  color: Colors.grey, size: 20)),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper().draw2Row("Platform", model.platform),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Provider", model.provider),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Objective", model.objective),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Initial Investment",
                      getCurSign() +
                          model.initialInvestmentAmount.toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Start Date",
                      DateFun.getDate(
                          model.initialInvestmentStartDate, "dd-MMM-yyyy")),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Valuation",
                      AppDefine.CUR_SIGN +
                          model.valuationAmount.toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Valuation Date",
                      DateFun.getDate(model.valuationDate, "dd-MMM-yyyy")),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Interest rate/yield (%)",
                      model.interestRate.toStringAsFixed(0) + "%"),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Maturity Date",
                      DateFun.getDate(model.maturityDate, "dd-MMM-yyyy")),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Account Number", model.accountNumber),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Repay before mortgage completes?",
                      model.areRegularDepositOrOrInvestmentBeingMade),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Amount",
                      AppDefine.CUR_SIGN +
                          model.initialInvestmentAmount.toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Frequency", model.depositOrInvestmentFrequency),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Is regular income currently being drawn?",
                      model.isRegularIncomeCurrentlyBeingDrawn),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Amount",
                      AppDefine.CUR_SIGN +
                          model.incomeCurrentlyBeingDrawnAmount
                              .toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Frequency", model.incomeCurrentlyBeingDrawnFrequency),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Will this form part of the estate on death?",
                      model.willThisFormPartOfTheEstateOnDeath),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Will this form part of the Spouse’s estate on death?",
                      model.willThisFormPartOfTheEstateOnDeath),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Further Details", model.furtherDetails),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _drawOtherAssets(
      BuildContext context,
      bool isShowAction,
      MortgageUserSavingOrInvestmentItem model,
      Function(MortgageUserSavingOrInvestmentItem model, bool isEdit)
          callback) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isShowAction ? 2 : 3,
                child: Txt(
                    txt: model.savingsType,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: 2,
                child: Txt(
                    txt: model.owners ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isShowAction
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callback(model, true);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          GestureDetector(
                            onTap: () {
                              confirmDialog(
                                  context: context,
                                  title: "Confirmation!!",
                                  msg: "Are you sure to delete this item?",
                                  callbackYes: () {
                                    callback(model, false);
                                  });
                            },
                            child: Icon(Icons.delete_outline,
                                color: Colors.grey, size: 20),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper()
                      .draw2Row("Asset description", model.assetDescription),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Purchase Price",
                      AppDefine.CUR_SIGN + model.purchasePrice.toString()),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Purchase Date",
                      DateFun.getDate(model.purchaseDate, "dd-MMM-yyyy")),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Valuation",
                      AppDefine.CUR_SIGN +
                          model.valuationAmount.toStringAsFixed(0)),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Valuation Date",
                      DateFun.getDate(model.valuationDate, "dd-MMM-yyyy")),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _drawEmergencyFund(
      BuildContext context,
      bool isShowAction,
      MortgageUserSavingOrInvestmentItem model,
      Function(MortgageUserSavingOrInvestmentItem model, bool isEdit)
          callback) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isShowAction ? 4 : 5,
                child: Txt(
                    txt: model.doesTheClientHaveAnEmergencyFundDetails != ''
                        ? model.doesTheClientHaveAnEmergencyFundDetails
                        : 'N/A',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isShowAction
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callback(model, true);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          GestureDetector(
                            onTap: () {
                              confirmDialog(
                                  context: context,
                                  title: "Confirmation!!",
                                  msg: "Are you sure to delete this item?",
                                  callbackYes: () {
                                    callback(model, false);
                                  });
                            },
                            child: Icon(Icons.delete_outline,
                                color: Colors.grey, size: 20),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper().draw2Row(
                      "Were any investments surrendered or cashed in during the last 12 months?",
                      model
                          .wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months),
                  SizedBox(height: 5),
                  UIHelper().draw2Row(
                      "Details:",
                      model
                          .wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
