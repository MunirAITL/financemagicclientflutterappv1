import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/MortgageUserExistingPolicyItem.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/PriceBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

mixin ReqStep8ExistingPolicyMixin {
  final listPolicy = [
    'Life Insurance',
    'Income Protection Policies',
    'Buildings & Contents Policy',
    'Cancelled Protection'
  ];

  drawExistingPolicyList(
    BuildContext context,
    bool isShowAction,
    List<MortgageUserExistingPolicyItem> listExistingPolicyModel,
    Function(MortgageUserExistingPolicyItem model, bool isEdit) callback,
  ) {
    if (listExistingPolicyModel.length == 0) return SizedBox();
    final List<String> listPolicyType = [];

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...listPolicy.map(
            (e) {
              print(e);
              //sleep(Duration(seconds: 1));
              return Column(
                children: [
                  ...listExistingPolicyModel.map((v) {
                    if (v.policyType == e && !listPolicyType.contains(e)) {
                      listPolicyType.add(e);
                      return _waitFun(
                          context, isShowAction, listExistingPolicyModel, e,
                          (model, isEdit) {
                        callback(model, isEdit);
                      });
                    } else {
                      return SizedBox();
                    }
                  })
                ],
              );
            },
          ),
        ],
      ),
    );
  }

  _waitFun(
    BuildContext context,
    bool isShowAction,
    List<MortgageUserExistingPolicyItem> listExistingPolicyModel,
    String e,
    Function(MortgageUserExistingPolicyItem model, bool isEdit) callback,
  ) {
    bool isDone = false;
    final ww = Padding(
        padding: EdgeInsets.only(
            left: isShowAction ? 20 : 5, right: isShowAction ? 20 : 5, top: 5),
        child: Card(
          elevation: 5,
          color: MyTheme.bgColor2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    border:
                        Border.all(color: MyTheme.statusBarColor, width: .5)),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ...listExistingPolicyModel.map(
                        ((model) {
                          final txt = e == "Life Insurance"
                              ? "Life Policy Type"
                              : e == "Income Protection Policies"
                                  ? "IP Cover Types"
                                  : e == "Buildings & Contents Policy"
                                      ? "Policy Type"
                                      : "";
                          if (model.policyType == e) {
                            Widget wid = SizedBox();
                            Widget widTitle = SizedBox();
                            widTitle = !isDone
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Txt(
                                          txt: e,
                                          txtColor: MyTheme.statusBarColor,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w500,
                                          isBold: false),
                                      SizedBox(height: 10),
                                      Material(
                                        elevation: 2,
                                        color: MyTheme.bgColor,
                                        child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              txt != ''
                                                  ? Expanded(
                                                      child: Txt(
                                                          txt: txt,
                                                          txtColor:
                                                              Colors.white,
                                                          txtSize:
                                                              MyTheme.txtSize -
                                                                  .6,
                                                          txtAlign:
                                                              TextAlign.start,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          isBold: true),
                                                    )
                                                  : SizedBox(),
                                              Expanded(
                                                child: Txt(
                                                    txt: e ==
                                                            "Cancelled Protection"
                                                        ? "Details"
                                                        : "Policy Number",
                                                    txtColor: Colors.white,
                                                    txtSize:
                                                        MyTheme.txtSize - .6,
                                                    txtAlign: TextAlign.start,
                                                    fontWeight: FontWeight.w500,
                                                    isBold: true),
                                              ),
                                              isShowAction
                                                  ? Expanded(
                                                      child: Txt(
                                                          txt: "Action",
                                                          txtColor:
                                                              Colors.white,
                                                          txtSize:
                                                              MyTheme.txtSize -
                                                                  .6,
                                                          txtAlign:
                                                              TextAlign.start,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          isBold: true),
                                                    )
                                                  : SizedBox(),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                : SizedBox();
                            isDone = true;
                            switch (model.policyType) {
                              case 'Life Insurance':
                                wid = _drawLifeInsurance(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              case 'Income Protection Policies':
                                wid = _drawIncomeProtectionPolicies(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              case 'Buildings & Contents Policy':
                                wid = _drawBuildingsContentsPolicy(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              case 'Cancelled Protection':
                                wid = _drawCancelledProtection(
                                    context, isShowAction, model,
                                    (model, isEdit) {
                                  callback(model, isEdit);
                                });
                                break;
                              default:
                            }
                            return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  widTitle,
                                  wid,
                                ]);
                          } else {
                            return SizedBox();
                          }
                        }),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
    return ww;
  }

  _drawLifeInsurance(
      BuildContext context,
      bool isShowAction,
      MortgageUserExistingPolicyItem model,
      Function(MortgageUserExistingPolicyItem model, bool isEdit) callback) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTileTheme(
          contentPadding: EdgeInsets.all(0),
          iconColor: Colors.black,
          child: Theme(
            data: ThemeData.light()
                .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
            child: ExpansionTile(
              //trailing: SizedBox.shrink(),
              title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: isShowAction ? 2 : 4,
                    child: Txt(
                        txt: model.policyNumber,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Expanded(
                    flex: isShowAction ? 2 : 3,
                    child: Txt(
                        txt: model.lifePolicyType ?? '',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  isShowAction
                      ? Flexible(
                          child: Row(
                            children: [
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      callback(model, true);
                                    },
                                    child: Icon(Icons.edit_outlined,
                                        color: Colors.grey, size: 20)),
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      confirmDialog(
                                          context: context,
                                          title: "Confirmation!!",
                                          msg:
                                              "Are you sure to delete this item?",
                                          callbackYes: () {
                                            callback(model, false);
                                          });
                                    },
                                    child: Icon(Icons.delete_outline,
                                        color: Colors.grey, size: 20)),
                              ),
                            ],
                          ),
                        )
                      : SizedBox()
                ],
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      UIHelper().draw2Row("Provider", model.provider),
                      SizedBox(height: 5),
                      UIHelper()
                          .draw2Row("Other Provider", model.providerOtherName),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Purpose", model.purpose),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Premium",
                          getCurSign() + model.premium.toStringAsFixed(0)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Frequency", model.frequency),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Cover Amount",
                          getCurSign() + model.coverAmount.toStringAsFixed(0)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "CIC/SIC Sum Insured", model.cICOrSICSumInsured),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "CIC/SIC Benefit Type", model.cICOrSICBenefitType),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Policy Start Date",
                          DateFun.getDate(
                              model.policyStartDate, "dd-MMM-yyyy")),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Term Basis", model.termBasis),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Guaranteed", model.guaranteed),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Renewable", model.renewable),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Is the policy indexed?", model.isThePolicyIndexed),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Does the policy include waiver of premium?",
                          model.doesThePolicyIncludeWaiverOfPremium),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Is the policy on trust?", model.isThePolicyOnTrust),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Is the policy used with a mortgage?",
                          model.isThePolicyUsedWithAMortgage),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Policy to be cancelled or replaced?",
                          model.isThePolicyToBeCancelledOrReplaced),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Need for cover", model.needForCover),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Other Applicable Benefits",
                          model.otherApplicableBenefits),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Notes", model.notes),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  _drawIncomeProtectionPolicies(
      BuildContext context,
      bool isShowAction,
      MortgageUserExistingPolicyItem model,
      Function(MortgageUserExistingPolicyItem model, bool isEdit) callback) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTileTheme(
          contentPadding: EdgeInsets.all(0),
          iconColor: Colors.black,
          child: Theme(
            data: ThemeData.light()
                .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
            child: ExpansionTile(
              //trailing: SizedBox.shrink(),
              title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: isShowAction ? 3 : 4,
                    child: Txt(
                        txt: model.lifePolicyType ?? '',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Expanded(
                    flex: isShowAction ? 2 : 3,
                    child: Txt(
                        txt: model.policyNumber,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  isShowAction
                      ? Flexible(
                          child: Row(
                            children: [
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      callback(model, true);
                                    },
                                    child: Icon(Icons.edit_outlined,
                                        color: Colors.grey, size: 20)),
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      confirmDialog(
                                          context: context,
                                          title: "Confirmation!!",
                                          msg:
                                              "Are you sure to delete this item?",
                                          callbackYes: () {
                                            callback(model, false);
                                          });
                                    },
                                    child: Icon(Icons.delete_outline,
                                        color: Colors.grey, size: 20)),
                              ),
                            ],
                          ),
                        )
                      : SizedBox()
                ],
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      UIHelper().draw2Row("Provider", model.provider),
                      model.providerOtherName != ""
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: UIHelper().draw2Row(
                                  "Other Provider", model.providerOtherName))
                          : SizedBox(),
                      //SizedBox(height: 5),
                      //UIHelper().draw2Row("Purpose", model.purpose),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Premium",
                          getCurSign() + model.premium.toStringAsFixed(0)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Frequency", model.frequency),
                      SizedBox(height: 5),
                      UIHelper()
                          .draw2Row("Initial benefit", model.initialbenefit),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Initial deferred period",
                          model.initialDeferredPeriod),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Additional Benefit", model.additionalBenefit),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Additional deferred period",
                          model.additionalDeferredPeriod),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Term Basis", model.termBasis),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Is the policy indexed?", model.isThePolicyIndexed),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Is there a waiver of premium?",
                          model.doesThePolicyIncludeWaiverOfPremium),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Is the policy used with a mortgage?",
                          model.isThePolicyUsedWithAMortgage),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Policy to be cancelled or rejected?",
                          model.isThePolicyToBeCancelledOrReplaced),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Need for cover", model.needForCover),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Other Applicable Benefits",
                          model.otherApplicableBenefits),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Notes", model.notes),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  _drawBuildingsContentsPolicy(
      BuildContext context,
      bool isShowAction,
      MortgageUserExistingPolicyItem model,
      Function(MortgageUserExistingPolicyItem model, bool isEdit) callback) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTileTheme(
          contentPadding: EdgeInsets.all(0),
          iconColor: Colors.black,
          child: Theme(
            data: ThemeData.light()
                .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
            child: ExpansionTile(
              //trailing: SizedBox.shrink(),
              title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: isShowAction ? 1 : 3,
                    child: Txt(
                        txt: model.policyNumber,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Expanded(
                    flex: isShowAction ? 2 : 3,
                    child: Txt(
                        txt: model.lifePolicyType ?? '',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  isShowAction
                      ? Flexible(
                          child: Row(
                            children: [
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      callback(model, true);
                                    },
                                    child: Icon(Icons.edit_outlined,
                                        color: Colors.grey, size: 20)),
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      confirmDialog(
                                          context: context,
                                          title: "Confirmation!!",
                                          msg:
                                              "Are you sure to delete this item?",
                                          callbackYes: () {
                                            callback(model, false);
                                          });
                                    },
                                    child: Icon(Icons.delete_outline,
                                        color: Colors.grey, size: 20)),
                              ),
                            ],
                          ),
                        )
                      : SizedBox()
                ],
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      UIHelper().draw2Row("Provider", model.provider),
                      SizedBox(height: 5),
                      UIHelper()
                          .draw2Row("Other Provider", model.providerOtherName),
                      SizedBox(height: 5),
                      UIHelper()
                          .draw2Row("Property Address", model.propertyAddress),
                      SizedBox(height: 5),
                      UIHelper()
                          .draw2Row("No claims bonus", model.noClaimsBonus),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Premium",
                          getCurSign() + model.premium.toStringAsFixed(0)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Frequency", model.frequency),
                      SizedBox(height: 5),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Cover Amount",
                          getCurSign() + model.coverAmount.toStringAsFixed(0)),
                      UIHelper().draw2Row(
                          "Accidental damage?", model.accidentalDamage),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Are personal possessions included?",
                          model.arePersonalPossessionsIncluded),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Is cycle cover included?",
                          model.isCycleCoverIncluded),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Are specific items included?",
                          model.areSpecificItemsIncluded),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Is the policy to be cancelled or replaced?",
                          model.isThePolicyToBeCancelledOrReplaced),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Is the policy used with a mortgage?",
                          model.isThePolicyUsedWithAMortgage),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Policy Start Date",
                          DateFun.getDate(
                              model.policyStartDate, "dd-MMM-yyyy")),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Policy Expiry Date",
                          DateFun.getDate(
                              model.policyExpiryDate, "dd-MMM-yyyy")),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Need for cover", model.needForCover),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Other Applicable Benefits",
                          model.otherApplicableBenefits),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Notes", model.notes),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  _drawCancelledProtection(
      BuildContext context,
      bool isShowAction,
      MortgageUserExistingPolicyItem model,
      Function(MortgageUserExistingPolicyItem model, bool isEdit) callback) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTileTheme(
          contentPadding: EdgeInsets.all(0),
          iconColor: Colors.black,
          child: Theme(
            data: ThemeData.light()
                .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
            child: ExpansionTile(
              //trailing: SizedBox.shrink(),
              title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: isShowAction ? 3 : 5,
                    child: Txt(
                        txt: model.details == "" ? "N/A" : model.details,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  isShowAction
                      ? Flexible(
                          child: Row(
                            children: [
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      callback(model, true);
                                    },
                                    child: Icon(Icons.edit_outlined,
                                        color: Colors.grey, size: 20)),
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      confirmDialog(
                                          context: context,
                                          title: "Confirmation!!",
                                          msg:
                                              "Are you sure to delete this item?",
                                          callbackYes: () {
                                            callback(model, false);
                                          });
                                    },
                                    child: Icon(Icons.delete_outline,
                                        color: Colors.grey, size: 20)),
                              ),
                            ],
                          ),
                        )
                      : SizedBox()
                ],
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      UIHelper().draw2Row(
                          "Any policies lapsed or cancelled in the last 12 months?",
                          model.anyPoliciesLapsedOrCancelledInTheLast12Months),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
