import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../../config/MyTheme.dart';
import '../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../view_model/rx/web2native/requirements/requirements_ctrl.dart';
import '../../../../../widgets/txt/Txt.dart';

mixin ReqERMixin {
  drawERDetailsView({
    BuildContext context,
    RequirementsCtrl requirementsCtrl,
    Task caseModel,
  }) {
    final model = requirementsCtrl.requirementsModelAPIModel;
    if (model.value == null) return SizedBox();
    final modelER = requirementsCtrl.mortgageEquityReleasePowerOfAttorney;
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().draw2Row("Current Address & Security Property?",
                model.value.addressOfPropertyToBeMortgaged ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Do you have any existing lifetime mortgage (Equity Release product) on this property?",
                model.value.doYouHaveExistingMortgageOnTheProperty ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Current Balance Amount",
                model.value.balanceOutstanding != null
                    ? UIHelper().addCurSign(
                        model.value.balanceOutstanding.toStringAsFixed(0))
                    : ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Current Lender", model.value.currentLender ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Current Lender Account Number",
                model.value.currentLenderAccountNumber ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Date property purchased",
                DateFun.getDate(
                    model.value.datePropertyPurchased, 'dd MMMM yyyy')),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Purchase price",
                model.value.purchasePrice != null
                    ? UIHelper().addCurSign(
                        model.value.purchasePrice.toStringAsFixed(0))
                    : ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much do you want to raise (Loan Amount)?",
                model.value.howMuchDoYouWishToBorrow != null
                    ? UIHelper().addCurSign(
                        model.value.howMuchDoYouWishToBorrow.toStringAsFixed(0))
                    : '0'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Preferred mortgage term",
                model.value.preferredMortgageTermYear.toString() +
                    ' Year ' +
                    model.value.preferredMortgageTermMonth.toString() +
                    ' Month'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Preferred Repayment type",
                model.value.preferredRepaymentType ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Are funds available to pay fees in connection with mortgage?",
                model.value
                        .areFundsAvailableToPayFeesInConnectionWithMortgage ??
                    ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much funds available to pay fees in connection with mortgage?",
                model.value.areFundsAvailableToPayFeesInConnectionWithMortgageValue !=
                        null
                    ? UIHelper().addCurSign(model.value
                        .areFundsAvailableToPayFeesInConnectionWithMortgageValue
                        .toStringAsFixed(0))
                    : ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Security Property Details\nWhat type of property is this?",
                model.value.propertyType ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Property Tenure", model.value.propertyTenure ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Year left on lease",
                model.value.ifLeaseholdHowLongIsLeftOnTheLease ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of bedrooms",
                model.value.numberOfBedrooms.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of Kitchens",
                model.value.numberOfKoichens.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of Bathroom",
                model.value.numberOfBaahroom.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much is the ground rent?",
                model.value.groundRentAmount != null
                    ? model.value.groundRentAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.groundRentAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much is the service charge?",
                model.value.serviceChargeAmount != null
                    ? model.value.serviceChargeAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.serviceChargeAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Number of W/C", model.value.numberOfWC.toString() ?? '0'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Floors in the building",
                model.value.floorsInTheBuilding ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is the flat above shop/ restaurant?",
                model.value.isTheFlatAboveShopRestaurant ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Does the property have a lift?",
                model.value.doesThePropertyHaveALift ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Which floor is the property",
                model.value.whichFloorIsTheProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Year property was built",
                model.value.yearPropertyWasBuilt ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Any extension or Loft Conversion done?",
                model.value.anyExtensionOrLoftConversionDone ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Is the property of a non-standard construction (ie. thatched roof, barn conversion etc)",
                model.value.isThePropertyOfANonStandardConstruction ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Non-standard Construction Details",
                model.value.nonstandardConstructionDetails ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Is it Ex Council? Or purchased from council directly",
                model.value.isItExCouncil ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Does the flat have deck or balcony access?",
                model.value.doesTheFlatHaveDeckORBalconyAccess ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is this a new built property?",
                model.value.isThisANewBuiltProperty ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Dose this property have warranty?",
                model.value.doesThisPropertyHaveWarranty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "If so what warranty", model.value.warrantyType ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Notes", model.value.notes ?? ''),
            SizedBox(height: 10),
            modelER.value != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper().draw2Row(
                          "Third party present at the discussion?",
                          modelER.value.thirdPartyAtDiscussion ?? 'No'),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("Relationship To Other Client",
                          modelER.value.relationshipToOtherClient ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Statement of Health including medications, etc",
                          modelER.value.statementOfHealthIncludingMedication ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Detail about client(s) objectives and priorities",
                          modelER.value
                                  .detailHereTheClientObjectivesAndPriorities ??
                              ''),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Power Of Attorney",
                          txtColor: MyTheme.statusBarColor,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          fontWeight: FontWeight.w500,
                          isBold: false),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Do you have any arrangements in place?",
                          modelER.value.haveArrangementsInPlaceDetails ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Can other assets be considered as an alternative?",
                          modelER.value.canOtherAssetsConsideredAsAlternative ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Have you considered accessing pension benefits as an alternative?",
                          modelER.value.haveConsideredPensionAsAlternative ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Will any state benefits be affected?",
                          modelER.value.willStateBenefitsAffected ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Would family members or friends be prepared to provide financial support?",
                          modelER.value.wouldFamilyPreparedForSupport ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Would you be prepared to pay monthly interest on any monies released?",
                          modelER.value.wouldPreparedToPayMonthlyInterest ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Does client(s) wish to consider lifetime mortgage or unsecured lending?",
                          modelER.value.doesClientConsiderLifetimeMortgage ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Would you wish to retain full ownership of the property?",
                          modelER.value.wouldYouWishFullOwnershipOfProperty ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Have you considered renting out all or part of your home?",
                          modelER.value.haveConsideredRentingOutYourHome ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("Have you considered downsizing?",
                          modelER.value.haveConsideredDownsizing ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Are you aware this will reduce the value of your estate on death and the amount paid to your beneficiaries? Have you made this aware to the beneficiaries of your estate?",
                          modelER.value
                                  .areYouAwarethisWillReduceValueOfStateOnDeath ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "How much capital / what percentage of the value would you like to release from your property?",
                          modelER.value
                                  .howMuchCapitalYouLikeToReleaseFromProperty ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Would you wish to release the maximum value from your property immediately?",
                          modelER.value
                                  .wouldYouLIkeToReleaseMaximumValueFromPropety ??
                              ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Do you wish to release smaller proportions are ad hoc intervals?",
                          modelER.value.doYouWishSmallerProportion ?? ''),
                    ],
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
