import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserBankDetailList.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherContactInfos.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step2_es_ctrl.dart';
import 'package:flutter/material.dart';

mixin ReqStep2OtherESMixin {
  drawESDetailsView({
    BuildContext context,
    ReqStep2ESCtrl otherESCtrl,
    Task caseModel,
    bool isShowAction,
  }) {
    final model1 = otherESCtrl.getES1Model.value;
    final model2 = otherESCtrl.getES2Model.value;

    if (model1 == null && model2 == null) return SizedBox();

    return Container(
        child: Padding(
      padding: const EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          otherESCtrl.listContactModel.length > 0 &&
                  caseModel.title !=
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Development_Finance)
              ? Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: .5),
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: 'PROFESSIONAL CONTACT INFORMATION',
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          Container(
                            color: MyTheme.bgColor,
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: 'Contact Type',
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        isBold: true),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: 'Contact Name',
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        isBold: true),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: 'Company Name',
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        isBold: true),
                                  ),
                                  isShowAction != null
                                      ? Expanded(
                                          flex: 2,
                                          child: Txt(
                                              txt: 'Action',
                                              txtColor: Colors.white,
                                              txtSize: MyTheme.txtSize - .6,
                                              txtAlign: TextAlign.center,
                                              fontWeight: FontWeight.w500,
                                              isBold: true),
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                          ),
                          ...otherESCtrl.listContactModel.map((data) => Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  drawContactItems(
                                      context, data, false, (model, isEdit) {}),
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          otherESCtrl.listBnkModel.length > 0
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: .5),
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: 'Bank Details: (For Direct Debit Set Up)',
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                          SizedBox(height: 10),
                          Container(
                            color: MyTheme.bgColor,
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: 'Account Name',
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: 'Bank Name',
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: 'Account Number',
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          ...otherESCtrl.listBnkModel.map((data) => Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  drawBnkItems(
                                      context, data, false, (model, isEdit) {}),
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          caseModel.title ==
                      NewCaseCfg.getEnumCaseTitle(eCaseTitle
                          .Second_Charge_0HYPN0_Buy_to_Let_0AND0_Commercial) ||
                  caseModel.title ==
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Buy_to_Let_Mortgage)
              ? model1 != null
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        UIHelper().draw2Row(
                            "1. Interest Only",
                            model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested ??
                                'No'),
                        SizedBox(height: 10),
                        UIHelper().draw2Row(
                            "Has an Interest only loan in full or part been requested?",
                            model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested ??
                                'No'),
                        SizedBox(height: 10),
                        UIHelper().draw2Row("Amount of Interest Only requested",
                            model1.ifYesWhatAreTheReasons ?? ''),
                        SizedBox(height: 10),
                        UIHelper().draw2Row(
                            "Your Exit strategy",
                            model1.methodOfCapitalRepaymentInvestmentVehiclesInheritance ??
                                ''),
                        SizedBox(height: 10),
                        UIHelper().draw2Row(
                            "2. Lending into Retirement\nDo lending go into retirement",
                            model1.doPaymentsGoIntoRetirement ?? ''),
                        SizedBox(height: 10),
                        UIHelper().draw2Row(
                            "What will be you retirement income from",
                            model1.whatWillBeTheSourceOfIncomeAtThatTime ?? ''),
                      ],
                    )
                  : SizedBox()
              : caseModel.title ==
                          NewCaseCfg.getEnumCaseTitle(eCaseTitle.Let_to_Buy) ||
                      caseModel.title ==
                          NewCaseCfg.getEnumCaseTitle(
                              eCaseTitle.Buy_to_Let_Remortgage)
                  ? model1 != null
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIHelper().draw2Row(
                                "1. Interest Only",
                                model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested ??
                                    'No'),
                            SizedBox(height: 10),
                            UIHelper().draw2Row(
                                "Has an Interest only loan in full or part been requested?",
                                model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested ??
                                    'No'),
                            SizedBox(height: 10),
                            UIHelper().draw2Row(
                                "2. Lending into Retirement\nDo lending go into retirement",
                                model1.doPaymentsGoIntoRetirement ?? ''),
                            SizedBox(height: 10),
                            UIHelper().draw2Row(
                                "What will be you retirement income from",
                                model1.whatWillBeTheSourceOfIncomeAtThatTime ??
                                    ''),
                          ],
                        )
                      : SizedBox()
                  : caseModel.title !=
                              NewCaseCfg.getEnumCaseTitle(
                                  eCaseTitle.Bridging_Loan) &&
                          caseModel.title !=
                              NewCaseCfg.getEnumCaseTitle(
                                  eCaseTitle.Development_Finance) &&
                          caseModel.title !=
                              NewCaseCfg.getEnumCaseTitle(
                                  eCaseTitle.Business_Lending) &&
                          caseModel.title !=
                              NewCaseCfg.getEnumCaseTitle(
                                  eCaseTitle.Commercial_Mortgages0SPLASH0_Loans)
                      ? Padding(
                          padding: const EdgeInsets.only(left: 5, right: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              model1 != null
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        UIHelper().draw2Row(
                                            "1. Interest Only",
                                            model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested ??
                                                'No'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "Has an Interest only loan in full or part been requested?",
                                            model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested ??
                                                'No'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "2. Lending into Retirement\nDo lending go into retirement",
                                            model1.doPaymentsGoIntoRetirement ??
                                                ''),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "Is the client(s) willing to potentially pay a greater amount over the term of the mortgage",
                                            model1.isTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage ??
                                                ''),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "Has the client(s) had any difficulties with past repayments of the loan(s)",
                                            model1.hasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan ??
                                                'N/A'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "Notes", model1.notes ?? 'N/A'),
                                        SizedBox(height: 10),
                                      ],
                                    )
                                  : SizedBox(),
                              model2 != null
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Txt(
                                            txt:
                                                "Key Information About The Type Of Mortgage Applicable To You",
                                            txtColor: MyTheme.statusBarColor,
                                            txtSize: MyTheme.txtSize - .4,
                                            txtAlign: TextAlign.start,
                                            isBold: true),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "1. Might your income or expenditure change significantly within the foreseeable future?",
                                            ''),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row("a. Income",
                                            model2.isIncome ?? 'No'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row("b. Expenditure",
                                            model2.isExpenditure ?? 'No'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "2. Do you have any plans to pay off some or all of the mortgage in the foreseeable future?",
                                            model2.doYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture ??
                                                'No'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "3. Are you likely to move home within the foreseeable future (other than this transaction)?",
                                            model2.areYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction ??
                                                'No'),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "4. Mortgage Requirements", ''),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "a. An upper limit on your mortgage costs for a specific period",
                                            model2
                                                .anUpperLimitOnYourMortgageCostsForASpecificPeriod),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "b. To fix your mortgage costs for a specific period",
                                            model2
                                                .toFixYourMortgageCostsForASpecificPeriod),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "c. A rate linked to the Bank of England base rate",
                                            model2
                                                .aRateLinkedToTheBankOfEnglandBaseRate),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "d. A discount on your mortgage repayments in the early years",
                                            model2
                                                .aDiscountOnYourMortgageRepaymentsInTheEarlyYears),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "e. Access to an initial cash sum (known as a cashback)",
                                            model2
                                                .accessToAnInitialCashSumReasonAndForHowLong),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "5. Which of the following are important to you",
                                            ''),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "a. No early repayment charge on your mortgage at any point",
                                            model2
                                                .noEarlyRepaymentChargeOnYourMortgageAtAnyPoint),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "b. No early repayment charge overhang after selected rate ends",
                                            model2
                                                .noEarlyRepaymentChargeOverhangAfterSelectedRateEnds),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "c. No high lending charge",
                                            model2.noHighLendingCharge),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "d. Speed of mortgage completion",
                                            model2.speedOfMortgageCompletion),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "e. Ability to add fees to the loan",
                                            model2.abilityToAddFeesToTheLoan),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "f. Ability to take repayment holidays",
                                            model2
                                                .abilityToTakeRepaymentHolidays),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "g. Ability to make underpayments or overpayments",
                                            model2
                                                .abilityToMakeUnderpaymentsOrOverpayments),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "h. Free legal fees",
                                            model2.freeLegalFees),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "i. No valuation fee",
                                            model2.novaluationFee),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "j. Have valuations fees refunded",
                                            model2.haveValuationsFeesRefunded),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "k. Do you require any mortgage features?",
                                            model2
                                                .wHATMORTGAGEFEATURESDOYOUREQUIRE),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "Would you like any other mortgage features such as overpayments or portability",
                                            model2
                                                .wouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability),
                                        SizedBox(height: 10),
                                        UIHelper().draw2Row(
                                            "Notes", model2.notes ?? ''),
                                        SizedBox(height: 10),
                                      ],
                                    )
                                  : SizedBox()
                            ],
                          ),
                        )
                      : SizedBox(),
        ],
      ),
    ));
  }

  drawContactItems(
      BuildContext context,
      MortgageUserOtherContactInfos model,
      bool isActionShow,
      Function(MortgageUserOtherContactInfos, bool) callback) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isActionShow ? 2 : 3,
                child: Txt(
                    txt: model.agentType ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: isActionShow ? 2 : 3,
                child: Txt(
                    txt: model.contactName ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: 2,
                child: Txt(
                    txt: model.companyName ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isActionShow
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callback(model, true);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  confirmDialog(
                                      context: context,
                                      title: "Confirmation!!",
                                      msg: "Are you sure to delete this item?",
                                      callbackYes: () {
                                        callback(model, false);
                                      });
                                },
                                child: Icon(Icons.delete_outline,
                                    color: Colors.grey, size: 20)),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper().draw2Row("Phone", model.phone ?? ''),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Email", model.email ?? ''),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Address", model.address ?? ''),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Post code", model.postcode ?? ''),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  drawBnkItems(BuildContext context, MortgageUserBankDetailList model,
      bool isActionShow, Function(MortgageUserBankDetailList, bool) callback) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isActionShow ? 2 : 3,
                child: Txt(
                    txt: model.accountName ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: isActionShow ? 2 : 3,
                child: Txt(
                    txt: model.bankName ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: 2,
                child: Txt(
                    txt: model.accountNumber ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isActionShow
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callback(model, true);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  confirmDialog(
                                      context: context,
                                      title: "Confirmation!!",
                                      msg: "Are you sure to delete this item?",
                                      callbackYes: () {
                                        callback(model, false);
                                      });
                                },
                                child: Icon(Icons.delete_outline,
                                    color: Colors.grey, size: 20)),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper().draw2Row("Address", model.address ?? ''),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Post code", model.postcode ?? ''),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Sort code", model.sortCode ?? ''),
                  SizedBox(height: 5),
                  UIHelper()
                      .draw2Row("Time with bank", model.timeWithBank ?? ''),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
