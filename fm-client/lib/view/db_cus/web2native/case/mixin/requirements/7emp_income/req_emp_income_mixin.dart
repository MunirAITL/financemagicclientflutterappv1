import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserInCome.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserOccupation.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

mixin ReqStep7EmpIncomeMixin {
  drawEmpExpandableList(
      BuildContext context,
      List<MortgageUserOccupation> listEmpModel,
      bool isShowAction,
      Function(MortgageUserOccupation model, bool isEdit) callback) {
    if (listEmpModel == null) return SizedBox();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          listEmpModel.length > 0
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Material(
                    elevation: 2,
                    color: MyTheme.bgColor,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            //flex: 2,
                            child: Txt(
                                txt: "Status",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          isShowAction
                              ? Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 40),
                                    child: Txt(
                                        txt: "Action",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...listEmpModel.map((model) {
                Widget wid;
                switch (model.basisOfEmployment) {
                  case "Employed":
                    wid = _drawEmployedView(model);
                    break;
                  case "Self-employed":
                    wid = _drawSelfEmployedView(model);
                    break;
                  case "Contractor":
                    wid = _drawContractorView(model);
                    break;
                  case "Retired":
                    wid = _drawRetiredView(model);
                    break;
                  default:
                    wid = SizedBox();
                }
                return ListTileTheme(
                  contentPadding: EdgeInsets.all(0),
                  iconColor: Colors.black,
                  child: Theme(
                    data: ThemeData.light().copyWith(
                        accentColor: Colors.black, primaryColor: Colors.red),
                    child: ExpansionTile(
                      //trailing: SizedBox.shrink(),
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: isShowAction ? 5 : 8,
                            child: Txt(
                                txt: model.basisOfEmployment ?? '',
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: false),
                          ),
                          isShowAction
                              ? Flexible(
                                  child: Row(
                                    children: [
                                      Flexible(
                                        child: GestureDetector(
                                            onTap: () {
                                              callback(model, true);
                                            },
                                            child: Icon(Icons.edit_outlined,
                                                color: Colors.grey, size: 20)),
                                      ),
                                      SizedBox(width: 10),
                                      Flexible(
                                        child: GestureDetector(
                                            onTap: () {
                                              confirmDialog(
                                                  context: context,
                                                  title: "Confirmation!!",
                                                  msg:
                                                      "Are you sure to delete this item?",
                                                  callbackYes: () {
                                                    callback(model, false);
                                                  });
                                            },
                                            child: Icon(Icons.delete_outline,
                                                color: Colors.grey, size: 20)),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox()
                        ],
                      ),
                      children: [wid],
                    ),
                  ),
                );
              })
            ],
          ),
        ],
      ),
    );
  }

  _drawEmployedView(MortgageUserOccupation model) {
    return Column(
      children: [
        UIHelper().draw2Row(
            "What is your employment status?", model.basisOfEmployment),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What's the name of your employer?", model.currentEmployer),
        SizedBox(height: 5),
        UIHelper().draw2Row("What's the Email of your employer?", model.email),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What's the Contact Number of your employer?", model.contactNumber),
        SizedBox(height: 5),
        UIHelper()
            .draw2Row("Do you own shares in this company?", model.isOwnShares),
        SizedBox(height: 5),
        UIHelper().draw2Row("Do you own 20% or more of this company?",
            model.isOwnSharesMoreThanTwenty),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the nature of your business?", model.natureOfYourBusiness),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the trading name of the company?", model.tradeName),
        SizedBox(height: 5),
        UIHelper().draw2Row("When was the company incorporated?",
            DateFun.getDate(model.incorporatedDate, "dd-MMM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Company Address",
            (model.buildingNumber +
                    " " +
                    model.buildingName +
                    " " +
                    model.address)
                .trim()),
        SizedBox(height: 5),
        UIHelper().draw2Row("How are you employed?", model.howAreYouEmployed),
        SizedBox(height: 5),
        UIHelper().draw2Row("What's your job title?", model.occupation),
        SizedBox(height: 5),
        UIHelper().draw2Row("What was your start date?",
            DateFun.getDate(model.whatWasYourStartDate, "dd-MMM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row("What percentage of the company do you own?",
            model.percentageShare.toStringAsFixed(0) + "%"),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "In what month does the company's accounting period start?",
            DateFun.getDate(model.remarks, "MMMM")),
        SizedBox(height: 5),
        UIHelper().draw2Row("At what age do you plan to retire?",
            model.aticipatedRetirementAge.toString()),
        SizedBox(height: 5),
        UIHelper().draw2Row("What is your current salary?",
            AppDefine.CUR_SIGN + model.currentSalary.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the net monthly income from recent month's payslip?",
            AppDefine.CUR_SIGN +
                model.monthlyIncomePayslipAmount.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Payslip reference number/ Employers ID Number",
            model.paySlipReferenceNumber),
        SizedBox(height: 5),
        UIHelper().draw2Row("Do you earn overtime?", model.isOverTime),
        SizedBox(height: 5),
        UIHelper().draw2Row("How much a year?",
            AppDefine.CUR_SIGN + model.overTimeSalary.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Do you earn bonuses?", model.isEarnBonuses),
        SizedBox(height: 5),
        UIHelper().draw2Row("How much a year?",
            AppDefine.CUR_SIGN + model.bonusesAmount.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Do you earn commission?", model.isEarnCommission),
        SizedBox(height: 5),
        UIHelper().draw2Row("How much a year?",
            AppDefine.CUR_SIGN + model.earnCommission.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Notes", model.notes),
        SizedBox(height: 5),
      ],
    );
  }

  _drawSelfEmployedView(MortgageUserOccupation model) {
    return Column(
      children: [
        UIHelper().draw2Row(
            "What is your employment status?", model.basisOfEmployment),
        SizedBox(height: 5),
        UIHelper()
            .draw2Row("What's Your Business Name?", model.currentEmployer),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is your self-employed status?", model.employmentStatus),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the nature of your business?", model.natureOfYourBusiness),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the trading name of the company?", model.tradeName),
        SizedBox(height: 5),
        UIHelper().draw2Row("What's your job title?", model.occupation),
        SizedBox(height: 5),
        UIHelper().draw2Row("Business Address", model.businessAddress),
        SizedBox(height: 5),
        UIHelper().draw2Row("What was your start date?",
            DateFun.getDate(model.whatWasYourStartDate, "dd-MMM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row("At what age do you plan to retire?",
            model.aticipatedRetirementAge.toString()),
        SizedBox(height: 5),
        UIHelper().draw2Row("Income recent year",
            AppDefine.CUR_SIGN + model.incomeRecentYear.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Recent Year ending",
            DateFun.getDate(model.recentYearEndingDate, "MM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row("Income previous year",
            AppDefine.CUR_SIGN + model.incomePreviousYear.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Previous Year ending",
            DateFun.getDate(model.previousYearEndingDate, "MM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Net profit year before that",
            AppDefine.CUR_SIGN +
                model.netProfitYearBeforeThat.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Net Profit Year ending",
            DateFun.getDate(model.netProfitYearEndingDate, "MM-yyyy")),
        SizedBox(height: 5),
        UIHelper()
            .draw2Row("Are accounts available?", model.isAreAccountsAvailable),
        SizedBox(height: 5),
        UIHelper()
            .draw2Row("Are SA302's available?", model.isAreSA302sAvailable),
        SizedBox(height: 5),
        UIHelper().draw2Row("Notes", model.notes),
        SizedBox(height: 5),
      ],
    );
  }

  _drawContractorView(MortgageUserOccupation model) {
    return Column(
      children: [
        UIHelper().draw2Row(
            "What is your employment status?", model.basisOfEmployment),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the name of the company with whom you have the contract with?",
            model.currentEmployer),
        SizedBox(height: 5),
        UIHelper().draw2Row("Email address", model.email),
        SizedBox(height: 5),
        UIHelper().draw2Row("Phone number", model.contactNumber),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Company Address",
            (model.buildingNumber +
                    " " +
                    model.buildingName +
                    " " +
                    model.address)
                .trim()),
        SizedBox(height: 5),
        UIHelper().draw2Row("When did the contract start?",
            DateFun.getDate(model.whatWasYourStartDate, "dd-MMM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row("How long has the contract left to run",
            model.howLongHasTheContractLeftToRun),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Is the contract renewable?", model.isTheContractRenewable),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is your daily pay rate?",
            AppDefine.CUR_SIGN +
                model.whatIsYourDailyPayRate.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Your annual Income",
            AppDefine.CUR_SIGN + model.yourAnnualIncome.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Have you submitted your self assessment tax returns?",
            model.haveYouSubmittedYourSelfAssessmentTaxReturns),
        SizedBox(height: 5),
        UIHelper().draw2Row("Income recent year",
            AppDefine.CUR_SIGN + model.incomeRecentYear.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Recent Year ending",
            DateFun.getDate(model.recentYearEndingDate, "MM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row("Income previous year",
            AppDefine.CUR_SIGN + model.incomePreviousYear.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Previous Year ending",
            DateFun.getDate(model.previousYearEndingDate, "MM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Net profit year before that",
            AppDefine.CUR_SIGN +
                model.netProfitYearBeforeThat.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Net Profit Year ending",
            DateFun.getDate(model.netProfitYearEndingDate, "MM-yyyy")),
        SizedBox(height: 5),
        UIHelper().draw2Row("Notes", model.notes),
        SizedBox(height: 5),
      ],
    );
  }

  _drawRetiredView(MortgageUserOccupation model) {
    return Column(
      children: [
        UIHelper().draw2Row(
            "What is your employment status?", model.basisOfEmployment),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "What is the amount you receive from your state pension annually?",
            AppDefine.CUR_SIGN + model.yourAnnualIncome.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row(
            "Do you receive any other personal/ occupational pension?",
            model.isDoYouReceiveAnyOtherPersonalOccupationalPension),
        SizedBox(height: 5),
        UIHelper().draw2Row("Other pension",
            AppDefine.CUR_SIGN + model.otherPensionAmount.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Do you receive any other benefits?",
            model.isDoYouReceiveAnyOtherBenefits),
        SizedBox(height: 5),
        UIHelper()
            .draw2Row("Description of benefit", model.descriptionOfBenefit),
        SizedBox(height: 5),
        UIHelper().draw2Row("Amount",
            AppDefine.CUR_SIGN + model.benefitAmount.toStringAsFixed(0)),
        SizedBox(height: 5),
        UIHelper().draw2Row("Notes", model.notes),
        SizedBox(height: 5),
      ],
    );
  }

  //  *******************************************************************************

  drawIncomeExpandableList(
    BuildContext context,
    List<MortgageUserInCome> listModel,
    bool isShowAction,
    Function(MortgageUserInCome model, bool isEdit) callback,
  ) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          listModel.length > 0
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Material(
                    elevation: 2,
                    color: MyTheme.bgColor,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: "Income type",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            //flex: 2,
                            child: Txt(
                                txt: "Amount per year",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            //flex: 2,
                            child: Txt(
                                txt: "Notes",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          isShowAction
                              ? Expanded(
                                  child: Txt(
                                      txt: "Action",
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...listModel.map((model) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 2,
                                child: Txt(
                                    txt: model.inComeType,
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: true)),
                            Expanded(
                                //flex: isShowAction ? 2 : 3,
                                child: Txt(
                                    txt: AppDefine.CUR_SIGN +
                                        model.amountPerYear.toStringAsFixed(0) +
                                        "/year",
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: false)),
                            Flexible(
                                //flex: 4,
                                child: Txt(
                                    txt: model.remarks,
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: false)),
                            isShowAction
                                ? Flexible(
                                    child: Row(
                                      children: [
                                        Flexible(
                                          child: GestureDetector(
                                              onTap: () {
                                                callback(model, true);
                                              },
                                              child: Icon(Icons.edit_outlined,
                                                  color: Colors.grey,
                                                  size: 20)),
                                        ),
                                        SizedBox(width: 10),
                                        Flexible(
                                          child: GestureDetector(
                                              onTap: () {
                                                confirmDialog(
                                                    context: context,
                                                    title: "Confirmation!!",
                                                    msg:
                                                        "Are you sure to delete this item?",
                                                    callbackYes: () {
                                                      callback(model, false);
                                                    });
                                              },
                                              child: Icon(Icons.delete_outline,
                                                  color: Colors.grey,
                                                  size: 20)),
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox()
                          ],
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
