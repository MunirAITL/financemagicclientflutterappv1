import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:flutter/material.dart';
import '../../../../../../config/MyTheme.dart';
import '../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../view_model/rx/web2native/requirements/requirements_ctrl.dart';
import '../../../../../widgets/txt/Txt.dart';
import '../../dialogs/requirements/req_ginc_dialog.dart';

mixin ReqGIncMixin {
  drawGenIncDetailsView({
    BuildContext context,
    RequirementsCtrl requirementsCtrl,
    Task caseModel,
  }) {
    final model = requirementsCtrl.mortgageBuildingAndContentInsurance;
    if (model.value == null) return SizedBox();
    if (caseModel.description == '') {
      return Container(
          child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().draw2Row("Type", model.value.type ?? 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Risk Property Address", model.value.riskPropertyAddress ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Type Of Property", model.value.typeOfProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Year of Construction", model.value.yearOfConstruction ?? ''),
          ],
        ),
      ));
    } else if (caseModel.description == 'Contents Only') {
      return Container(
          child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().draw2Row("Type", model.value.type ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Risk Property Address", model.value.riskPropertyAddress ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Type Of Property", model.value.typeOfProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Year of Construction", model.value.yearOfConstruction ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Number Of Bedrooms",
                model.value.numberOfBedrooms == 0
                    ? 'NA'
                    : model.value.numberOfBedrooms.toString()),
            SizedBox(height: 10),
            UIHelper().draw2Row("Do you want to add Accidental Damage Cover?",
                model.value.doWantToAddAccidentalDamageCover ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Buildings Cover Amount",
                model.value.buildingsCoverAmount != null
                    ? model.value.buildingsCoverAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.buildingsCoverAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "No. Of Claims Discount",
                model.value.noOfClaimsDiscount != null
                    ? model.value.noOfClaimsDiscount > 0
                        ? model.value.noOfClaimsDiscount.toStringAsFixed(0)
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            Txt(
                txt: "Contents Cover",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Cover Amount",
                model.value.contentsCoverAmount != null
                    ? model.value.contentsCoverAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.contentsCoverAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Contents Excess", model.value.contentsExcess ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Accidental Damage", model.value.accidentalDamage ?? ''),
            SizedBox(height: 10),
            Txt(
                txt: "Personal Possessions:",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false),
            SizedBox(height: 10),
            Txt(
                txt: "Additional Covers:",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Home Emergency Cover?", model.value.homeEmergencyCover ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Family Legal Expenses?",
                model.value.familyLegalExpenses ?? ''),
          ],
        ),
      ));
    } else if (caseModel.description == 'Buildings Only') {
      return Container(
          child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().draw2Row("Type", model.value.type ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Risk Property Address", model.value.riskPropertyAddress ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Type Of Property", model.value.typeOfProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Year of Construction", model.value.yearOfConstruction ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Number Of Bedrooms",
                model.value.numberOfBedrooms == 0
                    ? 'NA'
                    : model.value.numberOfBedrooms.toString()),
            SizedBox(height: 10),
            UIHelper().draw2Row("Do you want to add Accidental Damage Cover?",
                model.value.doWantToAddAccidentalDamageCover ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Buildings Cover Amount",
                model.value.buildingsCoverAmount != null
                    ? model.value.buildingsCoverAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.buildingsCoverAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "No. Of Claims Discount",
                model.value.noOfClaimsDiscount != null
                    ? model.value.noOfClaimsDiscount > 0
                        ? model.value.noOfClaimsDiscount.toStringAsFixed(0)
                        : 'N/A'
                    : 'N/A'),
          ],
        ),
      ));
    } else if (caseModel.description == 'Buildings & Contents') {
      return Container(
          child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().draw2Row("Type", model.value.type ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Risk Property Address", model.value.riskPropertyAddress ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Type Of Property", model.value.typeOfProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Year of Construction", model.value.yearOfConstruction ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Number Of Bedrooms",
                model.value.numberOfBedrooms == 0
                    ? 'NA'
                    : model.value.numberOfBedrooms.toString()),
            SizedBox(height: 10),
            UIHelper().draw2Row("Do you want to add Accidental Damage Cover?",
                model.value.doWantToAddAccidentalDamageCover ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Buildings Cover Amount",
                model.value.buildingsCoverAmount != null
                    ? model.value.buildingsCoverAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.buildingsCoverAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "No. Of Claims Discount",
                model.value.noOfClaimsDiscount != null
                    ? model.value.noOfClaimsDiscount > 0
                        ? model.value.noOfClaimsDiscount.toStringAsFixed(0)
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            Txt(
                txt: "Contents Cover",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Cover Amount",
                model.value.contentsCoverAmount != null
                    ? model.value.contentsCoverAmount > 0
                        ? UIHelper().addCurSign(
                            model.value.contentsCoverAmount.toStringAsFixed(0))
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Contents Excess", model.value.contentsExcess ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Accidental Damage", model.value.accidentalDamage ?? ''),
            SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                    txt: "Personal Possessions:",
                    txtColor: MyTheme.statusBarColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    fontWeight: FontWeight.w500,
                    isBold: false),
                SizedBox(height: 10),
                requirementsCtrl.listInsuranceItems.length > 0
                    ? drawGIncItems(context, 'Phone & Bike',
                        requirementsCtrl.listInsuranceItems, null, null)
                    : SizedBox(),
              ],
            ),
            SizedBox(height: 10),
            Txt(
                txt: "Additional Covers:",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Home Emergency Cover?", model.value.homeEmergencyCover ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Family Legal Expenses?",
                model.value.familyLegalExpenses ?? ''),
            SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                requirementsCtrl.listInsuranceItems.length > 0
                    ? drawGIncItems(context, 'High value',
                        requirementsCtrl.listInsuranceItems, null, null)
                    : SizedBox(),
              ],
            ),
          ],
        ),
      ));
    }
  }
}
