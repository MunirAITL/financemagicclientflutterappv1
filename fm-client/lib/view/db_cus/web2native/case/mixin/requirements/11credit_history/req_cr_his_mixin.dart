import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistory.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

mixin ReqStep11CrHisMixin {
  drawCrHisDetails(
    MortgageUserCreditHistory model,
  ) {
    if (model == null) return SizedBox();
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIHelper().draw2Row(
              "Have you ever had a mortgage or loan application refused?",
              model.haveYouEverHadAMortgageOrLoanApplicationRefused ?? 'No'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Details",
              model.haveYouEverHadAMortgageOrLoanApplicationRefusedDetails ??
                  'N/A'),
          SizedBox(height: 5),
          UIHelper().draw2Row("Have you ever been declared bankrupt?",
              model.haveYouEverBeenDeclaredBankrupt ?? 'No'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Details", model.haveYouEverBeenDeclaredBankruptDetails ?? 'N/A'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Have you ever failed to keep up repayments under any previous or current mortgage?",
              model.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage ??
                  'No'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Details",
              model.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails ??
                  'N/A'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Have you ever failed to keep up repayments under any previous or current rental or loan agreement?",
              model.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement ??
                  'No'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Details",
              model.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails ??
                  'N/A'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Have you ever had a judgement for (a gurantor) debt or loan default or any other default registered against you?",
              model.haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou ??
                  'No'),
          SizedBox(height: 5),
          UIHelper().draw2Row(
              "Have you ever had a judgement for (a gurantor) debt or loan default or any other default registered against you?",
              model.haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou ??
                  'No'),
        ],
      ),
    );
  }

  drawCrHisItems(
    BuildContext context,
    bool isShowAction,
    List<MortgageUserCreditHistoryItems> mortgageUserCreditHistoryItems,
    Function(MortgageUserCreditHistoryItems, bool) callback,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        mortgageUserCreditHistoryItems.length > 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    //color: Colors.grey.shade200,
                    decoration: BoxDecoration(
                      color: MyTheme.bgColor,
                      border: Border.all(width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(
                              5.0) //                 <--- border radius here
                          ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: isShowAction ? 2 : 3,
                            child: Txt(
                                txt: "Type",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                                isBold: true),
                          ),
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: "Date Registered",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: "Amount",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                                isBold: true),
                          ),
                          isShowAction
                              ? Flexible(
                                  flex: 2,
                                  child: Txt(
                                      txt: "Action",
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true))
                              : SizedBox(),
                          SizedBox(width: 20),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : SizedBox(),
        ...mortgageUserCreditHistoryItems.map(
          (e) => ListTileTheme(
            dense: true,
            contentPadding: EdgeInsets.all(0),
            iconColor: Colors.black,
            child: Theme(
              data: ThemeData.light().copyWith(
                accentColor: Colors.black,
              ),
              child: ExpansionTile(
                title: Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          flex: isShowAction ? 2 : 3,
                          child: Txt(
                              txt: e.type,
                              txtColor: MyTheme.statusBarColor,
                              txtSize: MyTheme.txtSize - .5,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true)),
                      Expanded(
                        flex: 2,
                        child: Text(
                          DateFun.getDate(e.dateofRegistered, "dd-MMM-yyyy"),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black, fontSize: 12),
                          softWrap: true,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Expanded(
                        child: Text(
                          AppDefine.CUR_SIGN +
                              e.amount.toStringAsFixed(2).replaceAll(".00", ""),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black, fontSize: 12),
                          softWrap: true,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      isShowAction
                          ? Flexible(
                              child: Row(
                                children: [
                                  Flexible(
                                    child: GestureDetector(
                                        onTap: () {
                                          callback(e, true);
                                        },
                                        child: Icon(Icons.edit_outlined,
                                            color: Colors.grey, size: 20)),
                                  ),
                                  SizedBox(width: 10),
                                  Flexible(
                                    child: GestureDetector(
                                        onTap: () {
                                          confirmDialog(
                                              context: context,
                                              title: "Confirmation!!",
                                              msg:
                                                  "Are you sure to delete this item?",
                                              callbackYes: () {
                                                callback(e, false);
                                              });
                                        },
                                        child: Icon(Icons.delete_outline,
                                            color: Colors.grey, size: 20)),
                                  ),
                                ],
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 5, bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //UIHelper().draw2Row("Is This Settled", e.isThisSettled),
                        //SizedBox(height: 5),
                        e.isThisSettled == "Yes"
                            ? UIHelper().draw2Row("Date Setteled",
                                DateFun.getDate(e.dateofSettled, "dd-MMM-yyyy"))
                            : SizedBox(),
                        SizedBox(height: 5),
                        UIHelper().draw2Row("Reason why this has happened",
                            e.reasonWhyThisHasHappended),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
