import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbility.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbilityItems.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/SignText.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step12_assess_afford_ctrl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

mixin ReqStep12AssessAffordMixin {
  drawAssessAffordDetails(
      BuildContext context, ReqStep12AssessAffordCtrl assessAffordCtrl) {
    final model = assessAffordCtrl.assessAffordModel.value;
    final list = assessAffordCtrl.listAssessAffordItemModel;

    if (model == null) return SizedBox();
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: .5,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Txt(
                  txt: "Household",
                  txtColor: MyTheme.statusBarColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: true),
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /*UIHelper().draw2Row(
                                    "Mortgage/Rent:",
                                    UIHelper().addCurSign(
                                        model.propertyMaintenanceGroundRent)),*/
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Electric:", UIHelper().addCurSign(model.electric)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Council Tax:",
                          UIHelper().addCurSign(model.councilTax)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Insurance:",
                          UIHelper()
                              .addCurSign(model.buildingsContentsInsurance)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Television:",
                          UIHelper().addCurSign(model
                              .tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences)),
                      SizedBox(height: 5),
                      UIHelper()
                          .draw2Row("Gas:", UIHelper().addCurSign(model.gas)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Water:", UIHelper().addCurSign(model.water)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Other:", UIHelper().addCurSign(model.otherDdetails)),
                    ]),
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Living Cost",
                  txtColor: MyTheme.statusBarColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: true),
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper().draw2Row(
                          "Groceries:",
                          UIHelper()
                              .addCurSign(model.familyFoodHouseholdCosts)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "HomeHelp:", UIHelper().addCurSign(model.homeHelp)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Clothing:", UIHelper().addCurSign(model.clothing)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Laundry:", UIHelper().addCurSign(model.laundry)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Home Phone:",
                          UIHelper().addCurSign(model.homePhone)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Mobile Phone:",
                          UIHelper().addCurSign(model.mobilePhoneCosts)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Internet:", UIHelper().addCurSign(model.internet)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Investments:",
                          UIHelper().addCurSign(model.investments)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Maintenance Payments:",
                          UIHelper().addCurSign(model.maintenancePayments)),
                    ]),
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Life Style",
                  txtColor: MyTheme.statusBarColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: true),
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper().draw2Row(
                          "Entertainment:",
                          UIHelper().addCurSign(
                              model.socialCostsMealsOutDrinksTheatre)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Holiday:", UIHelper().addCurSign(model.holiday)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Sports:", UIHelper().addCurSign(model.sports)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Cigarettes:",
                          UIHelper().addCurSign(model.cigarettes)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Alcohol:",
                          UIHelper()
                              .addCurSign(model.tobaccoOrRelatedProducts)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Pension:",
                          UIHelper().addCurSign(model.pensionContributions)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Leisure:", UIHelper().addCurSign(model.leisure)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Travel:", UIHelper().addCurSign(model.travel)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Regular subscriptions ie. newspaper, magazine, films, health club, golf, tennis, football, etc:",
                          UIHelper().addCurSign(model.regularSubscriptions)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Pets – food, insurance, grooming, etc:",
                          UIHelper()
                              .addCurSign(model.petsFoodInsuranceGrooming)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Insurances (other than B&C) ie, life, health, medical, dental, phone, etc:",
                          UIHelper().addCurSign(
                              model.insurancesLifeHealthMedicalDentalPhone)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Regular savings:",
                          UIHelper().addCurSign(model.regularSavings)),
                    ]),
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Transport Costs",
                  txtColor: MyTheme.statusBarColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: true),
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper().draw2Row(
                          "Petrol:", UIHelper().addCurSign(model.petrol)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Commuting Costs:",
                          UIHelper().addCurSign(model.commutingCosts)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row(
                          "Car Costs:", UIHelper().addCurSign(model.carCosts)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Car fuel costs:",
                          UIHelper().addCurSign(model.carFuelCosts)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Motor Insurance:",
                          UIHelper().addCurSign(model.motorInsurance)),
                      SizedBox(height: 5),
                      UIHelper().draw2Row("Other Transport Cost:",
                          UIHelper().addCurSign(model.transportTrainTramBus)),
                    ]),
              ),
              SizedBox(height: 5),
            ]),
            Txt(
                txt: "Others",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: true),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 10),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...list.map((e) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 5),
                            UIHelper().draw2Row(e.itemType,
                                UIHelper().addCurSign(e.itemAmount)),
                          ],
                        ))
                  ]),
            ),
            SizedBox(height: 10),
            Txt(
                txt: "Notes",
                txtColor: MyTheme.statusBarColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: true),
            model != null
                ? Padding(
                    padding: const EdgeInsets.only(top: 5, left: 20, right: 10),
                    child: Txt(
                        txt: model.remarks ?? '',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  )
                : SizedBox(),
            SizedBox(height: 10),
            assessAffordCtrl.totalExpenses.value > 0
                ? Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Txt(
                                txt: "Total expenses",
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: AppDefine.CUR_SIGN +
                                    assessAffordCtrl.totalExpenses.value
                                        .toStringAsFixed(2)
                                        .replaceAll(".00", ""),
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: true),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  drawAssessAffordItems(
      BuildContext context,
      bool isShowAction,
      List<MortgageUserAssesmentOfAffordAbilityItems> list,
      Function(MortgageUserAssesmentOfAffordAbilityItems, bool) callback) {
    final assessAffordCtrl = Get.put(ReqStep12AssessAffordCtrl());
    assessAffordCtrl.totalOtherCustom = 0.0.obs;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...list.map(
          (e) {
            assessAffordCtrl.totalOtherCustom += e.itemAmount;
            return Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: isShowAction ? 2 : 4,
                              child: Txt(
                                  txt: e.itemType,
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                            ),
                            isShowAction
                                ? Flexible(
                                    child: IconButton(
                                      onPressed: () {
                                        callback(e, true);
                                      },
                                      icon: Icon(Icons.edit_outlined,
                                          color: Colors.black),
                                      iconSize: 20,
                                    ),
                                  )
                                : SizedBox(),
                            SizedBox(width: isShowAction ? 10 : 0),
                            isShowAction
                                ? Flexible(
                                    child: IconButton(
                                        onPressed: () {
                                          confirmDialog(
                                              context: context,
                                              title: "Confirmation!!",
                                              msg:
                                                  "Are you sure to delete this item?",
                                              callbackYes: () {
                                                callback(e, false);
                                              });
                                        },
                                        icon: Icon(Icons.close_outlined,
                                            color: Colors.black)),
                                  )
                                : SizedBox()
                          ],
                        ),
                      ),
                      Flexible(
                          child: InkWell(
                              onTap: () {
                                callback(e, true);
                              },
                              child: drawSignText(
                                  context: context,
                                  title: null,
                                  txt: e.itemAmount
                                      .toStringAsFixed(2)
                                      .replaceAll(".00", ""),
                                  padding: 6)))
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ],
    );
  }
}
