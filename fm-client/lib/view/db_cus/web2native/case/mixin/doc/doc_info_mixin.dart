import 'dart:io';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/web2native/case/doc/MortgageCaseDocumentInfos.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/dialog/doc_opt_dialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;
import 'package:permission_handler/permission_handler.dart';

mixin DocInfoMixin {
  drawDocLayout(
      BuildContext context,
      List<MortgageCaseDocumentInfos> listDocModel,
      Function(MortgageCaseDocumentInfos) callback,
      Function(File) callbackDownloadDoc) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(Icons.file_download_outlined, color: Colors.black, size: 20),
              SizedBox(width: 5),
              Flexible(
                child: Txt(
                    txt: "Client Documents",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
            ],
          ),
        ),
        SizedBox(height: 10),
        Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: TableBorder(
                horizontalInside: BorderSide(
                    width: .5, color: Colors.black, style: BorderStyle.solid),
                verticalInside: BorderSide(
                    width: .5, color: Colors.black, style: BorderStyle.solid)),
            children: [
              TableRow(children: [
                _drawTxt(txt: "Name", isBold: true, align: TextAlign.center),
                _drawTxt(txt: "Purpose", isBold: true, align: TextAlign.center),
                _drawTxt(
                    txt: "Description", isBold: true, align: TextAlign.center),
                _drawTxt(
                    txt: "Uploaded Date",
                    isBold: true,
                    align: TextAlign.center),
                _drawTxt(txt: "Action", isBold: true, align: TextAlign.center),
              ]),
              for (final e in listDocModel)
                TableRow(children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 5, top: 10, bottom: 10, right: 5),
                    child: InkWell(
                      onTap: () {
                        docOptDialog(
                            context: context,
                            docName: e.name,
                            callbackDownload: () async {
                              final serviceStatus =
                                  await Permission.storage.status;
                              if (!serviceStatus.isGranted) {
                                await [
                                  Permission.storage,
                                ].request();
                              }

                              NetworkMgr.shared.downloadFile(
                                  context: context,
                                  url: e.documentUrl,
                                  isLoading: true,
                                  callback: (file) {
                                    callbackDownloadDoc(file);
                                  });
                            },
                            callbackView: () {
                              if (MyNetworkImage.isValidUrl(e.documentUrl)) {
                                final extension = p.extension(e.documentUrl);
                                //print(extension);
                                if (extension.toLowerCase() == ".pdf") {
                                  Get.to(
                                    () => PDFDocumentPage(
                                      title: "Document View",
                                      url: e.name,
                                    ),
                                  );
                                } else if (extension.toLowerCase() == ".doc" ||
                                    extension.toLowerCase() == ".docx" ||
                                    extension.toLowerCase() == ".xls") {
                                  Get.to(WebScreen(
                                      url:
                                          "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                                              e.documentUrl,
                                      title: e.name));
                                } else {
                                  Get.to(PicFullView(
                                      title: e.name, url: e.documentUrl));
                                }
                              }
                            });
                      },
                      child: Text(
                        e.name,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.blue,
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  _drawTxt(txt: e.purpose ?? ''),
                  _drawTxt(txt: e.remarks ?? ''),
                  _drawTxt(txt: DateFun.getDate(e.updatedDate, 'dd-MMM-yyyy')),
                  InkWell(
                    onTap: () {
                      callback(e);
                    },
                    child: Icon(Icons.upload_file_outlined,
                        color: Colors.blue, size: 20),
                  ),
                ])
            ]),
        Padding(
          padding: const EdgeInsets.only(top: 20, left: 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(Icons.file_copy_outlined, color: Colors.black, size: 20),
              SizedBox(width: 5),
              Flexible(
                child: Txt(
                    txt: "Compliance Documents",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
            ],
          ),
        ),
        SizedBox(height: 5),
        Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: TableBorder(
                horizontalInside: BorderSide(
                    width: .5, color: Colors.black, style: BorderStyle.solid),
                verticalInside: BorderSide(
                    width: .5, color: Colors.black, style: BorderStyle.solid)),
            children: [
              TableRow(children: [
                _drawTxt(txt: "Name", isBold: true, align: TextAlign.center),
                _drawTxt(txt: "Purpose", isBold: true, align: TextAlign.center),
                _drawTxt(
                    txt: "Description", isBold: true, align: TextAlign.center),
                _drawTxt(
                    txt: "Creation Date",
                    isBold: true,
                    align: TextAlign.center),
                _drawTxt(txt: "Action", isBold: true, align: TextAlign.center),
              ]),
              /*for (final e in listDocModel)
                TableRow(children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 5, top: 10, bottom: 10, right: 5),
                    child: InkWell(
                      onTap: () {
                        if (MyNetworkImage.isValidUrl(e.documentUrl)) {
                          final extension = p.extension(e.documentUrl);
                          //print(extension);
                          if (extension.toLowerCase() == ".pdf") {
                            Get.to(
                              () => PDFDocumentPage(
                                title: "Document View",
                                url: e.name,
                              ),
                            );
                          } else if (extension.toLowerCase() == ".doc" ||
                              extension.toLowerCase() == ".docx" ||
                              extension.toLowerCase() == ".xls") {
                            Get.to(WebScreen(
                                url:
                                    "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                                        e.documentUrl,
                                title: e.name));
                          } else {
                            Get.to(
                                PicFullView(title: e.name, url: e.documentUrl));
                          }
                        }
                      },
                      child: Text(
                        e.name,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.blue,
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  _drawTxt(txt: e.purpose ?? ''),
                  _drawTxt(txt: e.remarks ?? ''),
                  _drawTxt(txt: DateFun.getDate(e.updatedDate, 'dd-MMM-yyyy')),
                  InkWell(
                    onTap: () {
                      callback(e);
                    },
                    child: Icon(Icons.upload_file_outlined,
                        color: Colors.blue, size: 20),
                  ),
                ])*/
            ]),
      ]),
    );
  }

  _drawTxt(
          {@required String txt,
          bool isBold = false,
          double txtSize = 11.5,
          TextAlign align = TextAlign.start}) =>
      Padding(
        padding: const EdgeInsets.only(left: 5, top: 10, bottom: 10, right: 5),
        child: Text(
          txt,
          textAlign: align,
          style: TextStyle(
            color: Colors.black,
            fontSize: txtSize,
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
          ),
        ),
      );
}
