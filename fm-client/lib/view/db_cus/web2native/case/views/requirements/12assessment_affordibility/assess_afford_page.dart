import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/PostAssessAffordAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/13btl_portfolio/btl_portf_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'assess_afford_base.dart';

class AssessAffordPage extends StatefulWidget {
  const AssessAffordPage({Key key}) : super(key: key);

  @override
  State<AssessAffordPage> createState() => _AssessAffordPageState();
}

class _AssessAffordPageState extends AssessAffordBase<AssessAffordPage> {
  var total = 0.0.obs;

  calculation() {
    try {
      total.value = 0.0;
      //assessAffordCtrl.totalExpenses.value = 0;
      total.value += assessAffordCtrl.totalOtherCustom.value;
      total.value += getVal(monthlyRent);
      total.value += getVal(mortgageRent);
      total.value += getVal(electricity);
      total.value += getVal(councilTax);
      total.value += getVal(tv);
      total.value += getVal(insurance);
      total.value += getVal(gas);
      total.value += getVal(water);
      total.value += getVal(other);
      //
      total.value += getVal(groceries);
      total.value += getVal(clothing);
      total.value += getVal(homeHelp);
      total.value += getVal(laundry);
      total.value += getVal(mobilePhone);
      total.value += getVal(homePhone);
      total.value += getVal(internet);
      total.value += getVal(investments);
      total.value += getVal(maintenancePayments);
      total.value += getVal(entertainment);
      total.value += getVal(holiday);
      total.value += getVal(sports);
      total.value += getVal(cigarettes);
      total.value += getVal(alcohol);
      total.value += getVal(pension);
      total.value += getVal(leisure);
      total.value += getVal(travel);
      total.value += getVal(regSubs);
      total.value += getVal(pets);
      total.value += getVal(insurancePhone);
      total.value += getVal(regSaving);
      total.value += getVal(petrol);
      total.value += getVal(comCost);
      total.value += getVal(carCost);
      total.value += getVal(carFuelCost);
      total.value += getVal(motorInsurance);
      total.value += getVal(otherTransCost);
      total.value += getVal(dayCare);
      total.value += getVal(schooling);
      total.value += getVal(outing);
      total.value += getVal(clothes);

      assessAffordCtrl.totalExpenses.value = total.value;
      //assessAffordCtrl.update();
      //setState(() {});
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        updateData();
        calculation();
        setState(() {});
      });
    } catch (e) {}
  }

  updateData() async {
    try {
      crComCtrl.balOutstanding.value = 0;
      crComCtrl.creditLimit.value = 0;
      crComCtrl.monthlyPayment.value = 0;
      for (final e in crComCtrl.crCommitCtrl) {
        if (e.isAddItemForAffordabilityCalculation == "true") {
          crComCtrl.balOutstanding.value += e.outstandingBalance;
          crComCtrl.creditLimit.value += e.creditLimit;
          crComCtrl.monthlyPayment.value += e.monthlyPayment;
        }
      }
      crComCtrl.update();

      assessAffordCtrl.totalExpenses.value = 0;
      final model = assessAffordCtrl.assessAffordModel.value;
      setData(monthlyRent, crComCtrl.monthlyPayment.value.toStringAsFixed(2));
      //
      setData(
          mortgageRent, model.propertyMaintenanceGroundRent.toStringAsFixed(0));
      setData(electricity, model.electric.toStringAsFixed(0));
      setData(councilTax, model.councilTax.toStringAsFixed(0));
      setData(
          tv,
          model.tVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences
              .toStringAsFixed(0));
      setData(insurance, model.buildingsContentsInsurance.toStringAsFixed(0));
      setData(gas, model.gas.toStringAsFixed(0));
      setData(water, model.water.toStringAsFixed(0));
      setData(other, model.otherDdetails.toStringAsFixed(0));
      setData(groceries, model.familyFoodHouseholdCosts.toStringAsFixed(0));
      setData(clothing, model.clothing.toStringAsFixed(0));
      setData(homeHelp, model.homeHelp.toStringAsFixed(0));
      setData(laundry, model.laundry.toStringAsFixed(0));
      setData(mobilePhone, model.mobilePhoneCosts.toStringAsFixed(0));
      setData(homePhone, model.homePhone.toStringAsFixed(0));
      setData(internet, model.internet.toStringAsFixed(0));
      setData(investments, model.investments.toStringAsFixed(0));
      setData(
          maintenancePayments, model.maintenancePayments.toStringAsFixed(0));
      setData(entertainment,
          model.socialCostsMealsOutDrinksTheatre.toStringAsFixed(0));
      setData(holiday, model.holiday.toStringAsFixed(0));
      setData(sports, model.sports.toStringAsFixed(0));
      setData(cigarettes, model.cigarettes.toStringAsFixed(0));
      setData(alcohol, model.tobaccoOrRelatedProducts.toStringAsFixed(0));
      setData(pension, model.pensionContributions.toStringAsFixed(0));
      setData(leisure, model.leisure.toStringAsFixed(0));
      setData(travel, model.travel.toStringAsFixed(0));
      setData(regSubs, model.regularSubscriptions.toStringAsFixed(0));
      setData(pets, model.petsFoodInsuranceGrooming.toStringAsFixed(0));
      setData(insurancePhone,
          model.insurancesLifeHealthMedicalDentalPhone.toStringAsFixed(0));
      setData(regSaving, model.regularSavings.toStringAsFixed(0));
      setData(petrol, model.petrol.toStringAsFixed(0));
      setData(comCost, model.commutingCosts.toStringAsFixed(0));
      setData(carCost, model.carCosts.toStringAsFixed(0));
      setData(carFuelCost, model.carFuelCosts.toStringAsFixed(0));
      setData(motorInsurance, model.motorInsurance.toStringAsFixed(0));
      setData(otherTransCost, model.transportTrainTramBus.toStringAsFixed(0));
      setData(dayCare, model.childCareCosts.toStringAsFixed(0));
      setData(
          schooling, model.regularSchoolFeesContributions.toStringAsFixed(0));
      setData(outing,
          model.otherSchoolingCostsMealsUniformOutings.toStringAsFixed(0));
      setData(clothes, model.clothes.toStringAsFixed(0));
      setData(notes, model.remarks.trim());
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper()
                    .drawAppbarTitle(title: "Assessment of Affordability"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(() => Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Txt(
                                    txt: "Total expenses",
                                    txtColor: MyTheme.statusBarColor,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                              ),
                              Expanded(
                                child: Txt(
                                    txt: AppDefine.CUR_SIGN +
                                        total.value
                                            .toStringAsFixed(2)
                                            .replaceAll(".00", ""),
                                    txtColor: MyTheme.statusBarColor,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.end,
                                    isBold: true),
                              ),
                            ],
                          )),
                      SizedBox(height: 10),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: MMBtn(
                                txt: "Back",
                                height: getHP(context, 7),
                                width: getW(context),
                                bgColor: Colors.grey,
                                radius: 10,
                                callback: () async {
                                  Get.back();
                                }),
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: MMBtn(
                                txt: "Save & continue",
                                height: getHP(context, 7),
                                width: getW(context),
                                radius: 10,
                                callback: () async {
                                  final now = DateFun.getDate(
                                      DateTime.now().toString(), "dd-MMM-yyyy");
                                  bool isPost = assessAffordCtrl
                                              .assessAffordModel.value.id ==
                                          null
                                      ? true
                                      : false;
                                  var _councilTax = 0;
                                  var _gas = 0;
                                  var _elec = 0;
                                  var _water = 0;
                                  var _mortRent = 0;
                                  var _insurance = 0;
                                  var _groc = 0;
                                  var _dayCare = 0;
                                  var _clothg = 0;
                                  var _tv = 0;
                                  var _mobPhone = 0;
                                  var _sub = 0;
                                  var _carCost = 0;
                                  var _carFuel = 0;
                                  var _otherTrans = 0;
                                  var _pet = 0;
                                  var _maint = 0;
                                  var _schooling = 0;
                                  var _outing = 0;
                                  var _insPh = 0;
                                  var _pen = 0;
                                  var _regSv = 0;
                                  var _alcho = 0;
                                  var _ent = 0;
                                  var _hol = 0;
                                  var _oth = 0;
                                  var _hhelp = 0;
                                  var _laundry = 0;
                                  var _homePh = 0;
                                  var _internet = 0;
                                  var _inv = 0;
                                  var _sprt = 0;
                                  var _cig = 0;
                                  var _lesure = 0;
                                  var _trav = 0;
                                  var _petrol = 0;
                                  var _comCost = 0;
                                  var _motIns = 0;
                                  var _clothes = 0;

                                  try {
                                    _councilTax = int.parse(councilTax.text);
                                  } catch (e) {}
                                  try {
                                    _gas = int.parse(gas.text);
                                  } catch (e) {}
                                  try {
                                    _elec = int.parse(electricity.text);
                                  } catch (e) {}
                                  try {
                                    _water = int.parse(water.text);
                                  } catch (e) {}
                                  try {
                                    _mortRent = int.parse(mortgageRent.text);
                                  } catch (e) {}
                                  try {
                                    _insurance = int.parse(insurance.text);
                                  } catch (e) {}
                                  try {
                                    _groc = int.parse(groceries.text);
                                  } catch (e) {}
                                  try {
                                    _dayCare = int.parse(dayCare.text);
                                  } catch (e) {}
                                  try {
                                    _clothg = int.parse(clothing.text);
                                  } catch (e) {}
                                  try {
                                    _tv = int.parse(tv.text);
                                  } catch (e) {}
                                  try {
                                    _mobPhone = int.parse(mobilePhone.text);
                                  } catch (e) {}
                                  try {
                                    _sub = int.parse(regSubs.text);
                                  } catch (e) {}
                                  try {
                                    _carCost = int.parse(carCost.text);
                                  } catch (e) {}
                                  try {
                                    _carFuel = int.parse(carFuelCost.text);
                                  } catch (e) {}
                                  try {
                                    _otherTrans =
                                        int.parse(otherTransCost.text);
                                  } catch (e) {}
                                  try {
                                    _pet = int.parse(pets.text);
                                  } catch (e) {}
                                  try {
                                    _maint =
                                        int.parse(maintenancePayments.text);
                                  } catch (e) {}
                                  try {
                                    _schooling = int.parse(schooling.text);
                                  } catch (e) {}
                                  try {
                                    _outing = int.parse(outing.text);
                                  } catch (e) {}
                                  try {
                                    _insPh = int.parse(insurancePhone.text);
                                  } catch (e) {}
                                  try {
                                    _pen = int.parse(pension.text);
                                  } catch (e) {}
                                  try {
                                    _regSv = int.parse(regSaving.text);
                                  } catch (e) {}
                                  try {
                                    _alcho = int.parse(alcohol.text);
                                  } catch (e) {}
                                  try {
                                    _ent = int.parse(entertainment.text);
                                  } catch (e) {}
                                  try {
                                    _hol = int.parse(holiday.text);
                                  } catch (e) {}
                                  try {
                                    _oth = int.parse(other.text);
                                  } catch (e) {}
                                  try {
                                    _hhelp = int.parse(homeHelp.text);
                                  } catch (e) {}
                                  try {
                                    _laundry = int.parse(laundry.text);
                                  } catch (e) {}
                                  try {
                                    _homePh = int.parse(homePhone.text);
                                  } catch (e) {}
                                  try {
                                    _internet = int.parse(internet.text);
                                  } catch (e) {}
                                  try {
                                    _inv = int.parse(investments.text);
                                  } catch (e) {}
                                  try {
                                    _sprt = int.parse(sports.text);
                                  } catch (e) {}
                                  try {
                                    _cig = int.parse(cigarettes.text);
                                  } catch (e) {}
                                  try {
                                    _lesure = int.parse(leisure.text);
                                  } catch (e) {}
                                  try {
                                    _trav = int.parse(travel.text);
                                  } catch (e) {}
                                  try {
                                    _petrol = int.parse(petrol.text);
                                  } catch (e) {}
                                  try {
                                    _comCost = int.parse(comCost.text);
                                  } catch (e) {}
                                  try {
                                    _motIns = int.parse(motorInsurance.text);
                                  } catch (e) {}
                                  try {
                                    _clothes = int.parse(clothes.text);
                                  } catch (e) {}

                                  final param = {
                                    "UserId": userData.userModel.id,
                                    "User": null,
                                    "CompanyId":
                                        userData.userModel.userCompanyInfo.id,
                                    "Status": 101,
                                    "MortgageCaseInfoId": 0,
                                    "CreationDate": now,
                                    "TotalOfContinuingLoansCreditAsAbove":
                                        crComCtrl.monthlyPayment.value,
                                    "CurrentOrExpectedFutureExpenditure": 0,
                                    "CouncilTax": _councilTax,
                                    "Gas": _gas,
                                    "Electric": _elec,
                                    "Water": _water,
                                    "PropertyMaintenanceGroundRent": _mortRent,
                                    "BuildingsContentsInsurance": _insurance,
                                    "FamilyFoodHouseholdCosts": _groc,
                                    "ChildCareCosts": _dayCare,
                                    "Clothing": _clothg,
                                    "TVBroadbandTelephoneCostsIncludingSubscriptionsServicesLicences":
                                        _tv,
                                    "MobilePhoneCosts": _mobPhone,
                                    "RegularSubscriptions": _sub,
                                    "CarCosts": _carCost,
                                    "MOTRoadTaxGeneralMaintenanceRepairs": 0,
                                    "CarFuelCosts": _carFuel,
                                    "TransportTrainTramBus": _otherTrans,
                                    "PetsFoodInsuranceGrooming": _pet,
                                    "MaintenancePayments": _maint,
                                    "RegularSchoolFeesContributions":
                                        _schooling,
                                    "OtherSchoolingCostsMealsUniformOutings":
                                        _outing,
                                    "InsurancesLifeHealthMedicalDentalPhone":
                                        _insPh,
                                    "PensionContributions": _pen,
                                    "RegularSavings": _regSv,
                                    "TobaccoOrRelatedProducts": _alcho,
                                    "SocialCostsMealsOutDrinksTheatre": _ent,
                                    "Holiday": _hol,
                                    "OtherDdetails": _oth,
                                    "TotalMonthlyExpenditure":
                                        assessAffordCtrl.totalExpenses.value,
                                    "NetMonthlyIncome": 0,
                                    "NetMonthlyDisposableIncome": 0,
                                    "Television": 0,
                                    "Groceries": 0,
                                    "HomeHelp": _hhelp,
                                    "LivingCostClothing": 0,
                                    "Laundry": _laundry,
                                    "HomePhone": _homePh,
                                    "Internet": _internet,
                                    "Investments": _inv,
                                    "Entertainment": 0,
                                    "Sports": _sprt,
                                    "Cigarettes": _cig,
                                    "Alcohol": 0,
                                    "Pension": 0,
                                    "Leisure": _lesure,
                                    "Travel": _trav,
                                    "Petrol": _petrol,
                                    "CommutingCosts": _comCost,
                                    "MotorInsurance": _motIns,
                                    "OtherTransportCost": 0,
                                    "DayCare": 0,
                                    "Clothes": _clothes,
                                    "Remarks": "notes",
                                    "Id": isPost
                                        ? 0
                                        : assessAffordCtrl
                                                    .assessAffordModel.value !=
                                                null
                                            ? assessAffordCtrl
                                                .assessAffordModel.value.id
                                            : 0, // coming issue in
                                    "MobilePhone": 0,
                                    "TaskId": caseModelCtrl.locModel.value.id,
                                    "MortgageUserCreditHistory": {
                                      "HaveYouEverHadAMortgageOrLoanApplicationRefused":
                                          "No",
                                      "HaveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou":
                                          "No",
                                      "HaveYouEverBeenDeclaredBankrupt": "No",
                                      "HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage":
                                          "No",
                                      "HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement":
                                          "No",
                                      "IfYesToAnyOfTheAboveProvideFullDetails":
                                          "",
                                      "Remarks": notes.text.trim()
                                    },
                                    "MortgageUserAffordAbilityList": []
                                  };
                                  await APIViewModel().req<
                                          PostAssessAffordAPIModel>(
                                      context: context,
                                      url: isPost
                                          ? SrvW2NCase.POST_ASSESS_AFFORD_URL
                                          : SrvW2NCase.PUT_ASSESS_AFFORD_URL,
                                      reqType:
                                          isPost ? ReqType.Post : ReqType.Put,
                                      param: param,
                                      callback: (model2) async {
                                        if (mounted && model2 != null) {
                                          if (model2.success) {
                                            assessAffordCtrl
                                                    .assessAffordModel.value =
                                                model2.responseData
                                                    .mortgageUserAssesmentOfAffordAbility;
                                            Get.off(() => BtlPortfolioPage());
                                          }
                                        }
                                      });
                                }),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.all(20),
                    child: drawProgressView(87)),
                drawForm(),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      drawTextArea(tf: notes, title: "Notes"),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
