import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/PostCreditHisAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/PostCreditHisItemsAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/requirements/11credit_history/cr_his_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/11credit_history/cr_his_base.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/12assessment_affordibility/assess_afford_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreditHistoryPage extends StatefulWidget {
  const CreditHistoryPage({Key key}) : super(key: key);
  @override
  State<CreditHistoryPage> createState() => _CreditHistoryPageState();
}

class _CreditHistoryPageState extends CrHisBase<CreditHistoryPage> {
  onOpenAddEditDialog(MortgageUserCreditHistoryItems model2) {
    try {
      Get.dialog(reqStep11CrHisDialog(
          context: context,
          taskId: caseModelCtrl.locModel.value.id,
          model2: model2,
          callbackFailed: (err) {
            showToast(context: context, msg: err);
          }));
    } catch (e) {}
  }

  onDelItem(MortgageUserCreditHistoryItems model) async {
    try {
      await APIViewModel().req<PostCreditHisItemsAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_CREDIT_HISTORYITEMS_URL + model.id.toString(),
          reqType: ReqType.Delete,
          callback: (model2) async {
            if (mounted && model2 != null) {
              if (model2.success) {
                crHisCtrl.listCrHisItemsCtrl.remove(model);
                crHisCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      updateData();
    } catch (e) {}
  }

  updateData() async {
    try {
      final model = crHisCtrl.crHisCtrl.value;

      try {
        loanRefusedRBIndex = Common.getMapKeyByVal(listLoanRefusedRB,
            model.haveYouEverHadAMortgageOrLoanApplicationRefused);
      } catch (e) {}
      try {
        loanRefDetails.text =
            model.haveYouEverHadAMortgageOrLoanApplicationRefusedDetails;
      } catch (e) {}

      try {
        haveYouEverItemsRBIndex = Common.getMapKeyByVal(
            listHaveYouEverItemsRB,
            model
                .haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou);
      } catch (e) {}

      try {
        bankRuptRBIndex = Common.getMapKeyByVal(
            listBankruptRB, model.haveYouEverBeenDeclaredBankrupt);
      } catch (e) {}
      try {
        bankRuptDetails.text = model.haveYouEverBeenDeclaredBankruptDetails;
      } catch (e) {}

      try {
        repayPreviousMortRBIndex = Common.getMapKeyByVal(
            listRepayPreviousMortRB,
            model
                .haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage);
      } catch (e) {}
      try {
        repayPreviousMortDetails.text = model
            .haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails;
      } catch (e) {}

      try {
        repayPreviousLoanRBIndex = Common.getMapKeyByVal(
            listRepayPreviousLoanRB,
            model
                .haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement);
      } catch (e) {}
      try {
        repayPreviousLoanDetails.text = model
            .haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails;
      } catch (e) {}
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Credit History"),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawProgressView(83),
                  drawForm(),
                  SizedBox(height: 30),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Save & continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              try {
                                final now = DateFun.getDate(
                                    DateTime.now().toString(), "dd-MMM-yyyy");
                                bool isPost = crHisCtrl.crHisCtrl.value == null
                                    ? true
                                    : false;
                                final param = {
                                  "UserId": userData.userModel.id,
                                  "User": null,
                                  "CompanyId":
                                      userData.userModel.userCompanyInfo.id,
                                  "Status": 101,
                                  "MortgageCaseInfoId": 0,
                                  "CreationDate": now,
                                  "HaveYouEverHadAMortgageOrLoanApplicationRefused":
                                      listLoanRefusedRB[loanRefusedRBIndex],
                                  "HaveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou":
                                      listHaveYouEverItemsRB[
                                          haveYouEverItemsRBIndex],
                                  "HaveYouEverBeenDeclaredBankrupt":
                                      listBankruptRB[bankRuptRBIndex],
                                  "HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage":
                                      listRepayPreviousMortRB[
                                          repayPreviousMortRBIndex],
                                  "HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement":
                                      listRepayPreviousLoanRB[
                                          repayPreviousLoanRBIndex],
                                  "HaveYouEverHadAMortgageOrLoanApplicationRefusedDetails":
                                      loanRefDetails.text.trim(),
                                  "HaveYouEverBeenDeclaredBankruptDetails":
                                      bankRuptDetails.text.trim(),
                                  "HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails":
                                      repayPreviousMortDetails.text.trim(),
                                  "HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails":
                                      repayPreviousLoanDetails.text.trim(),
                                  "Remarks": "",
                                  "HaveYouHadAMissedMortgagePayment": null,
                                  "Id":
                                      isPost ? 0 : crHisCtrl.crHisCtrl.value.id,
                                  "TaskId":
                                      0, //caseModelCtrl.locModel.value.id,
                                };
                                await APIViewModel().req<PostCreditHisAPIModel>(
                                    context: context,
                                    url: isPost
                                        ? SrvW2NCase.POST_CREDIT_HISTORY_URL
                                        : SrvW2NCase.PUT_CREDIT_HISTORY_URL,
                                    reqType:
                                        isPost ? ReqType.Post : ReqType.Put,
                                    param: param,
                                    callback: (model2) async {
                                      if (mounted && model2 != null) {
                                        if (model2.success) {
                                          crHisCtrl.crHisCtrl.value = model2
                                              .responseData
                                              .mortgageUserCreditHistory;
                                          crHisCtrl.update();
                                          /*final msg = isPost
                                        ? "Credit history has been added successfully"
                                        : "Credit history has been updated successfully";
                                    showToast(context: context, msg: msg);
                                    setState(() {});*/
                                          Get.off(() => AssessAffordPage());
                                        }
                                      }
                                    });
                              } catch (e) {}
                            }),
                      ),
                    ],
                  ),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
