import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/PostBtlPortfolioAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/13btl_portfolio/btl_portf_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/SignText.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

import '../../../../../../widgets/gplaces/GPlacesView.dart';
//import 'package:keyboard_actions/keyboard_actions.dart';

class BtlPortfolioDetails extends StatefulWidget {
  final MortgageUserPortfolios model;
  const BtlPortfolioDetails({Key key, this.model}) : super(key: key);
  @override
  State<BtlPortfolioDetails> createState() => _BtlPortfolioDetailsState();
}

class _BtlPortfolioDetailsState extends BtlPortFolioBase<BtlPortfolioDetails> {
  MortgageUserPortfolios mortgageUserPortfolios;

  onDelBtlPortFolioItem(MortgageUserPortfolios model) {}

  DropListModel ddAssetType = DropListModel([
    OptionItem(id: "1", title: "Residential BTL"),
    OptionItem(id: "2", title: "HMO"),
    OptionItem(id: "3", title: "Semi Commercial"),
    OptionItem(id: "4", title: "Commercial"),
  ]);
  var optAssetType = OptionItem(id: null, title: "Select asset type").obs;

  DropListModel ddOwnership = DropListModel([
    OptionItem(id: "1", title: "Individual"),
    OptionItem(id: "2", title: "Joint"),
    OptionItem(id: "3", title: "SPV Company"),
  ]);
  var optOwnership = OptionItem(id: null, title: "Select ownership").obs;

  DropListModel ddInterestType = DropListModel([
    OptionItem(id: "1", title: "Capital Repayment"),
    OptionItem(id: "2", title: "Interest Only"),
  ]);
  var optInterestType = OptionItem(id: null, title: "Select interest type").obs;

  final listResBtlRB = {
    0: "Detached House",
    1: "Semi detached House",
    2: "Mid Terraced House",
    3: "End Terraced House",
    4: "Link Detached House",
    5: "Manor House",
    6: "Purpose Built Flat",
    7: "Converted Flat",
    8: "House Boat",
  };
  int resBtlRBIndex = 0;

  final listHmoRB = {
    0: "Multiple Flats in One title",
    1: "Student Lets",
    2: "Multi Lets",
    3: "House",
    4: "Flat",
    5: "Semi Commercial Unit",
  };
  int hmoRBIndex = 0;

  final listSemiComRB = {
    0: "Shop and Flats",
    1: "Restaurant and Flats",
    2: "Multi Let proerty",
    3: "Commercial and Resi mix units",
  };
  int semiComRBIndex = 0;

  final listComRB = {
    0: "Shop",
    1: "Restaurant",
    2: "Offices",
    3: "Blocks of flats",
    4: "Other",
  };
  int comRBIndex = 0;

  final comName = TextEditingController();
  final purchasePrice = TextEditingController();
  final curValue = TextEditingController();
  final monthlyIncome = TextEditingController();
  final monthlyPayment = TextEditingController();
  final lenderName = TextEditingController();
  final curMortBal = TextEditingController();
  final curMortInterestRate = TextEditingController();
  final mortCurLender = TextEditingController();

  var addrProperty = "".obs;
  var dtPurchase = "".obs;
  var dtEndDate = "".obs;

  var ltv = 0.0.obs;
  var mortgageType = "".obs;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    mortgageUserPortfolios = null;
    super.dispose();
  }

  initPage() async {
    try {
      mortgageUserPortfolios = widget.model;
      try {
        if (mortgageUserPortfolios != null) {
          final index = getDDIndex(
              ddAssetType.listOptionItems, mortgageUserPortfolios.propertyType);
          if (index != null) {
            optAssetType = OptionItem(
                    id: index, title: mortgageUserPortfolios.propertyType)
                .obs;
          }
        }
      } catch (e) {}
      try {
        resBtlRBIndex = Common.getMapKeyByVal(
            listResBtlRB, mortgageUserPortfolios.propertyStyle);
      } catch (e) {}
      try {
        hmoRBIndex = Common.getMapKeyByVal(
            listHmoRB, mortgageUserPortfolios.propertyStyle);
      } catch (e) {}
      try {
        semiComRBIndex = Common.getMapKeyByVal(
            listSemiComRB, mortgageUserPortfolios.propertyStyle);
      } catch (e) {}
      try {
        comRBIndex = Common.getMapKeyByVal(
            listComRB, mortgageUserPortfolios.propertyStyle);
      } catch (e) {}
      try {
        final index = getDDIndex(
            ddOwnership.listOptionItems, mortgageUserPortfolios.owner);
        if (index != null) {
          optOwnership =
              OptionItem(id: index, title: mortgageUserPortfolios.owner).obs;
        }
      } catch (e) {}
      try {
        comName.text = mortgageUserPortfolios.sPVCompanyName ?? '';
      } catch (e) {}
      try {
        addrProperty.value = (mortgageUserPortfolios.address1 +
                " " +
                mortgageUserPortfolios.address2 +
                " " +
                mortgageUserPortfolios.address3)
            .trim();
      } catch (e) {}
      try {
        dtPurchase.value = DateFun.getDate(
            mortgageUserPortfolios.datePurchased, "dd-MMM-yyyy");
        if (dtPurchase.contains("1970")) {
          dtPurchase.value = "";
        }
      } catch (e) {}
      try {
        purchasePrice.text =
            mortgageUserPortfolios.purchasePrice.toStringAsFixed(0);
      } catch (e) {}
      try {
        curValue.text = mortgageUserPortfolios.currentValue.toStringAsFixed(0);
      } catch (e) {}
      try {
        monthlyIncome.text =
            mortgageUserPortfolios.rentalIncome.toStringAsFixed(0);
      } catch (e) {}
      try {
        monthlyPayment.text =
            mortgageUserPortfolios.monthlyMortgage.toStringAsFixed(0);
      } catch (e) {}
      try {
        lenderName.text = mortgageUserPortfolios.lender ?? '';
      } catch (e) {}
      try {
        curMortBal.text =
            mortgageUserPortfolios.mortgageBalance.toStringAsFixed(0);
      } catch (e) {}
      try {
        ltv.value = mortgageUserPortfolios.lTV;
      } catch (e) {}
      try {
        curMortInterestRate.text = mortgageUserPortfolios
            .currentMortgageInterestRate
            .toStringAsFixed(0);
      } catch (e) {}
      try {
        mortCurLender.text =
            mortgageUserPortfolios.mortgageTermRemainingWithCurrentLender ?? '';
      } catch (e) {}
      try {
        dtEndDate.value = DateFun.getDate(
            mortgageUserPortfolios.currentFixedPeriodEndDate, "dd-MMM-yyyy");
        if (dtEndDate.contains("1970")) {
          dtEndDate.value = "";
        }
      } catch (e) {}
      try {
        optInterestType =
            OptionItem(id: "1", title: mortgageUserPortfolios.interestType).obs;
      } catch (e) {}
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: mortgageUserPortfolios == null
                        ? "Add BTL Portfolio"
                        : "Edit BTL Portfolio"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: MMBtn(
                      txt: mortgageUserPortfolios == null ? "Add" : "Update",
                      height: getHP(context, 7),
                      width: getW(context),
                      radius: 5,
                      callback: () async {
                        if (optAssetType.value.id == null) {
                          showToast(context: context, msg: "Select asset type");
                          return;
                        } else if (optOwnership.value.id == null) {
                          showToast(context: context, msg: "Select ownership");
                          return;
                        } else if (optOwnership.value.id == "3" &&
                            comName.text.isEmpty) {
                          showToast(
                              context: context, msg: "Select company name");
                          return;
                        } else if (curValue.text.isEmpty) {
                          showToast(
                              context: context, msg: "Enter current value");
                          return;
                        } else if (curMortBal.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Enter current mortgage balance");
                          return;
                        }
                        final now = DateFun.getDate(
                            DateTime.now().toString(), "dd-MMM-yyyy");
                        bool isPost =
                            mortgageUserPortfolios == null ? true : false;

                        var _purPrice = 0;
                        var _curVal = 0;
                        var _mortBal = 0;
                        var _monthMort = 0;
                        var _rentIncome = 0;
                        var _rate = 0;

                        try {
                          _purPrice = int.parse(purchasePrice.text);
                        } catch (e) {}
                        try {
                          _curVal = int.parse(curValue.text);
                        } catch (e) {}
                        try {
                          _mortBal = int.parse(curMortBal.text);
                        } catch (e) {}
                        try {
                          _monthMort = int.parse(monthlyPayment.text);
                        } catch (e) {}
                        try {
                          _rentIncome = int.parse(monthlyIncome.text);
                        } catch (e) {}
                        try {
                          _rate = int.parse(curMortInterestRate.text);
                        } catch (e) {}

                        final param = {
                          "UserId": userData.userModel.id,
                          "User": null,
                          "CompanyId": userData.userModel.userCompanyInfo.id,
                          "Status": 101,
                          "MortgageCaseInfoId": 0,
                          "CreationDate": now,
                          "TaskId": caseModelCtrl.locModel.value.id,
                          "DoYouOwnOneOrMoreInvestmentProperties": "0",
                          "PropertyType": optAssetType.value.title,
                          "Address1": addrProperty.value,
                          "Address2": "",
                          "Address3": "",
                          "City": "",
                          "Country": "",
                          "PostCode": "",
                          "DatePurchased": dtPurchase.value == ""
                              ? AppConfig.date1970
                              : dtPurchase.value ?? AppConfig.date1970,
                          "PurchasePrice": _purPrice,
                          "CurrentValue": _curVal,
                          "Lender": lenderName.text.trim(),
                          "MortgageBalance": _mortBal,
                          "MonthlyMortgage": _monthMort,
                          "RentalIncome": _rentIncome,
                          "Notes": "",
                          "Remarks": null,
                          "HMO": "",
                          "Owner": optOwnership.value.title,
                          "LTV": ltv.value ?? 0,
                          "NumberofBed": "",
                          "MortgageType": caseModelCtrl.locModel.value.title,
                          "PropertyStyle": mortgageType.value,
                          "InterestType": optInterestType.value.title,
                          "CurrentRateandExpiryDate": "",
                          "MortgageTermRemainingWithCurrentLender":
                              mortCurLender.text.trim(),
                          "CurrentMortgageInterestRate": _rate,
                          "CurrentFixedPeriodEndDate": dtEndDate.value == ""
                              ? AppConfig.date1970
                              : dtEndDate.value ?? AppConfig.date1970,
                          "SecurityPropertyTypeOthers": "",
                          "SPVCompanyName": comName.text.trim(),
                          "Id": mortgageUserPortfolios == null
                              ? 0
                              : mortgageUserPortfolios.id,
                          "DatePurchased1": "18",
                          "DatePurchased2": "05",
                          "DatePurchased3": "2022"
                        };
                        await APIViewModel().req<PostBtlPortfolioAPIModel>(
                            context: context,
                            url: isPost
                                ? SrvW2NCase.POST_BTL_PORTFOLIO_URL
                                : SrvW2NCase.PUT_BTL_PORTFOLIO_URL,
                            reqType: isPost ? ReqType.Post : ReqType.Put,
                            param: param,
                            callback: (model2) async {
                              if (mounted && model2 != null) {
                                if (model2.success) {
                                  btlPortfCtrl.listBtlPortModel
                                      .remove(mortgageUserPortfolios);
                                  btlPortfCtrl.listBtlPortModel.add(model2
                                      .responseData.mortgageUserPortfolio);
                                  btlPortfCtrl.update();
                                  showToast(
                                      context: context,
                                      msg: isPost
                                          ? "User Portfolio added successfully"
                                          : "User Portfolio updated successfully");
                                  Future.delayed(
                                      Duration(seconds: 3), () => Get.back());
                                }
                              }
                            });
                      }),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 100, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);

    return Obx(() => Container(
            child: SingleChildScrollView(
                child: Padding(
          padding: const EdgeInsets.all(20),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            DropDownListDialog(
              context: context,
              title: optAssetType.value.title,
              h1: "Select asset type",
              heading: "Asset type",
              ddTitleList: ddAssetType,
              vPadding: 3,
              callback: (optionItem) {
                optAssetType.value = optionItem;
              },
            ),
            SizedBox(height: 10),
            DropDownListDialog(
              context: context,
              title: optOwnership.value.title,
              h1: "Select ownership",
              heading: "Ownership",
              ddTitleList: ddOwnership,
              vPadding: 3,
              callback: (optionItem) {
                optOwnership.value = optionItem;
              },
            ),
            optOwnership.value.id == "3"
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: InputW2N(
                      title: "Company Name",
                      input: comName,
                      ph: "Company Name",
                      kbType: TextInputType.text,
                      len: 100,
                      isBold: true,
                    ),
                  )
                : SizedBox(),
            optAssetType.value.id == "1"
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: UIHelper().drawRadioTitle(
                        context: context,
                        title: "Security Property Type",
                        list: listResBtlRB,
                        index: resBtlRBIndex,
                        callback: (index) {
                          setState(() {
                            resBtlRBIndex = index;
                            mortgageType.value = listResBtlRB[index];
                          });
                        }),
                  )
                : optAssetType.value.id == "2"
                    ? Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: UIHelper().drawRadioTitle(
                            context: context,
                            title: "Security Property Type",
                            list: listHmoRB,
                            index: hmoRBIndex,
                            callback: (index) {
                              setState(() {
                                hmoRBIndex = index;
                                mortgageType.value = listHmoRB[index];
                              });
                            }),
                      )
                    : optAssetType.value.id == "3"
                        ? Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: UIHelper().drawRadioTitle(
                                context: context,
                                title: "Security Property Type",
                                list: listSemiComRB,
                                index: semiComRBIndex,
                                callback: (index) {
                                  setState(() {
                                    semiComRBIndex = index;
                                    mortgageType.value = listSemiComRB[index];
                                  });
                                }),
                          )
                        : optAssetType.value.id == "4"
                            ? Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: UIHelper().drawRadioTitle(
                                    context: context,
                                    title: "Security Property Type",
                                    list: listComRB,
                                    index: comRBIndex,
                                    callback: (index) {
                                      setState(() {
                                        comRBIndex = index;
                                        mortgageType.value = listComRB[index];
                                      });
                                    }),
                              )
                            : SizedBox(),
            SizedBox(height: 10),
            GPlacesView(
                title: "Property Address",
                titleColor: Colors.black,
                isBold: true,
                txtSize: MyTheme.txtSize - .2,
                address: addrProperty.value,
                callback: (String address, String postCode, Location loc) {
                  addrProperty.value = address;
                  setState(() {});
                }),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Date Purchased",
              dt: (dtPurchase.value == '') ? 'Select Date' : dtPurchase.value,
              fontWeight: FontWeight.bold,
              padding: 5,
              radius: 5,
              txtColor: Colors.black,
              initialDate: now,
              firstDate: old,
              lastDate: now,
              callback: (value) {
                try {
                  dtPurchase.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                } catch (e) {}
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                context: context,
                tf: purchasePrice,
                hintTxt: null,
                labelColor: Colors.black,
                labelTxt: "Purchase Price (approximate)",
                isBold: true,
                len: 10,
                focusNode: null,
                focusNodeNext: null,
                onChange: (v) {}),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                context: context,
                tf: curValue,
                hintTxt: null,
                labelColor: Colors.black,
                labelTxt: "Current Value",
                isBold: true,
                len: 10,
                focusNode: null,
                focusNodeNext: null,
                onChange: (v) {
                  try {
                    ltv.value = double.parse(calLTVAmount(
                        balOutstanding: curMortBal.text, curValProperty: v));
                  } catch (e) {}
                }),
            SizedBox(height: 10),
            drawInputCurrencyBox(
              context: context,
              tf: monthlyIncome,
              hintTxt: null,
              labelColor: Colors.black,
              labelTxt: "Monthly Rental Income",
              isBold: true,
              len: 10,
              focusNode: null,
              focusNodeNext: null,
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
              context: context,
              tf: monthlyPayment,
              hintTxt: null,
              labelColor: Colors.black,
              labelTxt: "Monthly Mortgage Payment",
              isBold: true,
              len: 10,
              focusNode: null,
              focusNodeNext: null,
            ),
            SizedBox(height: 10),
            InputW2N(
              title: "Lender Name",
              input: lenderName,
              isBold: true,
              ph: "Lender Name",
              kbType: TextInputType.text,
              len: 50,
            ),
            SizedBox(height: 5),
            drawInputCurrencyBox(
                context: context,
                tf: curMortBal,
                hintTxt: null,
                labelColor: Colors.black,
                labelTxt: "Current Mortgage Balance",
                isBold: true,
                len: 10,
                focusNode: null,
                focusNodeNext: null,
                onChange: (v) {
                  try {
                    ltv.value = double.parse(calLTVAmount(
                        balOutstanding: v, curValProperty: curValue.text));
                  } catch (e) {}
                }),
            SizedBox(height: 15),
            drawSignText(
                context: context,
                sign: "%",
                title: "LTV(%)",
                txtColor: Colors.black,
                isBold: true,
                txt: ltv.value.toStringAsFixed(2).replaceAll(".00", ""),
                padding: 8),
            SizedBox(height: 15),
            drawInputCurrencyBox(
              sign: "%",
              context: context,
              tf: curMortInterestRate,
              hintTxt: null,
              labelColor: Colors.black,
              labelTxt: "Current Mortgage Interest Rate: (if known)",
              isBold: true,
              len: 3,
              focusNode: null,
              focusNodeNext: null,
            ),
            SizedBox(height: 10),
            InputW2N(
              title: "Mortgage Term Remaining with current lender",
              input: mortCurLender,
              isBold: true,
              ph: "Mortgage Term Remaining with current lender",
              kbType: TextInputType.text,
              len: 100,
            ),
            SizedBox(height: 5),
            DatePickerView(
              cap: "Current Fixed Period end date",
              dt: (dtEndDate.value == '') ? 'Select Date' : dtEndDate.value,
              fontWeight: FontWeight.bold,
              padding: 5,
              radius: 5,
              txtColor: Colors.black,
              initialDate: now,
              firstDate: old,
              lastDate: next,
              callback: (value) {
                try {
                  dtEndDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                } catch (e) {}
              },
            ),
            SizedBox(height: 10),
            DropDownListDialog(
              context: context,
              title: optInterestType.value.title,
              h1: "Select interest type",
              heading: "Interest Type",
              ddTitleList: ddInterestType,
              vPadding: 3,
              callback: (optionItem) {
                optInterestType.value = optionItem;
              },
            ),
          ]),
        ))));
  }
}
