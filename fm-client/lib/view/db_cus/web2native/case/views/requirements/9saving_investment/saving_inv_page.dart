import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/PostSavingInvAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/10credit_commitment/cr_commit_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/9saving_investment/saving_inv_details_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'saving_inv_base.dart';

class SavingInvPage extends StatefulWidget {
  const SavingInvPage({Key key}) : super(key: key);

  @override
  State<SavingInvPage> createState() => _SavingInvPageState();
}

class _SavingInvPageState extends SavingInvBase<SavingInvPage> {
  int anySavingInvRBIndex = 0;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Saving & Investment"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              Get.off(() => CrCommitPage());
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawProgressView(77),
                  SizedBox(height: 20),
                  UIHelper().drawRadioTitle(
                      context: context,
                      title: "Is there any savings or investments to add?",
                      list: W2NLocalData.listYesNoRB,
                      index: anySavingInvRBIndex,
                      callback: (index) {
                        setState(() => anySavingInvRBIndex = index);
                      }),
                ],
              ),
            ),
            anySavingInvRBIndex == 0 && savingInvCtrl.savingInvCtrl != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: drawDottedBox("Add Savings & Investments", () {
                            Get.to(() => SavingInvDetailsPage()).then((value) {
                              setState(() {});
                            });
                          })),
                      drawSavingInvList(
                          context, true, savingInvCtrl.savingInvCtrl,
                          (model, isEdit) async {
                        if (isEdit) {
                          Get.to(() =>
                                  SavingInvDetailsPage(savingInvModel: model))
                              .then((value) {
                            setState(() {});
                          });
                        } else {
                          try {
                            await APIViewModel().req<PostSavingInvAPIModel>(
                                context: context,
                                url: SrvW2NCase.DEL_SAVING_INV_URL +
                                    model.id.toString(),
                                reqType: ReqType.Delete,
                                callback: (model2) async {
                                  if (mounted && model2 != null) {
                                    if (model2.success) {
                                      savingInvCtrl.savingInvCtrl.remove(model);
                                      savingInvCtrl.update();
                                      setState(() {});
                                    }
                                  }
                                });
                          } catch (e) {}
                        }
                      }),
                    ],
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
