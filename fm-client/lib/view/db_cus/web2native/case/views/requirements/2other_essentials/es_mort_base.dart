import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserBankDetailList.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherContactInfos.dart';
import 'package:flutter/material.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../widgets/input/InputTitleBox.dart';
import '../../../../../../widgets/input/InputWeb2Native.dart';
import '../../../../../../widgets/txt/Txt.dart';
import '../../../../case_base.dart';

abstract class ESMortBase<T extends StatefulWidget> extends CaseBase<T> {
  delContactAPI(MortgageUserOtherContactInfos model);
  editContactAPI(MortgageUserOtherContactInfos model);
  addContactAPI();
  openBnkDialog(MortgageUserBankDetailList model);
  delBnkAPI(MortgageUserBankDetailList model);

  final sharedOwner = TextEditingController();
  final r2bDisAmount = TextEditingController();
  final r2bDetailsCouncil = TextEditingController();
  final amountOfInterest = TextEditingController();
  final exitStrategry = TextEditingController();
  final retirementDesc = TextEditingController();
  final difficultiesDesc = TextEditingController();
  final desc = TextEditingController();
  final incomeChange = TextEditingController();
  final expenditureChange = TextEditingController();
  final payoffAmount = TextEditingController();
  final payoffReason = TextEditingController();
  final moveHomeReason = TextEditingController();
  final mortgageReqUpperLimitReason = TextEditingController();
  final mortgageReqFixReason = TextEditingController();
  final mortgageReqBaseRateReason = TextEditingController();
  final mortgageReqDisReason = TextEditingController();
  final mortgageReqCashbackReason = TextEditingController();
  final mortgageFeatureDetails = TextEditingController();
  final portability = TextEditingController();
  final notes = TextEditingController();

  final focusPortability = FocusNode();

  int bnkDirectDebitRBIndex = 0;

  int sharedOwnerRBIndex = 1;

  int r2BRBIndex = 1;

  int interestOnlyRBIndex = 1;

  int retirementRBIndex = 1;

  int greaterAmountRBIndex = 1;

  int difficultiesRBIndex = 1;

  int incomeChangeRBIndex = 1;

  int expenditureChangeRBIndex = 1;

  int payOffRBIndex = 1;

  int moveHomeRBIndex = 1;

  final listMoveHomeSizeRB = {0: 'Larger', 1: 'Smaller'};
  int moveHomeSizeRBIndex = 1;

  int mortgageReqUpperLimitRBIndex = 1;

  int mortgageReqFixRBIndex = 1;

  int mortgageReqBaseRateRBIndex = 1;

  int mortgageReqDisRBIndex = 1;

  int mortgageReqCashbackRBIndex = 1;

  int earlyRepaymentRBIndex = 1;

  int earlyRepaymentOverhangRBIndex = 1;

  int highLandingRBIndex = 1;

  int speedMortRBIndex = 1;

  int addFeeLoanRBIndex = 1;

  int repaymentHolidayRBIndex = 1;

  int overPaymentRBIndex = 1;

  int freeLegalFeeRBIndex = 1;

  int noValFeeRBIndex = 1;

  int valFeeRefundedRBIndex = 1;

  int reqMortFeaturesRBIndex = 1;

  drawForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          drawTitleHeading(
              "ADD PROFESSIONAL CONTACT INFORMATION",
              "Please add details of your Solicitors, Accountant, Estate Agent, Landlord in this section. To Add, please click on " +
                  " then select category and complete the form. If you do not have the information, please add the ones you have"),
          SizedBox(height: 10),
          drawDottedBox("Add Contact", () {
            addContactAPI();
          }),
          otherESCtrl.listContactModel.length > 0
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(
                    color: MyTheme.bgColor,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 2,
                            child: Txt(
                                txt: 'Contact Type',
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Flexible(
                            flex: 2,
                            child: Txt(
                                txt: 'Contact Name',
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Flexible(
                            flex: 2,
                            child: Txt(
                                txt: 'Company Name',
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: 'Action',
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          ...otherESCtrl.listContactModel.map((element) =>
              drawContactItems(context, element, true, (model, isEdit) {
                if (isEdit) {
                  editContactAPI(model);
                } else {
                  delContactAPI(model);
                }
              })),
          _drawBankDetails(),
          caseModelCtrl.locModel.value.title ==
                  NewCaseCfg.getEnumCaseTitle(eCaseTitle.Residential_Mortgage)
              ? _drawSharedOwnership()
              : SizedBox(),
          caseModelCtrl.locModel.value.title ==
                  NewCaseCfg.getEnumCaseTitle(eCaseTitle.Residential_Remortgage)
              ? _drawRight2Buy()
              : SizedBox(),
          caseModelCtrl.locModel.value.title !=
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Commercial_Mortgages0SPLASH0_Loans) &&
                  caseModelCtrl.locModel.value.title !=
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Business_Lending) &&
                  caseModelCtrl.locModel.value.title !=
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Development_Finance) &&
                  caseModelCtrl.locModel.value.title !=
                      NewCaseCfg.getEnumCaseTitle(eCaseTitle.Bridging_Loan)
              ? _drawFormParts()
              : SizedBox(),
        ],
      ),
    );
  }

  _drawBankDetails() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(height: 20),
      UIHelper().drawRadioTitle(
          context: context,
          title: "Bank Details: (For Direct Debit Set Up)",
          subTitle:
              "Do you want to provide bank details for direct debit setup?",
          list: W2NLocalData.listYesNoRB,
          index: bnkDirectDebitRBIndex,
          callback: (index) {
            setState(() => bnkDirectDebitRBIndex = index);
          }),
      SizedBox(height: 10),
      bnkDirectDebitRBIndex == 0
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                drawDottedBox("Add Bank", () {
                  openBnkDialog(null);
                }),
                otherESCtrl.listBnkModel.length > 0
                    ? Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Container(
                          color: MyTheme.bgColor,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  flex: 2,
                                  child: Txt(
                                      txt: 'Account Name',
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                ),
                                Flexible(
                                  flex: 2,
                                  child: Txt(
                                      txt: 'Bank Name',
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                ),
                                Flexible(
                                  flex: 2,
                                  child: Txt(
                                      txt: 'Account Number',
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Txt(
                                      txt: 'Action',
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                ),
                                //SizedBox(),
                              ],
                            ),
                          ),
                        ),
                      )
                    : SizedBox(),
                ...otherESCtrl.listBnkModel.map((element) =>
                    drawBnkItems(context, element, true, (model, isEdit) {
                      if (isEdit) {
                        openBnkDialog(model);
                      } else {
                        delBnkAPI(model);
                      }
                    })),
                SizedBox(height: 10),
              ],
            )
          : SizedBox(),
    ]);
  }

  _drawRight2Buy() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UIHelper().drawRadioTitle(
            context: context,
            title: "Right to Buy",
            subTitle: "Is It right to buy?",
            list: W2NLocalData.listYesNoRB,
            index: r2BRBIndex,
            callback: (index) {
              setState(() => r2BRBIndex = index);
            }),
        r2BRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(children: [
                  drawCurrencyBox("What is the discount amount received", null,
                      r2bDisAmount, null, null, (v) {}),
                  SizedBox(height: 10),
                  drawTextArea(
                      title: "Details of the Council/ Association",
                      subTitle: "Please enclose a copy of the award letter",
                      tf: r2bDetailsCouncil)
                ]))
            : SizedBox(),
        SizedBox(height: 10),
      ],
    );
  }

  _drawSharedOwnership() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UIHelper().drawRadioTitle(
            context: context,
            title: "Shared Ownership",
            subTitle: "Is It shared ownership?",
            list: W2NLocalData.listYesNoRB,
            index: sharedOwnerRBIndex,
            callback: (index) {
              setState(() => sharedOwnerRBIndex = index);
            }),
        sharedOwnerRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Inputweb2native(
                  title: "Details of shared ownership body you are buying from",
                  isBold: true,
                  ph: "",
                  input: sharedOwner,
                  kbType: TextInputType.text,
                  len: 255,
                ))
            : SizedBox(),
        SizedBox(height: 10),
      ],
    );
  }

  _drawFormParts() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      //SizedBox(height: 20),
      UIHelper().drawRadioTitle(
          context: context,
          title: "Interest Only",
          subTitle: "Has an Interest only loan in full or part been requested?",
          list: W2NLocalData.listYesNoRB,
          index: interestOnlyRBIndex,
          callback: (index) {
            setState(() => interestOnlyRBIndex = index);
          }),
      interestOnlyRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawCurrencyBox("Amount of Interest Only requested", null,
                      amountOfInterest, null, null, (v) {}),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Your Exit strategy",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  Txt(
                      txt:
                          "If you opt for Interest Only loan, you must remember that your mortgage will still be outstanding at the end of the mortgage term. The interest only part will need to be paid off when the mortgage term comes to an end. Your security property will be repossessed if you do not keep up with mortgage payment or unable to redeem your outstanding mortgage at the end of term.",
                      txtColor: Colors.black54,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 5),
                  Inputweb2native(
                    title: null,
                    isBold: true,
                    ph: "",
                    input: exitStrategry,
                    kbType: TextInputType.text,
                    len: 100,
                  ),
                ],
              ),
            )
          : SizedBox(),
      SizedBox(height: 10),
      caseModelCtrl.locModel.value.title ==
                  NewCaseCfg.getEnumCaseTitle(
                      eCaseTitle.Residential_Mortgage) ||
              caseModelCtrl.locModel.value.title ==
                  NewCaseCfg.getEnumCaseTitle(
                      eCaseTitle.Residential_Remortgage) ||
              caseModelCtrl.locModel.value.title ==
                  NewCaseCfg.getEnumCaseTitle(
                      eCaseTitle.Second_Charge_0HYPN0_Residential)
          ? drawCase1()
          : caseModelCtrl.locModel.value.title ==
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Buy_to_Let_Mortgage) ||
                  caseModelCtrl.locModel.value.title ==
                      NewCaseCfg.getEnumCaseTitle(
                          eCaseTitle.Buy_to_Let_Remortgage) ||
                  caseModelCtrl.locModel.value.title ==
                      NewCaseCfg.getEnumCaseTitle(eCaseTitle
                          .Second_Charge_0HYPN0_Buy_to_Let_0AND0_Commercial) ||
                  caseModelCtrl.locModel.value.title ==
                      NewCaseCfg.getEnumCaseTitle(eCaseTitle.Let_to_Buy)
              ? drawCase4()
              : SizedBox()
    ]);
  }

  drawCase1() {
    return Column(children: [
      UIHelper().drawRadioTitle(
          context: context,
          title: "Lending into Retirement",
          subTitle: "Do lending go into retirement?",
          list: W2NLocalData.listYesNoRB,
          index: retirementRBIndex,
          callback: (index) {
            setState(() => retirementRBIndex = index);
          }),
      retirementRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawTextArea(
                      title:
                          "What will be you retirement income from, i.e.: pensions?",
                      subTitle:
                          "You must understand that if your mortgage term goes beyond your selected retirement age, you must have ways to pay for that mortgage. It is understood that on retirement the income normally reduces than when working, but the mortgage repayment amount is unlikely to change.",
                      tf: retirementDesc)
                ],
              ),
            )
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title:
              "Is the client(s) willing to potentially pay a greater amount over the term of the mortgage",
          list: W2NLocalData.listYesNoRB,
          index: greaterAmountRBIndex,
          callback: (index) {
            setState(() => greaterAmountRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title:
              "Has the client(s) had any difficulties with past repayments of the loan(s)",
          list: W2NLocalData.listYesNoRB,
          index: difficultiesRBIndex,
          callback: (index) {
            setState(() => difficultiesRBIndex = index);
          }),
      difficultiesRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title:
                    "If yes, when and have they negotiated any special arrangements with the creditor or should they consider this?",
                isBold: true,
                ph: "",
                input: difficultiesDesc,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      drawTextArea(title: "Notes", tf: desc),
      SizedBox(height: 10),
      Column(children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Txt(
                txt:
                    "Key Information About The Type Of Mortgage Applicable To You",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Txt(
                txt:
                    "In order to give you a high standard of service, we need to understand your requirements, attitudes and objectives to help us to provide you with a mortgage fitting your needs and relevant to your circumstances. State a reason where you answer Yes.",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
          )
        ]),
      ]),
      SizedBox(height: 10),
      Container(
        color: Colors.grey[200],
        width: getW(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Txt(
              txt:
                  "1. Might your income or expenditure change significantly within the foreseeable future?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
        ),
      ),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "a. Income",
          list: W2NLocalData.listYesNoRB,
          index: incomeChangeRBIndex,
          callback: (index) {
            setState(() => incomeChangeRBIndex = index);
          }),
      incomeChangeRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Approximate timescale / Amount / Reason",
                isBold: true,
                ph: "",
                input: incomeChange,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "b. Expenditure",
          list: W2NLocalData.listYesNoRB,
          index: expenditureChangeRBIndex,
          callback: (index) {
            setState(() => expenditureChangeRBIndex = index);
          }),
      expenditureChangeRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Approximate timescale / Amount / Reason",
                isBold: true,
                ph: "",
                input: expenditureChange,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 20),
      Container(
        color: Colors.grey[200],
        width: getW(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Txt(
              txt:
                  "2. Do you have any plans to pay off some or all of the mortgage in the foreseeable future?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
        ),
      ),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          list: W2NLocalData.listYesNoRB,
          index: payOffRBIndex,
          callback: (index) {
            setState(() => payOffRBIndex = index);
          }),
      SizedBox(height: 10),
      payOffRBIndex == 0
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                drawCurrencyBox("Approximate Amount", null, payoffAmount, null,
                    null, (v) {}),
                SizedBox(height: 10),
                Inputweb2native(
                  title: "Approximate timescale / Amount / Reason",
                  isBold: true,
                  ph: "",
                  input: payoffReason,
                  kbType: TextInputType.text,
                  len: 100,
                )
              ],
            )
          : SizedBox(),
      SizedBox(height: 10),
      Container(
        color: Colors.grey[200],
        width: getW(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Txt(
              txt:
                  "3. Are you likely to move home within the foreseeable future (other than this transaction)?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
        ),
      ),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          list: W2NLocalData.listYesNoRB,
          index: moveHomeRBIndex,
          callback: (index) {
            setState(() => moveHomeRBIndex = index);
          }),
      moveHomeRBIndex == 0
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                UIHelper().drawRadioTitle(
                    context: context,
                    list: listMoveHomeSizeRB,
                    index: moveHomeSizeRBIndex,
                    isMainBG: false,
                    callback: (index) {
                      setState(() => moveHomeSizeRBIndex = index);
                    }),
                SizedBox(height: 5),
                Inputweb2native(
                  title: "Approximate timescale / Amount / Reason",
                  isBold: true,
                  ph: "",
                  input: moveHomeReason,
                  kbType: TextInputType.text,
                  len: 100,
                )
              ],
            )
          : SizedBox(),
      SizedBox(height: 20),
      Container(
        color: Colors.grey[200],
        width: getW(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Txt(
              txt: "4. Mortgage Requirements",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
        ),
      ),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title:
              "a. An upper limit on your mortgage costs for a specific period",
          list: W2NLocalData.listYesNoRB,
          index: mortgageReqUpperLimitRBIndex,
          callback: (index) {
            setState(() => mortgageReqUpperLimitRBIndex = index);
          }),
      mortgageReqUpperLimitRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Reason and how long for",
                isBold: true,
                ph: "",
                input: mortgageReqUpperLimitReason,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "b. To fix your mortgage costs for a specific period",
          list: W2NLocalData.listYesNoRB,
          index: mortgageReqFixRBIndex,
          callback: (index) {
            setState(() => mortgageReqFixRBIndex = index);
          }),
      mortgageReqFixRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Reason and how long for",
                isBold: true,
                ph: "",
                input: mortgageReqFixReason,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "c. A rate linked to the Bank of England base rate",
          list: W2NLocalData.listYesNoRB,
          index: mortgageReqBaseRateRBIndex,
          callback: (index) {
            setState(() => mortgageReqBaseRateRBIndex = index);
          }),
      mortgageReqBaseRateRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Reason and how long for",
                isBold: true,
                ph: "",
                input: mortgageReqBaseRateReason,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "d. A discount on your mortgage repayments in the early years",
          list: W2NLocalData.listYesNoRB,
          index: mortgageReqDisRBIndex,
          callback: (index) {
            setState(() => mortgageReqDisRBIndex = index);
          }),
      mortgageReqDisRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Reason and how long for",
                isBold: true,
                ph: "",
                input: mortgageReqDisReason,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "e. Access to an initial cash sum (known as a cashback)",
          list: W2NLocalData.listYesNoRB,
          index: mortgageReqCashbackRBIndex,
          callback: (index) {
            setState(() => mortgageReqCashbackRBIndex = index);
          }),
      mortgageReqCashbackRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Reason and how long for",
                isBold: true,
                ph: "",
                input: mortgageReqCashbackReason,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 20),
      Container(
        color: Colors.grey[200],
        width: getW(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Txt(
              txt: "5. Which of the following are important to you",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
        ),
      ),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "a. No early repayment charge on your mortgage at any point",
          list: W2NLocalData.listYesNoRB,
          index: earlyRepaymentRBIndex,
          callback: (index) {
            setState(() => earlyRepaymentRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title:
              "b. No early repayment charge overhang after selected rate ends",
          list: W2NLocalData.listYesNoRB,
          index: earlyRepaymentOverhangRBIndex,
          callback: (index) {
            setState(() => earlyRepaymentOverhangRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "c. No high lending charge",
          list: W2NLocalData.listYesNoRB,
          index: highLandingRBIndex,
          callback: (index) {
            setState(() => highLandingRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "d. Speed of mortgage completion",
          list: W2NLocalData.listYesNoRB,
          index: speedMortRBIndex,
          callback: (index) {
            setState(() => speedMortRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "e. Ability to add fees to the loan",
          list: W2NLocalData.listYesNoRB,
          index: addFeeLoanRBIndex,
          callback: (index) {
            setState(() => addFeeLoanRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "f. Ability to take repayment holidays",
          list: W2NLocalData.listYesNoRB,
          index: repaymentHolidayRBIndex,
          callback: (index) {
            setState(() => repaymentHolidayRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "g. Ability to make underpayments or overpayments",
          list: W2NLocalData.listYesNoRB,
          index: overPaymentRBIndex,
          callback: (index) {
            setState(() => overPaymentRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "h. Free legal fees",
          list: W2NLocalData.listYesNoRB,
          index: freeLegalFeeRBIndex,
          callback: (index) {
            setState(() => freeLegalFeeRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "i. No valuation fee",
          list: W2NLocalData.listYesNoRB,
          index: noValFeeRBIndex,
          callback: (index) {
            setState(() => noValFeeRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "j. Have valuations fees refunded",
          list: W2NLocalData.listYesNoRB,
          index: valFeeRefundedRBIndex,
          callback: (index) {
            setState(() => valFeeRefundedRBIndex = index);
          }),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "k. Do you require any mortgage features?",
          list: W2NLocalData.listYesNoRB,
          index: reqMortFeaturesRBIndex,
          callback: (index) {
            setState(() => reqMortFeaturesRBIndex = index);
          }),
      reqMortFeaturesRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Inputweb2native(
                title: "Mortgage features details",
                isBold: true,
                ph: "",
                input: mortgageFeatureDetails,
                kbType: TextInputType.text,
                len: 100,
              ))
          : SizedBox(),
      SizedBox(height: 10),
      drawInputBox(
        context: context,
        title:
            "Would you like any other mortgage features such as overpayments or portability",
        ph: "",
        input: portability,
        len: 50,
        txtColor: Colors.black,
        isBold: true,
        kbType: TextInputType.text,
        inputAction: TextInputAction.next,
        focusNode: focusPortability,
        focusNodeNext: null,
      ),
      SizedBox(height: 10),
      drawTextArea(title: "Notes", tf: notes),
      SizedBox(height: 10),
    ]);
  }

  drawCase4() {
    return Column(children: [
      UIHelper().drawRadioTitle(
          context: context,
          title: "Lending into Retirement",
          subTitle: "Do lending go into retirement?",
          list: W2NLocalData.listYesNoRB,
          index: retirementRBIndex,
          callback: (index) {
            setState(() => retirementRBIndex = index);
          }),
      retirementRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawTextArea(
                      title:
                          "What will be you retirement income from, i.e.: pensions?",
                      subTitle:
                          "You must understand that if your mortgage term goes beyond your selected retirement age, you must have ways to pay for that mortgage. It is understood that on retirement the income normally reduces than when working, but the mortgage repayment amount is unlikely to change.",
                      tf: retirementDesc)
                ],
              ),
            )
          : SizedBox(),
    ]);
  }
}
