import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserInCome.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserOccupation.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/7emp_income/req_emp_income_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step6_cur_res_mort_ctrl.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step7_emp_income_ctrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class EmpIncomeBase<T extends StatefulWidget> extends CaseBase<T> {
  onEmpClicked(MortgageUserOccupation model);
  onDelEmpAPI(MortgageUserOccupation model);
  onIncomeClicked(MortgageUserInCome model);
  onDelIncomeAPI(MortgageUserInCome mode);

  drawEmpForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Txt(
            txt:
                "You can get most of these details from either your payslip (if you are employed full/part-time) or your Tax Year Overview (SA302) from HMRC",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            fontWeight: FontWeight.w500,
            txtAlign: TextAlign.start,
          ),
          SizedBox(height: 10),
          drawDottedBox("Add employment", () {
            onEmpClicked(null);
          }),
          drawEmpExpandableList(context, empIncomeCtrl.empCtrl, true,
              (model, isEdit) {
            if (isEdit) {
              onEmpClicked(model);
            } else {
              onDelEmpAPI(model);
            }
          }),
        ],
      ),
    );
  }

  drawIncomeForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Txt(
            txt:
                "Do you have any additional income? You can add more than one source.",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            fontWeight: FontWeight.w500,
            txtAlign: TextAlign.start,
          ),
          SizedBox(height: 10),
          drawDottedBox("Add income", () {
            onIncomeClicked(null);
          }),
          drawIncomeExpandableList(context, empIncomeCtrl.incomeCtrl, true,
              (model, isEdit) {
            if (isEdit) {
              onIncomeClicked(model);
            } else {
              onDelIncomeAPI(model);
            }
          }),
        ],
      ),
    );
  }
}
