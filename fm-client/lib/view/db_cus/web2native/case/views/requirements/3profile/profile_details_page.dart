import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/more/profile/EditProfileBase.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/5dependents/dependents_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileDetailsPage extends StatefulWidget {
  @override
  State createState() => _ProfileDetailsState();
}

class _ProfileDetailsState extends EditProfileBase<ProfileDetailsPage> {
  @override
  void initState() {
    super.initState();
    isExtAccess = true;
    updateData();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Your details"),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: ListView(
        shrinkWrap: true,
        children: [
          SizedBox(height: 10),
          drawProgressView(35),
          SizedBox(height: 10),
          Txt(
              txt:
                  "You can get most of these details from either your passport or your driver’s license. The more accurate the data entered here the fewer errors there will be in your application and the quicker your whole mortgage process will be!",
              txtColor: MyTheme.statusBarColor,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 20),
          drawPersonalInfoView(),
          SizedBox(height: 40),
          drawVisaInfoView(),
          SizedBox(height: 40),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: MMBtn(
                    txt: "Back",
                    height: getHP(context, 7),
                    width: getW(context),
                    bgColor: Colors.grey,
                    radius: 10,
                    callback: () async {
                      Get.back();
                    }),
              ),
              SizedBox(width: 10),
              Flexible(
                child: MMBtn(
                    txt: "Save & continue",
                    height: getHP(context, 7),
                    width: getW(context),
                    radius: 10,
                    callback: () {
                      updateProfileAPI(
                          profileImageId: profileImageId.toString(),
                          coverImageId: coverImageId.toString(),
                          callback: (model) {
                            Get.off(() => DependentsPage());
                          });
                    }),
              ),
            ],
          ),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}
