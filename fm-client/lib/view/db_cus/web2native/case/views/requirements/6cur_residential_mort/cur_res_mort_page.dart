import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/cur_res_mort/PostCurResMortAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/6cur_residential_mort/cur_res_mort_base.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/7emp_income/emp_income_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CurResMortPage extends StatefulWidget {
  const CurResMortPage({Key key}) : super(key: key);
  @override
  State<CurResMortPage> createState() => _CurResMortPageState();
}

class _CurResMortPageState extends CurResMortBase<CurResMortPage> {
  onPostPutCurResMortAPI() async {
    var id;
    try {
      if (curResMortCtrl.curResMortCtrl != null)
        id = curResMortCtrl.curResMortCtrl.value.id;
    } catch (e) {}

    try {
      var _currentValProperty = 0;
      var _purchasePrice = 0;
      var _currentMortOutstanding = 0;
      var _monthlyPay = 0;

      try {
        _currentValProperty = int.parse(currentValProperty.text.trim());
      } catch (e) {}
      try {
        _purchasePrice = int.parse(purchasePrice.text.trim());
      } catch (e) {}
      try {
        _currentMortOutstanding = int.parse(currentMortOutstanding.text.trim());
      } catch (e) {}
      try {
        _monthlyPay = int.parse(monthlyPay.text.trim());
      } catch (e) {}

      await APIViewModel().req<PostCurResMortAPIModel>(
          context: context,
          url: id == null
              ? SrvW2NCase.POST_CUR_RES_MORT_URL
              : SrvW2NCase.PUT_CUR_RES_MORT_URL,
          reqType: id == null ? ReqType.Post : ReqType.Put,
          param: {
            "Id": id == null ? 0 : id,
            "User": null,
            "UserId": userData.userModel.id,
            "CompanyId": userData.userModel.userCompanyInfo.id,
            "Status": 101,
            "MortgageCaseInfoId": 0,
            "TaskId": caseModelCtrl.locModel.value.id,
            "CreationDate": DateTime.now().toString(),
            "AmountOutstanding": _currentMortOutstanding,
            "TermOutstanding": termsOutstanding.text.trim(),
            "RepaymentType": listRepayTypeRB[repayTypeRBIndex],
            "Lender": currentLender.text.trim(),
            "AccountNumber": "",
            "CurrentMonthlyPaymentAndInterestRate": _monthlyPay,
            "InterestRateType": listFixedRatePeriodRB[fixedRatePeriodRBIndex],
            "EndDateForInterestRateType": dtEndFixedPrice.value,
            "AreThereAnyEarlyRepaymentChargesPayable": "No",
            "IfEarlyRepaymentChargesArePayableStateFigure": 0,
            "AreYouPreparedToPayTheEarlyRepaymentCharges": "No",
            "IsTheCurrentMortgagePortableToANewProperty": "No",
            "IfTheCurrentPropertyIsBeingSoldWhatIsTheSalePrice": 0,
            "IsTheCurrentMortgageRepaidInTheEventOfDeath": "No",
            "IsTheCurrentMortgageRepaidInTheEventOfCriticalIllness": "No",
            "IsTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy":
                "No",
            "IsBuildingsAndContentsInsuranceInPlace": "No",
            "Remarks": notes.text.trim(),
            "CurrentValueOfTheResidentialProperty": _currentValProperty,
            "PurchasePrice": _purchasePrice,
            "EndDateForInterestRateTypeDD": "10",
            "EndDateForInterestRateTypeMM": "09",
            "EndDateForInterestRateTypeYY": "1900",
            "PurchaseDate": dtPurchase.value,
            "PurchaseDateDD": "10",
            "PurchaseDateMM": "10",
            "PurchaseDateYY": "2000",
            "LandlordName": name.text.trim(),
            "ContactNumber": mobile.text.trim(),
            "Address": addr.value,
            "EmailAddress": email.text.trim(),
            "OccupantType": listOccupantTypeRB[occupantTypeRBIndex],
            "TermMonth": 0,
          },
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                curResMortCtrl.curResMortCtrl.value =
                    model.responseData.mortgageUserCurrentResidentialMortgage;
                curResMortCtrl.update();
                Get.off(() => EmpIncomePage());
              }
            }
          });
    } catch (e) {}
  }

  onValidate() {
    return true;
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      final model = curResMortCtrl.curResMortCtrl.value;
      try {
        occupantTypeRBIndex =
            Common.getMapKeyByVal(listOccupantTypeRB, model.occupantType);
      } catch (e) {}
      try {
        repayTypeRBIndex =
            Common.getMapKeyByVal(listRepayTypeRB, model.repaymentType);
      } catch (e) {}
      try {
        fixedRatePeriodRBIndex = Common.getMapKeyByVal(
            listFixedRatePeriodRB, model.interestRateType);
      } catch (e) {}
      try {
        currentValProperty.text =
            model.currentValueOfTheResidentialProperty.toStringAsFixed(0) ?? '';
      } catch (e) {}
      try {
        purchasePrice.text = model.purchasePrice.toStringAsFixed(0) ?? '';
      } catch (e) {}
      try {
        currentMortOutstanding.text =
            model.amountOutstanding.toStringAsFixed(0) ?? '';
      } catch (e) {}
      try {
        currentLender.text = model.lender ?? '';
      } catch (e) {}
      try {
        termsOutstanding.text = model.termOutstanding ?? '';
      } catch (e) {}
      try {
        monthlyPay.text =
            model.currentMonthlyPaymentAndInterestRate.toStringAsFixed(0) ?? '';
      } catch (e) {}
      try {
        notes.text = model.remarks ?? '';
      } catch (e) {}
      try {
        name.text = model.landlordName ?? '';
      } catch (e) {}
      try {
        mobile.text = model.contactNumber ?? '';
      } catch (e) {}
      try {
        email.text = model.emailAddress ?? '';
      } catch (e) {}
      try {
        dtPurchase.value = DateFun.getDate(model.purchaseDate, 'MM/dd/yyyy');
      } catch (e) {}
      try {
        dtEndFixedPrice.value =
            DateFun.getDate(model.endDateForInterestRateType, 'MM/dd/yyyy');
      } catch (e) {}
      try {
        addr.value = model.address;
      } catch (e) {}
    } catch (e) {}
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper()
                    .drawAppbarTitle(title: "Current Residential Mortgage"),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              drawProgressView(60),
              SizedBox(height: 20),
              drawForm(),
              SizedBox(height: 30),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: MMBtn(
                        txt: "Back",
                        height: getHP(context, 7),
                        width: getW(context),
                        bgColor: Colors.grey,
                        radius: 10,
                        callback: () async {
                          Get.back();
                        }),
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    child: MMBtn(
                        txt: "Save & continue",
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          if (onValidate()) {
                            onPostPutCurResMortAPI();
                          }
                        }),
                  ),
                ],
              ),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}
