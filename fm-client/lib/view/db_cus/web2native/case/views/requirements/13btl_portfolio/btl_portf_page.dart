import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/13btl_portfolio/btl_portf_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BtlPortfolioPage extends StatefulWidget {
  const BtlPortfolioPage({Key key}) : super(key: key);
  @override
  State<BtlPortfolioPage> createState() => _BtlPortfolioPageState();
}

class _BtlPortfolioPageState extends BtlPortFolioBase<BtlPortfolioPage> {
  onDelBtlPortFolioItem(MortgageUserPortfolios model) async {
    try {
      await APIViewModel().req<CommonAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_BTL_PORTFOLIO_URL + model.id.toString(),
          reqType: ReqType.Delete,
          callback: (model2) async {
            if (mounted && model2 != null) {
              if (model2.success) {
                btlPortfCtrl.listBtlPortModel.remove(model);
                btlPortfCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "BTL Portfolio"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: MMBtn(
                      txt: "Continue",
                      height: getHP(context, 7),
                      width: getW(context),
                      radius: 10,
                      callback: () async {
                        Get.back();
                      }),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(padding: const EdgeInsets.all(20), child: drawProgressView(90)),
      drawForm(),
    ])));
  }
}
