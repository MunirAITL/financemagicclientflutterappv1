import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserInCome.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserOccupation.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/PostEmpAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/gplaces/GPlacesView.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/IcoTxtIco.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

import 'emp_income_base.dart';

class EmpDetailsPage extends StatefulWidget {
  final MortgageUserOccupation empModel;
  const EmpDetailsPage({Key key, this.empModel}) : super(key: key);
  @override
  State<EmpDetailsPage> createState() => _EmpDetailsPageState();
}

class _EmpDetailsPageState extends EmpIncomeBase<EmpDetailsPage> {
  //  Employed
  DropListModel ddEmpType = DropListModel([
    OptionItem(id: "1", title: "Employed"),
    OptionItem(id: "2", title: "Self-employed"),
    OptionItem(id: "3", title: "Contractor"),
    OptionItem(id: "4", title: "Retired"),
    //OptionItem(id: "5", title: "Unemployed"),
  ]);
  var optEmpType = OptionItem(id: "1", title: "Employed").obs;

  DropListModel ddEmpNature = DropListModel([
    OptionItem(id: "Accountant", title: "Accountant"),
    OptionItem(id: "Actor", title: "Actor"),
    OptionItem(
        id: "Administration Assistant", title: "Administration Assistant"),
    OptionItem(id: "Analyst", title: "Analyst"),
    OptionItem(id: "Architect", title: "Architect"),
    OptionItem(id: "Armed Forces", title: "Armed Forces"),
    OptionItem(id: "Artist", title: "Artist"),
    OptionItem(id: "Bailiff", title: "Bailiff"),
    OptionItem(id: "Banker", title: "Banker"),
    OptionItem(id: "Bar Worker", title: "Bar Worker"),
    OptionItem(id: "Barber", title: "Barber"),
    OptionItem(id: "Beautician", title: "Beautician"),
    OptionItem(id: "Bookmaker", title: "Bookmaker"),
    OptionItem(id: "Bricklayer", title: "Bricklayer"),
    OptionItem(id: "Broker", title: "Broker"),
    OptionItem(id: "Builder", title: "Builder"),
    OptionItem(id: "Bus Driver", title: "Bus Driver"),
    OptionItem(id: "Business Consultant", title: "Business Consultant"),
    OptionItem(id: "Butcher", title: "Butcher"),
    OptionItem(id: "Call Centre Operator", title: "Call Centre Operator"),
    OptionItem(id: "Car Dealer", title: "Car Dealer"),
    OptionItem(id: "Care Worker", title: "Care Worker"),
    OptionItem(id: "Caretaker", title: "Caretaker"),
    OptionItem(id: "Carpenter", title: "Carpenter"),
    OptionItem(id: "Carpet Fitter", title: "Carpet Fitter"),
    OptionItem(id: "Cashier", title: "Cashier"),
    OptionItem(id: "Caterer", title: "Caterer"),
    OptionItem(id: "Chartered Surveyor", title: "Chartered Surveyor"),
    OptionItem(id: "Chauffeur", title: "Chauffeur"),
    OptionItem(id: "Chef", title: "Chef"),
    OptionItem(id: "Chiropodist", title: "Chiropodist"),
    OptionItem(id: "Chiropractor", title: "Chiropractor"),
    OptionItem(id: "Civil Servant", title: "Civil Servant"),
    OptionItem(id: "Cleaner", title: "Cleaner"),
    OptionItem(id: "Clerical Worker", title: "Clerical Worker"),
    OptionItem(id: "College Lecturer", title: "College Lecturer"),
    OptionItem(id: "Consultant", title: "Consultant"),
    OptionItem(id: "Contractor", title: "Contractor"),
    OptionItem(id: "Cook", title: "Cook"),
    OptionItem(id: "Council Worker", title: "Council Worker"),
    OptionItem(
        id: "Customer Service Advisor", title: "Customer Service Advisor"),
    OptionItem(id: "Delivery Driver", title: "Delivery Driver"),
    OptionItem(id: "Dentist", title: "Dentist"),
    OptionItem(id: "Director", title: "Director"),
    OptionItem(id: "Doctor", title: "Doctor"),
    OptionItem(id: "Driving Instructor", title: "Driving Instructor"),
    OptionItem(id: "Electrician", title: "Electrician"),
    OptionItem(id: "Farmer", title: "Farmer"),
    OptionItem(id: "Farrier", title: "Farrier"),
    OptionItem(id: "Firefighter", title: "Firefighter"),
    OptionItem(id: "Fitness Instructor", title: "Fitness Instructor"),
    OptionItem(id: "Forest Ranger", title: "Forest Ranger"),
    OptionItem(id: "Gardener", title: "Gardener"),
    OptionItem(id: "Government", title: "Government"),
    OptionItem(id: "Hairdresser", title: "Hairdresser"),
    OptionItem(id: "HGV Driver", title: "HGV Driver"),
    OptionItem(id: "Jeweller", title: "Jeweller"),
    OptionItem(id: "Judge", title: "Judge"),
    OptionItem(id: "Lawyer", title: "Lawyer"),
    OptionItem(id: "Lecturer", title: "Lecturer"),
    OptionItem(id: "Librarian", title: "Librarian"),
    OptionItem(id: "Management", title: "Management"),
    OptionItem(id: "Managing Director", title: "Managing Director"),
    OptionItem(id: "Manual Worker", title: "Manual Worker"),
    OptionItem(id: "Market Trader", title: "Market Trader"),
    OptionItem(id: "Model", title: "Model"),
    OptionItem(id: "Musician", title: "Musician"),
    OptionItem(id: "Nurse", title: "Nurse"),
    OptionItem(id: "Paramedic", title: "Paramedic"),
    OptionItem(id: "Photographer", title: "Photographer"),
    OptionItem(id: "Physiotherapist", title: "Physiotherapist"),
    OptionItem(id: "Plumber", title: "Plumber"),
    OptionItem(id: "Police Force", title: "Police Force"),
    OptionItem(id: "Project Manager", title: "Project Manager"),
    OptionItem(id: "Property Developer", title: "Property Developer"),
    OptionItem(id: "Public Relations Person", title: "Public Relations Person"),
    OptionItem(id: "Publican", title: "Publican"),
    OptionItem(id: "Shop Assistant", title: "Shop Assistant"),
    OptionItem(id: "Social Worker", title: "Social Worker"),
    OptionItem(id: "Software Engineer", title: "Software Engineer"),
    OptionItem(id: "Solicitor", title: "Solicitor"),
    OptionItem(id: "Sports Trainer", title: "Sports Trainer"),
    OptionItem(id: "Taxi Driver", title: "Taxi Driver"),
    OptionItem(id: "Teacher", title: "Teacher"),
    OptionItem(id: "Technology Manager", title: "Technology Manager"),
    OptionItem(
        id: "Technology Systems / Software Developer",
        title: "Technology Systems / Software Developer"),
    OptionItem(id: "Tree Surgeon", title: "Tree Surgeon"),
    OptionItem(id: "Vet", title: "Vet"),
    OptionItem(id: "Window Cleaner", title: "Window Cleaner"),
    OptionItem(id: "Other", title: "Other"),
  ]);
  var optEmpNature = OptionItem(id: null, title: "Please select").obs;

  int ownShareRBIndex = 1;

  int ownShare20RBIndex = 1;

  final listHowEmployedRB = {0: 'Permanently', 1: 'Temporarily'};
  int howEmployedRBIndex = 1;

  int curWorkingRBIndex = 1;

  int earnByOTRBIndex = 1;

  int earnByBonusRBIndex = 1;

  int earnByComRBIndex = 1;

  final empName = TextEditingController();
  final email = TextEditingController();
  final mobile = TextEditingController();
  final houseNo = TextEditingController();
  final tradingName = TextEditingController();
  final jobTitle = TextEditingController();
  final pa = TextEditingController();
  final retireAge = TextEditingController();
  final annualIncome = TextEditingController();
  final monthlyIncome = TextEditingController();
  final payslipRef = TextEditingController();
  final otYear = TextEditingController();
  final bonusYear = TextEditingController();
  final comYear = TextEditingController();
  final notes = TextEditingController();

  var addr = "".obs;
  var postCode = "".obs;
  var dtWhenIncorp = "".obs;
  var dtStartJob = "".obs;
  var dtEndJob = "".obs;

  //  self-employed
  final listSelfEmpStatusRB = {
    0: "Sole Trader",
    1: "Contractor",
    2: "Director of Limited Company",
    3: "Other"
  };
  int selfEmpStatusRBIndex = 0;

  int areAccAvailableRBIndex = 1;

  int areSA302AvailableRBIndex = 1;

  final incomeRecentY = TextEditingController();
  final incomePreviousY = TextEditingController();
  final netProfitY = TextEditingController();
  final otherSelfEmpStatus = TextEditingController();

  var dtRecentY = "".obs;
  var dtPreviousY = "".obs;
  var dtNetProfitY = "".obs;

  //  contractor
  int contractRenewableRBIndex = 1;

  int haveSubSelfAssessmentTaxReturnRBIndex = 1;

  final contractLeft = TextEditingController();
  final dailyPayRate = TextEditingController();

  //  retired
  int otherPensionRBIndex = 1;

  int otherBenefitsRBIndex = 1;

  final pensionAnnually = TextEditingController();
  final otherPension = TextEditingController();
  final descBenefits = TextEditingController();
  final benefitAmounts = TextEditingController();

  calculateDailyPayRate() async {
    try {
      if (dailyPayRate.text.isNotEmpty) {
        try {
          final payRate = double.parse(dailyPayRate.text);
          if (payRate > 0) {
            annualIncome.text = (payRate * 200).toStringAsFixed(3);
          } else {
            annualIncome.text = "0";
          }
        } catch (e) {}
      } else {
        annualIncome.text = "0";
      }
    } catch (e) {}
  }

  calculateAnnaulIncome() async {
    try {
      if (annualIncome.text.isNotEmpty) {
        try {
          final anualIncome = double.parse(annualIncome.text);
          if (anualIncome > 0) {
            dailyPayRate.text = (anualIncome * 0.005).toStringAsFixed(3);
          } else {
            dailyPayRate.text = "0";
          }
        } catch (e) {}
      } else {
        dailyPayRate.text = "0";
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      if (widget.empModel != null) {
        final model = widget.empModel;
        try {
          optEmpType = OptionItem(id: "1", title: model.basisOfEmployment).obs;
        } catch (e) {}
        try {
          empName.text = model.currentEmployer;
        } catch (e) {}
        try {
          email.text = model.email;
        } catch (e) {}
        try {
          mobile.text = model.contactNumber;
        } catch (e) {}
        try {
          if (model.basisOfEmployment == 'Self-employed')
            addr.value = model.businessAddress;
          else
            addr.value = model.address;
        } catch (e) {}
        try {
          selfEmpStatusRBIndex = Common.getMapKeyByVal(
              listSelfEmpStatusRB, model.employmentStatus);
        } catch (e) {}
        try {
          houseNo.text =
              (model.buildingNumber + " " + model.buildingName).trim();
        } catch (e) {}
        try {
          dtStartJob.value =
              DateFun.getDate(model.whatWasYourStartDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          curWorkingRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.areYouCurrentlyWorkingThere);
        } catch (e) {}
        try {
          dtEndJob.value =
              DateFun.getDate(model.whatWasYourEndDate, "dd-MM-yyyy");
        } catch (e) {}
        try {
          contractLeft.text = model.howLongHasTheContractLeftToRun;
        } catch (e) {}
        try {
          contractRenewableRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isTheContractRenewable);
        } catch (e) {}
        try {
          dailyPayRate.text = model.whatIsYourDailyPayRate.toStringAsFixed(3);
        } catch (e) {}
        try {
          if (model.basisOfEmployment == 'Contractor')
            annualIncome.text = model.yourAnnualIncome.toStringAsFixed(3);
          else if (model.basisOfEmployment == 'Employed')
            annualIncome.text = model.currentSalary.toStringAsFixed(0);
          else
            annualIncome.text = model.yourAnnualIncome.toStringAsFixed(0);
        } catch (e) {}
        try {
          haveSubSelfAssessmentTaxReturnRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB,
              model.haveYouSubmittedYourSelfAssessmentTaxReturns);
        } catch (e) {}
        try {
          incomeRecentY.text = model.incomeRecentYear.toStringAsFixed(0);
        } catch (e) {}
        try {
          incomePreviousY.text = model.incomePreviousYear.toStringAsFixed(0);
        } catch (e) {}
        try {
          netProfitY.text = model.netProfitYearBeforeThat.toStringAsFixed(0);
        } catch (e) {}

        try {
          dtRecentY.value =
              DateFun.getDate(model.recentYearEndingDate, "MM-yyyy");
        } catch (e) {}
        try {
          dtPreviousY.value =
              DateFun.getDate(model.previousYearEndingDate, "MM-yyyy");
        } catch (e) {}
        try {
          dtNetProfitY.value =
              DateFun.getDate(model.netProfitYearEndingDate, "MM-yyyy");
        } catch (e) {}
        try {
          notes.text = model.notes ?? '';
        } catch (e) {}
        try {
          ownShareRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isOwnShares);
        } catch (e) {}
        try {
          ownShare20RBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isOwnSharesMoreThanTwenty);
        } catch (e) {}
        try {
          optEmpNature =
              OptionItem(id: "1", title: model.natureOfYourBusiness).obs;
        } catch (e) {}
        try {
          tradingName.text = model.tradeName;
        } catch (e) {}
        try {
          dtWhenIncorp.value =
              DateFun.getDate(model.incorporatedDate, "dd-MM-yyyy");
        } catch (e) {}
        try {
          howEmployedRBIndex =
              Common.getMapKeyByVal(listHowEmployedRB, model.howAreYouEmployed);
        } catch (e) {}
        try {
          jobTitle.text = model.occupation;
        } catch (e) {}
        try {
          pa.text = model.percentageShare.toStringAsFixed(0);
        } catch (e) {}
        try {
          AppDefine.optMM = OptionItem(id: "1", title: model.remarks).obs;
        } catch (e) {}
        try {
          retireAge.text = model.aticipatedRetirementAge.toString();
        } catch (e) {}
        try {
          monthlyIncome.text =
              model.monthlyIncomePayslipAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          payslipRef.text = model.paySlipReferenceNumber;
        } catch (e) {}
        try {
          earnByOTRBIndex =
              Common.getMapKeyByVal(W2NLocalData.listYesNoRB, model.isOverTime);
        } catch (e) {}
        try {
          otYear.text = model.overTimeSalary.toStringAsFixed(0);
        } catch (e) {}
        try {
          earnByBonusRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isEarnBonuses);
        } catch (e) {}
        try {
          bonusYear.text = model.bonusesAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          earnByComRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isEarnCommission);
        } catch (e) {}
        try {
          comYear.text = model.earnCommission.toStringAsFixed(0);
        } catch (e) {}
        try {
          areAccAvailableRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isAreAccountsAvailable);
        } catch (e) {}
        try {
          areSA302AvailableRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isAreSA302sAvailable);
        } catch (e) {}
        //
        try {
          pensionAnnually.text = model.yourAnnualIncome.toStringAsFixed(0);
        } catch (e) {}
        try {
          otherPensionRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
              model.isDoYouReceiveAnyOtherPersonalOccupationalPension);
        } catch (e) {}
        try {
          otherPension.text = model.otherPensionAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          otherBenefitsRBIndex = Common.getMapKeyByVal(
              W2NLocalData.listYesNoRB, model.isDoYouReceiveAnyOtherBenefits);
        } catch (e) {}
        try {
          descBenefits.text = model.descriptionOfBenefit ?? '';
        } catch (e) {}
        try {
          benefitAmounts.text = model.benefitAmount.toStringAsFixed(0);
        } catch (e) {}
        try {} catch (e) {}
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: widget.empModel == null
                        ? "Add Employment"
                        : "Edit Employment"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: MMBtn(
                      txt: widget.empModel == null ? "Add" : "Update",
                      height: getHP(context, 7),
                      width: getW(context),
                      radius: 5,
                      callback: () async {
                        /*if (empName.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter your employer name");
                          return;
                        }
                        if (email.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter your employer email address");
                          return;
                        }
                        if (mobile.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter your employer mobile number");
                          return;
                        }*/

                        var _retireAge = 0;
                        var _annualIncome = 0.0;
                        var _otYear = 0;
                        var _bonusYear = 0;
                        var _comYear = 0;
                        var _pa = 0;
                        var _dailyPayRate = 0.0;
                        var _otherPension = 0;
                        var _benefitAmounts = 0;
                        var _incomeRecentY = 0;
                        var _incomePreviousY = 0;
                        var _netProfitY = 0;
                        var _monthlyIncome = 0;

                        try {
                          _retireAge = int.parse(retireAge.text);
                        } catch (e) {}
                        try {
                          _annualIncome = double.parse(annualIncome.text);
                        } catch (e) {}
                        try {
                          _otYear = int.parse(otYear.text);
                        } catch (e) {}
                        try {
                          _bonusYear = int.parse(bonusYear.text);
                        } catch (e) {}
                        try {
                          _comYear = int.parse(comYear.text);
                        } catch (e) {}
                        try {
                          _pa = int.parse(pa.text);
                        } catch (e) {}
                        try {
                          _dailyPayRate = double.parse(dailyPayRate.text);
                        } catch (e) {}
                        try {
                          _annualIncome = double.parse(annualIncome.text);
                        } catch (e) {}
                        try {
                          _otherPension = int.parse(otherPension.text);
                        } catch (e) {}
                        try {
                          _benefitAmounts = int.parse(benefitAmounts.text);
                        } catch (e) {}
                        try {
                          _incomeRecentY = int.parse(incomeRecentY.text);
                        } catch (e) {}
                        try {
                          _incomePreviousY = int.parse(incomePreviousY.text);
                        } catch (e) {}
                        try {
                          _netProfitY = int.parse(netProfitY.text);
                        } catch (e) {}
                        try {
                          _monthlyIncome = int.parse(monthlyIncome.text);
                        } catch (e) {}

                        final param = {
                          "UserId": userData.userModel.id,
                          "User": null,
                          "CompanyId": userData.userModel.userCompanyInfo.id,
                          "Status": 101,
                          "MortgageCaseInfoId": 0,
                          "CreationDate": DateTime.now().toString(),
                          "CurrentEmployer": empName.text,
                          "BasisOfEmployment": optEmpType.value.title,
                          "Email": email.text.trim(),
                          "Occupation": jobTitle.text.trim(),
                          "ContactNumber": mobile.text.trim(),
                          "AticipatedRetirementAge": _retireAge,
                          "EmploymentStatus":
                              listSelfEmpStatusRB[selfEmpStatusRBIndex],
                          "CustomerName": userData.userModel.name,
                          "Remarks": AppDefine.optMM.value.title,
                          "IsOwnSharesMoreThanTwenty":
                              W2NLocalData.listYesNoRB[ownShare20RBIndex],
                          "NatureOfYourBusiness": optEmpNature.value.title,
                          "TradeName": tradingName.text.trim(),
                          "ApartmentNumber": "",
                          "BuildingName": houseNo.text.trim(),
                          "BuildingNumber": "",
                          "Street": "",
                          "City": "",
                          "Postcode": postCode.value,
                          "HowAreYouEmployed":
                              listHowEmployedRB[howEmployedRBIndex],
                          "WhatWasYourStartDate": dtStartJob.value == ''
                              ? AppConfig.date1970
                              : dtStartJob.value ?? AppConfig.date1970,
                          "CurrentSalary": _annualIncome,
                          "IsOverTime":
                              W2NLocalData.listYesNoRB[earnByOTRBIndex],
                          "OverTimeSalary": _otYear,
                          "IsEarnBonuses":
                              W2NLocalData.listYesNoRB[earnByBonusRBIndex],
                          "BonusesAmount": _bonusYear,
                          "IsEarnCommission":
                              W2NLocalData.listYesNoRB[earnByComRBIndex],
                          "EarnCommission": _comYear,
                          "IsOthersourcesOfIncome": "",
                          "IncorporatedDate": dtWhenIncorp.value == ''
                              ? AppConfig.date1970
                              : dtWhenIncorp.value ?? AppConfig.date1970,
                          "PercentageShare": _pa,
                          "IsOwnShares":
                              W2NLocalData.listYesNoRB[ownShareRBIndex],
                          "HowLongHasTheContractLeftToRun":
                              contractLeft.text.trim(),
                          "IsTheContractRenewable": W2NLocalData
                              .listYesNoRB[contractRenewableRBIndex],
                          "WhatIsYourDailyPayRate": _dailyPayRate,
                          "YourAnnualIncome": _annualIncome,
                          "IsHaveYouSubmittedYourSelfAssessmentTaxReturns":
                              "No",
                          "IsDoYouReceiveAnyOtherPersonalOccupationalPension":
                              W2NLocalData.listYesNoRB[otherPensionRBIndex],
                          "OtherPensionAmount": _otherPension,
                          "IsDoYouReceiveAnyOtherBenefits":
                              W2NLocalData.listYesNoRB[otherBenefitsRBIndex],
                          "DescriptionOfBenefit": descBenefits.text.trim(),
                          "BenefitAmount": _benefitAmounts,
                          "HaveYouSubmittedYourSelfAssessmentTaxReturns":
                              W2NLocalData.listYesNoRB[
                                  haveSubSelfAssessmentTaxReturnRBIndex],
                          "IncomeRecentYear": _incomeRecentY,
                          "RecentYearEndingDate": dtRecentY.value,
                          "IncomePreviousYear": _incomePreviousY,
                          "PreviousYearEndingDate": dtPreviousY.value,
                          "NetProfitYearBeforeThat": _netProfitY,
                          "NetProfitYearEndingDate": dtNetProfitY.value,
                          "IsAreAccountsAvailable":
                              W2NLocalData.listYesNoRB[areAccAvailableRBIndex],
                          "IsAreSA302sAvailable": W2NLocalData
                              .listYesNoRB[areSA302AvailableRBIndex],
                          "MonthlyIncomePayslipAmount": _monthlyIncome,
                          "PaySlipReferenceNumber": payslipRef.text.trim(),
                          "Notes": notes.text.trim(),
                          "BusinessAddress": addr.value,
                          "EmploymentOthersStatus": "other self emp status",
                          "Address": addr.value,
                          "AreYouCurrentlyWorkingThere":
                              W2NLocalData.listYesNoRB[curWorkingRBIndex],
                          "WhatWasYourEndDate": dtEndJob.value == ''
                              ? AppConfig.date1970
                              : dtEndJob.value ?? AppConfig.date1970,
                          "Id":
                              widget.empModel == null ? 0 : widget.empModel.id,
                          "LeaveEndDate1": "",
                          "LeaveEndDate2": "",
                          "LeaveEndDate3": "",
                          "LeaveStartDate1": "",
                          "LeaveStartDate2": "",
                          "LeaveStartDate3": "",
                          "WhatWasYourStartDate1": "",
                          "WhatWasYourStartDate2": "",
                          "WhatWasYourStartDate3": "",
                          "WhatWasYourEndDate1": "",
                          "WhatWasYourEndDate2": "",
                          "WhatWasYourEndDate3": "",
                          "IncorporatedDate1": "",
                          "IncorporatedDate2": "",
                          "IncorporatedDate3": "",
                          "RecentYearEndingDateMM": "",
                          "RecentYearEndingDateYY": "",
                          "PreviousYearEndingDateMM": "",
                          "PreviousYearEndingDateYY": "",
                          "NetProfitYearEndingDateMM": "",
                          "NetProfitYearEndingDateYY": "",
                          "TaskId": caseModelCtrl.locModel.value.id
                        };
                        await APIViewModel().req<PostEmpAPIModel>(
                            context: context,
                            url: widget.empModel == null
                                ? SrvW2NCase.POST_EMP_URL
                                : SrvW2NCase.PUT_EMP_URL,
                            reqType: widget.empModel == null
                                ? ReqType.Post
                                : ReqType.Put,
                            param: param,
                            callback: (model) async {
                              if (mounted && model != null) {
                                if (model.success) {
                                  if (widget.empModel != null) {
                                    empIncomeCtrl.empCtrl
                                        .remove(widget.empModel);
                                    empIncomeCtrl.update();
                                  }
                                  empIncomeCtrl.empCtrl.add(model
                                      .responseData.mortgageUserOccupation);
                                  empIncomeCtrl.update();
                                  showToast(
                                      context: context,
                                      msg: model.messages.occupationPost[0]);
                                  Future.delayed(
                                      Duration(seconds: 3), () => Get.back());
                                }
                              }
                            });
                      }),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    Widget wid = SizedBox();
    switch (optEmpType.value.title) {
      case "Employed":
        wid = drawEmploymentView();
        break;
      case "Self-employed":
        wid = drawSelfEmployedView();
        break;
      case "Contractor":
        wid = drawContractorView();
        break;
      case "Retired":
        wid = drawRetiredView();
        break;
      default:
    }
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Obx(() => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  DropDownListDialog(
                    context: context,
                    title: optEmpType.value.title,
                    h1: "Select employment status",
                    heading: "What is your employment status?",
                    ddTitleList: ddEmpType,
                    vPadding: 3,
                    callback: (optionItem) {
                      optEmpType.value = optionItem;
                      setState(() {});
                    },
                  ),
                  wid,
                ],
              )),
        ),
      ),
    );
  }

  drawEmploymentView() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        InputW2N(
          title: "What's the name of your employer?",
          input: empName,
          ph: "What's the name of your employer?",
          kbType: TextInputType.name,
          len: 100,
          isBold: true,
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "What's the Email of your employer?",
          input: email,
          ph: "What's the Email of your employer?",
          kbType: TextInputType.emailAddress,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "What's the Contact Number of your employer?",
          input: mobile,
          ph: "What's the Contact Number of your employer?",
          kbType: TextInputType.phone,
          len: 20,
          isBold: true,
        ),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Do you own shares in this company?",
            list: W2NLocalData.listYesNoRB,
            index: ownShareRBIndex,
            callback: (index) {
              setState(() => ownShareRBIndex = index);
            }),
        SizedBox(height: 10),
        ownShareRBIndex == 0
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIHelper().drawRadioTitle(
                      context: context,
                      title: "Do you own 20% or more of this company?",
                      list: W2NLocalData.listYesNoRB,
                      index: ownShare20RBIndex,
                      callback: (index) {
                        setState(() => ownShare20RBIndex = index);
                      }),
                  ownShare20RBIndex == 0
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            DropDownListDialog(
                              context: context,
                              title: optEmpNature.value.title,
                              h1: "Select nature of your employer",
                              heading: "What is the nature of your employer?",
                              ddTitleList: ddEmpNature,
                              vPadding: 3,
                              callback: (optionItem) {
                                optEmpNature.value = optionItem;
                              },
                            ),
                            SizedBox(height: 10),
                            InputW2N(
                              title: "What is the trading name of the company?",
                              input: tradingName,
                              ph: "What is the trading name of the company?",
                              kbType: TextInputType.name,
                              len: 100,
                              isBold: true,
                            ),
                            DatePickerView(
                              cap: "When was the company incorporated?",
                              dt: (dtWhenIncorp.value == '')
                                  ? 'Select Date'
                                  : dtWhenIncorp.value,
                              txtColor: Colors.black,
                              fontWeight: FontWeight.bold,
                              initialDate: now,
                              firstDate: old,
                              lastDate: now,
                              padding: 5,
                              radius: 5,
                              callback: (value) {
                                if (mounted) {
                                  dtWhenIncorp.value = DateFormat('dd-MM-yyyy')
                                      .format(value)
                                      .toString();
                                }
                              },
                            ),
                            SizedBox(height: 10),
                          ],
                        )
                      : SizedBox(),
                ],
              )
            : SizedBox(),
        SizedBox(height: 10),
        GPlacesView(
            title: "Address",
            titleColor: Colors.black,
            isBold: true,
            txtSize: MyTheme.txtSize - .2,
            address: addr.value,
            callback: (String address, String postCode2, Location loc) {
              addr.value = address;
              postCode.value = postCode2;
              setState(() {});
            }),
        SizedBox(height: 10),
        InputW2N(
          title: "House or flat number or house name",
          input: houseNo,
          ph: "House or flat number or house name",
          kbType: TextInputType.streetAddress,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        UIHelper().drawRadioTitle(
            context: context,
            title: "How are you employed?",
            list: listHowEmployedRB,
            index: howEmployedRBIndex,
            callback: (index) {
              setState(() => howEmployedRBIndex = index);
            }),
        SizedBox(height: 10),
        InputW2N(
          title: "What's your job title?",
          input: jobTitle,
          ph: "What's your job title?",
          kbType: TextInputType.text,
          len: 100,
          isBold: true,
        ),
        SizedBox(height: 5),
        DatePickerView(
          cap: "What was your start date?",
          dt: (dtStartJob.value == '') ? 'Select Date' : dtStartJob.value,
          txtColor: Colors.black,
          fontWeight: FontWeight.bold,
          initialDate: now,
          firstDate: old,
          lastDate: now,
          padding: 5,
          radius: 5,
          callback: (value) {
            if (mounted) {
              dtStartJob.value =
                  DateFormat('dd-MMM-yyyy').format(value).toString();
            }
          },
        ),
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are you currently working at this role?",
            list: W2NLocalData.listYesNoRB,
            index: curWorkingRBIndex,
            callback: (index) {
              setState(() => curWorkingRBIndex = index);
            }),
        curWorkingRBIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DatePickerView(
                  cap: "What was your end date?",
                  dt: (dtEndJob.value == '') ? 'Select Date' : dtEndJob.value,
                  txtColor: Colors.black,
                  fontWeight: FontWeight.bold,
                  initialDate: now,
                  firstDate: old,
                  lastDate: next,
                  padding: 5,
                  radius: 5,
                  callback: (value) {
                    if (mounted) {
                      dtEndJob.value =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    }
                  },
                ))
            : SizedBox(),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            sign: "%",
            context: context,
            tf: pa,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "What percentage of the company do you own?",
            isBold: true,
            len: 3,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: AppDefine.optMM.value.title,
          h1: "Select accounting period",
          heading: "In what month does the company's accounting period start?",
          ddTitleList: AppDefine.ddMM,
          vPadding: 3,
          callback: (optionItem) {
            AppDefine.optMM.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            sign: "Year",
            context: context,
            tf: retireAge,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "At what age do you plan to retire?",
            isBold: true,
            len: 2,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            context: context,
            tf: annualIncome,
            hintTxt: 0,
            labelColor: Colors.black,
            labelTxt: "What is your annual income?",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            context: context,
            tf: monthlyIncome,
            hintTxt: 0,
            labelColor: Colors.black,
            labelTxt:
                "What is the net monthly income from recent month's payslip?",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        InputW2N(
          title: "Payslip reference number/ Employers ID Number",
          input: payslipRef,
          ph: "Payslip reference number/ Employers ID",
          kbType: TextInputType.text,
          len: 20,
          isBold: true,
        ),
        SizedBox(height: 5),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Do you earn overtime?",
            list: W2NLocalData.listYesNoRB,
            index: earnByOTRBIndex,
            callback: (index) {
              setState(() => earnByOTRBIndex = index);
            }),
        earnByOTRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawInputCurrencyBox(
                        context: context,
                        tf: otYear,
                        hintTxt: 0,
                        labelColor: Colors.black,
                        labelTxt: "How much a year? (approximately)",
                        isBold: true,
                        len: 10,
                        focusNode: null,
                        focusNodeNext: null)
                  ],
                ))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Do you earn bonuses?",
            list: W2NLocalData.listYesNoRB,
            index: earnByBonusRBIndex,
            callback: (index) {
              setState(() => earnByBonusRBIndex = index);
            }),
        earnByBonusRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawInputCurrencyBox(
                        context: context,
                        tf: bonusYear,
                        hintTxt: 0,
                        labelColor: Colors.black,
                        labelTxt: "How much a year? (approximately)",
                        isBold: true,
                        len: 10,
                        focusNode: null,
                        focusNodeNext: null)
                  ],
                ))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Do you earn commission?",
            list: W2NLocalData.listYesNoRB,
            index: earnByComRBIndex,
            callback: (index) {
              setState(() => earnByComRBIndex = index);
            }),
        earnByComRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawInputCurrencyBox(
                        context: context,
                        tf: comYear,
                        hintTxt: 0,
                        labelColor: Colors.black,
                        labelTxt: "How much a year? (approximately)",
                        isBold: true,
                        len: 10,
                        focusNode: null,
                        focusNodeNext: null)
                  ],
                ))
            : SizedBox(),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  drawSelfEmployedView() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        InputW2N(
          title: "What's Your Business Name?",
          input: empName,
          ph: "What's Your Business Name?",
          kbType: TextInputType.name,
          len: 100,
          isBold: true,
        ),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "What is your self-employed status?",
            list: listSelfEmpStatusRB,
            index: selfEmpStatusRBIndex,
            callback: (index) {
              setState(() => selfEmpStatusRBIndex = index);
            }),
        selfEmpStatusRBIndex == 0 || selfEmpStatusRBIndex == 2
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 15),
                  DropDownListDialog(
                    context: context,
                    title: optEmpNature.value.title,
                    h1: "Select nature of your business",
                    heading: "What is the nature of your business?",
                    ddTitleList: ddEmpNature,
                    vPadding: 3,
                    callback: (optionItem) {
                      optEmpNature.value = optionItem;
                    },
                  ),
                  SizedBox(height: 10),
                  InputW2N(
                    title: "What is the trading name of the company?",
                    input: tradingName,
                    ph: "What is the trading name of the company?",
                    kbType: TextInputType.name,
                    len: 100,
                    isBold: true,
                  ),
                  SizedBox(height: 5),
                  InputW2N(
                    title: "What's your job title?",
                    input: jobTitle,
                    ph: "What's your job title?",
                    kbType: TextInputType.text,
                    len: 100,
                    isBold: true,
                  ),
                ],
              )
            : SizedBox(),
        selfEmpStatusRBIndex == 3
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: InputW2N(
                  title: "Others self-employed status",
                  input: otherSelfEmpStatus,
                  ph: "Others self-employed status",
                  kbType: TextInputType.name,
                  len: 50,
                  isBold: true,
                ))
            : SizedBox(),
        SizedBox(height: 10),
        GPlacesView(
            title: "Business Address",
            titleColor: Colors.black,
            isBold: true,
            txtSize: MyTheme.txtSize - .2,
            address: addr.value,
            callback: (String address, String postCode, Location loc) {
              addr.value = address;
              setState(() {});
            }),
        SizedBox(height: 10),
        DatePickerView(
          cap: "What was your start date?",
          dt: (dtStartJob.value == '') ? 'Select Date' : dtStartJob.value,
          txtColor: Colors.black,
          fontWeight: FontWeight.bold,
          initialDate: now,
          firstDate: old,
          lastDate: now,
          padding: 5,
          radius: 5,
          callback: (value) {
            if (mounted) {
              dtStartJob.value =
                  DateFormat('dd-MMM-yyyy').format(value).toString();
            }
          },
        ),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are you currently working at this role?",
            list: W2NLocalData.listYesNoRB,
            index: curWorkingRBIndex,
            callback: (index) {
              setState(() => curWorkingRBIndex = index);
            }),
        curWorkingRBIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DatePickerView(
                  cap: "What was your end date?",
                  dt: (dtEndJob.value == '') ? 'Select Date' : dtEndJob.value,
                  txtColor: Colors.black,
                  fontWeight: FontWeight.bold,
                  initialDate: now,
                  firstDate: old,
                  lastDate: next,
                  padding: 5,
                  radius: 5,
                  callback: (value) {
                    if (mounted) {
                      dtEndJob.value =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    }
                  },
                ))
            : SizedBox(),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            sign: "Year",
            context: context,
            tf: retireAge,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "At what age do you plan to retire?",
            isBold: true,
            len: 2,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        _draw3IncomeYearsView(
            title1: "Income recent year",
            tf: incomeRecentY,
            title2: "Year ending",
            dt: dtRecentY.value,
            callback: (dt) {
              dtRecentY.value = dt;
            }),
        SizedBox(height: 10),
        _draw3IncomeYearsView(
            title1: "Income previous year",
            tf: incomePreviousY,
            title2: "Year ending",
            dt: dtPreviousY.value,
            callback: (dt) {
              dtPreviousY.value = dt;
            }),
        SizedBox(height: 10),
        _draw3IncomeYearsView(
            title1: "Net profit year before that",
            tf: netProfitY,
            title2: "Year ending",
            dt: dtNetProfitY.value,
            callback: (dt) {
              dtNetProfitY.value = dt;
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are accounts available?",
            list: W2NLocalData.listYesNoRB,
            index: areAccAvailableRBIndex,
            callback: (index) {
              setState(() => areAccAvailableRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are SA302's available?",
            list: W2NLocalData.listYesNoRB,
            index: areSA302AvailableRBIndex,
            callback: (index) {
              setState(() => areSA302AvailableRBIndex = index);
            }),
        SizedBox(height: 15),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  drawContractorView() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        InputW2N(
          title:
              "What is the name of the company with whom you have the contract with",
          input: empName,
          ph: "What is the name of the company with whom you have the contract with",
          kbType: TextInputType.name,
          len: 100,
          isBold: true,
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "Email address",
          input: email,
          ph: "Email address",
          kbType: TextInputType.emailAddress,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "Phone number",
          input: mobile,
          ph: "Phone number",
          kbType: TextInputType.phone,
          len: 20,
          isBold: true,
        ),
        SizedBox(height: 10),
        GPlacesView(
            title: "Address",
            titleColor: Colors.black,
            isBold: true,
            txtSize: MyTheme.txtSize - .2,
            address: addr.value,
            callback: (String address, String postCode, Location loc) {
              addr.value = address;
              setState(() {});
            }),
        SizedBox(height: 10),
        InputW2N(
          title: "House or flat number or house name",
          input: houseNo,
          ph: "House or flat number or house name",
          kbType: TextInputType.streetAddress,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        DatePickerView(
          cap: "When did the contract start?",
          dt: (dtStartJob.value == '') ? 'Select Date' : dtStartJob.value,
          txtColor: Colors.black,
          fontWeight: FontWeight.bold,
          initialDate: now,
          firstDate: old,
          lastDate: now,
          padding: 5,
          radius: 5,
          callback: (value) {
            if (mounted) {
              dtStartJob.value =
                  DateFormat('dd-MMM-yyyy').format(value).toString();
            }
          },
        ),
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are you currently working at this role?",
            list: W2NLocalData.listYesNoRB,
            index: curWorkingRBIndex,
            callback: (index) {
              setState(() => curWorkingRBIndex = index);
            }),
        curWorkingRBIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DatePickerView(
                  cap: "What was the contract end date?",
                  dt: (dtEndJob.value == '') ? 'Select Date' : dtEndJob.value,
                  txtColor: Colors.black,
                  fontWeight: FontWeight.bold,
                  initialDate: now,
                  firstDate: old,
                  lastDate: next,
                  padding: 5,
                  radius: 5,
                  callback: (value) {
                    if (mounted) {
                      dtEndJob.value =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    }
                  },
                ))
            : SizedBox(),
        SizedBox(height: 10),
        InputW2N(
          title: "How long has the contract left to run",
          input: contractLeft,
          ph: "How long has the contract left to run",
          kbType: TextInputType.text,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the contract renewable?",
            list: W2NLocalData.listYesNoRB,
            index: contractRenewableRBIndex,
            callback: (index) {
              setState(() => contractRenewableRBIndex = index);
            }),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            context: context,
            tf: dailyPayRate,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "What is your daily pay rate?",
            isBold: true,
            len: 3,
            focusNode: null,
            focusNodeNext: null,
            onChange: (v) {
              calculateDailyPayRate();
            }),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            context: context,
            tf: annualIncome,
            hintTxt: 0,
            labelColor: Colors.black,
            labelTxt: "Your annual Income",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null,
            onChange: (v) {
              calculateAnnaulIncome();
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Have you submitted your self assessment tax returns?",
            list: W2NLocalData.listYesNoRB,
            index: haveSubSelfAssessmentTaxReturnRBIndex,
            callback: (index) {
              setState(() => haveSubSelfAssessmentTaxReturnRBIndex = index);
            }),
        haveSubSelfAssessmentTaxReturnRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _draw3IncomeYearsView(
                        title1: "Income recent year",
                        tf: incomeRecentY,
                        title2: "Year ending",
                        dt: dtRecentY.value,
                        callback: (dt) {
                          dtRecentY.value = dt;
                        }),
                    SizedBox(height: 10),
                    _draw3IncomeYearsView(
                        title1: "Income previous year",
                        tf: incomePreviousY,
                        title2: "Year ending",
                        dt: dtPreviousY.value,
                        callback: (dt) {
                          dtPreviousY.value = dt;
                        }),
                    SizedBox(height: 10),
                    _draw3IncomeYearsView(
                        title1: "Net profit year before that",
                        tf: netProfitY,
                        title2: "Year ending",
                        dt: dtNetProfitY.value,
                        callback: (dt) {
                          dtNetProfitY.value = dt;
                        }),
                  ],
                ))
            : SizedBox(),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  drawRetiredView() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(height: 15),
      drawInputCurrencyBox(
          context: context,
          tf: pensionAnnually,
          hintTxt: 0,
          labelColor: Colors.black,
          labelTxt:
              "What is the amount you receive from your state pension annually?",
          isBold: true,
          len: 10,
          focusNode: null,
          focusNodeNext: null),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "Do you receive any other personal/ occupational pension?",
          list: W2NLocalData.listYesNoRB,
          index: otherPensionRBIndex,
          callback: (index) {
            setState(() => otherPensionRBIndex = index);
          }),
      otherPensionRBIndex == 0
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawInputCurrencyBox(
                      context: context,
                      tf: otherPension,
                      hintTxt: 0,
                      labelColor: Colors.black,
                      labelTxt: "Other pension",
                      isBold: true,
                      len: 10,
                      focusNode: null,
                      focusNodeNext: null)
                ],
              ))
          : SizedBox(),
      SizedBox(height: 10),
      UIHelper().drawRadioTitle(
          context: context,
          title: "Do you receive any other benefits?",
          list: W2NLocalData.listYesNoRB,
          index: otherBenefitsRBIndex,
          callback: (index) {
            setState(() => otherBenefitsRBIndex = index);
          }),
      otherBenefitsRBIndex == 0
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                drawTextArea(title: "Description of benefit", tf: descBenefits),
                SizedBox(height: 10),
                drawInputCurrencyBox(
                    context: context,
                    tf: benefitAmounts,
                    hintTxt: 0,
                    labelColor: Colors.black,
                    labelTxt: "Amount",
                    isBold: true,
                    len: 10,
                    focusNode: null,
                    focusNodeNext: null)
              ],
            )
          : SizedBox(),
      SizedBox(height: 15),
      drawTextArea(title: "Notes", tf: notes),
    ]);
  }

  _draw3IncomeYearsView(
      {String title1,
      TextEditingController tf,
      String title2,
      String dt,
      Function callback}) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                drawInputCurrencyBox(
                    context: context,
                    tf: tf,
                    hintTxt: null,
                    labelColor: Colors.black,
                    labelTxt: title1,
                    isBold: true,
                    len: 10,
                    focusNode: null,
                    focusNodeNext: null),
              ],
            ),
          ),
          SizedBox(width: 10),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Txt(
                    txt: title2,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: true),
                SizedBox(height: 5),
                GestureDetector(
                  onTap: () {
                    var mm = DateTime.now().month;
                    var yy = DateTime.now().year;
                    try {
                      final dtArr = dt.split("-");
                      mm = int.parse(dtArr[0]);
                      yy = int.parse(dtArr[1]);
                    } catch (e) {}
                    showMonthPicker(
                      context: context,
                      firstDate: DateTime(
                          DateTime.now().year - 50, DateTime.now().month),
                      lastDate: DateTime(
                          DateTime.now().year + 1, DateTime.now().month),
                      initialDate: DateTime(yy, mm),
                      locale: Locale("en"),
                    ).then((date) {
                      if (date != null) {
                        callback(DateFormat('MM-yyyy').format(date).toString());
                      }
                    });
                  },
                  child: IcoTxtIco(
                    leftIcon: Icons.calendar_today,
                    txt: dt,
                    txtSize: MyTheme.txtSize - .2,
                    txtColor: Colors.black,
                    rightIcon: Icons.keyboard_arrow_down,
                    txtAlign: TextAlign.left,
                    height: 6,
                    radius: 5,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  onDelEmpAPI(MortgageUserOccupation model) {
    // TODO: implement onDelEmpAPI
    throw UnimplementedError();
  }

  @override
  onDelIncomeAPI(MortgageUserInCome mode) {
    // TODO: implement onDelIncomeAPI
    throw UnimplementedError();
  }

  @override
  onEmpClicked(MortgageUserOccupation model) {
    // TODO: implement onEmpClicked
    throw UnimplementedError();
  }

  @override
  onIncomeClicked(MortgageUserInCome model) {
    // TODO: implement onIncomeClicked
    throw UnimplementedError();
  }
}
