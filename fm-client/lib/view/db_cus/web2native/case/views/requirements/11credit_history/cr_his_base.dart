import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/11credit_history/req_cr_his_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step11_cr_his_ctrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class CrHisBase<T extends StatefulWidget> extends CaseBase<T> {
  onOpenAddEditDialog(MortgageUserCreditHistoryItems model2);
  onDelItem(MortgageUserCreditHistoryItems model);

  final listLoanRefusedRB = {0: "Yes", 1: "No"};
  int loanRefusedRBIndex = 1;

  final listHaveYouEverItemsRB = {0: "Yes", 1: "No"};
  int haveYouEverItemsRBIndex = 1;

  final listBankruptRB = {0: "Yes", 1: "No"};
  int bankRuptRBIndex = 1;

  final listRepayPreviousMortRB = {0: "Yes", 1: "No"};
  int repayPreviousMortRBIndex = 1;

  final listRepayPreviousLoanRB = {0: "Yes", 1: "No"};
  int repayPreviousLoanRBIndex = 1;

  final loanRefDetails = TextEditingController();
  final bankRuptDetails = TextEditingController();
  final repayPreviousMortDetails = TextEditingController();
  final repayPreviousLoanDetails = TextEditingController();

  drawForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Have you ever had a mortgage or loan application refused?",
            list: listLoanRefusedRB,
            index: loanRefusedRBIndex,
            callback: (index) {
              setState(() => loanRefusedRBIndex = index);
            }),
        loanRefusedRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: drawTextArea(
                    title: "Details",
                    subTitle:
                        "please provide details. Approximately when you have applied, to whom you have applied and the circumstances",
                    tf: loanRefDetails))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Have you ever had a judgement for (a gurantor) debt or loan default or any other default registered against you?",
            list: listHaveYouEverItemsRB,
            index: haveYouEverItemsRBIndex,
            callback: (index) {
              setState(() => haveYouEverItemsRBIndex = index);
            }),
        haveYouEverItemsRBIndex == 0 ? _drawItems() : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Have you ever been declared bankrupt?",
            list: listBankruptRB,
            index: bankRuptRBIndex,
            callback: (index) {
              setState(() => bankRuptRBIndex = index);
            }),
        bankRuptRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: drawTextArea(title: "Details", tf: bankRuptDetails))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Have you ever failed to keep up repayments under any previous or current mortgage?",
            list: listRepayPreviousMortRB,
            index: repayPreviousMortRBIndex,
            callback: (index) {
              setState(() => repayPreviousMortRBIndex = index);
            }),
        repayPreviousMortRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: drawTextArea(
                    title: "Details",
                    subTitle:
                        "Please provide details of the occurrence. Please specify when was it missed, how many times was it missed and is it still in arrears",
                    tf: repayPreviousMortDetails))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Have you ever failed to keep up repayments under any previous or current rental or loan agreement?",
            list: listRepayPreviousLoanRB,
            index: repayPreviousLoanRBIndex,
            callback: (index) {
              setState(() => repayPreviousLoanRBIndex = index);
            }),
        repayPreviousLoanRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: drawTextArea(
                    title: "Details",
                    subTitle:
                        "Please provide details of the occurrence. Please specify when was it missed, how many times was it missed and is it still in arrears",
                    tf: repayPreviousLoanDetails))
            : SizedBox(),
      ],
    );
  }

  _drawItems() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        drawDottedBox("Add", () {
          onOpenAddEditDialog(null);
        }),
        drawCrHisItems(context, true, crHisCtrl.listCrHisItemsCtrl,
            (model, isEdit) {
          if (isEdit) {
            onOpenAddEditDialog(model);
          } else {
            onDelItem(model);
          }
        }),
      ],
    );
  }
}
