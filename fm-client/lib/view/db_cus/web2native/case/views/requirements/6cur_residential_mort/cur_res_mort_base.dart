import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/gplaces/GPlacesView.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step6_cur_res_mort_ctrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../../widgets/radio/draw_radio_group.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_webservice/geolocation.dart';

abstract class CurResMortBase<T extends StatefulWidget> extends CaseBase<T> {
  final listOccupantTypeRB = {
    0: "Homeowner with mortgage",
    1: "Homeowner without mortgage (unencumbered)",
    2: "Tenant",
    3: "Living with Family",
    4: "Living with Parents",
    5: "Other"
  };
  int occupantTypeRBIndex = 0;

  final listRepayTypeRB = {0: "Interest Only", 1: "Capital Repayment"};
  int repayTypeRBIndex = 0;

  final listFixedRatePeriodRB = {0: "Yes", 1: "No"};
  int fixedRatePeriodRBIndex = 1;

  final currentValProperty = TextEditingController();
  final purchasePrice = TextEditingController();
  final currentMortOutstanding = TextEditingController();
  final currentLender = TextEditingController();
  final termsOutstanding = TextEditingController();
  final monthlyPay = TextEditingController();
  final notes = TextEditingController();

  final name = TextEditingController();
  final mobile = TextEditingController();
  final email = TextEditingController();

  final focusCurrentLender = FocusNode();
  final focusTermsOutstanding = FocusNode();
  final focusName = FocusNode();
  final focusMobile = FocusNode();
  final focusEmail = FocusNode();

  var dtPurchase = "".obs;
  var dtEndFixedPrice = "".obs;
  var addr = "".obs;

  drawForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIHelper().drawRadioTitle(
              context: context,
              title: "What type of occupant are you?",
              list: listOccupantTypeRB,
              index: occupantTypeRBIndex,
              radioType: eRadioType.GRID2,
              callback: (index) {
                setState(() => occupantTypeRBIndex = index);
              }),
          SizedBox(height: 5),
          drawSubView(),
        ],
      ),
    );
  }

  drawSubView() {
    switch (occupantTypeRBIndex) {
      case 0:
        return draw0();
        break;
      case 1:
        return draw1();
        break;
      case 2:
        return draw2();
        break;
      case 3:
        return draw3();
        break;
      case 4:
        return draw4();
        break;
      case 5:
        return draw5();
        break;
      default:
        return SizedBox();
    }
  }

  draw0() {
    final now = DateTime.now();
    final next = DateTime(now.year + 10, now.month, now.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          drawCurrencyBox("Current Value of the Residential Property", null,
              currentValProperty, null, null, (v) {}),
          SizedBox(height: 10),
          DatePickerView(
            cap: "When did you purchase the property?",
            dt: (dtPurchase.value == '') ? 'Select Date' : dtPurchase.value,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900, 1, 1),
            lastDate: DateTime.now(),
            padding: 5,
            radius: 5,
            callback: (value) {
              if (mounted) {
                dtPurchase.value =
                    DateFormat('MM/dd/yyyy').format(value).toString();
              }
            },
          ),
          SizedBox(height: 10),
          drawCurrencyBox("Original Purchase Price", null, purchasePrice, null,
              null, (v) {}),
          SizedBox(height: 10),
          drawCurrencyBox("Mortgage amount outstanding", null,
              currentMortOutstanding, null, null, (v) {}),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Lender Name",
            ph: "Lender Name",
            input: currentLender,
            len: 50,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.text,
            inputAction: TextInputAction.next,
            focusNode: focusCurrentLender,
            focusNodeNext: focusTermsOutstanding,
          ),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Term outstanding",
            ph: "Term outstanding",
            input: termsOutstanding,
            len: 20,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.text,
            inputAction: TextInputAction.next,
            focusNode: focusTermsOutstanding,
          ),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title: "Repayment type",
              list: listRepayTypeRB,
              index: repayTypeRBIndex,
              radioType: eRadioType.GRID2,
              callback: (index) {
                setState(() => repayTypeRBIndex = index);
              }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Current Monthly Payment", null, monthlyPay, null, null, (v) {}),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title: "Is the current mortgage out of fixed rate period?",
              list: listFixedRatePeriodRB,
              index: fixedRatePeriodRBIndex,
              radioType: eRadioType.GRID2,
              callback: (index) {
                setState(() => fixedRatePeriodRBIndex = index);
              }),
          SizedBox(height: 10),
          DatePickerView(
            cap: "End Date of fixed period",
            dt: (dtEndFixedPrice.value == '')
                ? 'Select Date'
                : dtEndFixedPrice.value,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900, 1, 1),
            lastDate: next,
            padding: 5,
            radius: 5,
            callback: (value) {
              if (mounted) {
                dtEndFixedPrice.value =
                    DateFormat('MM/dd/yyyy').format(value).toString();
              }
            },
          ),
          SizedBox(height: 10),
          drawTextArea(title: "Notes", tf: notes),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  draw1() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          drawCurrencyBox("Current Value of the Residential Property", null,
              currentValProperty, null, null, (v) {}),
          SizedBox(height: 10),
          DatePickerView(
            cap: "When did you purchase the property?",
            dt: (dtPurchase.value == '') ? 'Select Date' : dtPurchase.value,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900, 1, 1),
            lastDate: DateTime.now(),
            padding: 5,
            radius: 5,
            callback: (value) {
              if (mounted) {
                dtPurchase.value =
                    DateFormat('MM/dd/yyyy').format(value).toString();
              }
            },
          ),
          SizedBox(height: 10),
          drawCurrencyBox("Original Purchase Price", null, purchasePrice, null,
              null, (v) {}),
          SizedBox(height: 10),
          drawTextArea(title: "Notes", tf: notes),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  draw2() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          drawCurrencyBox("How much is your monthly rental?", null, monthlyPay,
              null, null, (v) {}),
          SizedBox(height: 20),
          Txt(
              txt: "Landlord Details:",
              txtColor: MyTheme.statusBarColor,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Name",
            input: name,
            ph: "",
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusName,
            focusNodeNext: focusEmail,
            len: 50,
            isBold: true,
            txtColor: Colors.black,
          ),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Email",
            input: email,
            ph: "",
            kbType: TextInputType.emailAddress,
            inputAction: TextInputAction.next,
            focusNode: focusEmail,
            focusNodeNext: focusMobile,
            len: 50,
            isBold: true,
            txtColor: Colors.black,
          ),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Contact number",
            input: mobile,
            ph: "",
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusMobile,
            len: 20,
            isBold: true,
            txtColor: Colors.black,
          ),
          SizedBox(height: 10),
          GPlacesView(
              title: "Address",
              titleColor: Colors.black,
              isBold: true,
              txtSize: MyTheme.txtSize - .2,
              address: addr.value,
              callback: (String address, String postCode, Location loc) {
                addr.value = address;
                setState(() {});
              }),
          SizedBox(height: 10),
          drawTextArea(title: "Notes", tf: notes),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  draw3() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          drawTextArea(title: "Notes", tf: notes),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  draw4() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          drawTextArea(title: "Notes", tf: notes),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  draw5() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          drawTextArea(title: "Please Specify", tf: notes),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
