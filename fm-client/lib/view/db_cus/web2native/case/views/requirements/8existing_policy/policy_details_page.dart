import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/MortgageUserExistingPolicyItem.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/PostExistingPolicyAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/8existing_policy/existing_policy_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/gplaces/GPlacesView.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

class PolicyDetailsPage extends StatefulWidget {
  final MortgageUserExistingPolicyItem policyModel;
  const PolicyDetailsPage({Key key, this.policyModel}) : super(key: key);
  @override
  State<PolicyDetailsPage> createState() => _PolicyDetailsState();
}

class _PolicyDetailsState extends ExistingPolicyBase<PolicyDetailsPage> {
  DropListModel ddPolicyType = DropListModel([
    OptionItem(id: "1", title: "Accident, Sickness & Unemployment (ASU)"),
    OptionItem(id: "2", title: "Business Protection"),
    OptionItem(id: "3", title: "CIC"),
    OptionItem(id: "4", title: "DTA"),
    OptionItem(id: "5", title: "DTA + CIC"),
    OptionItem(id: "6", title: "DTA + SIC"),
    OptionItem(id: "7", title: "Family Income Benefit"),
    OptionItem(id: "8", title: "Family Income Benefit + CIC"),
    OptionItem(id: "9", title: "Group Life"),
    OptionItem(id: "10", title: "Health Care"),
    OptionItem(id: "11", title: "Investment"),
    OptionItem(id: "12", title: "LTA"),
    OptionItem(id: "13", title: "LTA + CIC"),
    OptionItem(id: "14", title: "LTA + SIC"),
    OptionItem(id: "15", title: "MPI"),
    OptionItem(id: "16", title: "MPU"),
    OptionItem(id: "17", title: "Old Policy"),
    OptionItem(id: "18", title: "Pension Plan"),
    OptionItem(id: "19", title: "PHI"),
    OptionItem(id: "20", title: "PTA – Decreasing"),
    OptionItem(id: "21", title: "PTA – Level"),
    OptionItem(id: "22", title: "Unemployment"),
    OptionItem(id: "23", title: "Whole of Life")
  ]);
  var optPolicyType =
      OptionItem(id: null, title: "Select Life Policy Type").obs;

  DropListModel ddBldgPolicyType = DropListModel([
    OptionItem(id: "1", title: "Buildings Only"),
    OptionItem(id: "2", title: "Contents Only"),
    OptionItem(id: "3", title: "B & C"),
    OptionItem(id: "4", title: "Commercial"),
  ]);
  var optBldgPolicyType =
      OptionItem(id: null, title: "Select Life Policy Type").obs;

  DropListModel ddProvider = DropListModel([
    OptionItem(id: "1", title: "AA Insurance"),
    OptionItem(id: "1", title: "Ageas"),
    OptionItem(id: "1", title: "Ageas Protect Limited"),
    OptionItem(id: "1", title: "Aegon"),
    OptionItem(id: "1", title: "AIG"),
    OptionItem(id: "1", title: "Assurance Solutions"),
    OptionItem(id: "1", title: "Aviva"),
    OptionItem(id: "1", title: "AXA"),
    OptionItem(id: "1", title: "Blue Drop"),
    OptionItem(id: "1", title: "Bright Grey"),
    OptionItem(id: "1", title: "British Friendly"),
    OptionItem(id: "1", title: "Calibre Home Solutions"),
    OptionItem(id: "1", title: "Canada Life"),
    OptionItem(id: "1", title: "Canopious"),
    OptionItem(id: "1", title: "Centrepoint Insurance Service"),
    OptionItem(id: "1", title: "Churchill"),
    OptionItem(id: "1", title: "Close Brothers Premium Finance"),
    OptionItem(id: "1", title: "Fortis Insurance"),
    OptionItem(id: "1", title: "Fortress"),
    OptionItem(id: "1", title: "GI Secure"),
    OptionItem(id: "1", title: "GRE Insurance"),
    OptionItem(id: "1", title: "Groupama"),
    OptionItem(id: "1", title: "Guardian 1821"),
    OptionItem(id: "1", title: "Halifax"),
    OptionItem(id: "1", title: "Holloway Friendly"),
    OptionItem(id: "1", title: "HSBC"),
    OptionItem(id: "1", title: "Legal & General"),
    OptionItem(id: "1", title: "LV="),
    OptionItem(id: "1", title: "Metlife"),
    OptionItem(id: "1", title: "National Friendly"),
    OptionItem(id: "1", title: "One Family"),
    OptionItem(id: "1", title: "Payment Shield"),
    OptionItem(id: "1", title: "RSA"),
    OptionItem(id: "1", title: "Royal London"),
    OptionItem(id: "1", title: "Santander"),
    OptionItem(id: "1", title: "Scottish Widows"),
    OptionItem(id: "1", title: "Select and Protect"),
    OptionItem(id: "1", title: "Shepherds Friendly"),
    OptionItem(id: "1", title: "The Exeter"),
    OptionItem(id: "1", title: "Towergate Home & Protect"),
    OptionItem(id: "1", title: "Towergate Risk Solutions"),
    OptionItem(id: "1", title: "Uinsure"),
    OptionItem(id: "1", title: "Unum"),
    OptionItem(id: "1", title: "Uris Group"),
    OptionItem(id: "1", title: "Vitality Life"),
    OptionItem(id: "1", title: "Zurich"),
    OptionItem(id: "1", title: "Other"),
  ]);
  var optProvider = OptionItem(id: null, title: "Select Provider").obs;

  DropListModel ddPurpose = DropListModel([
    OptionItem(id: "1", title: "Family"),
    OptionItem(id: "1", title: "Inheritance Tax planning"),
    OptionItem(id: "1", title: "Mortgage Cover"),
  ]);
  var optPurpose = OptionItem(id: null, title: "Select Purpose").obs;

  DropListModel ddFreq = DropListModel([
    OptionItem(id: "1", title: "Monthly"),
    OptionItem(id: "1", title: "Bi-Annual"),
    OptionItem(id: "1", title: "Quarterly"),
    OptionItem(id: "1", title: "Annual"),
    OptionItem(id: "1", title: "Single Premium"),
    OptionItem(id: "1", title: "4 Weekly"),
    OptionItem(id: "1", title: "Weekly"),
  ]);
  var optFreq = OptionItem(id: null, title: "Select Frequency").obs;

  ///
  DropListModel ddIPCoverType = DropListModel([
    OptionItem(id: "1", title: "Accident & Sickness"),
    OptionItem(id: "1", title: "ASU"),
    OptionItem(id: "1", title: "MPPI - Disability & Unemployment"),
    OptionItem(id: "1", title: "MPPI - Disability Only"),
    OptionItem(id: "1", title: "MPPI - Unemployment Only"),
    OptionItem(id: "1", title: "PHI"),
    OptionItem(id: "1", title: "Unemployment"),
  ]);
  var optIPCoverType = OptionItem(id: null, title: "Select IP Cover Types").obs;

  DropListModel ddInitDefPeriod = DropListModel([
    OptionItem(id: "1", title: "13 Weeks"),
    OptionItem(id: "1", title: "26 weeks"),
    OptionItem(id: "1", title: "30 days"),
    OptionItem(id: "1", title: "4 weeks"),
    OptionItem(id: "1", title: "52 weeks"),
    OptionItem(id: "1", title: "60 days"),
    OptionItem(id: "1", title: "No deferred period"),
  ]);
  var optInitDefPeriod =
      OptionItem(id: null, title: "Initial deferred period").obs;
  var optInitDefPeriodAdditional =
      OptionItem(id: null, title: "Additional deferred period").obs;

  //
  DropListModel ddNoClaimBonus = DropListModel([]);
  var optNoClaimBonus = OptionItem(id: null, title: "No claims bonus").obs;

  //
  final listPolicyTypeRB = {
    0: "Life Insurance",
    1: "Income Protection Policies",
    2: "Buildings & Contents Policy",
    3: "Cancelled Protection"
  };
  int policyTypeRBIndex = 0;

  final listTermBasisRB = {0: "Age", 1: "Year"};
  int termBasisRBIndex = 1;

  final listGuaranteedRB = {0: "Yes", 1: "No"};
  int guaranteedRBIndex = 1;

  final listRenewableRB = {0: "Yes", 1: "No"};
  int renewableRBIndex = 1;

  final listPolicyIndexedRB = {0: "Yes", 1: "No"};
  int policyIndexedRBIndex = 1;

  final listWaiverPremiumRB = {0: "Yes", 1: "No"};
  int waiverPremiumRBIndex = 1;

  final listPolicyTrustRB = {0: "Yes", 1: "No"};
  int policyTrustRBIndex = 1;

  final listPolicyUsedWithMortRB = {0: "Yes", 1: "No"};
  int policyUsedWithMortRBIndex = 1;

  final listPolicyCancelledRB = {0: "Yes", 1: "No"};
  int policyCancelledRBIndex = 1;

  //
  final listAccDamageRB = {0: "Yes", 1: "No"};
  int accDamageRBIndex = 1;

  final listArePersPossRB = {0: "Yes", 1: "No"};
  int arePersPossRBIndex = 1;

  final listCycleCoverRB = {0: "Yes", 1: "No"};
  int cycleCoverRBIndex = 1;

  final listAreSpeciItemIncludedRB = {0: "Yes", 1: "No"};
  int areSpeciItemIncludedRBIndex = 1;

  //
  final listAnyPolicyLapsedRB = {0: "Yes", 1: "No"};
  int anyPolicyLapsedRBIndex = 1;

  //
  final provOther = TextEditingController();
  final policyNo = TextEditingController();
  final premium = TextEditingController();
  final coverAmount = TextEditingController();
  final sumInsured = TextEditingController();
  final benefitType = TextEditingController();
  final needCover = TextEditingController();
  final otherBenefits = TextEditingController();
  final notes = TextEditingController();
  final details = TextEditingController();

  ///
  final initialBenefit = TextEditingController();
  final additionalBenefit = TextEditingController();

  //
  var dtStartPolicy = "".obs;
  var dtEndPolicy = "".obs;

  //
  var addrProperty = "".obs;

  onExistingPolicyAPI() async {
    try {
      var id;
      var _premium = 0;
      var _coverAmount = 0;
      final model2 = widget.policyModel;
      try {
        id = model2.id;
      } catch (e) {}
      try {
        _premium = int.parse(premium.text);
      } catch (e) {}
      try {
        _coverAmount = int.parse(coverAmount.text);
      } catch (e) {}

      final param = {
        "UserId": userData.userModel.id,
        "UserCompanyId": userData.userModel.userCompanyInfo.id,
        "Status": 101,
        "MortgageCaseInfoId": 0,
        "CreationDate": DateTime.now().toString(),
        "UpdatedDate": DateTime.now().toString(),
        "VersionNumber": 0,
        "TaskId": caseModelCtrl.locModel.value.id,
        "PolicyType": listPolicyTypeRB[policyTypeRBIndex],
        "LifePolicyType": policyTypeRBIndex == 0
            ? optPolicyType.value.title
            : policyTypeRBIndex == 1
                ? optIPCoverType.value.title
                : policyTypeRBIndex == 2
                    ? optBldgPolicyType.value.title
                    : "",
        "Provider": optProvider.value.title,
        "ProviderOtherName": provOther.text.trim(),
        "Purpose": optPurpose.value.title,
        "PolicyNumber": policyNo.text.trim(),
        "Premium": _premium,
        "Frequency": optFreq.value.title,
        "CoverAmount": _coverAmount,
        "CICOrSICSumInsured": sumInsured.text.trim(),
        "CICOrSICBenefitType": benefitType.text.trim(),
        "PolicyStartDate": DateFun.getDate(
            dtStartPolicy.value == ""
                ? AppConfig.date1970
                : dtStartPolicy.value ?? AppConfig.date1970,
            "dd-MMM-yyyy"),
        "TermBasis": listTermBasisRB[termBasisRBIndex],
        "Guaranteed": listGuaranteedRB[guaranteedRBIndex],
        "Renewable": listRenewableRB[renewableRBIndex],
        "IsThePolicyIndexed": listPolicyIndexedRB[policyIndexedRBIndex],
        "DoesThePolicyIncludeWaiverOfPremium":
            listWaiverPremiumRB[waiverPremiumRBIndex],
        "IsThePolicyOnTrust": listPolicyTrustRB[policyTrustRBIndex],
        "IsThePolicyUsedWithAMortgage":
            listPolicyUsedWithMortRB[policyUsedWithMortRBIndex],
        "PolicyToBeCancelledOrReplaced": null,
        "NeedForCover": needCover.text.trim(),
        "OtherApplicableBenefits": otherBenefits.text.trim(),
        "Notes": notes.text.trim(),
        "Initialbenefit": initialBenefit.text.trim(),
        "InitialDeferredPeriod": optInitDefPeriod.value.title,
        "AdditionalBenefit": additionalBenefit.text.trim(),
        "AdditionalDeferredPeriod": optInitDefPeriodAdditional.value.title,
        "PropertyAddress": addrProperty.value,
        "NoClaimsBonus": optNoClaimBonus.value.title,
        "AccidentalDamage": listAccDamageRB[accDamageRBIndex],
        "ArePersonalPossessionsIncluded": listArePersPossRB[arePersPossRBIndex],
        "IsCycleCoverIncluded": listCycleCoverRB[cycleCoverRBIndex],
        "AreSpecificItemsIncluded":
            listAreSpeciItemIncludedRB[areSpeciItemIncludedRBIndex],
        "IsThePolicyToBeCancelledOrReplaced":
            listPolicyCancelledRB[policyCancelledRBIndex],
        "PolicyExpiryDate": DateFun.getDate(
            dtEndPolicy.value == ""
                ? DateTime.now().toString()
                : dtEndPolicy.value,
            "dd-MMM-yyyy"),
        "AnyPoliciesLapsedOrCancelledInTheLast12Months":
            listAnyPolicyLapsedRB[anyPolicyLapsedRBIndex],
        "Details": details.text.trim(),
        "Id": id ?? 0
      };
      await APIViewModel().req<PostExistingPolicyAPIModel>(
          context: context,
          url: model2 == null
              ? SrvW2NCase.POST_EXISTING_POLICY_URL
              : SrvW2NCase.PUT_EXISTING_POLICY_URL,
          reqType: model2 == null ? ReqType.Post : ReqType.Put,
          param: param,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                if (model2 != null) {
                  existingPolicyCtrl.existingPolicyCtrl.remove(model2);
                }
                existingPolicyCtrl.existingPolicyCtrl
                    .add(model.responseData.mortgageUserExistingPolicyItem);
                existingPolicyCtrl.update();
                if (model2 != null) {
                  showToast(
                      context: context,
                      msg: "Existing policy updated successfully");
                } else {
                  showToast(
                      context: context,
                      msg: "Existing policy added successfully");
                }
                Future.delayed(Duration(seconds: 3), () {
                  Get.back();
                });
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      final List<OptionItem> list = [];
      for (var i = 1; i < 21; i++)
        list.add(OptionItem(id: i.toString(), title: i.toString()));
      ddNoClaimBonus = DropListModel(list);
    } catch (e) {}
    try {
      if (widget.policyModel != null) {
        final model = widget.policyModel;
        try {
          policyTypeRBIndex =
              Common.getMapKeyByVal(listPolicyTypeRB, model.policyType);
        } catch (e) {}
        try {
          optPolicyType = OptionItem(id: "1", title: model.lifePolicyType).obs;
        } catch (e) {}
        try {
          optProvider = OptionItem(id: "1", title: model.provider).obs;
        } catch (e) {}
        try {
          provOther.text = model.providerOtherName ?? '';
        } catch (e) {}
        try {
          optPurpose = OptionItem(id: "1", title: model.purpose).obs;
        } catch (e) {}
        try {
          policyNo.text = model.policyNumber;
        } catch (e) {}
        try {
          premium.text = model.premium.toStringAsFixed(0);
        } catch (e) {}
        try {
          optFreq = OptionItem(id: "1", title: model.frequency).obs;
        } catch (e) {}
        try {
          coverAmount.text = model.coverAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          sumInsured.text = model.cICOrSICSumInsured;
        } catch (e) {}
        try {
          benefitType.text = model.cICOrSICBenefitType;
        } catch (e) {}
        try {
          dtStartPolicy.value =
              DateFun.getDate(model.policyStartDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          dtEndPolicy.value =
              DateFun.getDate(model.policyExpiryDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          termBasisRBIndex =
              Common.getMapKeyByVal(listTermBasisRB, model.termBasis);
        } catch (e) {}
        try {
          guaranteedRBIndex =
              Common.getMapKeyByVal(listGuaranteedRB, model.guaranteed);
        } catch (e) {}
        try {
          renewableRBIndex =
              Common.getMapKeyByVal(listRenewableRB, model.renewable);
        } catch (e) {}
        try {
          policyIndexedRBIndex = Common.getMapKeyByVal(
              listPolicyIndexedRB, model.isThePolicyIndexed);
        } catch (e) {}
        try {
          waiverPremiumRBIndex = Common.getMapKeyByVal(
              listWaiverPremiumRB, model.doesThePolicyIncludeWaiverOfPremium);
        } catch (e) {}
        try {
          policyTrustRBIndex = Common.getMapKeyByVal(
              listPolicyTrustRB, model.isThePolicyOnTrust);
        } catch (e) {}
        try {
          policyUsedWithMortRBIndex = Common.getMapKeyByVal(
              listPolicyUsedWithMortRB, model.isThePolicyUsedWithAMortgage);
        } catch (e) {}
        try {
          policyCancelledRBIndex = Common.getMapKeyByVal(
              listPolicyCancelledRB, model.isThePolicyToBeCancelledOrReplaced);
        } catch (e) {}
        try {
          needCover.text = model.needForCover;
        } catch (e) {}
        try {
          otherBenefits.text = model.otherApplicableBenefits;
        } catch (e) {}
        try {
          notes.text = model.notes;
        } catch (e) {}
        //
        try {
          optIPCoverType = OptionItem(id: "1", title: model.lifePolicyType).obs;
        } catch (e) {}
        try {
          initialBenefit.text = model.initialbenefit;
        } catch (e) {}
        try {
          optInitDefPeriod =
              OptionItem(id: "1", title: model.initialDeferredPeriod).obs;
        } catch (e) {}
        try {
          additionalBenefit.text = model.additionalBenefit;
        } catch (e) {}
        try {
          optInitDefPeriodAdditional =
              OptionItem(id: "1", title: model.additionalDeferredPeriod).obs;
        } catch (e) {}
        //
        optBldgPolicyType =
            OptionItem(id: "1", title: model.lifePolicyType).obs;
        addrProperty.value = model.propertyAddress;
        try {
          optNoClaimBonus = OptionItem(id: "1", title: model.noClaimsBonus).obs;
        } catch (e) {}
        try {
          accDamageRBIndex =
              Common.getMapKeyByVal(listAccDamageRB, model.accidentalDamage);
        } catch (e) {}
        try {
          arePersPossRBIndex = Common.getMapKeyByVal(
              listArePersPossRB, model.arePersonalPossessionsIncluded);
        } catch (e) {}
        try {
          cycleCoverRBIndex = Common.getMapKeyByVal(
              listCycleCoverRB, model.isCycleCoverIncluded);
        } catch (e) {}
        try {
          areSpeciItemIncludedRBIndex = Common.getMapKeyByVal(
              listAreSpeciItemIncludedRB, model.areSpecificItemsIncluded);
        } catch (e) {}
        //
        try {
          anyPolicyLapsedRBIndex = Common.getMapKeyByVal(listAnyPolicyLapsedRB,
              model.anyPoliciesLapsedOrCancelledInTheLast12Months);
        } catch (e) {}
        try {
          details.text = model.details;
        } catch (e) {}
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: widget.policyModel == null
                        ? "Add Existing Policies"
                        : "Update Existing Policies"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: MMBtn(
                      txt: widget.policyModel == null ? "Save" : "Update",
                      height: getHP(context, 7),
                      width: getW(context),
                      radius: 5,
                      callback: () async {
                        //  common validation  ***************************
                        if (policyTypeRBIndex == 0 &&
                            optPolicyType.value.id == null) {
                          showToast(
                              context: context,
                              msg: "Please choose life policy type");
                          return;
                        }
                        if (policyTypeRBIndex == 1 &&
                            optIPCoverType.value.id == null) {
                          showToast(
                              context: context,
                              msg: "Please choose ip cover type");
                          return;
                        }
                        if (policyTypeRBIndex == 2 &&
                            optBldgPolicyType.value.id == null) {
                          showToast(
                              context: context,
                              msg: "Please choose policy type");
                          return;
                        }
                        if (policyTypeRBIndex == 2 &&
                            addrProperty.value == "") {
                          showToast(
                              context: context,
                              msg: "Please select property address");
                          return;
                        }
                        if (policyTypeRBIndex == 3 &&
                            anyPolicyLapsedRBIndex == 0 &&
                            details.text.isEmpty) {
                          showToast(
                              context: context,
                              msg:
                                  "Please enter any policies lapsed or cancelled in the last 12 months in description");
                          return;
                        }
                        if (policyTypeRBIndex != 3 &&
                            optProvider.value.id == null) {
                          showToast(
                              context: context, msg: "Please choose provider");
                          return;
                        }
                        if (policyTypeRBIndex != 3 && policyNo.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter policy number");
                          return;
                        }
                        if (policyTypeRBIndex != 3 &&
                            optFreq.value.id == null) {
                          showToast(
                              context: context, msg: "Please choose frequency");
                          return;
                        }

                        //  not common validation ***************************
                        if ((policyTypeRBIndex == 0 ||
                                policyTypeRBIndex == 2) &&
                            coverAmount.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter cover amount");
                          return;
                        }

                        if (policyTypeRBIndex != 3 &&
                            optProvider.value.title == "Other" &&
                            provOther.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter other provider name");
                          return;
                        }

                        //  for Life Insurance
                        if (policyTypeRBIndex == 0 &&
                            optPurpose.value.id == null) {
                          showToast(
                              context: context, msg: "Please choose purpose");
                          return;
                        }
                        if ((policyTypeRBIndex == 0 ||
                                policyTypeRBIndex == 1) &&
                            premium.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter premium amount");
                          return;
                        }

                        onExistingPolicyAPI();
                      }),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    Widget wid = SizedBox();
    switch (policyTypeRBIndex) {
      case 0:
        wid = drawLifeInsurance();
        break;
      case 1:
        wid = drawIncomeProtectionPolicy();
        break;
      case 2:
        wid = drawBldgContentPolicy();
        break;
      case 3:
        wid = drawCancelledProtection();
        break;
      default:
    }
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIHelper().drawRadioTitle(
                  context: context,
                  title: "What Policy is this?",
                  list: listPolicyTypeRB,
                  index: policyTypeRBIndex,
                  callback: (index) {
                    setState(() => policyTypeRBIndex = index);
                  }),
              wid,
            ],
          ),
        ),
      ),
    );
  }

  drawLifeInsurance() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        DropDownListDialog(
          context: context,
          title: optPolicyType.value.title,
          h1: "Select life policy type",
          heading: "Life Policy Type",
          ddTitleList: ddPolicyType,
          vPadding: 3,
          callback: (optionItem) {
            optPolicyType.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optProvider.value.title,
          h1: "Select provider",
          heading: "Provider",
          ddTitleList: ddProvider,
          vPadding: 3,
          callback: (optionItem) {
            optProvider.value = optionItem;
          },
        ),
        optProvider.value.title == "Other"
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: InputW2N(
                  title: "Other Provider Name",
                  input: provOther,
                  ph: "Other Provider Name",
                  kbType: TextInputType.text,
                  len: 50,
                  isBold: true,
                ))
            : SizedBox(),
        SizedBox(height: 5),
        DropDownListDialog(
          context: context,
          title: optPurpose.value.title,
          h1: "Select purpose",
          heading: "Purpose",
          ddTitleList: ddPurpose,
          vPadding: 3,
          callback: (optionItem) {
            optPurpose.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "Policy Number",
          input: policyNo,
          ph: "Policy Number",
          kbType: TextInputType.text,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        drawInputCurrencyBox(
            context: context,
            tf: premium,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "Premium",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optFreq.value.title,
          h1: "Select frequency",
          heading: "Frequency",
          ddTitleList: ddFreq,
          vPadding: 3,
          callback: (optionItem) {
            optFreq.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            context: context,
            tf: coverAmount,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "Cover Amount",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 15),
        InputW2N(
          title: "CIC/SIC Sum Insured",
          input: sumInsured,
          ph: "CIC/SIC Sum Insured",
          kbType: TextInputType.text,
          len: 100,
          isBold: true,
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "CIC/SIC Benefit Type",
          input: benefitType,
          ph: "CIC/SIC Benefit Type",
          kbType: TextInputType.text,
          len: 100,
          isBold: true,
        ),
        SizedBox(height: 5),
        DatePickerView(
          cap: "Policy Start Date",
          dt: (dtStartPolicy.value == '') ? 'Select Date' : dtStartPolicy.value,
          txtColor: Colors.black,
          fontWeight: FontWeight.bold,
          initialDate: now,
          firstDate: old,
          lastDate: now,
          padding: 5,
          radius: 5,
          callback: (value) {
            if (mounted) {
              dtStartPolicy.value =
                  DateFormat('dd-MMM-yyyy').format(value).toString();
            }
          },
        ),
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Term Basis",
            list: listTermBasisRB,
            index: termBasisRBIndex,
            callback: (index) {
              setState(() => termBasisRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Guaranteed?",
            list: listGuaranteedRB,
            index: guaranteedRBIndex,
            callback: (index) {
              setState(() => guaranteedRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Renewable?",
            list: listRenewableRB,
            index: renewableRBIndex,
            callback: (index) {
              setState(() => renewableRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the policy indexed?",
            list: listPolicyIndexedRB,
            index: policyIndexedRBIndex,
            callback: (index) {
              setState(() => policyIndexedRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Does the policy include waiver of premium?",
            list: listWaiverPremiumRB,
            index: waiverPremiumRBIndex,
            callback: (index) {
              setState(() => waiverPremiumRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the policy on trust?",
            list: listPolicyTrustRB,
            index: policyTrustRBIndex,
            callback: (index) {
              setState(() => policyTrustRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the policy used with a mortgage?",
            list: listPolicyUsedWithMortRB,
            index: policyUsedWithMortRBIndex,
            callback: (index) {
              setState(() => policyUsedWithMortRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Policy to be cancelled or replaced?",
            list: listPolicyCancelledRB,
            index: policyCancelledRBIndex,
            callback: (index) {
              setState(() => policyCancelledRBIndex = index);
            }),
        SizedBox(height: 10),
        drawTextArea(title: "Need for cover", tf: needCover),
        SizedBox(height: 10),
        drawTextArea(title: "Other Applicable Benefits", tf: otherBenefits),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  drawIncomeProtectionPolicy() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        DropDownListDialog(
          context: context,
          title: optIPCoverType.value.title,
          h1: "Select IP cover types",
          heading: "IP Cover Types",
          ddTitleList: ddIPCoverType,
          vPadding: 3,
          callback: (optionItem) {
            optIPCoverType.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optProvider.value.title,
          h1: "Select provider",
          heading: "Provider",
          ddTitleList: ddProvider,
          vPadding: 3,
          callback: (optionItem) {
            optProvider.value = optionItem;
          },
        ),
        optProvider.value.title == "Other"
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: InputW2N(
                  title: "Other Provider Name",
                  input: provOther,
                  ph: "Other Provider Name",
                  kbType: TextInputType.text,
                  len: 50,
                  isBold: true,
                ))
            : SizedBox(),
        SizedBox(height: 5),
        InputW2N(
          title: "Policy Number",
          input: policyNo,
          ph: "Policy Number",
          kbType: TextInputType.text,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        drawInputCurrencyBox(
            context: context,
            tf: premium,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "Premium",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optFreq.value.title,
          h1: "Select frequency",
          heading: "Frequency",
          ddTitleList: ddFreq,
          vPadding: 3,
          callback: (optionItem) {
            optFreq.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "Initial benefit",
          input: initialBenefit,
          ph: "Initial benefit",
          kbType: TextInputType.text,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        DropDownListDialog(
          context: context,
          title: optInitDefPeriod.value.title,
          h1: "Select initial deferred period",
          heading: "Initial deferred period",
          ddTitleList: ddInitDefPeriod,
          vPadding: 3,
          callback: (optionItem) {
            optInitDefPeriod.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "Additional Benefit",
          input: additionalBenefit,
          ph: "Additional Benefit",
          kbType: TextInputType.text,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        DropDownListDialog(
          context: context,
          title: optInitDefPeriodAdditional.value.title,
          h1: "Select additional deferred period",
          heading: "Additional deferred period",
          ddTitleList: ddInitDefPeriod,
          vPadding: 3,
          callback: (optionItem) {
            optInitDefPeriodAdditional.value = optionItem;
          },
        ),
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Term Basis",
            list: listTermBasisRB,
            index: termBasisRBIndex,
            callback: (index) {
              setState(() => termBasisRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the policy indexed?",
            list: listPolicyIndexedRB,
            index: policyIndexedRBIndex,
            callback: (index) {
              setState(() => policyIndexedRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is there a waiver of premium?",
            list: listWaiverPremiumRB,
            index: waiverPremiumRBIndex,
            callback: (index) {
              setState(() => waiverPremiumRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the policy used with a mortgage?",
            list: listPolicyUsedWithMortRB,
            index: policyUsedWithMortRBIndex,
            callback: (index) {
              setState(() => policyUsedWithMortRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Policy to be cancelled or rejected?",
            list: listPolicyCancelledRB,
            index: policyCancelledRBIndex,
            callback: (index) {
              setState(() => policyCancelledRBIndex = index);
            }),
        SizedBox(height: 10),
        drawTextArea(title: "Need for cover", tf: needCover),
        SizedBox(height: 10),
        drawTextArea(title: "Other Applicable Benefits", tf: otherBenefits),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  drawBldgContentPolicy() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        DropDownListDialog(
          context: context,
          title: optBldgPolicyType.value.title,
          h1: "Select policy type",
          heading: "Policy Type",
          ddTitleList: ddBldgPolicyType,
          vPadding: 3,
          callback: (optionItem) {
            optBldgPolicyType.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optProvider.value.title,
          h1: "Select provider",
          heading: "Provider",
          ddTitleList: ddProvider,
          vPadding: 3,
          callback: (optionItem) {
            optProvider.value = optionItem;
          },
        ),
        optProvider.value.title == "Other"
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: InputW2N(
                  title: "Other Provider Name",
                  input: provOther,
                  ph: "Other Provider Name",
                  kbType: TextInputType.text,
                  len: 50,
                  isBold: true,
                ))
            : SizedBox(),
        SizedBox(height: 5),
        GPlacesView(
            title: "Property Address",
            titleColor: Colors.black,
            isBold: true,
            txtSize: MyTheme.txtSize - .2,
            address: addrProperty.value,
            callback: (String address, String postCode, Location loc) {
              addrProperty.value = address;
              setState(() {});
            }),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optNoClaimBonus.value.title,
          h1: "Select number of claims bonus",
          heading: "Number of claims bonus",
          ddTitleList: ddNoClaimBonus,
          vPadding: 3,
          callback: (optionItem) {
            optNoClaimBonus.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        InputW2N(
          title: "Policy Number",
          input: policyNo,
          ph: "Policy Number",
          kbType: TextInputType.text,
          len: 50,
          isBold: true,
        ),
        SizedBox(height: 5),
        drawInputCurrencyBox(
            context: context,
            tf: premium,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "Premium",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        DropDownListDialog(
          context: context,
          title: optFreq.value.title,
          h1: "Select frequency",
          heading: "Frequency",
          ddTitleList: ddFreq,
          vPadding: 3,
          callback: (optionItem) {
            optFreq.value = optionItem;
          },
        ),
        SizedBox(height: 10),
        drawInputCurrencyBox(
            context: context,
            tf: coverAmount,
            hintTxt: null,
            labelColor: Colors.black,
            labelTxt: "Cover Amount",
            isBold: true,
            len: 10,
            focusNode: null,
            focusNodeNext: null),
        SizedBox(height: 10),
        DatePickerView(
          cap: "Policy start date",
          dt: (dtStartPolicy.value == '') ? 'Select Date' : dtStartPolicy.value,
          txtColor: Colors.black,
          fontWeight: FontWeight.bold,
          initialDate: now,
          firstDate: old,
          lastDate: now,
          padding: 5,
          radius: 5,
          callback: (value) {
            if (mounted) {
              dtStartPolicy.value =
                  DateFormat('dd-MMM-yyyy').format(value).toString();
            }
          },
        ),
        SizedBox(height: 10),
        DatePickerView(
          cap: "Policy expiry date",
          dt: (dtEndPolicy.value == '') ? 'Select Date' : dtEndPolicy.value,
          txtColor: Colors.black,
          fontWeight: FontWeight.bold,
          initialDate: now,
          firstDate: old,
          lastDate: next,
          padding: 5,
          radius: 5,
          callback: (value) {
            if (mounted) {
              dtEndPolicy.value =
                  DateFormat('dd-MMM-yyyy').format(value).toString();
            }
          },
        ),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Accidental damage?",
            list: listAccDamageRB,
            index: accDamageRBIndex,
            callback: (index) {
              setState(() => accDamageRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are personal possessions included?",
            list: listArePersPossRB,
            index: arePersPossRBIndex,
            callback: (index) {
              setState(() => arePersPossRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is cycle cover included?",
            list: listCycleCoverRB,
            index: cycleCoverRBIndex,
            callback: (index) {
              setState(() => cycleCoverRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Are specific items included?",
            list: listAreSpeciItemIncludedRB,
            index: areSpeciItemIncludedRBIndex,
            callback: (index) {
              setState(() => areSpeciItemIncludedRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is the policy used with a mortgage?",
            list: listPolicyUsedWithMortRB,
            index: policyUsedWithMortRBIndex,
            callback: (index) {
              setState(() => policyUsedWithMortRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Policy to be cancelled or replaced?",
            list: listPolicyCancelledRB,
            index: policyCancelledRBIndex,
            callback: (index) {
              setState(() => policyCancelledRBIndex = index);
            }),
        SizedBox(height: 10),
        drawTextArea(title: "Need for cover", tf: needCover),
        SizedBox(height: 10),
        drawTextArea(title: "Other Applicable Benefits", tf: otherBenefits),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  drawCancelledProtection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Any policies lapsed or cancelled in the last 12 months?",
            list: listAnyPolicyLapsedRB,
            index: anyPolicyLapsedRBIndex,
            callback: (index) {
              setState(() => anyPolicyLapsedRBIndex = index);
            }),
        anyPolicyLapsedRBIndex == 0
            ? Column(
                children: [
                  SizedBox(height: 10),
                  drawTextArea(title: "Details", tf: details),
                ],
              )
            : SizedBox(),
      ],
    );
  }
}
