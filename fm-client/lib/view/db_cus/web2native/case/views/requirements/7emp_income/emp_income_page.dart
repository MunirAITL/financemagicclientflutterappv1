import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserInCome.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserOccupation.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/PostIncomeAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/requirements/7emp_income/income_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/7emp_income/emp_income_base.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/8existing_policy/existing_policy_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'emp_details_page.dart';

class EmpIncomePage extends StatefulWidget {
  const EmpIncomePage({Key key}) : super(key: key);
  @override
  State<EmpIncomePage> createState() => _EmpIncomePageState();
}

class _EmpIncomePageState extends EmpIncomeBase<EmpIncomePage> {
  //  ***************************************** EMP

  onEmpClicked(MortgageUserOccupation model) {
    Get.to(() => EmpDetailsPage(empModel: model)).then((value) {
      setState(() {});
    });
  }

  onDelEmpAPI(MortgageUserOccupation model2) async {
    try {
      await APIViewModel().req<CommonAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_EMP_URL + model2.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                empIncomeCtrl.empCtrl.remove(model2);
                empIncomeCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  //  ***************************************** INCOME

  onIncomeClicked(MortgageUserInCome model2) {
    Get.dialog(reqStep7IncomeDialog(
        context: context,
        taskId: caseModelCtrl.locModel.value.id,
        model2: model2,
        callbackSuccess: (model) async {
          await APIViewModel().req<PostIncomeAPIModel>(
              context: context,
              url: model2 == null
                  ? SrvW2NCase.POST_INCOME_URL
                  : SrvW2NCase.PUT_INCOME_URL,
              reqType: model2 == null ? ReqType.Post : ReqType.Put,
              param: model.toJson(),
              callback: (model) async {
                if (mounted && model != null) {
                  if (model.success) {
                    if (model2 != null) {
                      empIncomeCtrl.incomeCtrl.remove(model2);
                      empIncomeCtrl.update();
                    }
                    empIncomeCtrl.incomeCtrl
                        .add(model.responseData.mortgageUserInCome);
                    empIncomeCtrl.update();
                    setState(() {});
                  }
                }
              });
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  onDelIncomeAPI(MortgageUserInCome model2) async {
    try {
      await APIViewModel().req<PostIncomeAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_INCOME_URL + model2.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                empIncomeCtrl.incomeCtrl.remove(model2);
                empIncomeCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Your employment"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              Get.off(() => ExistingPolicyPage());
                            }),
                      )
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              drawProgressView(70),
              drawEmpForm(),
              drawIncomeForm(),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}
