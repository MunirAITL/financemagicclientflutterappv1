import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/model/json/web2native/case/doc/MortgageCaseDocumentInfos.dart';
import 'package:aitl/model/json/web2native/case/doc/PostCaseDocAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/doc/doc_ctrl.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mime/mime.dart';

docCaseDialog({
  @required BuildContext context,
  int taskId,
  MortgageCaseDocumentInfos model2,
  @required Function(MortgageCaseDocumentInfos) callbackSuccess,
  @required Function(String) callbackFailed,
}) {
  final ddType = DropListModel([
    OptionItem(id: "1", title: "Client Documents"),
  ]);
  var optType = OptionItem(id: null, title: "Select type").obs;

  final ddPurpose = DropListModel([
    OptionItem(id: "1", title: "Accounts"),
    OptionItem(id: "1", title: "Accountants reference"),
    OptionItem(id: "1", title: "Affordability report"),
    OptionItem(id: "1", title: "Application form"),
    OptionItem(id: "1", title: "Bank statement"),
    OptionItem(id: "1", title: "Bank statements showing income/expenditure"),
    OptionItem(id: "1", title: "Bank statements showing rental income"),
    OptionItem(id: "1", title: "Certified Documents"),
    OptionItem(id: "1", title: "Client agreement"),
    OptionItem(id: "1", title: "Council tax"),
    OptionItem(id: "1", title: "Communication"),
    OptionItem(id: "1", title: "Compliance protection"),
    OptionItem(id: "1", title: "Compliance mortgage"),
    OptionItem(id: "1", title: "Cover letter"),
    OptionItem(id: "1", title: "Credit Report"),
    OptionItem(id: "1", title: "Disclosure Doc(s) issued"),
    OptionItem(id: "1", title: "Driving licence"),
    OptionItem(id: "1", title: "EDD"),
    OptionItem(id: "1", title: "Employment contract"),
    OptionItem(id: "1", title: "Enhanced protection comparison report"),
    OptionItem(id: "1", title: "Evidence of bonus"),
    OptionItem(id: "1", title: "Evidence of research"),
    OptionItem(id: "1", title: "Gifted deposit letter"),
    OptionItem(id: "1", title: "IDV"),
    OptionItem(id: "1", title: "Illustration"),
    OptionItem(id: "1", title: "Income protection comparison report"),
    OptionItem(id: "1", title: "KFD"),
    OptionItem(id: "1", title: "Marriage certificate"),
    OptionItem(id: "1", title: "Memorandum"),
    OptionItem(id: "1", title: "Mortgage statement"),
    OptionItem(id: "1", title: "National ID doc"),
    OptionItem(id: "1", title: "Offer letter"),
    OptionItem(id: "1", title: "Passport"),
    OptionItem(id: "1", title: "Payslips"),
    OptionItem(id: "1", title: "Payment schedule"),
    OptionItem(id: "1", title: "Pension"),
    OptionItem(id: "1", title: "Privacy Notice"),
    OptionItem(id: "1", title: "Proof of address"),
    OptionItem(id: "1", title: "Proof of deposit"),
    OptionItem(id: "1", title: "Proof of Income"),
    OptionItem(id: "1", title: "Protection report"),
    OptionItem(id: "1", title: "P60"),
    OptionItem(id: "1", title: "Quote/illustration"),
    OptionItem(id: "1", title: "Quotes and summary breakdown"),
    OptionItem(id: "1", title: "Recent CV"),
    OptionItem(id: "1", title: "Recommendation Letter"),
    OptionItem(id: "1", title: "Residence permit"),
    OptionItem(id: "1", title: "Signed accounts"),
    OptionItem(id: "1", title: "Signed mortgage declaration"),
    OptionItem(id: "1", title: "Signed direct-debit mandate"),
    OptionItem(id: "1", title: "Signed Recommendation Letter"),
    OptionItem(id: "1", title: "Letter of Suitability"),
    OptionItem(id: "1", title: "Tax calculations/SA302S"),
    OptionItem(id: "1", title: "Tax Year overviews"),
    OptionItem(id: "1", title: "Term comparison report"),
    OptionItem(id: "1", title: "Utility bill"),
    OptionItem(id: "1", title: "Valuation"),
    OptionItem(id: "1", title: "Visa documentation"),
    OptionItem(id: "1", title: "Others"),
  ]);
  var optPurpose = OptionItem(id: null, title: "Select purpose").obs;

  final title = TextEditingController();
  final desc = TextEditingController();

  final listDocCatRB = {0: "Client Document", 1: "Case Document"};
  var docCatRBIndex = 0.obs;

  final ctrl = Get.put(DocController());

  var id = 0;
  try {
    if (model2 != null) {
      id = model2.id;
      try {
        ctrl.imageModel.value = MediaUploadFilesModel.fromJson({
          'Url': model2.documentUrl,
          'Name': model2.name,
        });
      } catch (e) {}
      try {
        optType = OptionItem(id: "1", title: model2.documentCategory).obs;
      } catch (e) {}
      try {
        optPurpose = OptionItem(id: "1", title: model2.purpose).obs;
      } catch (e) {}
      try {
        title.text = model2.name;
      } catch (e) {}
      try {
        desc.text = model2.remarks.trim();
      } catch (e) {}
      try {
        docCatRBIndex.value = Common.getMapKeyByVal(listDocCatRB, model2.type);
      } catch (e) {}
    } else {
      ctrl.imageModel.value = null;
    }
  } catch (e) {}

  return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      insetPadding: EdgeInsets.only(
        left: 50,
        right: 50,
      ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
      child: Obx(
        () => Container(
          height: MediaQuery.of(context).size.height * .8,
          child: Card(
            color: Colors.white,
            elevation: 10,
            child: Stack(
                clipBehavior: Clip.none,
                alignment: Alignment.center,
                children: [
                  SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Txt(
                                txt: "Documents Upload",
                                txtColor: Colors.black,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                          SizedBox(height: 10),
                          DottedBorder(
                            borderType: BorderType.RRect,
                            radius: Radius.circular(10),
                            padding: EdgeInsets.all(5),
                            color: Colors.grey,
                            strokeWidth: .5,
                            child: GestureDetector(
                              onTap: () async {
                                //print(imageModel);
                                await _browseFiles(context, (model) {
                                  if (model.success) {
                                    ctrl.imageModel.value =
                                        model.responseData.images[0];
                                    print(ctrl.imageModel);
                                  }
                                });
                              },
                              child: Container(
                                //height: getHP(context, 5),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Txt(
                                          txt: "Add Document File",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                    Flexible(
                                      child: Icon(
                                        Icons.attach_file,
                                        color: Colors.grey,
                                        size: 25,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          ctrl.imageModel.value != null
                              ? ctrl.imageModel.value.url != ''
                                  ? GetBuilder<DocController>(
                                      init: DocController(),
                                      builder: (value) {
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10),
                                          child: Container(
                                            width: double.infinity,
                                            child: Card(
                                              elevation: 5,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(5),
                                                child: Txt(
                                                    txt: "Uploaded file: " +
                                                            value.imageModel
                                                                .value.name
                                                                .split('/')
                                                                .last ??
                                                        '',
                                                    txtColor:
                                                        MyTheme.statusBarColor,
                                                    txtSize:
                                                        MyTheme.txtSize - .2,
                                                    txtAlign: TextAlign.center,
                                                    isBold: false),
                                              ),
                                            ),
                                          ),
                                        )
                                            /*Container(
                                        color: Colors.transparent,
                                        child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Flexible(
                                                  child: IconButton(
                                                      icon: Icon(
                                                        Icons.remove_circle,
                                                        color: Colors.red,
                                                        size: 20,
                                                      ),
                                                      onPressed: () {
                                                        //ctrl.dispose();
                                                      })),
                                              Expanded(
                                                child: Txt(
                                                    txt: value.imageModel
                                                            .value.name
                                                            .split('/')
                                                            .last ??
                                                        '',
                                                    txtColor: Colors.black,
                                                    txtSize:
                                                        MyTheme.txtSize - .2,
                                                    txtAlign:
                                                        TextAlign.center,
                                                    isBold: false),
                                              )
                                            ]))*/
                                            ;
                                      })
                                  : SizedBox()
                              : SizedBox(),
                          SizedBox(height: 5),
                          DropDownListDialog(
                            context: context,
                            title: optType.value.title,
                            h1: "Select document type",
                            heading: "Type",
                            ddTitleList: ddType,
                            vPadding: 3,
                            callback: (optionItem) {
                              optType.value = optionItem;
                            },
                          ),
                          SizedBox(height: 5),
                          DropDownListDialog(
                            context: context,
                            title: optPurpose.value.title,
                            h1: "Select purpose",
                            heading: "Purpose",
                            ddTitleList: ddPurpose,
                            vPadding: 3,
                            callback: (optionItem) {
                              optPurpose.value = optionItem;
                            },
                          ),
                          SizedBox(height: 5),
                          InputW2N(
                            title: "Title",
                            input: title,
                            ph: "",
                            kbType: TextInputType.text,
                            len: 100,
                            isBold: true,
                          ),
                          SizedBox(height: 5),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: "Document category",
                              list: listDocCatRB,
                              index: docCatRBIndex.value,
                              callback: (index) => docCatRBIndex.value = index),
                          SizedBox(height: 10),
                          Txt(
                              txt: "Description",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Container(
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.grey, width: 1),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            child: TextField(
                              controller: desc,
                              minLines: 1,
                              maxLines: 2,
                              //expands: true,
                              autocorrect: false,
                              maxLength: 255,
                              keyboardType: TextInputType.multiline,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 15),
                              decoration: InputDecoration(
                                hintText: "",
                                hintStyle:
                                    TextStyle(color: Colors.grey, fontSize: 15),
                                //labelText: 'Your message',
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.all(10),
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          MMBtn(
                              txt: "Submit",
                              height: 35,
                              width: null,
                              callback: () async {
                                if (ctrl.imageModel.value == null) {
                                  callbackFailed("Please upload document file");
                                } else if (optType.value.id == null) {
                                  callbackFailed("Please select document type");
                                } else if (optPurpose.value.id == null) {
                                  callbackFailed(
                                      "Please select document purpose");
                                } else {
                                  final mimeType =
                                      lookupMimeType(ctrl.imageModel.value.url);
                                  final now = DateFun.getDate(
                                      DateTime.now().toString(), "dd-MMM-yyyy");
                                  await APIViewModel().req<PostCaseDocAPIModel>(
                                      context: context,
                                      url: id == 0
                                          ? SrvW2NCase.POST_UPLOAD_DOC_URL
                                          : SrvW2NCase.PUT_UPLOAD_DOC_URL,
                                      param: {
                                        "Id": id,
                                        "UserId": userData.userModel.id,
                                        "UserCompanyId": userData
                                            .userModel.userCompanyInfo.id,
                                        "Status": 101,
                                        "TaskId": taskId,
                                        "CreationDate": now,
                                        "UpdatedDate": now,
                                        "Name": ctrl.imageModel.value.name
                                            .split('/')
                                            .last,
                                        "Purpose": optPurpose.value.title,
                                        "Type": listDocCatRB[docCatRBIndex],
                                        "ExpiryDate": now,
                                        "DocumentUrl":
                                            ctrl.imageModel.value.url,
                                        "Remarks": desc.text.trim(),
                                        "MimeType": mimeType,
                                        "InitiatorId": userData.userModel.id,
                                        "DocumentCategory": optType.value.title,
                                        "DocumentRequestType": ""
                                      },
                                      reqType:
                                          id == 0 ? ReqType.Post : ReqType.Put,
                                      callback: (model) async {
                                        if (model != null) {
                                          if (model.success) {
                                            ctrl.imageModel.value = null;
                                            callbackSuccess(model.responseData
                                                .mortgageCaseDocumentInfo);
                                            Get.back();
                                          }
                                        }
                                      });

                                  Get.back();
                                }
                              })
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: -40,
                    child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            10), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                        ),
                        child: Icon(Icons.upload_outlined,
                            color: MyTheme.dBlueAirColor, size: 40)),
                  ),
                  Positioned(
                    top: -15,
                    right: -10,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: InkWell(
                          child: Container(
                            //width: 50.0,
                            //height: 50.0,
                            padding: const EdgeInsets.all(
                                2), //I used some padding without fixed width and height
                            decoration: new BoxDecoration(
                              shape: BoxShape
                                  .circle, // You can use like this way or like the below line
                              //borderRadius: new BorderRadius.circular(30.0),
                              color: Colors.white,
                              border: Border.all(
                                width: 1,
                                color: MyTheme.statusBarColor,
                                style: BorderStyle.solid,
                              ),
                            ),
                            child: Icon(
                              Icons.close,
                              color: Colors.black,
                            ),
                          ),
                          onTap: () async {
                            //ctrl.dispose();
                            Get.back();
                          }),
                    ),
                  ),
                ]),
          ),
        ),
      ));
}

_browseFiles(
    BuildContext context, Function(MediaUploadFilesAPIModel) callback) async {
  try {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx'],
      // type: FileType.any,
      /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
    );

    if (result != null) {
      MediaUploadAPIMgr().wsMediaUploadFileAPI(
        context: context,
        file: File(result.files.single.path),
        callback: (model) {
          if (model != null) {
            callback(model);
          }
        },
      );
    }
  } catch (e) {}
}
