import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

submitCaseApplicationDialog({
  @required BuildContext context,
  @required width,
  @required Function() callback,
}) {
  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .6,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Txt(
                          txt: "Are you sure want to submit application?",
                          txtColor: MyTheme.statusBarColor,
                          txtSize: 2,
                          txtAlign: TextAlign.center,
                          isBold: true),
                      SizedBox(height: 20),
                      Txt(
                          txt:
                              "Once the application is submitted our adviser will begin to process the application. The information you have provided will be checked for verification. We may also conduct credit search and other due-diligence as a part of processing your application and you are hereby giving us explicit permission to do so.",
                          txtColor: Colors.black,
                          txtSize: 1.6,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 10),
                      Txt(
                          txt:
                              "By submitting this application you are confirming that you have read the Terms and Conditions and you declare that the information provided in this fact-find is true and accurate.",
                          txtColor: Colors.red,
                          txtSize: 1.6,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 10),
                      Txt(
                          txt:
                              "Once submitted you can no longer edit this application.",
                          txtColor: Colors.redAccent,
                          txtSize: 1.6,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 100),
                    ],
                  ),
                ),
              ),
              Positioned(
                  bottom: 5, //-MediaQuery.of(context).size.height * .06,
                  child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: width * .65,
                        //color: MyTheme.bgColor2,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: MMBtn(
                                  txt: "Cancel",
                                  height: 35,
                                  bgColor: Colors.grey[300],
                                  txtColor: Colors.black,
                                  width: null,
                                  callback: () {
                                    Get.back();
                                  }),
                            ),
                            SizedBox(width: 10),
                            Flexible(
                              child: MMBtn(
                                  txt: "Confirm",
                                  height: 35,
                                  width: null,
                                  bgColor: Colors.red,
                                  callback: () {
                                    callback();
                                    Get.back();
                                  }),
                            ),
                          ],
                        ),
                      ))),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.app_registration,
                        color: Colors.red, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        //ctrl.dispose();
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
