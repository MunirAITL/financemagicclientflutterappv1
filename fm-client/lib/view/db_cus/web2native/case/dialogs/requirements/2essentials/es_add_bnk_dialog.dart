import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserBankDetailList.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../../widgets/btn/MMBtn.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/input/InputW2N.dart';
import 'package:google_maps_webservice/geolocation.dart';

reqStep2ESBankDialog({
  @required BuildContext context,
  int taskId,
  MortgageUserBankDetailList model2,
  @required Function(MortgageUserBankDetailList) callbackSuccess,
  @required Function(String) callbackFailed,
}) {
  final accName = TextEditingController();
  final bnkName = TextEditingController();
  final accNo = TextEditingController();
  final sortCode = TextEditingController();
  final timeBnk = TextEditingController();
  var postcode = TextEditingController().obs;
  var addr = "".obs;

  var id = 0;
  try {
    if (model2 != null) {
      id = model2.id;
      try {
        accName.text = model2.accountName;
      } catch (e) {}
      try {
        bnkName.text = model2.bankName;
      } catch (e) {}
      try {
        addr.value = model2.address;
      } catch (e) {}
      try {
        postcode.value.text = model2.postcode;
      } catch (e) {}
      try {
        sortCode.text = model2.sortCode;
      } catch (e) {}
      try {
        accNo.text = model2.accountNumber;
      } catch (e) {}
      try {
        timeBnk.text = model2.timeWithBank;
      } catch (e) {}
    }
  } catch (e) {}

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .7,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Txt(
                              txt: "Bank Details: (For Direct Debit Set Up)",
                              txtColor: Colors.black,
                              txtSize: 2,
                              txtAlign: TextAlign.center,
                              isBold: true),
                        ),
                        SizedBox(height: 5),
                        InputW2N(
                          title: "Account Name",
                          input: accName,
                          ph: "",
                          kbType: TextInputType.text,
                          len: 50,
                        ),
                        InputW2N(
                          title: "Bank Name",
                          input: bnkName,
                          ph: "",
                          kbType: TextInputType.text,
                          len: 50,
                        ),
                        GPlacesView(
                            title: "Address",
                            titleColor: Colors.black,
                            isBold: false,
                            txtSize: MyTheme.txtSize - .2,
                            address: addr.value,
                            callback: (String address, String postCode,
                                Location loc) {
                              addr.value = address;
                              postcode.value.text = postCode;
                            }),
                        SizedBox(height: 5),
                        InputW2N(
                          title: "Postcode",
                          input: postcode.value,
                          ph: "",
                          kbType: TextInputType.streetAddress,
                          len: 10,
                        ),
                        InputW2N(
                          title: "Account Number",
                          input: accNo,
                          ph: "",
                          kbType: TextInputType.number,
                          len: 10,
                        ),
                        InputW2N(
                          title: "Sort Code",
                          input: sortCode,
                          ph: "",
                          kbType: TextInputType.number,
                          len: 10,
                        ),
                        InputW2N(
                          title: "Time with Bank",
                          input: timeBnk,
                          ph: "",
                          kbType: TextInputType.text,
                          len: 50,
                        ),
                        SizedBox(height: 5),
                        MMBtn(
                            txt: id == 0 ? "Save" : "Update",
                            height: 35,
                            width: null,
                            callback: () async {
                              if (accName.text.isEmpty) {
                                callbackFailed(
                                    "Please enter valid Account Name");
                              } else if (bnkName.text.isEmpty) {
                                callbackFailed("Please enter valid Bank Name");
                              } else if (addr.value.isEmpty) {
                                callbackFailed("Please enter valid Address");
                              } else if (postcode.value.text.isEmpty) {
                                callbackFailed("Please enter valid Postcode");
                              } else if (accNo.text.isEmpty) {
                                callbackFailed(
                                    "Please enter valid Account Number");
                              } else if (sortCode.text.isEmpty) {
                                callbackFailed("Please enter valid Sort Code");
                              } else if (timeBnk.text.isEmpty) {
                                callbackFailed(
                                    "Please enter valid Time with Bank");
                              } else {
                                callbackSuccess(
                                    MortgageUserBankDetailList.fromJson({
                                  'UserId': userData.userModel.id,
                                  'User': null,
                                  'CompanyId':
                                      userData.userModel.userCompanyInfo.id,
                                  //'Status': this.status,
                                  //'MortgageCaseInfoId': this.mortgageCaseInfoId,
                                  'CreationDate': DateTime.now().toString(),
                                  'TaskId': taskId,
                                  'AccountName': accName.text.trim(),
                                  'BankName': bnkName.text.trim(),
                                  'Address': addr.value,
                                  'Postcode': postcode.value.text.trim(),
                                  'SortCode': sortCode.text.trim(),
                                  'AccountNumber': accNo.text.trim(),
                                  'TimeWithBank': timeBnk.text.trim(),
                                  'Remarks': "",
                                  'Id': id,
                                }));
                                Get.back();
                              }
                            })
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.account_balance_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
