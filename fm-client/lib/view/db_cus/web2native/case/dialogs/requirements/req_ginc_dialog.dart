import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../config/SrvW2NCase.dart';
import '../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../model/json/web2native/case/requirements/gen_insurance/MortgageBuildingAndContentInsuranceItem.dart';
import '../../../../../../model/json/web2native/case/requirements/gen_insurance/PostPersonalProcessionAPIModel.dart';
import '../../../../../../view_model/api/api_view_model.dart';
import '../../../../../widgets/btn/MMBtn.dart';
import '../../../../../widgets/dialog/ConfirmationDialog.dart';
import '../../../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../../../widgets/dropdown/DropListModel.dart';
import '../../../../../widgets/input/drawInputCurrencyBox.dart';
import '../../../../../widgets/radio/draw_radio_group.dart';

reqBikePhoneDialog({
  @required BuildContext context,
  int taskId,
  MortgageBuildingAndContentInsuranceItem model2,
  @required Function(MortgageBuildingAndContentInsuranceItem) callbackSuccess,
  @required Function(String) callbackFailed,
}) {
  final cost = TextEditingController();
  final desc = TextEditingController();
  final listItemRB = {0: 'Phone', 1: 'Bike'};
  var itemRBIndex = 1.obs;

  var id = 0;
  try {
    id = model2.id;
  } catch (e) {}
  try {
    itemRBIndex.value = Common.getMapKeyByVal(listItemRB, model2.coverTyped);
  } catch (e) {}
  try {
    cost.text = model2.costToReplaceAmount.toStringAsFixed(0);
  } catch (e) {}
  try {
    desc.text = model2.itemDescription ?? '';
  } catch (e) {}

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .55,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Txt(
                            txt: "Phone & Bikes",
                            txtColor: Colors.black,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true),
                      ),
                      SizedBox(height: 20),
                      Txt(
                          txt: "Item to be covered?",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 2),
                      Obx(() => drawRadioGroup(
                          type: eRadioType.GRID2,
                          isMainBG: true,
                          map: listItemRB,
                          selected: itemRBIndex.value,
                          callback: (v) {
                            itemRBIndex.value = v;
                          })),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Describe the item",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 5),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: desc,
                          minLines: 2,
                          maxLines: 3,
                          //expands: true,
                          autocorrect: false,
                          maxLength: 100,
                          keyboardType: TextInputType.multiline,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                          decoration: InputDecoration(
                            hintText: "",
                            hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            //labelText: 'Your message',
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding: EdgeInsets.all(10),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      drawInputCurrencyBox(
                          context: context,
                          tf: cost,
                          hintTxt: 0,
                          labelColor: Colors.black,
                          labelTxt: "Cost to replace it",
                          isBold: true,
                          len: 10,
                          focusNode: FocusNode(),
                          focusNodeNext: null),
                      SizedBox(height: 20),
                      MMBtn(
                          txt: id == 0 ? "Add" : "Update",
                          height: 35,
                          width: null,
                          callback: () async {
                            Get.back();
                            var _cost = 0;
                            try {
                              _cost = int.parse(cost.text.trim());
                            } catch (e) {}
                            var url = id == 0
                                ? SrvW2NCase.POST_PERSONAL_PROCESSION_URL
                                : SrvW2NCase.PUT_PERSONAL_PROCESSION_URL;
                            await APIViewModel().req<
                                    PostPersonalProcessionAPIModel>(
                                context: context,
                                url: url,
                                param: {
                                  "CreationDate": DateTime.now().toString(),
                                  "UpdatedDate": DateTime.now().toString(),
                                  "UserId": userData.userModel.id,
                                  "CompanyId": 0,
                                  "Type": "Phone & Bike",
                                  "TaskId": taskId,
                                  "Remarks": "",
                                  "VersionNumber": 0,
                                  "Id": id,
                                  "Status": 0,
                                  "ItemDescription": desc.text.trim(),
                                  "CoverTyped": listItemRB[itemRBIndex.value],
                                  "CostToReplaceAmount": _cost,
                                  "UserCompanyId":
                                      userData.userModel.userCompanyID
                                },
                                reqType: id == 0 ? ReqType.Post : ReqType.Put,
                                callback: (model) async {
                                  if (model.success) {
                                    callbackSuccess(model.responseData
                                        .mortgageBuildingAndContentInsuranceItem);
                                  }
                                });
                          })
                    ],
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.electric_bike,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}

reqAdditionalItemDialog({
  @required BuildContext context,
  int taskId,
  MortgageBuildingAndContentInsuranceItem model2,
  @required Function(MortgageBuildingAndContentInsuranceItem) callbackSuccess,
  @required Function(String) callbackFailed,
}) {
  var ddItem = DropListModel([
    OptionItem(id: "Jewellery / watches", title: "Jewellery / watches"),
    OptionItem(
        id: "Portable electonic equipment e.g. iPad",
        title: "Portable electonic equipment e.g. iPad"),
    OptionItem(
        id: "Sports equipment - winter sports",
        title: "Sports equipment - winter sports"),
    OptionItem(
        id: "Musical instruments - amateur",
        title: "Musical instruments - amateur"),
    OptionItem(id: "Clocks", title: "Clocks"),
    OptionItem(id: "Clothing", title: "Clothing"),
    OptionItem(id: "Coin collection", title: "Coin collection"),
    OptionItem(id: "Contact lenses", title: "Contact lenses"),
    OptionItem(
        id: "Cups, shields, trophies, masonic regalia",
        title: "Cups, shields, trophies, masonic regalia"),
    OptionItem(id: "Curios", title: "Curios"),
    OptionItem(id: "Dentures (incl crowns)", title: "Dentures (incl crowns)"),
    OptionItem(id: "Electric wheelchairs", title: "Electric wheelchairs"),
    OptionItem(id: "Furs", title: "Furs"),
    OptionItem(id: "Go-Karts", title: "Go-Kart"),
    OptionItem(id: "Gold items", title: "Gold items"),
    OptionItem(id: "Golf buggy", title: "Golf buggy"),
    OptionItem(id: "Guns", title: "Guns"),
    OptionItem(id: "Hearing aids", title: "Hearing aids"),
    OptionItem(id: "Medal collection", title: "Medal collection"),
    OptionItem(id: "Medical equipment", title: "Medical equipment"),
    OptionItem(id: "Model aircraft", title: "Model aircraft"),
    OptionItem(
        id: "Musical instruments - amateur",
        title: "Musical instruments - amateur"),
    OptionItem(id: "Paintings", title: "Paintings"),
    OptionItem(id: "Pearls", title: "Pearls"),
    OptionItem(
        id: "Photo equipment - amateur", title: "Photo equipment - amateur"),
    OptionItem(id: "Pianos", title: "Pianos"),
    OptionItem(id: "Pictures", title: "Pictures"),
    OptionItem(
        id: "Portable electonic equipment e.g. iPad",
        title: "Portable electonic equipment e.g. iPad"),
    OptionItem(id: "Prams and pushchairs", title: "Prams and pushchairs"),
    OptionItem(id: "Precious metals", title: "Precious metals"),
    OptionItem(id: "Sculptures", title: "Sculptures"),
    OptionItem(id: "Silver items", title: "Silver items"),
    OptionItem(id: "Sound equipment", title: "Sound equipment"),
    OptionItem(id: "Spectacles", title: "Spectacles"),
    OptionItem(
        id: "Sporting guns and shooting equipment",
        title: "Sporting guns and shooting equipment"),
    OptionItem(
        id: "Sports equipment - excluding winter",
        title: "Sports equipment - excluding winter"),
    OptionItem(id: "sports and cycles", title: "sports and cycles"),
    OptionItem(
        id: "Sports equipment - winter sports",
        title: "Sports equipment - winter sports"),
    OptionItem(id: "Stamp collection", title: "Stamp collection"),
    OptionItem(id: "Statues", title: "Statues"),
    OptionItem(id: "Tapestries", title: "Tapestries"),
    OptionItem(
        id: "Wheelchairs - self propelled",
        title: "Wheelchairs - self propelled"),
    OptionItem(id: "Works of art", title: "Works of art"),
  ]);
  var optItem = OptionItem(id: null, title: "Select type").obs;

  final cost = TextEditingController();
  final desc = TextEditingController();
  final listItemRB = {
    0: 'Home only cover',
    1: 'Both in and away from the home'
  };
  var itemRBIndex = 0.obs;

  var id = 0;
  try {
    id = model2.id;
  } catch (e) {}
  try {
    optItem = OptionItem(id: model2.remarks, title: model2.remarks).obs;
  } catch (e) {}
  try {
    itemRBIndex.value = Common.getMapKeyByVal(listItemRB, model2.coverTyped);
  } catch (e) {}
  try {
    cost.text = model2.costToReplaceAmount.toStringAsFixed(0);
  } catch (e) {}
  try {
    desc.text = model2.itemDescription ?? '';
  } catch (e) {}

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Center(
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Txt(
                            txt: "High Value Item",
                            txtColor: Colors.black,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true),
                      ),
                      SizedBox(height: 10),
                      Obx(() => DropDownListDialog(
                            context: context,
                            h1: "Select high value item",
                            heading: "What type of item is that?",
                            title: optItem.value.title,
                            ddTitleList: ddItem,
                            vPadding: 3,
                            callback: (optionItem) {
                              optItem.value = optionItem;
                            },
                          )),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Where would you like this item covered?",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 2),
                      Obx(() => drawRadioGroup(
                          type: eRadioType.VERTICAL,
                          isMainBG: false,
                          map: listItemRB,
                          selected: itemRBIndex.value,
                          callback: (v) {
                            itemRBIndex.value = v;
                          })),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Describe the item",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 5),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: desc,
                          minLines: 2,
                          maxLines: 3,
                          //expands: true,
                          autocorrect: false,
                          maxLength: 100,
                          keyboardType: TextInputType.multiline,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                          decoration: InputDecoration(
                            hintText: "",
                            hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            //labelText: 'Your message',
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding: EdgeInsets.all(10),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      drawInputCurrencyBox(
                          context: context,
                          tf: cost,
                          hintTxt: 0,
                          labelColor: Colors.black,
                          labelTxt: "Cost to replace it",
                          isBold: true,
                          len: 10,
                          focusNode: FocusNode(),
                          focusNodeNext: null),
                      SizedBox(height: 20),
                      MMBtn(
                          txt: id == 0 ? "Add" : "Update",
                          height: 35,
                          width: null,
                          callback: () async {
                            Get.back();
                            var _cost = 0;
                            try {
                              _cost = int.parse(cost.text.trim());
                            } catch (e) {}
                            var url = id == 0
                                ? SrvW2NCase.POST_PERSONAL_PROCESSION_URL
                                : SrvW2NCase.PUT_PERSONAL_PROCESSION_URL;
                            await APIViewModel().req<
                                    PostPersonalProcessionAPIModel>(
                                context: context,
                                url: url,
                                param: {
                                  "CreationDate": DateTime.now().toString(),
                                  "UpdatedDate": DateTime.now().toString(),
                                  "UserId": userData.userModel.id,
                                  "CompanyId": 0,
                                  "Type": "High value",
                                  "TaskId": taskId,
                                  "Remarks": optItem.value.id != null
                                      ? optItem.value.title
                                      : "",
                                  "VersionNumber": 0,
                                  "Id": id,
                                  "Status": 0,
                                  "ItemDescription": desc.text.trim(),
                                  "CoverTyped": listItemRB[itemRBIndex.value],
                                  "CostToReplaceAmount": _cost,
                                  "UserCompanyId":
                                      userData.userModel.userCompanyID
                                },
                                reqType: id == 0 ? ReqType.Post : ReqType.Put,
                                callback: (model) async {
                                  if (model.success) {
                                    callbackSuccess(model.responseData
                                        .mortgageBuildingAndContentInsuranceItem);
                                  }
                                });
                          })
                    ],
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.diamond_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}

drawGIncItems(
    BuildContext context,
    String type,
    List<MortgageBuildingAndContentInsuranceItem> list,
    Function(MortgageBuildingAndContentInsuranceItem) callbackOnEdit,
    Function(MortgageBuildingAndContentInsuranceItem) callbackOnDel) {
  return (list.length > 0)
      ? Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ...list.map((model) {
              return model.type == type && model.type == 'Phone & Bike'
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                          Txt(
                              txt: "Phone & Bike",
                              txtColor: MyTheme.statusBarColor,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 14,
                                  color: MyTheme.statusBarColor),
                              isBold: false),
                          SizedBox(height: 5),
                          Material(
                            elevation: 2,
                            color: MyTheme.bgColor,
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: "Item to be covered?",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: "Description",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: "Replacment cost",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.start,
                                        fontWeight: FontWeight.w500,
                                        isBold: true),
                                  ),
                                  callbackOnEdit != null
                                      ? Expanded(
                                          child: Txt(
                                              txt: "Action",
                                              txtColor: Colors.white,
                                              txtSize: MyTheme.txtSize - .6,
                                              txtAlign: TextAlign.start,
                                              fontWeight: FontWeight.w500,
                                              isBold: true),
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 5),
                          _drawTableItemsPhoneBike(
                              context, model, callbackOnEdit, callbackOnDel)
                        ])
                  : model.type == type && model.type == 'High value'
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            Txt(
                                txt: "What type of item is that?",
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 14,
                                    color: MyTheme.statusBarColor),
                                isBold: false),
                            SizedBox(height: 5),
                            Material(
                              elevation: 2,
                              color: MyTheme.bgColor,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Txt(
                                          txt: "What type of item is that?",
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize - .8,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w500,
                                          isBold: true),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      //flex: 2,
                                      child: Txt(
                                          txt: "Describe the item",
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize - .8,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w500,
                                          isBold: true),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      flex: 2,
                                      child: Txt(
                                          txt: "Cost to replace it",
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize - .8,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w500,
                                          isBold: true),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Txt(
                                          txt: "Item covered?",
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize - .8,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w500,
                                          isBold: true),
                                    ),
                                    callbackOnEdit != null
                                        ? Expanded(
                                            child: Txt(
                                                txt: "Action",
                                                txtColor: Colors.white,
                                                txtSize: MyTheme.txtSize - .8,
                                                txtAlign: TextAlign.start,
                                                fontWeight: FontWeight.w500,
                                                isBold: true),
                                          )
                                        : SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            _drawTableItemsAdditionalItemCover(
                                context, model, callbackOnEdit, callbackOnDel)
                          ],
                        )
                      : SizedBox();
            })
          ],
        )
      : SizedBox();
}

_drawTableItemsPhoneBike(
    BuildContext context,
    MortgageBuildingAndContentInsuranceItem model,
    Function(MortgageBuildingAndContentInsuranceItem) callbackOnEdit,
    Function(MortgageBuildingAndContentInsuranceItem) callbackOnDel) {
  final w = MediaQuery.of(context).size.width;
  return Padding(
    padding: const EdgeInsets.only(top: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: w * .3,
          child: Txt(
              txt: model.coverTyped ?? '',
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        SizedBox(
          width: w * .3,
          child: Txt(
              txt: model.itemDescription,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        SizedBox(
          width: w * .15,
          child: Txt(
              txt: AppDefine.CUR_SIGN +
                  model.costToReplaceAmount.toStringAsFixed(0),
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        callbackOnEdit != null
            ? Flexible(
                child: Row(
                  children: [
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            callbackOnEdit(model);
                          },
                          child: Icon(Icons.edit_outlined,
                              color: Colors.grey, size: 20)),
                    ),
                    SizedBox(width: 20),
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            confirmDialog(
                                context: context,
                                title: "Confirmation!!",
                                msg: "Are you sure to delete this item?",
                                callbackYes: () {
                                  callbackOnDel(model);
                                });
                          },
                          child: Icon(Icons.delete_outline,
                              color: Colors.grey, size: 20)),
                    ),
                  ],
                ),
              )
            : SizedBox()
      ],
    ),
  );
}

_drawTableItemsAdditionalItemCover(
    BuildContext context,
    MortgageBuildingAndContentInsuranceItem model,
    Function(MortgageBuildingAndContentInsuranceItem) callbackOnEdit,
    Function(MortgageBuildingAndContentInsuranceItem) callbackOnDel) {
  final w = MediaQuery.of(context).size.width;
  return Padding(
    padding: const EdgeInsets.only(top: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: w * .2,
          child: Txt(
              txt: model.remarks ?? '',
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        SizedBox(
          width: w * (callbackOnEdit != null ? 0.20 : 0.25),
          child: Txt(
              txt: model.coverTyped,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        SizedBox(
          width: w * (callbackOnEdit != null ? 0.20 : 0.25),
          child: Txt(
              txt: model.itemDescription,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        SizedBox(
          width: w * .15,
          child: Txt(
              txt: AppDefine.CUR_SIGN +
                  model.costToReplaceAmount.toStringAsFixed(0),
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        callbackOnEdit != null
            ? Flexible(
                child: Row(
                  children: [
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            callbackOnEdit(model);
                          },
                          child: Icon(Icons.edit_outlined,
                              color: Colors.grey, size: 20)),
                    ),
                    SizedBox(width: 20),
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            confirmDialog(
                                context: context,
                                title: "Confirmation!!",
                                msg: "Are you sure to delete this item?",
                                callbackYes: () {
                                  callbackOnDel(model);
                                });
                          },
                          child: Icon(Icons.delete_outline,
                              color: Colors.grey, size: 20)),
                    ),
                  ],
                ),
              )
            : SizedBox()
      ],
    ),
  );
}
