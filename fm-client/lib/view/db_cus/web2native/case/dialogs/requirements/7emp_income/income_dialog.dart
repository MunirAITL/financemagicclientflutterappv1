import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/MortgageUserInCome.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../../widgets/btn/MMBtn.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/input/InputW2N.dart';
import 'package:google_maps_webservice/geolocation.dart';

reqStep7IncomeDialog({
  @required BuildContext context,
  int taskId,
  MortgageUserInCome model2,
  @required Function(MortgageUserInCome) callbackSuccess,
  @required Function(String) callbackFailed,
}) {
  DropListModel dd = DropListModel([
    OptionItem(id: "1", title: "Salary from another job"),
    OptionItem(id: "2", title: "Overtime from another job"),
    OptionItem(id: "3", title: "Bonus from another job"),
    OptionItem(id: "4", title: "Commission from another job"),
    OptionItem(id: "5", title: "Net profit from another job"),
    OptionItem(id: "6", title: "Dividends"),
    OptionItem(id: "7", title: "Dividends from another job"),
    OptionItem(id: "8", title: "Pension"),
    OptionItem(id: "9", title: "Rental income"),
    OptionItem(id: "10", title: "Investment income"),
    OptionItem(id: "11", title: "Maintenance payments"),
    OptionItem(id: "12", title: "Car allowance"),
    OptionItem(id: "13", title: "Car allowance from another job"),
    OptionItem(
        id: "14", title: "Location/Territorial/London weighting allowance"),
    OptionItem(
        id: "15",
        title:
            "Location/Territorial/London weighting allowance from another job"),
    OptionItem(id: "16", title: "State benefits"),
  ]);
  var opt = OptionItem(id: null, title: "Select income type").obs;

  final amount = TextEditingController();
  final note = TextEditingController();

  var id = 0;
  try {
    if (model2 != null) {
      id = model2.id;
      try {
        opt = OptionItem(id: "0", title: model2.inComeType).obs;
      } catch (e) {}
      try {
        amount.text = model2.amountPerYear.toStringAsFixed(0);
      } catch (e) {}
      try {
        note.text = model2.remarks.trim();
      } catch (e) {}
    }
  } catch (e) {}

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .45,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        DropDownListDialog(
                          context: context,
                          title: opt.value.title,
                          h1: "Select income type",
                          heading: "Income type",
                          ddTitleList: dd,
                          vPadding: 3,
                          callback: (optionItem) {
                            opt.value = optionItem;
                          },
                        ),
                        SizedBox(height: 10),
                        drawInputCurrencyBox(
                          context: context,
                          tf: amount,
                          hintTxt: 0,
                          labelColor: Colors.black,
                          labelTxt: "Amount per year",
                          isBold: true,
                          len: 10,
                          focusNode: null,
                          focusNodeNext: null,
                          onChange: null,
                        ),
                        SizedBox(height: 10),
                        InputW2N(
                          title: "Notes",
                          input: note,
                          ph: "",
                          kbType: TextInputType.text,
                          len: 255,
                        ),
                        SizedBox(height: 5),
                        MMBtn(
                            txt: id == 0 ? "Add" : "Update",
                            height: 35,
                            width: null,
                            callback: () async {
                              if (opt.value.id == null) {
                                callbackFailed("Please select income type");
                              } else if (amount.text.isEmpty) {
                                callbackFailed("Please enter valid amount");
                              } else {
                                var _amount = 0;
                                try {
                                  _amount = int.parse(amount.text);
                                } catch (e) {}
                                callbackSuccess(MortgageUserInCome.fromJson({
                                  "Id": id,
                                  "UserId": userData.userModel.id,
                                  "CompanyId":
                                      userData.userModel.userCompanyInfo.id,
                                  "Status": 0,
                                  "MortgageCaseInfoId": 0,
                                  "TaskId": taskId,
                                  "CreationDate": DateTime.now().toString(),
                                  "Name": "",
                                  "InComeType": opt.value.title,
                                  "AmountPerYear": _amount.toDouble(),
                                  "Remarks": note.text.trim(),
                                }));
                                Get.back();
                              }
                            })
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.money_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
