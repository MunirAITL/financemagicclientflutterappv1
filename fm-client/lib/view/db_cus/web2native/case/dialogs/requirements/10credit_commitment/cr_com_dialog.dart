import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/MortgageUserAffordAbility.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/PostCreditCommitAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step10_cr_commit_ctrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

reqStep10CrComDialog({
  @required BuildContext context,
  int taskId,
  MortgageUserAffordAbility model2,
  @required Function(String) callbackFailed,
}) {
  final mainCrCommitCtrl = Get.put(ReqStep10CrCommitCtrl());
  DropListModel dd = DropListModel([
    OptionItem(id: "1", title: "Credit Card"),
    OptionItem(id: "1", title: "Unsecured Loan"),
    OptionItem(id: "1", title: "Hire Purchase"),
    OptionItem(id: "1", title: "Car Loan"),
    OptionItem(id: "1", title: "Secured Loan"),
    OptionItem(id: "1", title: "Student Loan"),
    OptionItem(id: "1", title: "Private Loans"),
    OptionItem(id: "1", title: "Store Card"),
    OptionItem(id: "1", title: "Others"),
  ]);
  var opt = OptionItem(id: null, title: "Select finance type").obs;

  final finProvider = TextEditingController();
  final outstandingBalance = TextEditingController();
  var creditLimit = TextEditingController();
  var monthlyPay = TextEditingController().obs;

  var repayB4RBIndex = 1.obs;

  var dtEnd = "".obs;
  var tick2Cal = false.obs;

  var id = 0;
  try {
    try {
      dtEnd.value = DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy");
    } catch (e) {}
    if (model2 != null) {
      id = model2.id;
      try {
        opt = OptionItem(id: "1", title: model2.typeOfFinance).obs;
      } catch (e) {}
      try {
        finProvider.text = model2.financeProvider ?? '';
      } catch (e) {}
      try {
        outstandingBalance.text = model2.outstandingBalance.toStringAsFixed(0);
      } catch (e) {}
      try {
        creditLimit.text = model2.creditLimit.toStringAsFixed(0);
      } catch (e) {}
      try {
        monthlyPay.value.text = model2.monthlyPayment.toStringAsFixed(2);
      } catch (e) {}
      try {
        tick2Cal.value = model2.isAddItemForAffordabilityCalculation == "false"
            ? false
            : true;
      } catch (e) {}
      try {
        dtEnd.value = DateFun.getDate(model2.endDate, "dd-MMM-yyyy");
      } catch (e) {}
      try {
        repayB4RBIndex.value = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.repayBeforeMortgage);
      } catch (e) {}
    }
  } catch (e) {}

  final DateTime now = DateTime.now();
  final old = DateTime(now.year - 100, now.month, now.day);
  final next = DateTime(now.year + 10, now.month, now.day);

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ),
    child: Container(
      height: MediaQuery.of(context).size.height * .7,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Scrollbar(
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Obx(
                      () => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Txt(
                                txt: id == 0
                                    ? "Add Credit Commitments"
                                    : "Edit Credit Commitments",
                                txtColor: Colors.black,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                          SizedBox(height: 15),
                          DropDownListDialog(
                            context: context,
                            title: opt.value.title,
                            h1: "Select finance type",
                            heading:
                                "Type of Finance(i.e.: Credit card, unsecured loan, hire purchase, PCP car loan, etc)",
                            ddTitleList: dd,
                            vPadding: 3,
                            callback: (optionItem) {
                              opt.value = optionItem;
                            },
                          ),
                          SizedBox(height: 10),
                          InputW2N(
                            title:
                                "Finance Provider (Name of the finance company)",
                            input: finProvider,
                            isBold: true,
                            ph: "",
                            kbType: TextInputType.streetAddress,
                            len: 100,
                          ),
                          SizedBox(height: 3),
                          drawInputCurrencyBox(
                              context: context,
                              tf: outstandingBalance,
                              hintTxt: null,
                              labelColor: Colors.black,
                              labelTxt:
                                  "How much is left to pay off? (approximate balance)",
                              isBold: true,
                              len: 10,
                              focusNode: null,
                              focusNodeNext: null,
                              onChange: (v) {
                                try {
                                  final val = int.parse(v);
                                  if (val > 0) {
                                    monthlyPay.value.text =
                                        (val * .03).toStringAsFixed(2);
                                  } else {
                                    monthlyPay.value.clear();
                                  }
                                } catch (e) {
                                  monthlyPay.value.clear();
                                }
                              }),
                          SizedBox(height: 10),
                          drawInputCurrencyBox(
                            context: context,
                            tf: creditLimit,
                            hintTxt: null,
                            labelColor: Colors.black,
                            labelTxt: "Credit Limit (For credit cards only)",
                            isBold: true,
                            len: 10,
                            focusNode: null,
                            focusNodeNext: null,
                          ),
                          SizedBox(height: 10),
                          drawInputCurrencyBox(
                            context: context,
                            tf: monthlyPay.value,
                            hintTxt: null,
                            labelColor: Colors.black,
                            labelTxt:
                                "Monthly payment (for credit card, please calculate 3% of the outstanding balance)",
                            isBold: true,
                            len: 10,
                            focusNode: null,
                            focusNodeNext: null,
                          ),
                          SizedBox(height: 10),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: "Repay before mortgage completes?",
                              list: W2NLocalData.listYesNoRB,
                              index: repayB4RBIndex.value,
                              callback: (index) {
                                repayB4RBIndex.value = index;
                              }),
                          SizedBox(height: 10),
                          DatePickerView(
                            cap: "End Date",
                            dt: (dtEnd.value == '')
                                ? 'Select Date'
                                : dtEnd.value,
                            padding: 5,
                            radius: 5,
                            txtColor: Colors.black,
                            fontWeight: FontWeight.bold,
                            initialDate: now,
                            firstDate: old,
                            lastDate: next,
                            callback: (value) {
                              try {
                                dtEnd.value = DateFormat('dd-MMM-yyyy')
                                    .format(value)
                                    .toString();
                              } catch (e) {}
                            },
                          ),
                          SizedBox(height: 10),
                          Theme(
                            data: MyTheme.radioThemeData,
                            child: Transform.translate(
                              offset: Offset(-10, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Checkbox(
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                      splashRadius: 20,
                                      fillColor: MaterialStateProperty.all(
                                          MyTheme.radioBGColor),
                                      focusColor: Colors.white,
                                      checkColor: Colors.white,
                                      value: tick2Cal.value,
                                      onChanged: (value) {
                                        tick2Cal.value = value;
                                      }),
                                  Flexible(
                                    child: Txt(
                                        txt:
                                            "Tick this box to add this item for Affordability Calculation",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize - .4,
                                        txtAlign: TextAlign.start,
                                        isBold: true),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          MMBtn(
                              txt: id == 0 ? "Save" : "Update",
                              height: 35,
                              width: null,
                              callback: () async {
                                if (opt.value.id == null) {
                                  callbackFailed(
                                      "Please select Type of Finance");
                                } else if (finProvider.text.isEmpty) {
                                  callbackFailed(
                                      "Please enter valid Finance Provider Name");
                                } else if (outstandingBalance.text.isEmpty) {
                                  callbackFailed(
                                      "Please enter valid Outstanding Balance Amount");
                                } else if (monthlyPay.value.text.isEmpty) {
                                  callbackFailed(
                                      "Please enter valid Monthly Payment Amount");
                                } else if (dtEnd.value.isEmpty) {
                                  callbackFailed("Please select end date");
                                } else {
                                  var _outBalance = 0;
                                  var _crLimit = 0;
                                  var _monPay = 0.0;
                                  try {
                                    _outBalance =
                                        int.parse(outstandingBalance.text);
                                  } catch (e) {}
                                  try {
                                    _crLimit = int.parse(creditLimit.text);
                                  } catch (e) {}
                                  try {
                                    _monPay =
                                        double.parse(monthlyPay.value.text);
                                  } catch (e) {}

                                  await APIViewModel().req<
                                          PostCreditCommitAPIModel>(
                                      context: context,
                                      url: id == 0
                                          ? SrvW2NCase
                                              .POST_CREDIT_COMMITMENT_URL
                                          : SrvW2NCase
                                              .PUT_CREDIT_COMMITMENT_URL,
                                      param: {
                                        "Id": id ?? 0,
                                        "UserId": userData.userModel.id,
                                        "CompanyId": userData
                                            .userModel.userCompanyInfo.id,
                                        "TaskId": taskId,
                                        "Status": 0,
                                        "CreationDate":
                                            DateTime.now().toString(),
                                        "MortgageCaseInfoId": 0,
                                        "TypeOfFinance": opt.value.title,
                                        "FinanceProvider": finProvider.text,
                                        "OutstandingBalance": _outBalance,
                                        "CreditLimit": _crLimit,
                                        "MonthlyPayment": _monPay,
                                        "Secured": "",
                                        "RepayBeforeMortgage": W2NLocalData
                                            .listYesNoRB[repayB4RBIndex.value],
                                        "Remarks": "",
                                        "CustomerName": "",
                                        "EndDate": dtEnd.value,
                                        "EndDateDD": "",
                                        "EndDateMM": "",
                                        "EndDateYY": "",
                                        "Notes": "",
                                        "IsAddItemForAffordabilityCalculation":
                                            tick2Cal.value ? "true" : "false",
                                      },
                                      reqType:
                                          id == 0 ? ReqType.Post : ReqType.Put,
                                      callback: (model) async {
                                        if (id == 0) {
                                          //  add
                                          mainCrCommitCtrl.crCommitCtrl.add(
                                              model.responseData
                                                  .mortgageUserAffordAbility);
                                          Get.back(
                                              result:
                                                  "Credit commitment added successfully.");
                                        } else {
                                          //  edit
                                          mainCrCommitCtrl.crCommitCtrl
                                              .remove(model2);
                                          mainCrCommitCtrl.crCommitCtrl.add(
                                              model.responseData
                                                  .mortgageUserAffordAbility);
                                          Get.back(
                                              result:
                                                  "Credit commitment updated successfully.");
                                        }
                                      });
                                }
                              })
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.credit_score,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
