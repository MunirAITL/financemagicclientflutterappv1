import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../../../../view_model/rx/web2native/requirements/main/req_step5_dependent_ctrl.dart';
import '../../../../../../widgets/btn/MMBtn.dart';
import '../../../../../../widgets/dialog/DatePickerView.dart';
import '../../../../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../../../../widgets/input/InputW2N.dart';

dependentDialog({
  @required BuildContext context,
  @required String title,
  @required TextEditingController name,
  @required TextEditingController livingWith,
  @required Function callbackSuccess,
  @required Function callbackFailed,
}) {
  final DateTime dateNow = DateTime.now();
  final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
  final dateDOBfirst = DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
  final dependentController = Get.put(ReqStep5DependentCtrl());
  return Dialog(
    /*shape: RoundedRectangleBorder(
                side: BorderSide(color: MyTheme.dBlueAirColor),
                borderRadius: BorderRadius.all(Radius.circular(5))),*/
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .7,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Txt(
                              txt: title,
                              txtColor: Colors.black,
                              txtSize: 2,
                              txtAlign: TextAlign.center,
                              isBold: true),
                        ),
                        SizedBox(height: 10),
                        InputW2N(
                          title: "Name",
                          ph: "",
                          input: name,
                          kbType: TextInputType.name,
                          len: 50,
                        ),
                        DatePickerView(
                          cap: "Date of birth",
                          dt: (dependentController.dob.value == '')
                              ? 'Select Date'
                              : dependentController.dob.value,
                          txtColor: Colors.black,
                          initialDate: dateDOBlast,
                          firstDate: dateDOBfirst,
                          lastDate: dateDOBlast,
                          padding: 5,
                          radius: 5,
                          callback: (value) {
                            try {
                              dependentController.dob.value =
                                  DateFormat('dd-MMM-yyyy')
                                      .format(value)
                                      .toString();
                            } catch (e) {}
                          },
                        ),
                        SizedBox(height: 5),
                        DropDownListDialog(
                          context: context,
                          h1: "Select relationship",
                          heading: "Relationship",
                          title:
                              dependentController.optRelationShip.value.title,
                          ddTitleList: dependentController.ddRelationShip.value,
                          callback: (optionItem) {
                            dependentController.optRelationShip.value =
                                optionItem;
                          },
                        ),
                        SizedBox(height: 5),
                        InputW2N(
                          title: "Living With",
                          ph: "",
                          input: livingWith,
                          kbType: TextInputType.text,
                          len: 50,
                        ),
                        SizedBox(height: 20),
                        MMBtn(
                            txt: "Save",
                            height: 35,
                            width: null,
                            callback: () {
                              if (name.text.trim().isEmpty) {
                                callbackFailed("Please enter valid name");
                              } else if (dependentController.dob.value == '') {
                                callbackFailed("Please select date of birth");
                              } else if (dependentController
                                      .optRelationShip.value.id ==
                                  null) {
                                callbackFailed(
                                    "Please enter valid relationship info");
                              } else if (livingWith.text.trim().isEmpty) {
                                callbackFailed(
                                    "Please enter valid living information");
                              } else {
                                callbackSuccess();
                                Get.back();
                              }
                            })
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.family_restroom_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
