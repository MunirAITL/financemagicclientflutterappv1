import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:intl/intl.dart';
import '../../../../../../model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';
import '../../../../../../view_model/rx/web2native/requirements/req_mortgage_ctrl.dart';
import '../../../../../../view_model/rx/web2native/requirements/req_remortgage_ctrl.dart';
import '../../../../../widgets/btn/MMBtn.dart';
import '../../../../../widgets/dialog/ConfirmationDialog.dart';
import '../../../../../widgets/dropdown/DropDownListDialog.dart';
import 'package:get/get.dart';
import '../../../../../widgets/input/InputW2N.dart';
import '../../../../../widgets/input/drawInputCurrencyBox.dart';

reqReMortgageDialog({
  @required BuildContext context,
  @required String title,
  @required TextEditingController amount,
  bool isNew = true,
  @required Function(String) callbackSuccess,
  @required Function callbackFailed,
}) {
  final requirementController = Get.put(ReqReMortgageCtrl());
  final focus = FocusNode();
  final other = TextEditingController();
  try {
    if (requirementController.optReq.value.title == "Other") {
      other.text = requirementController.optReq.value.id;
    }
  } catch (e) {}
  return Dialog(
    /*shape: RoundedRectangleBorder(
                side: BorderSide(color: MyTheme.dBlueAirColor),
                borderRadius: BorderRadius.all(Radius.circular(5))),*/
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .5,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Txt(
                              txt: title,
                              txtColor: Colors.black,
                              txtSize: 2,
                              txtAlign: TextAlign.center,
                              isBold: true),
                        ),
                        SizedBox(height: 20),
                        DropDownListDialog(
                          context: context,
                          h1: "Select reason for additional loan",
                          heading: "Reason for additional loan",
                          title: requirementController.optReq.value.title,
                          ddTitleList: requirementController.ddReq.value,
                          vPadding: 3,
                          callback: (optionItem) {
                            other.clear();
                            requirementController.optReq.value = optionItem;
                          },
                        ),
                        requirementController.optReq.value.title == 'Other'
                            ? Padding(
                                padding: const EdgeInsets.only(top: 5),
                                child: InputW2N(
                                  title: null,
                                  ph: "Other reason",
                                  input: other,
                                  kbType: TextInputType.text,
                                  len: 50,
                                ))
                            : SizedBox(),
                        SizedBox(height: 5),
                        drawInputCurrencyBox(
                            context: context,
                            tf: amount,
                            hintTxt: 0,
                            labelColor: Colors.black,
                            labelTxt: "Amount",
                            isBold: true,
                            len: 10,
                            focusNode: focus,
                            focusNodeNext: null),
                        SizedBox(height: 20),
                        MMBtn(
                            txt: isNew ? "Add" : "Update",
                            height: 35,
                            width: null,
                            callback: () {
                              if (requirementController.optReq.value.id ==
                                  null) {
                                callbackFailed("Please select Deposit Source");
                              }
                              if (requirementController.optReq.value.title ==
                                      'Other' &&
                                  other.text.trim().isEmpty) {
                                callbackFailed("Please enter other reason");
                              } else if (amount.text.trim().isEmpty) {
                                callbackFailed(
                                    "Please enter valid Deposit value");
                              } else {
                                callbackSuccess(other.text.trim());
                                Get.back();
                              }
                            })
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.home_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}

drawMortgageReasonsItems(
    BuildContext context,
    List<MortgageLoanReasonsModel> listMortgageReasons,
    Function(MortgageLoanReasonsModel) callbackOnEdit,
    Function(MortgageLoanReasonsModel) callbackOnDel) {
  return (listMortgageReasons.length > 0)
      ? Container(
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 5),
                Material(
                  elevation: 2,
                  color: MyTheme.bgColor,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: callbackOnEdit != null ? 4 : 3,
                          child: Txt(
                              txt: "Reason for addition loan",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                        ),
                        Expanded(
                          child: Txt(
                              txt: "Amount",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                        ),
                        callbackOnEdit != null
                            ? Expanded(
                                child: Txt(
                                    txt: "Action",
                                    txtColor: Colors.white,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: true),
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                ),
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: []),
                ...listMortgageReasons.map((model) {
                  return _drawTableItems(
                      context, model, callbackOnEdit, callbackOnDel);
                })
              ],
            ),
          ),
        )
      : SizedBox();
}

_drawTableItems(
    BuildContext context,
    MortgageLoanReasonsModel model,
    Function(MortgageLoanReasonsModel) callbackOnEdit,
    Function(MortgageLoanReasonsModel) callbackOnDel) {
  final txt = (model.loanReasonName ?? '') +
      (model.remarks.length > 0 ? ' - ' : '') +
      (model.remarks ?? '');
  return Padding(
    padding: const EdgeInsets.only(top: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 4,
          child: Txt(
              txt: txt,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        Expanded(
          child: Txt(
              txt: UIHelper().addCurSign(model.amount),
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        callbackOnEdit != null
            ? Flexible(
                child: Row(
                  children: [
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            callbackOnEdit(model);
                          },
                          child: Icon(Icons.edit_outlined,
                              color: Colors.grey, size: 20)),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            confirmDialog(
                                context: context,
                                title: "Confirmation!!",
                                msg: "Are you sure to delete this item?",
                                callbackYes: () {
                                  callbackOnDel(model);
                                });
                          },
                          child: Icon(Icons.delete_outline,
                              color: Colors.grey, size: 20)),
                    ),
                  ],
                ),
              )
            : SizedBox()
      ],
    ),
  );
}
