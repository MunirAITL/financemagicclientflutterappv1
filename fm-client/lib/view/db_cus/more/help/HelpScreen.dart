import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/db_cus/more/help/SupportCentreScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtnLeftIconDashBoard.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class HelpScreen extends StatefulWidget {
  @override
  State createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> with Mixin {
  final bottomViewHeight = 32;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: true,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Help"),
          centerTitle: false,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(getHP(context, bottomViewHeight)),
            child: Container(
              height: getHP(context, bottomViewHeight),
              width: getW(context),
              color: Colors.white,
              child: Container(
                height: getHP(context, bottomViewHeight),
                width: getW(context),
                decoration: new BoxDecoration(
                    color: MyTheme.statusBarColor,
                    borderRadius: new BorderRadius.only(
                      bottomLeft: const Radius.circular(20),
                      bottomRight: const Radius.circular(20),
                    )),
                child: Container(
                  width: getWP(context, 15),
                  height: getWP(context, 15),
                  child: Image.asset(
                    "assets/images/img/help_header.png",
                    color: Colors.white,
                    //fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ),
          ),
        ),
        body: Container(
          //decoration: MyTheme.bgBlueColor,
          width: getW(context),
          height: getH(context),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: Column(
                children: [
                  SizedBox(height: 100),
                  Container(
                    width: getW(context),
                    child: MMBtnLeftIconDashBoard(
                      imageFile: null,
                      bgColor: MyTheme.lGrayColor,
                      txt: "Support Centre",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        Get.to(
                          () => SupportCentreScreen(),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: getW(context),
                    child: MMBtnLeftIconDashBoard(
                      imageFile: null,
                      bgColor: MyTheme.lGrayColor,
                      txt: "Application Tutorial",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        Navigator.pop(context);
                        StateProvider()
                            .notify(ObserverState.STATE_CHANGED_tabbar1);
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: getW(context),
                    child: MMBtnLeftIconDashBoard(
                      imageFile: null,
                      bgColor: MyTheme.statusBarColor,
                      txt: "Faq",
                      textColor: Colors.white,
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        Get.to(() => WebScreen(
                              title: "Faq",
                              url: Server.FAQ_URL,
                            ));
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
