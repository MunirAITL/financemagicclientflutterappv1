import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/utils/tokbox_cfg.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class CallWidget extends StatefulWidget {
  const CallWidget({Key key}) : super(key: key);
  @override
  State createState() => _CallWidgetState();
}

class _CallWidgetState extends State<CallWidget> with Mixin {
  SdkState _sdkState = SdkState.LOGGED_OUT;
  bool _publishAudio = true;
  bool _publishVideo = true;

  static const platformMethodChannel = const MethodChannel('com.vonage');

  @override
  void dispose() {
    onDestroy();
    super.dispose();
  }

  onDestroy() async {
    platformMethodChannel.invokeMethod('dispose');
  }

  _CallWidgetState() {
    platformMethodChannel.setMethodCallHandler(methodCallHandler);
    _initSession();
  }

  Future<dynamic> methodCallHandler(MethodCall methodCall) async {
    switch (methodCall.method) {
      case 'updateState':
        {
          setState(() {
            var arguments = 'SdkState.${methodCall.arguments}';
            _sdkState = SdkState.values.firstWhere((v) {
              return v.toString() == arguments;
            });
          });
        }
        break;
      default:
        throw MissingPluginException('notImplemented');
    }
  }

  Future<void> _initSession() async {
    await requestPermissions();

    dynamic params = {
      'apiKey': OpenTokConfig.API_KEY,
      'sessionId': OpenTokConfig.SESSION_ID,
      'token': OpenTokConfig.TOKEN
    };

    try {
      await platformMethodChannel.invokeMethod('initSession', params);
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<void> _makeCall() async {
    try {
      await requestPermissions();
      await platformMethodChannel.invokeMethod('makeCall');
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Future<void> requestPermissions() async {
    await [Permission.microphone, Permission.camera].request();
  }

  Future<void> _swapCamera() async {
    try {
      await platformMethodChannel.invokeMethod('swapCamera');
    } on PlatformException catch (e) {}
  }

  Future<void> _toggleAudio() async {
    _publishAudio = !_publishAudio;

    dynamic params = {'publishAudio': _publishAudio};

    try {
      await platformMethodChannel.invokeMethod('publishAudio', params);
    } on PlatformException catch (e) {}
  }

  Future<void> _toggleVideo() async {
    _publishVideo = !_publishVideo;

    dynamic params = {'publishVideo': _publishVideo};

    try {
      await platformMethodChannel.invokeMethod('toggleVideo', params);
      _updateView();
    } on PlatformException catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Video Chat"),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Center(
            child: _updateView(),
          ),
        ),
      ),
    );
  }

  Widget _updateView() {
    //bool toggleVideoPressed = false;

    if (_sdkState == SdkState.LOGGED_OUT) {
      return ElevatedButton(
          onPressed: () {
            _initSession();
          },
          child: Text("Init session"));
    } else if (_sdkState == SdkState.WAIT) {
      return Center(
        child: CircularProgressIndicator(color: MyTheme.dBlueAirColor),
      );
    } else if (_sdkState == SdkState.LOGGED_IN) {
      return Container(
        width: getW(context),
        height: getH(context),
        child: Column(
          children: [
            Container(
              //height: MediaQuery.of(context).size.height * 0.5,
              //width: getW(context),
              //height: getH(context) * .85,
              child: Expanded(
                child: PlatformViewLink(
                  viewType:
                      'opentok-video-container', // custom platform-view-type
                  surfaceFactory: (BuildContext context,
                      PlatformViewController controller) {
                    return AndroidViewSurface(
                      controller: controller,
                      gestureRecognizers: const <
                          Factory<OneSequenceGestureRecognizer>>{},
                      hitTestBehavior: PlatformViewHitTestBehavior.opaque,
                    );
                  },
                  onCreatePlatformView: (PlatformViewCreationParams params) {
                    return PlatformViewsService.initSurfaceAndroidView(
                      id: params.id,
                      viewType: 'opentok-video-container',
                      // custom platform-view-type,
                      layoutDirection: TextDirection.ltr,
                      creationParams: {},
                      creationParamsCodec: StandardMessageCodec(),
                    )
                      ..addOnPlatformViewCreatedListener(
                          params.onPlatformViewCreated)
                      ..create();
                  },
                ),
              ),
            ),
            /*Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: () {
                    _swapCamera();
                  },
                  child: Text("Swap " "Camera"),
                ),
                ElevatedButton(
                  onPressed: () {
                    _toggleAudio();
                  },
                  child: Text("Toggle Audio"),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.resolveWith<Color>(
                      (Set<MaterialState> states) {
                        if (!_publishAudio) return Colors.grey;
                        return null; // Use the component's default.
                      },
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    _toggleVideo();
                  },
                  child: Text("Toggle Video"),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.resolveWith<Color>(
                      (Set<MaterialState> states) {
                        if (!_publishVideo) return Colors.grey;
                        return null; // Use the component's default.
                      },
                    ),
                  ),
                ),
              ],
            ),*/
          ],
        ),
      );
    } else {
      return Center(child: Text("ERROR"));
    }
  }
}

enum SdkState { LOGGED_OUT, LOGGED_IN, WAIT, ON_CALL, ERROR }
