import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/config/db_cus/TimelineCfg.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/GetCaseGroupChatAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'chat/TimeLinePostScreen.dart';

abstract class BaseTimeline<T extends StatefulWidget> extends State<T>
    with Mixin {
  List<TimelineUserModel> listTimelineUserModelChat = [];
  List<CaseGroupMessageData> listCaseGroupMessageData = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  bool isGroup = false;

  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  int caseStatus = NewCaseCfg.ALL;

  onPageLoadChat();
  getRefreshDataChat();

  onPageLoadGroup();
  getRefreshDataGroup();

  drawChatView() {
    return Container(
      child: (listTimelineUserModelChat.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    pageStart++;
                    onPageLoadChat();
                  }
                }
                return true;
              },
              child: RefreshIndicator(
                color: Colors.white,
                backgroundColor: MyTheme.statusBarColor,
                onRefresh: getRefreshDataChat,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  primary: true,
                  child: listTimelineUserModelChat.length > 0
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: ListView.builder(
                            primary: false,
                            addAutomaticKeepAlives: true,
                            cacheExtent: AppConfig.page_limit.toDouble(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: listTimelineUserModelChat.length,
                            itemBuilder: (context, index) {
                              return drawChatItem(
                                  listTimelineUserModelChat[index], true);
                            },
                          ),
                        )
                      : SizedBox(),
                ),
              ),
            )
          : (!isLoading)
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: getWP(context, 100),
                          height: getHP(context, 48),
                          child: Image.asset(
                            'assets/images/screens/db_cus/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt: "You have no message yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                            txt: "Refresh",
                            width: null, //getWP(context, 50),
                            height: getHP(context, 6),
                            callback: () {
                              onPageLoadChat();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : SizedBox(),
    );
  }

  drawGroupChatView() {
    return Container(
      child: (listCaseGroupMessageData.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    pageStart++;
                    onPageLoadGroup();
                  }
                }
                return true;
              },
              child: RefreshIndicator(
                color: Colors.white,
                backgroundColor: MyTheme.statusBarColor,
                onRefresh: getRefreshDataGroup,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  primary: true,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: ListView.builder(
                          primary: false,
                          addAutomaticKeepAlives: true,
                          cacheExtent: AppConfig.page_limit.toDouble(),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemCount: listCaseGroupMessageData.length,
                          itemBuilder: (context, index) {
                            return drawGroupChatItem(
                                listCaseGroupMessageData[index]);
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          : (!isLoading)
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: getWP(context, 100),
                          height: getHP(context, 48),
                          child: Image.asset(
                            'assets/images/screens/db_cus/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt: "You have no message yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                            txt: "Refresh",
                            width: null, //getWP(context, 50),
                            height: getHP(context, 6),
                            callback: () {
                              onPageLoadGroup();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : SizedBox(),
    );
  }

  drawChatItem(TimelineUserModel timelineUserModel, bool isChat) {
    try {
      final unreadMessageCount = (timelineUserModel.unreadMessageCount == 0);
      final doubletik =
          (unreadMessageCount) ? "check2_icon.png" : "check1_icon.png";
      String lastLoginDate = timelineUserModel.lastChatDateTime ?? '';
      bool isHideFieldsIfLessByYear = false;
      try {
        DateTime checkedTime = DateTime.parse(lastLoginDate);
        DateTime currentTime = DateTime.now();
        final diff = currentTime.difference(checkedTime);
        var year = ((diff.inDays) / 365).round();
        if (year > TimelineCfg.hideFieldsIfLessByYear) {
          isHideFieldsIfLessByYear = true;
        }
      } catch (e) {}
      try {
        //lastLoginDate = DateFun.getDateWhatsAppFormat(lastLoginDate);
      } catch (e) {}
      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
                () => TimeLinePostScreen(timelineUserModel: timelineUserModel));
          } catch (e) {
            myLog(e.toString());
          }
        },
        child: Container(
          color: Colors.transparent,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        isChat
                            ? CircleAvatar(
                                radius: 20,
                                backgroundColor: Colors.transparent,
                                backgroundImage: CachedNetworkImageProvider(
                                  MyNetworkImage.checkUrl(
                                      (timelineUserModel.profileImageUrl !=
                                              null)
                                          ? timelineUserModel.profileImageUrl
                                          : Server.MISSING_IMG),
                                ),
                              )
                            : Container(
                                width: 40,
                                height: 40,
                                decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: CachedNetworkImageProvider(
                                        MyNetworkImage.checkUrl(
                                            (timelineUserModel
                                                        .profileImageUrl !=
                                                    null)
                                                ? timelineUserModel
                                                    .profileImageUrl
                                                : Server.MISSING_IMG),
                                      ),
                                    ))),
                        SizedBox(width: 20),
                        Expanded(
                          flex: (!isHideFieldsIfLessByYear) ? 4 : 8,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //SizedBox(height: 5),
                              Txt(
                                  txt: timelineUserModel.name,
                                  txtColor: Color(0xFF2D4665),
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  fontWeight: FontWeight.w500,
                                  isBold: false),
                              SizedBox(height: 5),
                              Txt(
                                  txt: timelineUserModel.community,
                                  //caseModel.companyName,
                                  txtColor: Color(0xFF3D3D3D),
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              SizedBox(height: 5),
                              (!isHideFieldsIfLessByYear &&
                                      timelineUserModel.lastMessage.length > 0)
                                  ? Row(
                                      children: [
                                        Image.asset(
                                          "assets/images/icons/" + doubletik,
                                          width: 20,
                                          height: 20,
                                          color: (unreadMessageCount)
                                              ? Colors.green
                                              : Colors.black,
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(
                                            child: Txt(
                                          txt: timelineUserModel.lastMessage,
                                          txtColor: Colors.grey,
                                          txtSize: 1.5,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                          isOverflow: true,
                                        )),
                                      ],
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                        //Spacer(),
                        //SizedBox(width: 10),
                        (!isHideFieldsIfLessByYear)
                            ? Expanded(
                                flex: 4,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 5),
                                    Container(
                                      //color: Colors.black,
                                      child: Txt(
                                          txt: DateFun.getTimeAgoTxt(
                                              lastLoginDate),
                                          txtColor: Colors.grey,
                                          txtSize: MyTheme.txtSize - .6,
                                          txtAlign: TextAlign.end,
                                          isBold: false),
                                    ),
                                    SizedBox(height: 20),
                                    timelineUserModel.unreadMessageCount > 0
                                        ? Container(
                                            width: 20,
                                            height: 20,
                                            child: Center(
                                              child: Txt(
                                                  txt: (timelineUserModel
                                                          .unreadMessageCount)
                                                      .toString(),
                                                  txtColor: Colors.white,
                                                  txtSize: MyTheme.txtSize - .8,
                                                  txtAlign: TextAlign.start,
                                                  isBold: true),
                                            ),
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: MyTheme.brandColor),
                                          )
                                        : SizedBox(),
                                  ],
                                ),
                              )
                            : SizedBox(),
                        //SizedBox(width: 5),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 10, left: 10, right: 10, bottom: 10),
                      child: Container(color: Colors.grey, height: .5),
                    ),
                    //SizedBox(height: 20),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
  }

  drawGroupChatItem(CaseGroupMessageData groupMsg) {
    try {
      if (groupMsg == null) return SizedBox();
      var icon = NewCaseHelper().getCreateCaseIconByTitle(groupMsg.title);
      if (icon == null) return SizedBox();
      final width = getW(context);

      final unreadMessageCount = 0;
      //(caseModel.notificationUnreadTaskCount == 0);
      final doubletik = (groupMsg.isRead.toLowerCase() == 'true')
          ? "check2_icon.png"
          : "check1_icon.png";
      String lastLoginDate = groupMsg.lastMessageDateTime ?? '';
      bool isHideFieldsIfLessByYear = false;
      try {
        DateTime checkedTime = DateTime.parse(lastLoginDate);
        DateTime currentTime = DateTime.now();
        final diff = currentTime.difference(checkedTime);
        var year = ((diff.inDays) / 365).round();
        if (year > TimelineCfg.hideFieldsIfLessByYear) {
          isHideFieldsIfLessByYear = true;
        }
      } catch (e) {}
      try {
        //lastLoginDate = DateFun.getDateWhatsAppFormat(lastLoginDate);
      } catch (e) {}
      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
              () => TimeLinePostScreen(groupMsg: groupMsg),
            );
          } catch (e) {
            myLog(e.toString());
          }
        },
        child: Container(
          color: Colors.transparent,
          child: Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: width * 0.1,
                      height: width * 0.1,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          gradient: LinearGradient(
                              colors: [
                                HexColor.fromHex("#2B4564"),
                                HexColor.fromHex("#112B4A"),
                              ],
                              begin: const FractionalOffset(0.0, 0.0),
                              end: const FractionalOffset(0.0, 0.0),
                              stops: [0.0, 1.0],
                              tileMode: TileMode.clamp)),
                      child: Image.asset(icon),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //SizedBox(height: 5),
                          Txt(
                              txt: groupMsg.title,
                              txtColor: Color(0xFF2D4665),
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: false),
                          SizedBox(height: 5),
                          Txt(
                              txt: "Case ID: ${groupMsg.id}",
                              txtColor: Color(0xFF3D3D3D),
                              txtSize: MyTheme.txtSize - .4,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w400,
                              maxLines: 1,
                              isBold: false),
                          SizedBox(height: 5),
                          (!isHideFieldsIfLessByYear &&
                                  groupMsg.lastMessage.length > 0)
                              ? Row(
                                  children: [
                                    Image.asset(
                                      "assets/images/icons/" + doubletik,
                                      width: 20,
                                      height: 18,
                                      color: (groupMsg.isRead.toLowerCase() ==
                                              'true')
                                          ? Color(0xFF00D1FF)
                                          : Colors.black,
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                        child: Txt(
                                      txt: groupMsg.lastMessage,
                                      txtColor: Color(0xFF848484),
                                      txtSize: 1.5,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                      isOverflow: true,
                                    )),
                                  ],
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                    //Spacer(),
                    //SizedBox(width: 10),
                    (!isHideFieldsIfLessByYear)
                        ? Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(height: 5),
                                Txt(
                                    txt: DateFun.getTimeAgoTxt(lastLoginDate),
                                    txtColor: Colors.grey,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.end,
                                    isBold: false),
                                /*SizedBox(height: 20),
                                caseModel.notificationUnreadTaskCount > 0
                                    ? Container(
                                        width: 30,
                                        height: 30,
                                        child: Center(
                                          child: Txt(
                                              txt: (caseModel
                                                      .notificationUnreadTaskCount)
                                                  .toString(),
                                              txtColor: Colors.white,
                                              txtSize: MyTheme.txtSize - .4,
                                              txtAlign: TextAlign.start,
                                              isBold: true),
                                        ),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: MyTheme.brandColor),
                                      )
                                    : SizedBox(
                                        
                                      ),*/
                              ],
                            ),
                          )
                        : SizedBox(),
                    //SizedBox(width: 5),
                  ],
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                  child: Container(color: Colors.grey, height: .5),
                ),
                //SizedBox(height: 20),
              ],
            ),
          ),
        ),
      );
    } catch (e) {}
  }
}
