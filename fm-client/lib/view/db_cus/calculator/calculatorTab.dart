import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/MoreHelper.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';

class CalculatorTab extends StatefulWidget {
  @override
  State createState() => _MoreTabState();
}

class _MoreTabState extends State<CalculatorTab> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          title: UIHelper().drawAppbarTitle(title: "Calculator"),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
            child: ListView.builder(
              itemCount: MoreHelper.listMore.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> mapMore = MoreHelper.listMore[index];
                return GestureDetector(
                    onTap: () async {},
                    child: Center(
                        child: Container(
                            child: Txt(
                                txt: "Working on it ",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: true))));
              },
            ),
          ),
        ),
      ),
    );
  }
}
