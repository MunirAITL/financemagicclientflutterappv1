import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/view/widgets/txt/DottedText.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogAccountAndAlerts.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class MyScoreHistoryGraph extends StatelessWidget with Mixin {
  GetSummaryResponseData getSummaryResponse;

  TooltipBehavior _tooltipBehavior;

  List<SalesData> listData = [];

  @override
  Widget build(BuildContext context) {
    getSummaryResponse = CreditDashBoardTabControllerState.getSummaryResponse;
    _tooltipBehavior = TooltipBehavior(
      enable: true,
      color: Colors.white,
      canShowMarker: true,
      shouldAlwaysShow: true,
    );

    listData.clear();
    if (getSummaryResponse.summaryReport != null) {
      for (var list in getSummaryResponse
          .summaryReport.getSummaryReportResult.scoreHistory.score) {
        final yy = DateFun.getDate(list.scoreDate, "MMM");
        listData.add(SalesData(yy, int.parse(list.scoreValue)));
      }
    }

    final txtStyle = TextStyle(
      color: Colors.black,
      fontFamily: 'Roboto',
      fontSize: 12,
      fontStyle: FontStyle.italic,
    );
    return Card(
      elevation: 10,
      child: Container(
        width: getW(context),
        child: Column(
          children: [
            SizedBox(
              height: 15,
            ),
            GestureDetector(
              onTap: () {
                showToolTips(
                    context: context,
                    w: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Txt(
                              txt: "Your Score History",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: true),
                        ),
                        SizedBox(height: 10),
                        Txt(
                            txt:
                                "A record of your latest credit score will appear on the score history graph when your credit report refreshes.",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 10),
                        Txt(
                            txt: "Your credit report will refresh when:",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 10),
                        DottedText(
                          "You log in after 28 days from the date it last refreshed",
                          textAlign: TextAlign.start,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                        SizedBox(height: 10),
                        DottedText(
                          "You log in after receiving a Credit alert email notification",
                          textAlign: TextAlign.start,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                        SizedBox(height: 10),
                        DottedText(
                          "You log in after a dispute has completed successfully.",
                          textAlign: TextAlign.start,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                        SizedBox(height: 10),
                        Txt(
                            txt:
                                "Note: Your score history graph is for your reference to help track progress and is not visible to lenders.",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ));
              },
              child: Container(
                //color: HexColor.fromHex("#00A6CA"),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Icon(Icons.timer_rounded, color: Colors.black),
                      SizedBox(width: 10),
                      Txt(
                          txt: "My Score History",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: true),
                      SizedBox(width: 10),
                      Icon(Icons.info_outlined, color: Colors.grey),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: SfCartesianChart(
                    backgroundColor:
                        Colors.white, //HexColor.fromHex("#00A6CA"),

                    primaryXAxis: CategoryAxis(
                      isInversed: false,
                      labelStyle: txtStyle,
                      tickPosition: TickPosition.inside,
                      axisLine: AxisLine(color: Colors.black, width: 1),
                      majorGridLines: MajorGridLines(
                        width: .5,
                        color: Colors.black,
                      ),
                    ),
                    primaryYAxis: NumericAxis(
                        axisLine: AxisLine(color: Colors.black, width: 1),
                        isInversed: false,
                        labelStyle: txtStyle,
                        minimum: 0,
                        maximum: 600,
                        rangePadding: ChartRangePadding.none,
                        majorGridLines: MajorGridLines(
                            width: .5,
                            color: Colors.black,
                            dashArray: <double>[1, 1]),
                        minorGridLines: MinorGridLines(
                          width: .5,
                          color: Colors.black,
                        )),
                    // Enable tooltip
                    tooltipBehavior: _tooltipBehavior,
                    series: <LineSeries<SalesData, String>>[
                      LineSeries<SalesData, String>(
                          color: Colors.red,
                          markerSettings: MarkerSettings(
                              isVisible: true, color: Colors.red),
                          dataSource: listData,
                          xValueMapper: (SalesData sales, _) => sales.year,
                          yValueMapper: (SalesData sales, _) => sales.sales,
                          // Enable data label
                          dataLabelSettings: DataLabelSettings(
                              isVisible: true, textStyle: txtStyle))
                    ])),
          ],
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final int sales;
}
