import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/OtherSeaches.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/YourSearches.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class CreditReportSearchHistory extends StatefulWidget {
  @override
  State<CreditReportSearchHistory> createState() =>
      _CreditReportSearchHistoryState();
}

class _CreditReportSearchHistoryState extends State<CreditReportSearchHistory>
    with Mixin {
  bool isOthersSearches = true;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Container(
        padding: EdgeInsets.all(10),
        width: getW(context),
        child: Column(
          children: [
            Container(
                width: getW(context),
                child: Txt(
                    txt: "Credit Report Search History",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true)),
            SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Btn(
                      txt: "Other's Searches",
                      txtSize: 1.5,
                      width: getWP(context, 45),
                      height: getHP(context, 5),
                      bgColor: (isOthersSearches) ? Colors.orange : Colors.grey,
                      txtColor:
                          (isOthersSearches) ? Colors.white : Colors.black,
                      callback: () {
                        setState(() {
                          isOthersSearches = true;
                        });
                      }),
                ),
                Flexible(
                  child: Btn(
                      txt: "Your Searches",
                      txtSize: 1.5,
                      width: getWP(context, 45),
                      height: getHP(context, 5),
                      bgColor:
                          (!isOthersSearches) ? Colors.orange : Colors.grey,
                      txtColor:
                          (!isOthersSearches) ? Colors.white : Colors.black,
                      callback: () {
                        setState(() {
                          isOthersSearches = false;
                        });
                      }),
                ),
              ],
            ),
            SizedBox(height: 20),
            isOthersSearches
                ? Column(
                    children: [
                      Txt(
                          txt: "Others Searches",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      Txt(
                          txt:
                              "Queries on your details via TransUnion within the last 24 months. These searches may appear when a third party reviews the details in TransUnion.",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Searches on your current address",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      Txt(
                          txt:
                              "No searches for this address have been recorded in the past 24 months.",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 10),
                    ],
                  )
                : Column(
                    children: [
                      Txt(
                          txt: "Your Searches",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      Txt(
                          txt:
                              "Searches you've done on your credit report about TransUnion in the last 24 months. Lenders don't see these searches during your credit application.",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Searches on your current address",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      Txt(
                          txt:
                              "You haven't searched your report in the last 24 months.",
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 10),
                    ],
                  ),
            isOthersSearches ? OtherSeaches() : YourSeaches()
          ],
        ),
      ),
    );
  }
}
