import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class ConnectionOthersName extends StatelessWidget with Mixin {
  const ConnectionOthersName({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "Connections & Other Names",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 20),
              Txt(
                  txt: "Financial connections",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "Details of those you share a financial connection with.\n\nThere are no financial connections on your report.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "Other names",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "Other names such as your maiden name or variations of your full name that you may have been known by on the financial accounts that appear on your credit report now.\n\nThere are no other names recorded for your report.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 20),
              Txt(
                  txt: "Electoral Register",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "This displays the dates your name was registered on the Electoral Register at your current address. The local council supplies us the information.\n\nPlease note that your credit report only shows your most recent uninterrupted Electoral Register data. Lenders can see more historic data, but this isn't shown on your report.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 20),
              Txt(
                  txt: "Public Information",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "Bankruptcies, Insolvencies or Court Judgments that are in your name are detailed here.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "Bankruptcies and Insolvencies",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "There are no Bankruptcies or Insolvencies recorded on your report.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "Judgments",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt: "There are no Judgments recorded on your report.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 20),
              Txt(
                  txt: "Notices of Correction",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "Details of any statements you request to be added to your credit report explaining items of information on your report that are in fact correct, but you believe would give a misleading impression.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "Notices recorded on your report",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: true),
              SizedBox(height: 20),
              Txt(
                  txt:
                      "Your report has no Notices of Correction.\n\nHow do I add, amend or remove a Notice of Correction from my credit report?",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false),
            ],
          ),
        ));
  }
}
