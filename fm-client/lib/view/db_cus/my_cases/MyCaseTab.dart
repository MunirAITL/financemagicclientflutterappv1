import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/my_cases/MyCasesAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/MyCasesHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/view/db_cus/more/reviews/ReviewRatingListPage.dart';
import 'package:aitl/view/db_cus/new_case/NewCaseTab.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'MyCasesAppbarNew.dart';

class MyCaseTab extends StatefulWidget {
  MyCaseTab({
    Key key,
  }) : super(key: key);

  @override
  State createState() => MyCaseTabState();
}

class MyCaseTabState extends State<MyCaseTab>
    with Mixin, SingleTickerProviderStateMixin, StateListener {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Task> listTaskInfoSearchModel = [];

  StateProvider _stateProvider;

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  //  tab stuff start here
  int caseStatus = NewCaseCfg.ALL;

  @override
  void onStateChangedWithData(state, data) async {
    try {
      if (state == ObserverState.STATE_BADGE_TASK_COUNT && data == 1) {
        if (mounted) {
          final numberOfPendingTask =
              appData.taskNotificationCountAndChatUnreadCountData != null
                  ? appData.taskNotificationCountAndChatUnreadCountData
                      .numberOfPendingTask
                  : 0;
          if (numberOfPendingTask > 0) {
            appData.taskNotificationCountAndChatUnreadCountData
                .numberOfPendingTask = 0;
            await PrefMgr.shared
                .setPrefInt("NumberOfPendingTask", numberOfPendingTask);
          }
          StateProvider().notifyWithData(ObserverState.STATE_BOTNAV, null);
          //setState(() {});
        }
      }
    } catch (e) {}
  }

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar2_reload_case_api) {
      _getRefreshData();
    }
  }

  onPageLoad() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        MyCasesAPIMgr().wsOnPageLoad(
          context: context,
          pageStart: pageStart,
          pageCount: pageCount,
          caseStatus: caseStatus,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    final List<dynamic> locations =
                        model.responseData.locations;

                    if (locations != null && mounted) {
                      //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                      if (locations.length != pageCount) {
                        isPageDone = true;
                      }
                      try {
                        for (Task location in locations) {
                          listTaskInfoSearchModel.add(location);
                        }
                      } catch (e) {
                        myLog(e.toString());
                      }
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    } else {
                      if (mounted) {
                        if (mounted) {
                          setState(() {
                            isLoading = false;
                          });
                        }
                      }
                    }
                  } catch (e) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                    myLog(e.toString());
                  }
                } else {
                  try {
                    //final err = model.errorMessages.login[0].toString();
                    if (mounted) {
                      isLoading = false;
                      showToast(context: context, msg: "Cases not found");
                    }
                  } catch (e) {
                    myLog(e.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                myLog(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            } else {
              myLog("my case tab new not in");
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          },
        );
      }
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    if (mounted) {
      setState(() {
        pageStart = 0;
        isPageDone = false;
        isLoading = true;
        listTaskInfoSearchModel.clear();
      });
      onPageLoad();
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    listTaskInfoSearchModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    isLoading = false;
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        backgroundColor: MyTheme.bgColor2,
        appBar: MyCasesAppbarNew(
            context: context,
            h: getH(context),
            isLoading: isLoading,
            callback: (index2) {},
            callbackWebView: () {
              StateProvider().notify(ObserverState.STATE_CHANGED_tabbar5);
            },
            onReload: () {
              _getRefreshData();
            }),
        body: drawRecentCases(),
      ),
    );
  }

  drawRecentCases() {
    try {
      return Container(
        child: (listTaskInfoSearchModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      onPageLoad();
                    }
                  }
                  return true;
                },
                child: RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.statusBarColor,
                  onRefresh: _getRefreshData,
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: listTaskInfoSearchModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return MyCasesHelper().drawRecentCaseItem(
                          context: context,
                          caseModel: listTaskInfoSearchModel[index],
                          callbackModel: (model) {
                            if (mounted) {
                              if (model != null) {
                                if (model.success) {
                                  Get.to(() => ReviewRatingListPage(
                                        reviewRatingModel: model.responseData
                                            .reviewRatingFormSetups,
                                        locationModel:
                                            listTaskInfoSearchModel[index],
                                      ));
                                }
                              }
                            }
                          },
                          onReload: () {
                            _getRefreshData();
                          });
                    },
                  ),
                ),
              )
            : (!isLoading)
                ? Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: getWP(context, 100),
                          height: getHP(context, 48),
                          child: Image.asset(
                            'assets/images/screens/db_cus/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt:
                                  "Looks like you haven't created any cases yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                            txt: "Create Now!",
                            width: getWP(context, 50),
                            height: getHP(context, 6),
                            callback: () {
                              Get.to(() => NewCaseTab());
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
      );
    } catch (e) {}
  }
}
