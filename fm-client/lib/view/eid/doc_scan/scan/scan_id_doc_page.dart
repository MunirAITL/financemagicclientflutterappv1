import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'scan_id_doc_base.dart';

class ScanIDDocPage extends StatefulWidget {
  const ScanIDDocPage({Key key}) : super(key: key);

  @override
  _ScanIDDocPageState createState() => _ScanIDDocPageState();
}

class _ScanIDDocPageState extends ScanIDDocBase<ScanIDDocPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Scan identity document"),
          backgroundColor: MyTheme.statusBarColor,
          iconTheme: IconThemeData(color: Colors.white),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return drawRow();
  }
}
