import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import '../result_base.dart';

class PPResultPage extends StatefulWidget {
  final PostDocVerifyAPIModel model;
  const PPResultPage({Key key, @required this.model}) : super(key: key);
  @override
  State createState() => _PPResultPageState();
}

class _PPResultPageState extends ResultBase<PPResultPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title:
              UIHelper().drawAppbarTitle(title: "Identification of a person"),
          backgroundColor: MyTheme.statusBarColor,
          iconTheme: IconThemeData(color: Colors.white),
          centerTitle: false,
        ),
        body: drawLayout(eScanDocType.Passport, widget.model),
      ),
    );
  }
}
