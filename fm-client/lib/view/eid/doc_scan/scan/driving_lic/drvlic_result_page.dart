import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import '../result_base.dart';

class DrvLicResultPage extends StatefulWidget {
  final PostDocVerifyAPIModel model;
  const DrvLicResultPage({Key key, @required this.model}) : super(key: key);
  @override
  _DrvLicResultPageState createState() => _DrvLicResultPageState();
}

class _DrvLicResultPageState extends ResultBase<DrvLicResultPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title:
              UIHelper().drawAppbarTitle(title: "Identification of a person"),
          backgroundColor: MyTheme.statusBarColor,
          iconTheme: IconThemeData(color: Colors.white),
          centerTitle: false,
        ),
        body: drawLayout(eScanDocType.DL, widget.model),
      ),
    );
  }
}
