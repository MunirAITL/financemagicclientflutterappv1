import 'package:aitl/mixin.dart';
import 'package:flutter/material.dart';

abstract class SelfieBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();
}
