import 'package:aitl/mixin.dart';
import 'package:aitl/view_model/rx/UploadProgController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class UploadingBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  final progController = Get.put(UploadProgController());
  drawLayout();
}
