typedef Create<T> = T Function();

mixin ParserMixin {
  genericParser({
    int index,
    List<String> gbodyArr,
    List<dynamic> listNextLine,
    int nextLineNo = 1,
  }) {
    String data;
    for (int j = index + 1; j < gbodyArr.length; j++) {
      final txt = gbodyArr[j];
      final v = txt.replaceAll("\n", "");
      print(v);

      for (Map map in listNextLine) {
        if (map['startWith']) {
          if (v.toLowerCase().startsWith(map['str'])) {
            final txt2 = gbodyArr[j + nextLineNo];
            final v2 = txt2.replaceAll("\n", "");
            print(v2);
            data = v2;
          }
        } else if (map['contains']) {
          if (v.toLowerCase().contains(map['str'])) {
            final txt2 = gbodyArr[j + nextLineNo];
            final v2 = txt2.replaceAll("\n", "");
            print(v2);
            data = v2;
          }
        } else if (map['endWith']) {
          if (v.toLowerCase().endsWith(map['str'])) {
            final txt2 = gbodyArr[j + nextLineNo];
            final v2 = txt2.replaceAll("\n", "");
            print(v2);
            data = v2;
          }
        }
      }

      if (data == null) {
        data = v;
      }
    }

    return data;
  }

  findCommonCharacters(String str1, String str2) {
    int len = str1.length;
    //Set<String> uniqueList={};
    int i = 0;
    for (int i = 0; i < str1.length; i++) {
      if (str2.contains(str1[i])) {
        //uniqueList.add(str1[i]);
        //print("val : ${str1[i]}");
        i++;
      }
    }
    if (i >= (len - 2))
      return true;
    else
      return false;
  }
}
