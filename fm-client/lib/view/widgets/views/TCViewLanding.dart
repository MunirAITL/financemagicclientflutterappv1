import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TCViewLanding extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: "By continuing you agree with our ",
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: Colors.white,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize - .2),
                      fontWeight: FontWeight.normal),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Terms & Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            color: Colors.white,
                            fontSize: getTxtSize(
                                context: context,
                                txtSize: MyTheme.txtSize - .2),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                                    () => PDFDocumentPage(
                                          title: "Privacy",
                                          url: Server.BASE_URL +
                                              "/assets/img/privacy_policy.pdf",
                                        ),
                                    transition: Transition.rightToLeft,
                                    duration: Duration(
                                        milliseconds:
                                            AppConfig.pageAnimationMilliSecond))
                                .then((value) {
                              //callback(route);
                            });
                          }),
                    TextSpan(
                      text: ' and ',
                      style: TextStyle(
                        height: MyTheme.txtLineSpace,
                        color: Colors.white,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize - .2),
                      ),
                    ),
                    TextSpan(
                        text: 'Privacy',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            color: Colors.white,
                            fontSize: getTxtSize(
                                context: context,
                                txtSize: MyTheme.txtSize - .2),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                                    () => PDFDocumentPage(
                                          title: "Privacy",
                                          url: Server.BASE_URL +
                                              "/assets/img/privacy_policy.pdf",
                                        ),
                                    transition: Transition.rightToLeft,
                                    duration: Duration(
                                        milliseconds:
                                            AppConfig.pageAnimationMilliSecond))
                                .then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
