import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PicFullView extends StatelessWidget with Mixin {
  final String url;
  final String title;
  const PicFullView({Key key, @required this.url, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.statusBarColor,
          iconTheme: IconThemeData(color: Colors.white),
          /*leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
                  Get.back();
            },
          ),*/
          title:
              UIHelper().drawAppbarTitle(title: title, txtColor: Colors.white),
          centerTitle: false,
        ),
        body: drawLayout(context),
      ),
    );
  }

  drawLayout(context) {
    return Container(
      width: getW(context),
      height: getH(context),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: CachedNetworkImageProvider(
            url,
          ),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
