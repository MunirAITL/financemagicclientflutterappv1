import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/switchview/SwitchView.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/gplaces/GPlacesView.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import '../txt/IcoTxtIco.dart';

class SPVSwitchView extends StatefulWidget {
  bool isSwitch;
  bool pickAddress;
  final Function(String) callback;
  final Function(String) callbackAddress;
  final Function(bool) callbackSwitch;
  final Function(bool) callbackSwitchAddress;
  final TextEditingController compName;
  final TextEditingController localAddress;
  final TextEditingController regNo;
  //
  final FocusNode focusCompName;
  final FocusNode focusAddr;
  final FocusNode focusRegNo;

  final String regAddr;
  String regDate;

  SPVSwitchView({
    Key key,
    @required this.compName,
    @required this.regAddr,
    @required this.regNo,
    @required this.regDate,
    @required this.callback,
    @required this.callbackAddress,
    @required this.callbackSwitch,
    @required this.callbackSwitchAddress,
    @required this.localAddress,
    @required this.focusCompName,
    @required this.focusAddr,
    @required this.focusRegNo,
    this.isSwitch = false,
    this.pickAddress = false,
  }) : super(key: key);

  @override
  State createState() => _SPVSwitchViewState();
}

class _SPVSwitchViewState extends State<SPVSwitchView> with Mixin {
  String dd = "";
  String mm = "";
  String yy = "";

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: Txt(
                    txt:
                        "Are you buying the property in the name of a SPV (Limited Company)?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true,
                    //txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 5),
                SwitchView(
                  onTxt: "Yes",
                  offTxt: "No",
                  value: widget.isSwitch,
                  onChanged: (value) {
                    widget.isSwitch = value;
                    widget.callbackSwitch(value);
                    if (mounted) {
                      setState(() {});
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (widget.isSwitch) ? drawSPVInputFields() : SizedBox(),
        ],
      ),
    );
  }

  drawSPVInputFields() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Column(
          children: [
            drawInputBox(
              context: context,
              title: "Name of Company",
              input: widget.compName,
              kbType: TextInputType.name,
              inputAction: TextInputAction.next,
              focusNode: widget.focusCompName,
              focusNodeNext: widget.focusAddr,
              len: 100,
              txtColor: Colors.black,
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: Txt(
                    txt: "Pick address from Google",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true,
                    //txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 5),
                SwitchView(
                  onTxt: "Yes",
                  offTxt: "No",
                  value: widget.pickAddress,
                  onChanged: (value) {
                    setState(() {
                      widget.pickAddress = value;
                      widget.callbackSwitchAddress(value);
                    });
                  },
                ),
              ],
            ),
            SizedBox(height: 10),
            widget.pickAddress
                ? GPlacesView(
                    title: "Registered Address",
                    address: widget.regAddr,
                    callback: (String address, String postCode, Location loc) {
                      widget.callbackAddress(address);
                    })
                : drawInputBox(
                    context: context,
                    title: "Registered Address",
                    input: widget.localAddress,
                    kbType: TextInputType.streetAddress,
                    inputAction: TextInputAction.next,
                    focusNode: widget.focusAddr,
                    focusNodeNext: widget.focusRegNo,
                    len: 255,
                    txtColor: Colors.black,
                  ),
            SizedBox(height: 10),
            drawInputBox(
              context: context,
              title: "Company Registration Number",
              input: widget.regNo,
              kbType: TextInputType.name,
              inputAction: TextInputAction.done,
              focusNode: widget.focusRegNo,
              len: 50,
              txtColor: Colors.black,
            ),
            SizedBox(height: 10),
            drawRegDate(),
          ],
        ),
      ),
    );
  }

  drawRegDate() {
    DateTime date = DateTime.now();
    var newDate = new DateTime(date.year, date.month, date.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Txt(
            txt: "Date registered",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: true,
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: newDate,
                firstDate: DateTime(1950),
                lastDate: newDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme:
                          ColorScheme.light(primary: MyTheme.statusBarColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  if (mounted) {
                    setState(() {
                      try {
                        widget.regDate =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                        final dobArr = date.toString().split('-');
                        this.dd = dobArr[0];
                        this.mm = dobArr[1];
                        this.yy = dobArr[2];
                        widget.callback(widget.regDate);
                      } catch (e) {
                        myLog(e.toString());
                      }
                    });
                  }
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: (widget.regDate == "") ? "Select a date" : widget.regDate,
              rightIcon: Icons.arrow_drop_down,
              txtAlign: TextAlign.left,
              rightIconSize: 40,
            ),
          ),
        ],
      ),
    );
  }
}
