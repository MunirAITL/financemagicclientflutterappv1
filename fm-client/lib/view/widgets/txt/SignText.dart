import 'package:flutter/material.dart';
import '../../../config/AppDefine.dart';
import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

drawSignText({
  BuildContext context,
  String title,
  String txt,
  Color txtColor,
  bool isBold = false,
  String sign = AppDefine.CUR_SIGN,
  double padding = 12,
}) {
  final w = MediaQuery.of(context).size.width;
  return Container(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        title != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: title,
                    txtColor:
                        txtColor == null ? MyTheme.inputColor : Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: isBold),
              )
            : SizedBox(),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                bottomLeft: Radius.circular(5),
                topRight: Radius.circular(5),
                bottomRight: Radius.circular(5)),
          ),
          child: Row(
            children: [
              Container(
                //width: w * .15,
                color: Colors.grey.shade400,
                child: Padding(
                  padding: EdgeInsets.all(padding),
                  child: Txt(
                      txt: sign,
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize + .3,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Container(
                  child: Txt(
                      txt: txt,
                      txtColor: txt != '0' ? Colors.black : Colors.grey,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
