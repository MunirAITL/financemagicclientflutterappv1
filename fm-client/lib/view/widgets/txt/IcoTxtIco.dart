import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class IcoTxtIco extends StatelessWidget with Mixin {
  final IconData leftIcon;
  final IconData rightIcon;
  final txt;
  final double txtSize;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height = 5;
  Color iconColor;
  Color txtColor;
  double radius;

  IcoTxtIco({
    Key key,
    @required this.txt,
    @required this.leftIcon,
    @required this.rightIcon,
    this.txtSize = 2,
    this.iconColor = Colors.grey,
    this.txtColor = Colors.black87,
    this.txtAlign = TextAlign.center,
    this.leftIconSize = 20,
    this.rightIconSize = 25,
    this.height = 10,
    this.radius = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        //height: getHP(context, 6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            border: Border(
                left: BorderSide(color: Colors.grey, width: 1),
                right: BorderSide(color: Colors.grey, width: 1),
                top: BorderSide(color: Colors.grey, width: 1),
                bottom: BorderSide(color: Colors.grey, width: 1)),
            color: Colors.transparent),
        //alignment: Alignment.center,
        child: Padding(
          padding: EdgeInsets.all(height),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                (leftIcon != null)
                    ? Flexible(
                        fit: FlexFit.loose,
                        child: Icon(
                          leftIcon,
                          color: iconColor,
                          size: leftIconSize,
                        ))
                    : SizedBox(),
                SizedBox(width: 10),
                Expanded(
                    flex: 10,
                    child: Txt(
                      txt: txt,
                      txtColor: (txt.toString().toLowerCase().contains("date"))
                          ? Colors.grey
                          : Colors.black,
                      txtSize: txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    )),
                (rightIcon != null)
                    ? Flexible(
                        child: Icon(
                        rightIcon,
                        color: Colors.grey,
                        size: rightIconSize,
                      ))
                    : SizedBox(),
              ]),
        ));
  }
}
