import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import '../../../Mixin.dart';
import '../txt/Txt.dart';

radioButtonitem(
    {int index,
    String text,
    Color bgColor,
    Color textColor,
    Function(int) callback}) {
  return GestureDetector(
    onTap: () {
      callback(index);
    },
    child: Container(
      padding: EdgeInsets.only(left: 10),
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border(
              left: BorderSide(color: Colors.grey, width: 1),
              right: BorderSide(color: Colors.grey, width: 1),
              top: BorderSide(color: Colors.grey, width: 1),
              bottom: BorderSide(color: Colors.grey, width: 1)),
          color: bgColor),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            flex: 2,
            child: Container(
              width: 15,
              height: 15,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border(
                          left: BorderSide(color: Colors.grey, width: 1),
                          right: BorderSide(color: Colors.grey, width: 1),
                          top: BorderSide(color: Colors.grey, width: 1),
                          bottom: BorderSide(color: Colors.grey, width: 1)),
                      color: Colors.grey),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 8,
            child: Padding(
              padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
              child: Txt(
                  txt: text,
                  txtColor: textColor,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isOverflow: true,
                  isBold: false),
            ),
          ),
        ],
      ),
    ),
  );
}
