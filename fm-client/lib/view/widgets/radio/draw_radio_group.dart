import 'package:flutter/material.dart';
import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

enum eRadioType {
  VERTICAL,
  HORIZONTAL,
  GRID2,
}

drawRadioGroup(
    {BuildContext context,
    eRadioType type = eRadioType.VERTICAL,
    double txtSize = 12.5,
    bool isMainBG = false,
    //
    @required Map<int, String> map,
    @required int selected,
    @required Function(int) callback}) {
  final width =
      (context != null) ? MediaQuery.of(context).size.width / 3.35 : null;
  isMainBG = true;
  return type == eRadioType.VERTICAL
      ? Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          for (var entry in map.entries)
            GestureDetector(
              onTap: () {
                callback(entry.key);
              },
              child: _radioitem(
                  isMainBG: isMainBG,
                  type: type,
                  text: entry.value,
                  txtSize: txtSize,
                  isSelected: entry.key == selected,
                  bgColor: entry.key == selected
                      ? MyTheme.purpleColor
                      : Color(0xFF000),
                  textColor: Colors.black),
            )
        ]))
      : type == eRadioType.HORIZONTAL
          ? SingleChildScrollView(
              //shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    for (var entry in map.entries)
                      GestureDetector(
                          onTap: () {
                            callback(entry.key);
                          },
                          child: Container(
                            //color: Colors.black,
                            width: width ??
                                _textSize(
                                    entry.value,
                                    TextStyle(
                                      fontSize: txtSize,
                                    )),

                            child: _radioitem(
                                isMainBG: isMainBG,
                                type: type,
                                text: entry.value,
                                txtSize: txtSize,
                                isSelected: entry.key == selected,
                                bgColor: entry.key == selected
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black),
                          ))
                  ]))
          : GridView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: map.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisExtent: !isMainBG ? 35 : 50,
              ),
              itemBuilder: (_, index) {
                final value = map[index];
                return GestureDetector(
                    onTap: () {
                      callback(index);
                    },
                    child: Container(
                      //color: Colors.black,
                      width: width ??
                          _textSize(
                              value,
                              TextStyle(
                                fontSize: txtSize,
                              )),
                      child: _radioitem(
                          isMainBG: isMainBG,
                          type: type,
                          text: value,
                          txtSize: txtSize,
                          isSelected: index == selected,
                          bgColor: index == selected
                              ? MyTheme.purpleColor
                              : Color(0xFF000),
                          textColor: Colors.black),
                    ));
              });
}

_radioitem({
  eRadioType type,
  bool isMainBG,
  bool isSelected,
  String text,
  double txtSize = 12.5,
  Color bgColor,
  Color textColor,
}) {
  return Padding(
    padding: EdgeInsets.all(type == eRadioType.VERTICAL && !isMainBG ? 0 : 2),
    child: Container(
      decoration: isMainBG
          ? BoxDecoration(
              color: isSelected ? MyTheme.radioBGColor : Colors.grey,
              borderRadius: BorderRadius.circular(5),
            )
          : null,
      child: Padding(
        padding: EdgeInsets.only(
            left: isMainBG ? 10 : 0,
            top: type == eRadioType.VERTICAL
                ? !isMainBG
                    ? 0
                    : 5
                : 10,
            bottom: type == eRadioType.VERTICAL
                ? !isMainBG
                    ? 0
                    : 5
                : 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: 15,
              height: 15,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(3),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border(
                          left:
                              BorderSide(color: MyTheme.purpleColor, width: 1),
                          right:
                              BorderSide(color: MyTheme.purpleColor, width: 1),
                          top: BorderSide(color: MyTheme.purpleColor, width: 1),
                          bottom:
                              BorderSide(color: MyTheme.purpleColor, width: 1)),
                      color: bgColor),
                ),
              ),
            ),
            SizedBox(width: 2),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Text(
                  text,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      color: isMainBG ? Colors.white : textColor,
                      fontSize: txtSize,
                      fontWeight: isMainBG
                          ? isSelected
                              ? FontWeight.w500
                              : FontWeight.normal
                          : FontWeight.normal),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

//  get intrinsic text width as per font size
double _textSize(String text, TextStyle style) {
  return text.length * 15.0;
}
