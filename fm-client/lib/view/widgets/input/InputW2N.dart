import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../txt/Txt.dart';

InputW2N({
  String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required int len,
  String ph = "",
  bool isPwd = false,
  bool isBold = false,
}) =>
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: isBold,
                ),
              )
            : SizedBox(),
        TextField(
          controller: input,
          keyboardType: kbType == TextInputType.number
              ? TextInputType.numberWithOptions(signed: true)
              : kbType,
          inputFormatters: (kbType == TextInputType.phone)
              ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
              : (kbType == TextInputType.emailAddress)
                  ? [
                      FilteringTextInputFormatter.allow(RegExp(
                          "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                    ]
                  : null,
          obscureText: isPwd,
          maxLength: len,
          autocorrect: false,
          enableSuggestions: false,
          textAlign: TextAlign.start,
          style: TextStyle(
            color: Colors.black,
            fontSize: 15,
            height: MyTheme.txtLineSpace,
          ),
          decoration: new InputDecoration(
            isDense: true,
            counter: Offstage(),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            counterText: "",
            //filled: true,
            //fillColor: Colors.white,
            hintText: ph,
            hintStyle: new TextStyle(
              color: Colors.grey,
              fontSize: 15,
              height: MyTheme.txtLineSpace,
            ),
            labelStyle: new TextStyle(
              color: Colors.black,
              fontSize: 15,
              height: MyTheme.txtLineSpace,
            ),

            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: const BorderRadius.all(
                const Radius.circular(5),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 1),
              borderRadius: const BorderRadius.all(
                const Radius.circular(5),
              ),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 1),
              borderRadius: const BorderRadius.all(
                const Radius.circular(5),
              ),
            ),
          ),
        ),
      ],
    );
