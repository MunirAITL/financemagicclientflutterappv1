import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

import 'InputBox.dart';
import '../txt/Txt.dart';

drawInputBox({
  @required BuildContext context,
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required TextInputAction inputAction,
  @required FocusNode focusNode,
  FocusNode focusNodeNext,
  @required int len,
  String ph,
  Color txtColor,
  bool isPwd = false,
  bool autofocus = false,
  bool isobscureText = false,
  bool isBold = false,
  FontWeight fontWeight,
  Function(bool) callback,
}) {
  if (txtColor == null) txtColor = MyTheme.inputColor;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      title != null
          ? Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Txt(
                  txt: title,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  fontWeight: fontWeight,
                  isBold: isBold),
            )
          : SizedBox(),
      InputBox(
        context: context,
        ctrl: input,
        lableTxt: (ph == null) ? title : ph,
        kbType: kbType,
        inputAction: inputAction,
        focusNode: focusNode,
        focusNodeNext: focusNodeNext,
        len: len,
        isPwd: isPwd,
        autofocus: autofocus,
        isobscureText: isobscureText,
        callback: (v) {
          callback(v);
        },
      ),
    ],
  );
}
