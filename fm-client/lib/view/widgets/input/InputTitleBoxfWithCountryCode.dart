import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

import 'InputBoxWithCountryCode.dart';
import '../txt/Txt.dart';

drawInputBoxWithCountryCode({
  @required BuildContext context,
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required TextInputAction inputAction,
  @required FocusNode focusNode,
  FocusNode focusNodeNext,
  @required int len,
  @required String countryCode,
  @required String countryName,
  @required Function getCountryCode,
  String ph,
  bool isWhiteBG = false,
  bool isPwd = false,
  bool autofocus = false,
  double radius = 8,
  double fontSize = 17,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      (title != null)
          ? Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Txt(
                  txt: title,
                  txtColor: MyTheme.inputColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w400,
                  isBold: false),
            )
          : SizedBox(),
      InputBoxWithCountryCode(
        context: context,
        countryCode: countryCode,
        countryName: countryName,
        getCountryCode: getCountryCode,
        ctrl: input,
        lableTxt: (ph == null) ? title : ph,
        kbType: kbType,
        inputAction: inputAction,
        focusNode: focusNode,
        focusNodeNext: focusNodeNext,
        len: len,
        isPwd: isPwd,
        autofocus: autofocus,
        isWhiteBG: isWhiteBG,
        radius: radius,
        fontSize: fontSize,
      ),
    ],
  );
}
