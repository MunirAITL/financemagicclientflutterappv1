import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/src/theme/slider_theme.dart';

class SliderTrackShapes extends SfTrackShape {
  @override
  void paint(PaintingContext context, Offset offset, Offset thumbCenter,
      Offset startThumbCenter, Offset endThumbCenter,
      {RenderBox parentBox,
      SfSliderThemeData themeData,
      SfRangeValues currentValues,
      dynamic currentValue,
      Animation<double> enableAnimation,
      Paint inactivePaint,
      Paint activePaint,
      TextDirection textDirection}) {
    Paint paint = Paint()
      ..color = themeData.activeTrackColor
      ..style = PaintingStyle.fill
      ..strokeWidth = .1;

    super.paint(context, offset, thumbCenter, startThumbCenter, endThumbCenter,
        parentBox: parentBox,
        themeData: themeData,
        enableAnimation: enableAnimation,
        inactivePaint: inactivePaint,
        activePaint: paint,
        textDirection: textDirection);
  }
}
