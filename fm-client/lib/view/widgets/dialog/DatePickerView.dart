import 'package:aitl/view/widgets/txt/IcoTxtIco.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

class DatePickerView extends StatelessWidget {
  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final String cap;
  final String dt;
  final double txtSize;
  Color txtColor;
  FontWeight fontWeight;
  double padding;
  double radius;
  final Function callback;

  DatePickerView({
    Key key,
    @required this.cap,
    @required this.dt,
    @required this.callback,
    @required this.initialDate,
    @required this.firstDate,
    @required this.lastDate,
    this.txtSize = 2,
    this.txtColor,
    this.fontWeight = FontWeight.w400,
    this.padding = 10,
    this.radius = 10,
  }) {
    if (txtColor == null) txtColor = MyTheme.lightTxtColor;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          (this.cap != null)
              ? Padding(
                  padding: EdgeInsets.only(bottom: padding),
                  child: Txt(
                    txt: this.cap,
                    txtColor: txtColor,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    fontWeight: fontWeight,
                    txtAlign: TextAlign.start,
                  ),
                )
              : SizedBox(),
          GestureDetector(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: firstDate,
                lastDate: lastDate,
                fieldHintText: "dd/mm/yyyy",
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme:
                          ColorScheme.light(primary: MyTheme.statusBarColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: this.dt,
              txtSize: txtSize,
              txtColor: txtColor,
              rightIcon: Icons.keyboard_arrow_down,
              txtAlign: TextAlign.left,
              rightIconSize: 30,
              leftIconSize: 20,
              height: 5,
              radius: radius,
            ),
          ),
        ],
      ),
    );
  }
}
