import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:get/get.dart';

// ignore: non_constant_identifier_names
docOptDialog({
  BuildContext context,
  @required String docName,
  @required Function callbackView,
  @required Function callbackDownload,
}) {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      body: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.center,
          children: [
            SizedBox(height: 50),
            Txt(
              txtAlign: TextAlign.center,
              txt: docName,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              isBold: true,
            ),
            Positioned(
              top: -50,
              child: Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(
                      10), //I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape
                        .circle, // You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.white,
                  ),
                  child: Icon(Icons.file_open_outlined,
                      color: MyTheme.dBlueAirColor, size: 40)),
            ),
          ]),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      btnOkText: "View only",
      btnOkColor: MyTheme.titleColor,
      btnOkOnPress: () {
        callbackView();
      },
      btnCancelText: "Download",
      btnCancelColor: Colors.grey,
      btnCancelOnPress: () {
        callbackDownload();
      })
    ..show();
}
