import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'tabItem.dart';
import 'package:badges/badges.dart';

class BottomNavigation extends StatefulWidget {
  final BuildContext context;
  final ValueChanged<int> onSelectTab;
  final List<TabItem> tabs;
  final bool isHelpTut;
  final int totalMsg;
  final int totalNoti;
  BottomNavigation({
    @required this.context,
    @required this.onSelectTab,
    @required this.tabs,
    @required this.isHelpTut,
    @required this.totalMsg,
    @required this.totalNoti,
  });

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation>
    with Mixin, StateListener {
  int curTab = 0;

  StateProvider _stateProvider;

  @override
  onStateChangedWithData(ObserverState state, dynamic data) async {
    try {
      if (state == ObserverState.STATE_BOTNAV) {
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  void onStateChanged(ObserverState state) {
    // TODO: implement onStateChanged
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    curTab = MainCustomerScreenState.currentTab;
    // if (userData.communityId == 2)
    //   curTab = MainIntroducerScreenState.currentTab;

    return BottomAppBar(
      color: MyTheme.statusBarColor,
      shape: CircularNotchedRectangle(),
      //notchMargin: 4,
      //clipBehavior: Clip.antiAlias,
      child: BottomNavigationBar(
        //selectedLabelStyle: TextStyle(fontSize: 14),
        selectedItemColor: Colors.red,
        currentIndex: curTab,
        //unselectedLabelStyle: TextStyle(fontSize: 14),
        unselectedItemColor: Colors.white,
        backgroundColor: MyTheme.statusBarColor,
        type: BottomNavigationBarType.fixed,
        items: widget.tabs
            .map(
              (e) => _buildItem(
                index: e.getIndex(),
                icon: e.icon,
                tabName: e.tabName,
              ),
            )
            .toList(),
        onTap: (index) => widget.onSelectTab(
          index,
        ),
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, AssetImage icon, String tabName}) {
    final totalTask =
        appData.taskNotificationCountAndChatUnreadCountData != null
            ? appData
                .taskNotificationCountAndChatUnreadCountData.numberOfPendingTask
            : 0;
    final totalMsg = appData.taskNotificationCountAndChatUnreadCountData != null
        ? appData
            .taskNotificationCountAndChatUnreadCountData.numberOfUnReadMessage
        : 0;
    final totalNoti =
        appData.taskNotificationCountAndChatUnreadCountData != null
            ? appData.taskNotificationCountAndChatUnreadCountData
                .numberOfUnReadNotification
            : 0;
    int totalBadge = 0;
    if (index == 1 && totalTask > 0) totalBadge = totalTask;
    if (index == 2 && totalMsg > 0) totalBadge = totalMsg;
    if (index == 3 && totalNoti > 0) totalBadge = totalNoti;

    return BottomNavigationBarItem(
        icon: (widget.isHelpTut && curTab == index)
            ? Stack(clipBehavior: Clip.none, children: <Widget>[
                (index == 2 || index == 3)
                    ? Badge(
                        showBadge: (totalBadge > 0) ? true : false,
                        badgeContent: Text(
                          totalBadge.toString(),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        child: ImageIcon(
                          icon,
                          color: _tabColor(index: index),
                        ),
                      )
                    : ImageIcon(
                        icon,
                        color: _tabColor(index: index),
                      ),
                new Positioned(
                  top: -getHP(widget.context, 9),
                  right: -10,
                  child: Image.asset(
                    "assets/images/icons/help_hand3.png",
                    width: 60,
                    height: 70,
                  ),
                )
              ])
            : Badge(
                showBadge: (totalBadge > 0) ? true : false,
                badgeContent: Text(
                  totalBadge.toString(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                child: ImageIcon(
                  icon,
                  color: _tabColor(index: index),
                ),
              ),
        // ignore: deprecated_member_use
        label:
            tabName /*  Txt(
          txt: tabName,
          txtColor: _tabColor(index: index),
          txtSize: MyTheme.txtSize - .9,
          txtAlign: TextAlign.center,
          isBold: (curTab == index) ? true : false),*/
        );
  }

  Color _tabColor({int index}) {
    return curTab == index ? Colors.red : Colors.white;
  }
}
