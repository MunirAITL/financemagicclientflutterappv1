import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

class BtnCase extends StatelessWidget {
  final String txt;
  final Color txtColor;
  final double txtSize;
  final Color bgColor;
  final double radius;
  final Function callback;

  BtnCase({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.bgColor,
    this.radius = 5,
    this.txtSize,
    @required this.callback,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callback(),
      child: Container(
        decoration: BoxDecoration(
          color: bgColor,
          border: Border.all(width: .5, color: bgColor),
          borderRadius: BorderRadius.all(Radius.circular(radius)),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
          child: Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize:
                (this.txtSize == null) ? MyTheme.txtSize - .4 : this.txtSize,
            txtAlign: TextAlign.center,
            isBold: true,
          ),
        ),
      ),
    );
  }
}
